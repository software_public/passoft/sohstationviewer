================
SOHStationViewer
================

Description
-----------

  Visualize State-of-Health packets from raw data recorded by different type of dataloggers.


License
-------
  GNU General Public License v3 (GPLv3)
