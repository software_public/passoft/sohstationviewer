#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst', 'rt') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst', 'rt') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
    ],
    description="Visualize State-of-Health packets from raw data recorded by "
                "different type of dataloggers.",
    entry_points={
        'console_scripts': [
            'sohviewer=sohstationviewer.main:main',
        ],
    },
    install_requires=[
        'numpy >=1.23.0,<2.0',
        'obspy>=1.3.0',
        'PySide6>=6.5.2',
        'matplotlib>=3.6.2',
    ],
    setup_requires=[],
    extras_require={
        'dev': [
            'flake8',
            'tox'
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='sohstationviewer',
    name='sohviewer',
    packages=find_packages(include=['sohstationviewer*']),
    url='https://git.passcal.nmt.edu/software_public/passoft/sohstationviewer',
    version='2025.1.0.0',
    zip_safe=False,
)
