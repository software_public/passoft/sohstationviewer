from pathlib import Path
from typing import Literal

# The path to the package's root
ROOT_PATH = Path(__file__).resolve().parent.parent

# The current version of SOHStationViewer
SOFTWARE_VERSION = '2025.1.0.0'
BUILD_TIME = "January 10, 2025"

# waveform pattern
WF_1ST = 'A-HLM-V'
WF_2ND = 'HLN'
WF_3RD = 'ZNE123456'

# to calc min()
HIGHEST_INT = 1E100

# warn user if file bigger than this size
BIG_FILE_SIZE = 2 * 10**9       # 2 GB

# Matplotlib's performance be slow if data point total > than this limit
CHAN_SIZE_LIMIT = 10**6

# If total waveform datapoint > than this limit, not recalculate
RECAL_SIZE_LIMIT = 10**9

# rate of values to be cut of from means
CUT_FROM_MEAN_FACTOR = 0.1

# default start time
DEFAULT_START_TIME = "1970-01-01"

# TIME FORMAT according to Qt.ISODate
TM_FORMAT = "%Y-%m-%d"

# seconds in a day
SECOND_IN_DAY = 86400.0

# seconds in 5 minutes
SEC_5M = 300

# total of 5 minutes in a day
NUMBER_OF_5M_IN_DAY = 288

# total of 5 minutes in an hour
NO_5M_1H = int(60 * 60 / 300)

# name of table of contents file
TABLE_CONTENTS = "01 _ Table of Contents.help.md"

# name of search through all documents file
SEARCH_RESULTS = "Search Results.md"

# the list of all color modes
ALL_COLOR_MODES = {'B': 'black', 'W': 'white'}

# The location of the config file
CONFIG_PATH = 'conf/read_settings.ini'

# List of image formats. Have to put PNG at the beginning to go with
# dpi in dialog

IMG_FORMAT = ['PNG', 'PDF', 'EPS', 'SVG']

# ================================================================= #
#                      PLOTTING CONSTANT
# ================================================================= #
# Maximum height of figure to assure that all channel data are plotted and all
# the height of plotting_widget is covered => Use the number that is the
# possible maximum height of screen
MAX_HEIGHT_IN = 25.

# Where the plotting start (0-1)
BOTTOM = 1

# =========  A BASIC_HEIGHT_IN IS THE HEIGHT OF ONE UNIT OF SIZE_FACTOR ======
# Size in inch of 1 unit of SIZE_FACTOR
BASIC_HEIGHT_IN = 0.15

# DEFAULT_SIZE_FACTOR_TO_NORMALIZE is the default height_normalizing_factor
# which is the normalize factor corresponding to SIZE_FACTOR = 1
# Basically this factor is calculated based on the total height of all plots.
# However, when there's no channel height_normalizing_factor can't be
# calculated,  so DEFAULT_SIZE_FACTOR_TO_NORMALIZE will be used to plot
# timestamp.
DEFAULT_SIZE_FACTOR_TO_NORMALIZE = 0.005
# vertical margin
TOP_SPACE_SIZE_FACTOR = 3
BOT_SPACE_SIZE_FACTOR = 6       # to avoid hidden time bar partially
# space from previous bottom to time bar bottom
TIME_BAR_SIZE_FACTOR = 2
# space from time bar bottom to the bottom of gap bar
GAP_SIZE_FACTOR = 2
# size of empty plot bar at the end
PLOT_NONE_SIZE_FACTOR = 0.01
# distance between plot
PLOT_SEPARATOR_SIZE_FACTOR = 1
TPS_SEPARATOR_SIZE_FACTOR = 2
# Size factor of TPS legend height
TPS_LEGEND_SIZE_FACTOR = 15
# =============================================================================
# ================================= NORMALIZE =================================
# Normalized distance from left edge to the start of channel plots
PLOT_LEFT_NORMALIZE = 0.1
TPS_LEFT_NORMALIZE = 0.15

# Normalized distance from right edge to the end of channel plots
PLOT_RIGHT_NORMALIZE = 0.05
TPS_RIGHT_NORMALIZE = 0.05

# Normalized width of a plot
PLOT_WIDTH_NORMALIZE = 1 - (PLOT_LEFT_NORMALIZE + PLOT_RIGHT_NORMALIZE)
TPS_WIDTH_NORMALIZE = 1 - (TPS_LEFT_NORMALIZE + TPS_RIGHT_NORMALIZE)

# Order to show plotting items on top of each other. The higher value, the
# the higher priority
Z_ORDER = {'AXIS_SPINES': 0, 'CENTER_LINE': 1, 'LINE': 2, 'GAP': 3, 'DOT': 3,
           'TPS_MARKER': 4}

# Distance from 'Hour' label to timestamp bar
HOUR_TO_TMBAR_D = 50

# Base font size range
MIN_FONTSIZE = 6
MAX_FONTSIZE = 12
# day total limit for all tps channels to stay in one tab
DAY_LIMIT_FOR_TPS_IN_ONE_TAB = 180      # about half of a year

# PLOTTING SIZE MAP
SIZE_PIXEL_MAP = {'smaller': .5, 'normal': 2, 'bigger': 3}
SIZE_UNICODE_MAP = {'smaller': '&#9660;', 'normal': '', 'bigger': '&#9650;'}
# ================================================================= #
#                      TYPING CONSTANT
# ================================================================= #

ColorMode = Literal['B', 'W']
