import configparser
from pathlib import Path

from PySide6 import QtCore

from sohstationviewer.conf import constants
from sohstationviewer.conf.constants import CONFIG_PATH
from sohstationviewer.view.main_window import MainWindow

# The default configuration to load if there is a problem with the config file.
default_config = f'''
[FileRead]
from_data_card = False
data = False
sdata = False
log = False

[ColorMode]
black = True
white = False

[Gap]
min_gap_length =

[Channels]
mp123zne = False
mp456uvw = False
all_waveform_chans = False
ds1 = False
ds2 = False
ds3 = False
ds4 = False
ds5 = False
ds6 = False
ds7 = False
ds8 = False
mseed_wildcard =
plot_tps = False
plot_raw = False

[ChannelsPreference]
all_soh = True

[DateRange]
from_date = {QtCore.QDate(1970, 1, 1).toString("yyyy-MM-dd")}
to_date = {QtCore.QDate.currentDate().toString("yyyy-MM-dd")}

[MiscOptions]
mp_color_mode = regular
tps_color_mode = High
date_mode = YYYY-MM-DD
base_plot_font_size = 6
add_mass_pos_to_soh = False
'''


class BadConfigError(RuntimeError):
    """
    Error raised when there is a problem with the configuration
    """
    pass


class ConfigProcessor:
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.expected_keys = {
            'from_data_card', 'data', 'sdata', 'log',
            'black', 'white',
            'min_gap_length',
            'mp123zne', 'mp456uvw',
            'all_waveform_chans', 'ds1', 'ds2', 'ds3', 'ds4',
            'ds5', 'ds6', 'ds7', 'ds8', 'mseed_wildcard',
            'plot_tps', 'plot_raw',
            'all_soh',
            'from_date', 'to_date',
            'mp_color_mode', 'tps_color_mode', 'date_mode',
            'base_plot_font_size',
                'add_mass_pos_to_soh'  # noqa: E131
        }

    def load_config(self):
        """
        Load the config file if there is one, otherwise load the default
        config.
        """
        config_path = Path('conf/read_settings.ini')
        if not config_path.exists():
            self.config.read_string(default_config)
        else:
            self.config.read(config_path)

    def validate_config(self):
        """
        Perform a sanity-check to ensure that the loaded config does not have a
        problem. Perform the following check:
            - All the expected config keys are available.
            - Only one of data or sdata checkbox is chosen.
            - Only one color mode is chosen
            - If all waveform channels option is chosen, data stream choices
                and MSeed wildcard are empty.
            - Date range has valid values.
            - Mass-position and TPS plots' color mode has a good value.
            - Date mode has good value.
        """
        loaded_keys = {item[0]
                       for (section, _) in self.config.items()
                       for item in self.config.items(section)}
        missing_keys = self.expected_keys - loaded_keys
        if missing_keys:
            raise BadConfigError(
                f'Some required keys are missing. Please'
                f'make sure they are in the config file. The '
                f'missing keys are: {", ".join(missing_keys)}'
            )

        from_data_card_checked = self.config.getboolean('FileRead',
                                                        'from_data_card')
        data_checked = self.config.getboolean('FileRead', 'data')
        sdata_checked = self.config.getboolean('FileRead', 'sdata')
        log_checked = self.config.getboolean('FileRead', 'log')
        if data_checked and sdata_checked:
            raise BadConfigError('data and sdata cannot both be chosen.')
        if from_data_card_checked and log_checked:
            raise BadConfigError('Cannot read from data card and log file at '
                                 'the same time.')

        black_color_mode = self.config.getboolean('ColorMode', 'black')
        white_color_mode = self.config.getboolean('ColorMode', 'white')
        if black_color_mode and white_color_mode:
            raise BadConfigError('Cannot be in both color modes at once.')

        all_waveform_chans = self.config.getboolean('Channels',
                                                    'all_waveform_chans')
        data_stream_chosen = any(self.config.getboolean('Channels', f'ds{i}')
                                 for i in range(1, 9)
                                 )
        mseed_wildcard = self.config.get('Channels',
                                         'mseed_wildcard')
        wildcard_not_empty = (mseed_wildcard != '')
        if all_waveform_chans and (data_stream_chosen or wildcard_not_empty):
            raise BadConfigError(
                'Waveform channel selector can only be in one mode at a time.')

        from_date = self.config.get('DateRange', 'from_date')
        to_date = self.config.get('DateRange', 'to_date')
        is_from_date_bad = False
        is_to_date_bad = False
        if from_date != '':
            converted_from_date = QtCore.QDate.fromString(
                from_date, 'yyyy-MM-dd'
            )
            is_from_date_bad = not converted_from_date.isValid()
        if to_date != '':
            converted_to_date = QtCore.QDate.fromString(
                to_date, 'yyyy-MM-dd'
            )
            is_to_date_bad = not converted_to_date.isValid()
        if is_from_date_bad:
            if is_to_date_bad:
                raise BadConfigError('From date and to date have bad value.')
            else:
                raise BadConfigError('From date has bad value.')
        elif is_to_date_bad:
            raise BadConfigError('To date has bad value.')

        mp_color_mode = self.config.get('MiscOptions', 'mp_color_mode')
        expected_mass_poss_color_modes = {'regular', 'trillium'}
        if mp_color_mode not in expected_mass_poss_color_modes:
            raise BadConfigError('Mass-position color mode can only be '
                                 'regular or trillium.')

        tps_color_mode = self.config.get('MiscOptions', 'tps_color_mode')
        expected_tps_color_modes = {'High', 'Low', 'Antarctica', 'Medium'}
        if tps_color_mode not in expected_tps_color_modes:
            raise BadConfigError(f'TPS color mode can only have one of these '
                                 f'values: '
                                 f'{",  ".join(expected_tps_color_modes)}.')

        date_mode = self.config.get('MiscOptions', 'date_mode')
        expected_date_modes = {'YYYY-MM-DD', 'YYYYMMMDD', 'YYYY:DOY'}
        if date_mode not in expected_date_modes:
            raise BadConfigError(f'Date mode can only have one of these '
                                 f'values: '
                                 f'{",  ".join(expected_date_modes)}.')

        base_plot_font_size = self.config.get(
            'MiscOptions', 'base_plot_font_size')
        expected_base_plot_font_sizes = [
            str(i)
            for i in range(constants.MIN_FONTSIZE, constants.MAX_FONTSIZE + 1)
        ]

        if base_plot_font_size not in expected_base_plot_font_sizes:
            raise BadConfigError(
                f'Font size can only have one of these '
                f'values: '
                f'{",  ".join(expected_base_plot_font_sizes)}.')

    def apply_config(self, window: MainWindow) -> None:
        """
        Apply the loaded config to a window.

        :param window: the window to apply the config to.
        """
        window.config = self.config
        get_bool = self.config.getboolean

        window.from_data_card_check_box.setChecked(
            get_bool('FileRead', 'from_data_card')
        )
        window.data_radio_button.setChecked(get_bool('FileRead', 'data'))
        window.sdata_radio_button.setChecked(get_bool('FileRead', 'sdata'))
        window.log_checkbox.setChecked(get_bool('FileRead', 'log'))

        window.background_black_radio_button.setChecked(
            get_bool('ColorMode', 'black')
        )
        window.background_white_radio_button.setChecked(
            get_bool('ColorMode', 'white')
        )

        window.gap_len_line_edit.setText(
            window.config.get('Gap', 'min_gap_length')
        )

        window.mass_pos_123zne_check_box.setChecked(
            get_bool('Channels', 'mp123zne')
        )
        window.mass_pos_456uvw_check_box.setChecked(
            get_bool('Channels', 'mp456uvw')
        )
        window.all_wf_chans_check_box.setChecked(
            get_bool('Channels', 'all_waveform_chans')
        )
        for i, checkbox in enumerate(window.ds_check_boxes, start=1):
            checkbox.setChecked(get_bool('Channels', f'ds{i}'))
        window.mseed_wildcard_edit.setText(
            window.config.get('Channels', 'mseed_wildcard')
        )
        window.tps_check_box.setChecked(get_bool('Channels', 'plot_tps'))
        window.raw_check_box.setChecked(get_bool('Channels', 'plot_raw'))

        window.all_soh_chans_check_box.setChecked(
            get_bool('ChannelsPreference', 'all_soh')
        )

        from_date = window.config.get('DateRange', 'from_date')
        to_date = window.config.get('DateRange', 'to_date')
        # If the from date or to date is not stored, we populate them with
        # default values. In that case, the from date is the day of the Unix
        # epoch (1970-1-1) and the to date is the day the program was run on.
        if from_date == '':
            from_date = constants.DEFAULT_START_TIME
        if to_date == '':
            to_date = QtCore.QDate.currentDate().toString('yyyy-MM-dd')
        window.time_from_date_edit.setDate(
            QtCore.QDate.fromString(from_date, 'yyyy-MM-dd')
        )
        window.time_to_date_edit.setDate(
            QtCore.QDate.fromString(to_date, 'yyyy-MM-dd')
        )

        mp_color_mode = window.config.get('MiscOptions', 'mp_color_mode')
        if mp_color_mode == 'regular':
            window.mp_regular_color_action.trigger()
        elif mp_color_mode == 'trillium':
            window.mp_trillium_color_action.trigger()

        tps_color_mode = window.config.get('MiscOptions', 'tps_color_mode')
        window.tps_dlg.color_range_choice.setCurrentText(tps_color_mode)

        date_mode = window.config.get('MiscOptions', 'date_mode')
        if date_mode == 'YYYY-MM-DD':
            window.yyyy_mm_dd_action.trigger()
        elif date_mode == 'YYYYMMMDD':
            window.yyyymmmdd_action.trigger()
        elif date_mode == 'YYYY:DOY':
            window.yyyy_doy_action.trigger()

        base_plot_font_size = window.config.get(
            'MiscOptions', 'base_plot_font_size')
        window.base_plot_font_size_action_dict[
            int(base_plot_font_size)].trigger()

        window.add_masspos_to_rt130_soh.setChecked(
            get_bool('MiscOptions', 'add_mass_pos_to_soh')
        )

    def reset(self):
        self.config = configparser.ConfigParser()
        self.config.read_string(default_config)
        with open(CONFIG_PATH, 'w+') as config_file:
            self.config.write(config_file)
