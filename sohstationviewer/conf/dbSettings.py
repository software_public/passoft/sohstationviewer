import re
import os
from pathlib import Path

from sohstationviewer.conf.constants import (WF_1ST, WF_2ND, WF_3RD)
"""
seisRE: Seismic data channels' regex:
First letter(Band Code): ABCDEFGHLMOPQRSTUV
Second letter (Instrument Code): GHLMN  (remove GM - Alissa)
Third letter (Orientation Code): ZNE123
=> to check if the channel is seismic data: if conf['seisRE'].match(chan):
"""

dbConf = {
    'db_path': 'database/soh.db',
    'backup_db_path': 'database/backup.db',
    'seisRE': re.compile(f'[{WF_1ST}][{WF_2ND}][{WF_3RD}]'),
    # key is last char of chan
    'seisLabel': {'1': 'NS', '2': 'EW', 'N': 'NS', 'E': 'EW', 'Z': 'V'},
}


def modify_db_path():
    """
    Modify db_path to absolute path.

    This function is called in case database needs to be used when the working
    directory is a sub folder of root, sohstationviewer/. The following lines
    need to be added:
        from sohstationviewer.conf.dbSettings import modify_db_path
        modify_db_path()
    """
    global dbConf
    current_file_path = os.path.abspath(__file__)
    root = Path(current_file_path).parent.parent
    db_path = [x for x in root.glob('**/soh.db')][0]
    dbConf['db_path'] = db_path.as_posix()
