"""
basic executing database functions
"""
import sqlite3
from typing import Sequence, Union

from sohstationviewer.conf.dbSettings import dbConf


def execute_db(sql: str, parameters: Union[dict, Sequence] = (),
               db_path: str = 'db_path'):
    """
    Execute or fetch data from DB
    :param sql: str - request string to execute or fetch data from database
    :param parameters: the parameters used for executing parameterized queries
    :param db_path: key of path to db or backup db
    :return rows: [(str/int,] - result of querying DB
    """
    conn = sqlite3.connect(dbConf[db_path])
    cur = conn.cursor()
    try:
        cur.execute(sql, parameters)
    except sqlite3.OperationalError as e:
        print("sqlite3.OperationalError:%s\n\tSQL: %s\n\tpath: %s"
              % (str(e), sql, dbConf[db_path]))
    rows = cur.fetchall()
    conn.commit()       # used for execute: update/insert/delete
    cur.close()
    conn.close()
    return rows


def execute_db_dict(sql):
    """
    Fetch data and return rows in dictionary with fields as keys
    :param sql: str - request string to fetch data from database
    :return: [dict,] - result of fetching DB in which each row is a dict
    """
    conn = sqlite3.connect(dbConf['db_path'])
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    try:
        cur.execute(sql)
    except sqlite3.OperationalError as e:
        print("sqlite3.OperationalError:%s\n\tSQL%s" % (str(e), sql))
    rows = cur.fetchall()
    cur.close()
    conn.close()
    return [dict(row) for row in rows]


def trunc_add_db(table, sql_list):
    """
    Truncate table and refill with new data
    :param table: str - name of data table to process
    :param sql_list: [str, ] - list of INSERT query to add values to table
    :return: str: error message if not successful
        or bool(True): if successful
    """
    try:
        conn = sqlite3.connect(dbConf['db_path'])
        cur = conn.cursor()
        cur.execute('BEGIN')
        cur.execute(f'DELETE FROM {table}')
        for sql in sql_list:
            cur.execute(sql)
        cur.execute('COMMIT')
    except sqlite3.Error as e:
        try:
            cur.execute('ROLLBACK')
        except Exception:
            pass
        return (f'Cannot write to table {table}'
                f'because of database error:\n{str(e)}')
    except Exception as e:
        try:
            cur.execute('ROLLBACK')
        except Exception:
            pass
        return (f'Cannot write to table {table} '
                f'because of code error:\n{str(e)}')
    try:
        cur.close()
        conn.close()
    except Exception:
        pass
    return True
