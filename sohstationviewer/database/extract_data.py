import re
from typing import Dict, List, Set

from sohstationviewer.conf.constants import ColorMode
from sohstationviewer.database.process_db import execute_db_dict, execute_db
from sohstationviewer.conf.dbSettings import dbConf


def get_chan_plot_info(org_chan_id: str, data_type: str,
                       color_mode: ColorMode = 'B') -> Dict:
    """
    Given chanID read from raw data file and detected dataType
    Return plotting info from DB for that channel.

    :param org_chan_id: channel name read from data source
    :param chan_info: info of the channel read from data source
    :param data_type: type of data
    :param color_mode: B/W
    :return chan_db_info[0]: info of channel read from DB which is used for
        plotting. In which,
        + Key 'dbChannel' keeps channel's name in DB
        + Key 'channel' keeps channel's name read from data
        + Key 'dbLabel' keeps label value in DB
        + Key 'label' keeps label to be displayed in the plotting
    """
    chan = org_chan_id
    chan = convert_actual_channel_to_db_channel_w_question_mark(chan,
                                                                data_type)

    # RT130 waveform channels is formed by the string DS followed by the data
    # stream number, a dash character, and the channel number.
    # Doing incomplete checks for RT130 waveform channels have caused a bunch
    # of problems before (see https://git.passcal.nmt.edu/software_public/passoft/sohstationviewer/-/merge_requests/240?diff_id=5092&start_sha=55ac101ffac8860ba66ac5e7b694e70ee397919c  # noqa
    # and https://git.passcal.nmt.edu/software_public/passoft/sohstationviewer/-/issues/287).  # noqa
    # So, we match RT130 waveform channels exactly with a regex to avoid these
    # problems.
    rt130_waveform_regex = re.compile(r'DS\d-\d')
    if rt130_waveform_regex.match(org_chan_id):
        chan = 'SEISMIC'
    if dbConf['seisRE'].match(chan):
        chan = 'SEISMIC'
    # The valueColors for each color mode is stored in a separate column.
    # Seeing as we only need one of these columns for a color mode, we only
    # pull the needed valueColors column from the database.
    value_colors_column = 'valueColors' + color_mode
    o_sql = (f"SELECT C.param as param, channel as dbChannel, plotType,"
             f" height, unit, convertFactor, label as dbLabel, fixPoint, "
             f"{value_colors_column} AS valueColors "
             f"FROM Channels as C, Parameters as P")
    if data_type == 'Unknown':
        sql = f"{o_sql} WHERE channel='{chan}' and C.param=P.param"
    else:
        sql = (f"{o_sql} WHERE channel='{chan}' and C.param=P.param"
               f" and dataType='{data_type}'")
    chan_db_info = execute_db_dict(sql)
    seismic_label = None
    if len(chan_db_info) == 0:
        chan_db_info = execute_db_dict(
            f"{o_sql} WHERE channel='DEFAULT' and C.param=P.param")
        chan_db_info[0]['channel'] = chan_db_info[0]['dbChannel']
    else:
        if chan_db_info[0]['dbChannel'] == 'SEISMIC':
            seismic_label = get_seismic_chan_label(org_chan_id)
        chan_db_info[0]['channel'] = org_chan_id

    # add plotLabel key to be used in plotting.
    # the original key label is unchanged to help when editing the channel's.
    chan_db_info[0]['label'] = (
        '' if chan_db_info[0]['dbLabel'] is None
        else chan_db_info[0]['dbLabel'])
    chan_db_info[0]['unit'] = (
        '' if chan_db_info[0]['unit'] is None else chan_db_info[0]['unit'])
    chan_db_info[0]['fixPoint'] = (
        0 if chan_db_info[0]['fixPoint'] is None
        else chan_db_info[0]['fixPoint'])
    if (chan_db_info[0]['plotType'] == 'multiColorDots' and
            chan_db_info[0]['valueColors'] in [None, 'None', '']):
        chan_db_info[0]['valueColors'] = '*:W' if color_mode == 'B' else '*:B'

    if chan_db_info[0]['label'].strip() == '':
        chan_db_info[0]['label'] = chan_db_info[0]['channel']
    elif seismic_label is not None:
        chan_db_info[0]['label'] = seismic_label
    else:
        chan_db_info[0]['label'] = '-'.join(
            [chan_db_info[0]['channel'], chan_db_info[0]['label']])
    if chan_db_info[0]['label'].strip() == 'DEFAULT':
        chan_db_info[0]['label'] = 'DEFAULT-' + org_chan_id
    return chan_db_info[0]


def get_convert_factor(chan_id: str, data_type: str) -> float:
    """
    Get the convert factor to convert data from count (value read from file)
    to actual value to display.

    :param chan_id: actual channel name read from file
    :param data_type: type of data in data set
    :return: converting factor
    """
    chan_id = convert_actual_channel_to_db_channel_w_question_mark(chan_id,
                                                                   data_type)
    sql = f"SELECT convertFactor FROM Channels WHERE channel='{chan_id}' " \
          f"AND dataType='{data_type}'"
    ret = execute_db(sql)
    if ret:
        return ret[0][0]
    else:
        return None


def convert_actual_channel_to_db_channel_w_question_mark(
        chan_id: str, data_type: str) -> str:
    """
    The digit in channel end with a digit is represented with the question
        mark '?' in DB. This function change the real channel name to DB
        channel name with '?'.
    :param chan_id: real channel name
    :param data_type: the data type of the channel, used for handling masspos
        data of Q330 data
    :return chan_id: channel name with '?' at the end if available
    """
    sql = "SELECT * FROM Channels WHERE channel like '%?'"
    ret = execute_db(sql)
    question_channels = [c[0][:-1] for c in ret]
    if any(chan_id.startswith(x) for x in question_channels):
        if chan_id[-1].isdigit():
            # to prevent the case prefix similar to prefix of channel w/o ?
            chan_id = chan_id[:-1] + '?'
        if chan_id.startswith('VM'):
            chan_id = 'VM?'

    return chan_id


def get_seismic_chan_label(chan_id):
    """
    Get label for chan_id in which data stream can use chan_id for label while
        other seismic need to add coordinate to chan_id for label
    :param chan_id: name of channel
    :return label: label to put in front of the plot of the channel
    """
    if chan_id.startswith("DS"):
        label = chan_id
    else:
        try:
            label = chan_id + '-' + dbConf['seisLabel'][chan_id[-1]]
        except KeyError:
            label = chan_id
    return label


def get_signature_channels() -> Dict[str, str]:
    """
    return the dict {channel: dataType} in which channel is unique for dataType
    """
    sql = ("SELECT channel, dataType FROM Channels where channel in"
           "(SELECT channel FROM Channels GROUP BY channel"
           " HAVING COUNT(channel)=1)")
    rows = execute_db_dict(sql)
    sign_chan_data_type_dict = {r['channel']: r['dataType'] for r in rows}
    return sign_chan_data_type_dict


def get_all_channels() -> Dict[str, Set[str]]:
    """
    Get all the channels in the database alongside their associated data types.

    :return: the dictionary {channel: data types} that contains all channels in
        the database. A channel can be associated with multiple data types.
    """
    sql = "SELECT channel, dataType FROM Channels"
    rows = execute_db_dict(sql)
    all_channels_dict = {}
    for r in rows:
        channel_data_types = all_channels_dict.setdefault(r['channel'], set())
        channel_data_types.add(r['dataType'])
    return all_channels_dict


def get_color_def():
    sql = "SELECT color FROM TPS_ColorDefinition ORDER BY name ASC"
    rows = execute_db(sql)
    return [r[0] for r in rows]


def get_color_ranges():
    sql = "SELECT name, baseCounts FROM TPS_ColorRange"
    rows = execute_db(sql)
    range_names = [r[0] for r in rows]
    base_counts = [r[1] for r in rows]
    all_square_counts = []
    clr_labels = []
    cnt = 0
    # 7 : number of color definition, not include 'E'
    for idx, bc in enumerate(base_counts):
        all_square_counts.append([0] * 7)
        clr_labels.append(['0 counts'])
        for c_idx in range(1, 7):
            cnt = (bc * 10 ** (c_idx - 1))
            all_square_counts[idx][c_idx] = cnt ** 2
            clr_labels[idx].append("+/- {:,} counts".format(cnt))
        clr_labels[idx].append("> {:,} counts".format(cnt))
    return range_names, all_square_counts, clr_labels


def create_assign_string_for_db_query(col: str, val: str) -> str:
    """
    Create assign db string that assign value in single quote signs if val is
    a string or to NULL if val is empty str
    :param col: column name in the db table
    :param val: value to be assigned to the column
    :return: the assign db string
    """
    return f"{col}='{val}'" if val != '' else f"{col}=NULL"


def get_params():
    # get parameter list from database
    param_rows = execute_db("SELECT param from parameters")
    return sorted([d[0] for d in param_rows])


def get_param_info(param: str):
    # get all info of a param from DB
    sql = f"SELECT * FROM Parameters WHERE param='{param}'"
    param_info = execute_db_dict(sql)[0]
    return param_info


def get_data_types(include_default: bool = True, include_rt130: bool = True) \
        -> List[str]:
    """
    Get list of data types from DB.
    :param include_default: flag to include Default type in the list or not
    :param include_rt130: flag to include RT130 type in the list or not
    :return: list of data types
    """
    data_type_rows = execute_db(
        'SELECT * FROM DataTypes ORDER BY dataType ASC')
    data_type_list = [d[0] for d in data_type_rows]
    if not include_default:
        data_type_list = [d for d in data_type_list if d != 'Default']
    if not include_rt130:
        data_type_list = [d for d in data_type_list if d != 'RT130']
    return data_type_list
