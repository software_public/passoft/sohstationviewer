from numbers import Real
from typing import BinaryIO, Optional, Dict, Union, List, Tuple
from pathlib import Path

import numpy
from obspy import UTCDateTime

from sohstationviewer.model.mseed_data.record_reader import RecordReader
from sohstationviewer.model.mseed_data.record_reader_helper import \
    RecordMetadata

from sohstationviewer.controller.util import check_chan


def move_to_next_record(file, current_record_start: int,
                        record: RecordReader):
    """
    Move the current position of file to next record

    :param current_record_start: the start position of the current record
    :param reader: the record that is reading
    """
    # MSEED stores the size of a data record as an exponent of a
    # power of two, so we have to convert that to actual size before
    # doing anything else.
    record_length_exp = record.header_unpacker.unpack(
        'B', record.blockette_1000.record_length
    )[0]
    record_size = 2 ** record_length_exp

    file.seek(current_record_start)
    file.seek(record_size, 1)


LogData = Dict[str, Union[List[str], Dict[str, List[str]]]]


class MSeedReader:
    def __init__(self, file_path: Path,
                 read_start: float = UTCDateTime(0).timestamp,
                 read_end: float = UTCDateTime().timestamp,
                 is_multiplex: Optional[bool] = None,
                 req_soh_chans: List[str] = [],
                 req_wf_chans: List[str] = [],
                 include_mp123zne: bool = False,
                 include_mp456uvw: bool = False,
                 soh_data: Dict = {},
                 mass_pos_data: Dict = {},
                 waveform_data: Dict = {},
                 log_data: LogData = {},
                 gap_minimum: Optional[float] = None
                 ) -> None:
        """
        The object of the class is to read data from given file to add
            to given stream if meet requirements.
        If data_type is not multiplex, all records of a file are belong to the
            same channel; the info found from the first record can
            be used to determine to keep reading if the first one doesn't meet
            channel's requirement.
        If data_type is multiplex, every record have to be examined.
        All data_dicts' definition can be found in data_dict_structures.MD

        :param file_path: Absolute path to data file
        :param read_start: time that is required to start reading
        :param read_end: time that is required to end reading
        :param is_multiplex: multiplex status of the file's data_type
        :param req_soh_chans: requested SOH channel list
        :param req_wf_chans: requested waveform channel list
        :param include_mp123zne: if mass position channels 1,2,3 are requested
        :param include_mp456uvw: if mass position channels 4,5,6 are requested
        :param soh_data: data dict of SOH
        :param mass_pos_data: data dict of mass position
        :param waveform_data: data dict of waveform
        :param log_data: data dict of log_data
        :param gap_minimum: minimum length of gaps required to detect
            from record
        """
        self.read_start = read_start
        self.read_end = read_end
        self.is_multiplex = is_multiplex
        self.gap_minimum = gap_minimum
        self.req_soh_chans = req_soh_chans
        self.req_wf_chans = req_wf_chans
        self.include_mp123zne = include_mp123zne
        self.include_mp456uvw = include_mp456uvw
        self.soh_data = soh_data
        self.mass_pos_data = mass_pos_data
        self.waveform_data = waveform_data
        self.log_data = log_data
        self.file_path = file_path
        self.file: BinaryIO = open(file_path, 'rb')

        self.invalid_blockettes = False,

    def get_data_dict(self, metadata: RecordMetadata) -> Dict:
        """
        Find which data_dict to add data to from req_soh_chans, req_wf_chans,
            include_mp123zne, include_mp456uvw, samplerate
        :param metadata: record's metadata
        :return: data_dict to add data
        """
        chan_id = metadata.channel
        sample_rate = metadata.sample_rate
        chan_type = check_chan(chan_id, self.req_soh_chans, self.req_wf_chans,
                               self.include_mp123zne, self.include_mp456uvw)
        if chan_type == 'SOH':
            if self.req_soh_chans == [] and sample_rate > 1:
                # If 'All chans' is selected for SOH, channel with samplerate>1
                # will be skipped by default to improve performance.
                # Note: If user intentionally added channels with samplerate>1
                # using SOH Channel Preferences dialog, they are still read.
                return
            return self.soh_data
        if chan_type == 'MP':
            return self.mass_pos_data
        if chan_type == 'WF':
            return self.waveform_data

    def check_time(self, record: RecordReader) -> bool:
        """
        Check if record time in the time range that user require to read

        :param record: the record read from file
        :return: True when record time satisfy the requirement
        """
        meta = record.record_metadata
        if self.read_start > meta.end_time or self.read_end < meta.start_time:
            return False
        return True

    def append_log(self, record: RecordReader) -> None:
        """
        Add all text info retrieved from record to log_data

        :param record: the record read from file
        """
        logs = [record.ascii_text] + record.other_blockettes
        log_str = "===========\n".join(logs)
        if log_str == "":
            return
        meta = record.record_metadata
        log_str = "\n\nSTATE OF HEALTH: " + \
                  f"From:{meta.start_time}  To:{meta.end_time}\n" + log_str
        sta_id = meta.station
        chan_id = meta.channel
        if sta_id not in self.log_data.keys():
            self.log_data[sta_id] = {}
        if chan_id not in self.log_data[sta_id]:
            self.log_data[sta_id][chan_id] = []
        self.log_data[sta_id][chan_id].append(log_str)

    def append_data(self, data_dict: dict,
                    record: RecordReader,
                    data_points: List[Real],
                    times: List[Real]) -> None:
        """
        Append some data points to the given data_dict

        :param data_dict: the data dict to add data get from record
        :param record: the record read from file
        :param data_points: data points to be appended
        :param times: time data of the data point
        """
        if data_points is None:
            return
        meta = record.record_metadata
        sta_id = meta.station
        if sta_id not in data_dict.keys():
            data_dict[sta_id] = {}
        station = data_dict[sta_id]
        self.add_chan_data(station, meta, data_points, times)

    def _add_new_trace(self, channel: Dict, metadata: RecordMetadata,
                       data_points: List[Real],
                       times: List[Real]) -> None:
        """
        Start a new trace to channel['tracesInfo'] with data_point as
            the first data value and metadata's start_time as first time value

        :param channel: dict of channel's info
        :param metadata: record's metadata
        :param data_points: data points extracted from the record frame
        :param times: time data of the data point
        """
        channel['tracesInfo'].append({
            'startTmEpoch': metadata.start_time,
            'endTmEpoch': metadata.end_time,
            'data': list(data_points),
            'times': list(times)
        })

    def _append_trace(self, channel: Dict,
                      data_points: Tuple[Real, Real],
                      times: Tuple[Real, Real],
                      one_sample_apart: bool):
        """
        Appending data_point to the latest trace of channel['tracesInfo']

        If the added trace is one sample apart from the previous one, the last
        point before adding new trace will be removed.

        :param channel: dict of channel's info
        :param data_points: data points extracted from the record frame
        :param times: time data of the data point
        :param one_sample_apart: flag to tell if the added trace is one sample
            apart from the previous one.
        """
        if one_sample_apart:
            # Remove last point of the previous trace if it is only one sample
            # apart from beginning of the next one to not showing 2 points that
            # is very close together.
            channel['tracesInfo'][-1]['data'] = channel['tracesInfo'][-1][
                                                    'data'][:-1]
            channel['tracesInfo'][-1]['times'] = channel['tracesInfo'][-1][
                                                     'times'][:-1]

        channel['tracesInfo'][-1]['data'].extend(data_points)
        channel['tracesInfo'][-1]['times'].extend(times)

    def add_chan_data(self, station: dict, metadata: RecordMetadata,
                      data_points: List[Real],
                      times: List[Real]) -> None:
        """
        Add new channel to the passed station and append data_point to the
        channel if there's no gap/overlap or start a new trace of data
        when there's a gap.

        If gap/overlap > gap_minimum, add to gaps list but gap won't be added
        if the added trace is one sample apart from the previous one to keep
        the view sample rate for the channel consistent.

        :param station: dict of chan by id of a station
        :param metadata: metadata of the record the data points are extracted
            from
        :param data_points: data points to be added
        :param times: time data of the data point
        """
        meta = metadata
        chan_id = metadata.channel
        if chan_id not in station.keys():
            station[chan_id] = {
                'file_path': self.file_path,
                'chanID': chan_id,
                'samplerate': meta.sample_rate,
                'startTmEpoch': meta.start_time,
                'endTmEpoch': meta.end_time,
                'size': meta.sample_count,
                'nets': {meta.network},
                'gaps': [],
                'tracesInfo': [{
                    'startTmEpoch': meta.start_time,
                    'endTmEpoch': meta.end_time,
                    'data': list(data_points),
                    'times': list(times)
                }],
                'visible': True
            }
        else:
            channel = station[chan_id]
            record_start_time = meta.start_time
            previous_end_time = channel['endTmEpoch']
            delta = record_start_time - previous_end_time
            sample_interval = 1 / meta.sample_rate
            if channel['file_path'] != self.file_path:
                # Start new trace for each file to reorder trace and
                # combine traces again later
                channel['file_path'] = self.file_path
                self._add_new_trace(channel, meta, data_points, times)
            else:
                if (self.gap_minimum is not None and
                        abs(delta) >= self.gap_minimum and
                        delta != sample_interval):
                    gap = [previous_end_time, record_start_time]
                    channel['gaps'].append(gap)
                # appending data
                if delta == sample_interval:
                    one_sample_apart = True
                else:
                    one_sample_apart = False
                self._append_trace(channel, data_points, times,
                                   one_sample_apart)

            channel['tracesInfo'][-1]['endTmEpoch'] = meta.end_time
            # update channel's metadata
            channel['endTmEpoch'] = meta.end_time
            channel['size'] += meta.sample_count
            channel['nets'].add(meta.network)

    def read(self):
        while 1:
            # We know that end of file is reached when read() returns an empty
            # string.
            is_eof = (self.file.read(1) == b'')
            if is_eof:
                break
            # We need to move the file pointer back to its position after we
            # do the end of file check. Otherwise, we would be off by one
            # byte for all the reads afterward.
            self.file.seek(-1, 1)

            # We save the start of the current record so that after we are
            # done reading the record, we can move back. This makes moving
            # to the next record a lot easier, seeing as we can simply move
            # the file pointer a distance the size of the current record.
            current_record_start = self.file.tell()

            record = RecordReader(self.file)
            if record.invalid_blockettes:
                self.invalid_blockettes = True
            if not self.check_time(record):
                move_to_next_record(
                    self.file, current_record_start, record)
                continue
            data_dict = self.get_data_dict(record.record_metadata)
            if data_dict is None:
                if self.is_multiplex:
                    move_to_next_record(
                        self.file, current_record_start, record)
                    continue
                else:
                    break
            if data_dict is self.waveform_data:
                data_points = list(record.get_two_data_points())
                times = [record.record_metadata.start_time,
                         record.record_metadata.end_time]
            else:
                try:
                    data_points = record.get_data_points()
                    sample_interval = 1 / record.record_metadata.sample_rate
                    times = numpy.arange(
                            record.record_metadata.start_time,
                            record.record_metadata.end_time,
                            sample_interval).tolist()
                    # The previous calculation will always miss one time point
                    # at the end. We can adjust the stop argument to capture
                    # that point in one calculation, but the code for that is a
                    # lot harder to parse. It is a lot more straightforward to
                    # just add the final time point at the end.
                    times.append(times[-1] + sample_interval)  # noqa
                except ZeroDivisionError:
                    data_points = None
                    times = None
            self.append_data(data_dict, record, data_points, times)
            self.append_log(record)

            move_to_next_record(self.file, current_record_start, record)
        self.file.close()
