from typing import List, Dict
from copy import deepcopy
import numpy as np


def retrieve_nets_from_data_dict(data_dict: Dict,
                                 nets_by_sta: Dict[str, List[str]]) -> None:
    """
    Retrieve nets by sta_id from the given data_dict.

    :param data_dict: dict of data by station
    :param nets_by_sta: nets list by sta_id
    """
    for sta_id in data_dict.keys():
        if sta_id not in nets_by_sta:
            nets_by_sta[sta_id] = set()
        for c in data_dict[sta_id]:
            nets_by_sta[sta_id].update(
                data_dict[sta_id][c]['nets'])


def split_vst_channel(selected_data_set_id: str, data_dict: Dict):
    """
    Each value consists of 4 bit.
    This function will split each bit into one array that will be the data
    of the channel VST<bit position> which has other keys' info from the
    original channel VST.
    The original VST channel is removed at the end of the function.

    :param selected_data_set_id: the id of the selected data set
    :param data_dict: data of the selected key
    """
    try:
        selected_data_dict = data_dict[selected_data_set_id]
    except KeyError:
        return
    vst_channels_data = [[], [], [], []]
    if 'VST' not in selected_data_dict:
        return
    data = selected_data_dict['VST']['tracesInfo'][0]['data']
    for d in data:
        b = '{0:04b}'.format(d)
        for i in range(4):
            vst_channels_data[i].append(int(b[i]))
    for i in range(4):
        name = f'VST{3 - i}'
        selected_data_dict[name] = deepcopy(
            selected_data_dict['VST'])
        selected_data_dict[name]['tracesInfo'][0]['data'] = np.array(
            vst_channels_data[i])
    del selected_data_dict['VST']
