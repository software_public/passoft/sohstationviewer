from numbers import Real
from typing import Tuple

from sohstationviewer.model.general_data.general_record_helper import Unpacker


def get_data_points_as_bytes(record_data: bytes, sample_count: int,
                             point_size: int) -> bytes:
    """
    Get some data points from the record as a contiguous byte string.

    :param record_data: the data portion of the record
    :param sample_count: the number of sample in the record
    :param point_size: the size of a data point
    :return: some data points from the record as a contiguous byte string
    """
    first_data_point = record_data[:point_size]
    last_data_point_idx = (sample_count - 1) * point_size
    last_data_point = record_data[last_data_point_idx:
                                  last_data_point_idx + point_size]
    return first_data_point + last_data_point


def decode_int16(record_data: bytes, sample_count: int,
                 unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must have a data type of 16-bit integers.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    data_points = get_data_points_as_bytes(record_data, sample_count, 2)
    return unpacker.unpack('hh', data_points)


def decode_int24(record_data: bytes, sample_count: int,
                 unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must have a data type of 24-bit integers.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    data_points = get_data_points_as_bytes(record_data, sample_count, 3)
    byte_order = 'big' if unpacker.byte_order_char == '>' else 'little'
    # We delegate to int.from_bytes() because it takes a lot of work to make
    # struct.unpack() handle signed 24-bits integers.
    # See: https://stackoverflow.com/questions/3783677/how-to-read-integers-from-a-file-that-are-24bit-and-little-endian-using-python  # noqa
    point_1 = int.from_bytes(data_points[:3], byte_order)
    point_2 = int.from_bytes(data_points[3:], byte_order)
    return point_1, point_2


def decode_int32(record_data: bytes, sample_count: int,
                 unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must have a data type of 32-bit integers.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    data_points = get_data_points_as_bytes(record_data, sample_count, 4)
    return unpacker.unpack('ii', data_points)


def decode_ieee_float(record_data: bytes, sample_count: int,
                      unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must have a data type of IEEE-754 32-bit floats.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    data_points = get_data_points_as_bytes(record_data, sample_count, 4)
    return unpacker.unpack('ff', data_points)


def decode_ieee_double(record_data: bytes, sample_count: int,
                       unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must have a data type of IEEE-754 64-bit floats.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    data_points = get_data_points_as_bytes(record_data, sample_count, 8)
    return unpacker.unpack('dd', data_points)


def decode_steim(record_data: bytes, sample_count: int,
                 unpacker: Unpacker) -> Tuple[Real, Real]:
    """
    Decode the data part of an MSeed record and extract two points from it.
    The MSeed record must be compressed with the Steim compression algorithm.

    :param record_data: the data part of an MSeed record
    :param sample_count: the number of points in the record
    :param unpacker: the unpacker used to decode the record
    :return: two points from record_data
    """
    # The first 4 bytes in a Steim frame is metadata of the record. Since we
    # aren't decompressing the data, we are skipping. The next 8 bytes contain
    # the first and last data points of the MSEED data record, which is what we
    # need.
    data_points = record_data[4:12]
    return unpacker.unpack('ii', data_points)
