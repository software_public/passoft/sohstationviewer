from dataclasses import dataclass
import struct
from enum import Enum
from datetime import datetime, timezone

from sohstationviewer.conf.constants import SECOND_IN_DAY
from sohstationviewer.model.general_data.general_record_helper import Unpacker


class MSeedReadError(Exception):
    def __init__(self, msg):
        self.message = msg


@dataclass
class FixedHeader:
    """
    The fixed portion of the header of a data record. All fields are stored as
    bytes to minimize time wasted on decoding unused values.
    """
    sequence_number: bytes
    data_header_quality_indicator: bytes
    reserved: bytes
    station: bytes
    location: bytes
    channel: bytes
    net_code: bytes
    record_start_time: bytes
    sample_count: bytes
    sample_rate_factor: bytes
    sample_rate_multiplier: bytes
    activity_flags: bytes
    io_and_clock_flags: bytes
    data_quality_flags: bytes
    blockette_count: bytes
    time_correction: bytes
    data_offset: bytes
    first_blockette_offset: bytes


@dataclass
class Blockette1000:
    """
    Blockette 100 of a data record. All fields are stored as bytes to minimize
    time wasted on decoding unused values.
    """
    blockette_type: bytes
    next_blockette_offset: bytes
    encoding_format: bytes
    byte_order: bytes
    record_length: bytes
    reserved_byte: bytes


@dataclass
class RecordMetadata:
    """
    The metadata of a data record.
    """
    station: str
    location: str
    channel: str
    network: str
    start_time: float
    end_time: float
    sample_count: int
    sample_rate: float


class EncodingFormat(Enum):
    ASCII = 0
    INT_16_BIT = 1
    INT_24_BIT = 2
    INT_32_BIT = 3
    IEEE_FLOAT_32_BIT = 4
    IEEE_FLOAT_64_BIT = 5
    STEIM_1 = 10
    STEIM_2 = 11


def check_time_from_time_string(endian, time_string):

    try:
        record_start_time_tuple = struct.unpack(f'{endian}hhbbbbh',
                                                time_string)
    except struct.error:
        raise MSeedReadError("Not an MSeed file.")
    # libmseed uses 1900 to 2100 as the sane year range. We follow their
    # example here.
    year_is_good = (1900 <= record_start_time_tuple[0] <= 2100)
    # The upper range is 366 to account for leap years.
    day_is_good = (1 <= record_start_time_tuple[1] <= 366)
    return year_is_good and day_is_good


def get_header_endianness(header: FixedHeader):
    """
    Determine the endianness of the fixed header of a data record. Works by
    checking if the decoded record start time has a sane value if the header
    is assumed to be big-endian.

    WARNING: This check fails on three dates: 2056-1, 2056-256, and 2056-257.
    2056 is a palindrome when encoded as a pair of octets, so endianness does
    not affect it. Similarly, 257 is also 2-octet-palindromic. Meanwhile, 1 and
    256 are counterparts when encoded as pairs of octets. Because they are both
    valid values for day of year, it is impossible to make a conclusion about
    endianness based on day of year if it is either 1 or 256 in big-endian.
    These facts combined means that we cannot determine the endianness of the
    header whose record starts on the aforementioned dates. The way this
    function was written, the endianness will be recorded as big in these
    cases. This problem is also recorded in libmseed.

    :param header: the fixed header of the data record
    :return: either of the string 'big' or 'little' depending on the extracted
    endianness of header
    """
    record_start_time_string = header.record_start_time
    good_time = check_time_from_time_string('>', record_start_time_string)
    if good_time:
        endianness = 'big'
    else:
        good_time = check_time_from_time_string('<', record_start_time_string)
        if good_time:
            endianness = 'little'
        else:
            raise MSeedReadError("Not an MSeed file.")
    return endianness


def get_data_endianness(blockette_1000: Blockette1000):
    """
    Get endianness of a data record by examining blockette 1000.

    :param blockette_1000: the blockette 1000 of the data record.
    """
    # The byte order is only one byte so using big or little endian does not
    # matter.
    blockette_1000_endianness = int.from_bytes(
        blockette_1000.byte_order, 'big'
    )
    if blockette_1000_endianness:
        return 'big'
    else:
        return 'little'


def calculate_sample_rate(factor: int, multiplier: int) -> float:
    """
    Calculate the sample rate using the sample rate factor and multiplier. This
    algorithm is described around the start of Chapter 8 in the SEED manual.

    :param factor: the sample rate factor
    :param multiplier: the sample rate multiplier
    :return: the nominal sample rate
    """
    sample_rate = 0
    if factor == 0:
        sample_rate = 0
    elif factor > 0 and multiplier > 0:
        sample_rate = factor * multiplier
    elif factor > 0 and multiplier < 0:
        sample_rate = -(factor / multiplier)
    elif factor < 0 and multiplier > 0:
        sample_rate = -(multiplier / factor)
    elif factor < 0 and multiplier < 0:
        sample_rate = 1 / (factor * multiplier)
    return sample_rate


def get_record_metadata(header: FixedHeader, header_unpacker: Unpacker):
    """
    Extract and parse the metadata of a data record from its fixed header.

    :param header: the fixed header of the data record
    :param header_unpacker: the unpacker corresponding to the data record;
        needed so that the correct byte order can be used
    :return: the extract record metadata
    """
    try:
        station = header.station.decode('utf-8').rstrip()
        location = header.location.decode('utf-8').rstrip()
        channel = header.channel.decode('utf-8').rstrip()
        network = header.net_code.decode('utf-8').rstrip()

        record_start_time_string = header.record_start_time
        record_start_time_tuple = header_unpacker.unpack(
            'HHBBBBH', record_start_time_string)
        year = record_start_time_tuple[0]
        day_of_year = record_start_time_tuple[1]
        hour = record_start_time_tuple[2]
        minute = record_start_time_tuple[3]
        second = record_start_time_tuple[4]
        microsecond = record_start_time_tuple[6] * 100

        # We get timestamp of the first moment of the year, then add all the
        # remaining information later. This is the most efficient
        # implementation that is still relatively readable.
        record_start_time = datetime.fromisoformat(
            f'{str(year)}-01-01').replace(
            tzinfo=timezone.utc).timestamp()
        record_start_time += (
                (day_of_year - 1) * SECOND_IN_DAY +
                hour * 3600 +
                minute * 60 +
                second +
                microsecond / 1000000)

        sample_count = header_unpacker.unpack('H', header.sample_count)[0]

        sample_rate_factor = header_unpacker.unpack(
            'h', header.sample_rate_factor
        )[0]
        sample_rate_multiplier = header_unpacker.unpack(
            'h', header.sample_rate_multiplier
        )[0]
    except ValueError:
        raise MSeedReadError("Not an MSeed file.")
    sample_rate = calculate_sample_rate(sample_rate_factor,
                                        sample_rate_multiplier)
    if sample_rate == 0:
        record_end_time = record_start_time
    else:
        # We take 1 from the sample count to calculate the number of time
        # intervals in the record.
        record_time_elapsed = (sample_count - 1) / sample_rate
        record_end_time = record_start_time + record_time_elapsed

    return RecordMetadata(station, location, channel, network,
                          record_start_time, record_end_time,
                          sample_count, sample_rate)
