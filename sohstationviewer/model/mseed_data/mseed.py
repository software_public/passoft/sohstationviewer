"""
MSeed object to hold and process MSeed data
"""
import os
import re
from pathlib import Path
from typing import Dict, List

from sohstationviewer.view.util.enums import LogType

from sohstationviewer.model.general_data.general_data import GeneralData
from sohstationviewer.model.general_data.general_data_helper import read_text

from sohstationviewer.model.mseed_data.mseed_helper import \
    retrieve_nets_from_data_dict, split_vst_channel
from sohstationviewer.model.mseed_data.record_reader_helper import \
    MSeedReadError
from sohstationviewer.model.mseed_data.mseed_reader import MSeedReader


class MSeed(GeneralData):
    """
    read and process mseed file into object with properties can be used to
    plot SOH data, mass position data, waveform data and gaps
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.nets_by_sta: Dict[str, List[str]] = {}
        self.invalid_blockettes = False
        self.not_mseed_files = []
        self.processing_data()

    def finalize_data(self):
        """
        This function should be called after all folders finish reading to
            + get nets_by_sta from data_dict
            + other tasks in super().finalize_data()

        """
        self.distribute_log_text_to_station()
        self.retrieve_nets_from_data_dicts()
        super().finalize_data()
        if self.data_type == 'Pegasus':
            self.split_vst_channel()

    def read_folders(self) -> None:
        """
        Read data from list_of_dir for soh, mass position and waveform.
        """
        super().read_folders(self.list_of_dir)
        if self.not_mseed_files:
            self.track_info(
                f"Not MSeed files: {self.not_mseed_files}", LogType.WARNING)
        if self.invalid_blockettes:
            # This check to only print out message once
            print("We currently only handle blockettes 500, 1000,"
                  " and 1001.")

    def read_file(self, path2file: Path, file_name: str,
                  count: int, total: int) -> None:
        """
        Read data or text from file
        :param path2file: absolute path to file
        :param file_name: name of file
        :param count: total number of file read
        :param total: total number of all valid files
        """
        if count % 10 == 0:
            self.track_info(
                f'Read {count}/{total} files', LogType.INFO)
        log_text = read_text(path2file)
        if log_text is not None:
            self.log_texts[path2file] = log_text
            return
        reader = MSeedReader(
            path2file,
            read_start=self.read_start,
            read_end=self.read_end,
            is_multiplex=self.is_multiplex,
            req_soh_chans=self.req_soh_chans,
            req_wf_chans=self.req_wf_chans,
            include_mp123zne=self.include_mp123zne,
            include_mp456uvw=self.include_mp456uvw,
            soh_data=self.soh_data,
            mass_pos_data=self.mass_pos_data,
            waveform_data=self.waveform_data,
            log_data=self.log_data,
            gap_minimum=self.gap_minimum)
        try:
            reader.read()
            self.invalid_blockettes = (self.invalid_blockettes
                                       or reader.invalid_blockettes)
        except MSeedReadError:
            self.not_mseed_files.append(file_name)

    def retrieve_nets_from_data_dicts(self):
        """
        Going through stations of each data_dict to get all network codes found
            in all channel of a station to add to nets_by_station.
        """
        retrieve_nets_from_data_dict(self.soh_data, self.nets_by_sta)
        retrieve_nets_from_data_dict(self.mass_pos_data, self.nets_by_sta)
        retrieve_nets_from_data_dict(self.waveform_data, self.nets_by_sta)

    def select_data_set_id(self) -> str:
        """
        Get data_set_id for the selected data set
        Condition if not on_unittest to bypass message box for user to choose
            one of multiple data sets.

        :return selected_sta_id: the selected station id from available
            data_set_ids.
            + If there is only one station id, return it.
            + If there is more than one, show all ids, let user choose one to
                return.
        """
        self.data_set_ids = sorted(list(set(
            list(self.soh_data.keys()) +
            list(self.mass_pos_data.keys()) +
            list(self.waveform_data.keys()) +
            [k for k in list(self.log_data.keys()) if k != 'TEXT']
        )))
        sta_ids = self.data_set_ids

        if len(sta_ids) == 0:
            return

        selected_sta_id = sta_ids[0]
        if not self.on_unittest and len(sta_ids) > 1:
            msg = ("There are more than one stations in the given data.\n"
                   "Please select one to display")
            self.pause_signal.emit(msg, sta_ids)
            self.pause()
            selected_sta_id = sta_ids[self.pause_response]

        self.track_info(f'Select Station {selected_sta_id}', LogType.INFO)
        return selected_sta_id

    def distribute_log_text_to_station(self):
        """
        Loop through paths to text files to look for station id in the path.
            + If there is station id in the path, add the content to the
                station id with channel 'TXT'.
            + if station id not in the path, add the content to the key 'TEXT'
                which means don't know the station for these texts.
        """
        for path2file in self.log_texts:
            try:
                file_parts = re.split(rf"{os.sep}|\.", path2file.as_posix())
                sta = [s for s in self.data_set_ids if s in file_parts][0]
            except IndexError:
                self.log_data['TEXT'].append(self.log_texts[path2file])
                continue
            if 'TXT' not in self.log_data[sta]:
                self.log_data[sta]['TXT'] = []
            self.log_data[sta]['TXT'].append(self.log_texts[path2file])

    def split_vst_channel(self):
        for data_set_id in self.data_set_ids:
            split_vst_channel(data_set_id, self.soh_data)
