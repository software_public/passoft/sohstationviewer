"""
This module provides access to a class that loads data in a separate thread.
"""
import traceback
from pathlib import Path
from typing import Union, List, Optional

from PySide6.QtCore import Qt
from PySide6 import QtCore, QtWidgets

from sohstationviewer.conf import constants
from sohstationviewer.controller.util import (
    display_tracking_info,
    DisplayTrackingInfoWrapper,
)
from sohstationviewer.model.general_data.general_data import (
    GeneralData, ThreadStopped, ProcessingDataError)
from sohstationviewer.model.reftek_data.reftek_reader.log_file_reader import \
    LogReadError
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.create_muti_buttons_dialog import (
    create_multi_buttons_dialog)


class DataLoaderWorker(QtCore.QObject):
    """
    The worker class that executes the code to load the data.
    """
    finished = QtCore.Signal(GeneralData)
    failed = QtCore.Signal()
    stopped = QtCore.Signal()
    notification = QtCore.Signal(QtWidgets.QTextBrowser, str, str)
    button_dialog = QtCore.Signal(str, list)
    button_chosen = QtCore.Signal(int)

    def __init__(self, data_type: str, tracking_box: QtWidgets.QTextBrowser,
                 is_multiplex: Optional[bool],
                 list_of_dir: List[Path], list_of_rt130_paths: List[Path],
                 req_wf_chans: Union[List[str], List[int]] = [],
                 req_soh_chans: List[str] = [],
                 read_start: float = 0,
                 read_end: float = constants.HIGHEST_INT,
                 gap_minimum: Optional[float] = None,
                 include_mp123: bool = False, include_mp456: bool = False,
                 rt130_waveform_data_req: bool = False,
                 rt130_log_files: List[Path] = [],
                 include_masspos_in_soh_messages: bool = False,
                 parent_thread=None):
        super().__init__()
        self.data_type = data_type
        self.tracking_box = tracking_box
        self.is_multiplex = is_multiplex
        self.list_of_dir = list_of_dir
        self.list_of_rt130_paths = list_of_rt130_paths
        self.req_wf_chans = req_wf_chans
        self.req_soh_chans = req_soh_chans
        self.gap_minimum = gap_minimum
        self.read_start = read_start
        self.read_end = read_end
        self.include_mp123 = include_mp123
        self.include_mp456 = include_mp456
        self.rt130_waveform_data_req = rt130_waveform_data_req
        self.rt130_log_files = rt130_log_files
        self.include_masspos_in_soh_messages = include_masspos_in_soh_messages
        self.parent_thread = parent_thread
        # display_tracking_info updates a QtWidget, which can only be done in
        # the read. Since self.run runs in a background thread, we need to use
        # signal-slot mechanism to ensure that display_tracking_info runs in
        # the main thread.
        self.tracking_info_display = DisplayTrackingInfoWrapper()
        self.notification.connect(
            self.tracking_info_display.display_tracking_info,
            type=Qt.ConnectionType.QueuedConnection
        )
        self.end_msg = None
        # to change display tracking info to error when failed
        self.process_failed: bool = False

    def run(self):
        self.process_failed = False
        folders = (self.list_of_rt130_paths
                   if self.list_of_dir == [''] else self.list_of_dir)
        folders_str = ', '.join([dir.name for dir in folders])
        try:
            if self.data_type == 'RT130':
                from sohstationviewer.model.reftek_data.reftek import RT130
                object_type = RT130
            else:
                from sohstationviewer.model.mseed_data.mseed import MSeed
                object_type = MSeed
            # Create data object without loading any data in order to connect
            # its unpause slot to the loader's unpause signal
            data_object = object_type.get_empty_instance()
            self.button_chosen.connect(data_object.receive_pause_response,
                                       type=Qt.ConnectionType.DirectConnection)
            data_object.__init__(
                self.data_type, self.tracking_box,
                self.is_multiplex, self.list_of_dir,
                self.list_of_rt130_paths, req_wf_chans=self.req_wf_chans,
                req_soh_chans=self.req_soh_chans, gap_minimum=self.gap_minimum,
                read_start=self.read_start, read_end=self.read_end,
                include_mp123zne=self.include_mp123,
                include_mp456uvw=self.include_mp456,
                rt130_waveform_data_req=self.rt130_waveform_data_req,
                rt130_log_files=self.rt130_log_files,
                include_masspos_in_soh_messages=(
                    self.include_masspos_in_soh_messages),
                creator_thread=self.parent_thread,
                notification_signal=self.notification,
                pause_signal=self.button_dialog
            )

        except ThreadStopped:
            self.end_msg = 'Data loading has been stopped'
            self.stopped.emit()
        except ProcessingDataError as e:
            self.end_msg = e.message
            self.failed.emit()
        except LogReadError as e:
            self.end_msg = e.message
            self.process_failed = True
            self.failed.emit()
        except Exception:
            fmt = traceback.format_exc()
            self.end_msg = (f"Some in {folders_str} can't be read "
                            f"due to error: {str(fmt)}")
            self.process_failed = True
            self.failed.emit()
        else:
            if data_object.selected_data_set_id is None:
                self.process_failed = True
                self.end_msg = ("No data was found. Please check the selected "
                                "time range and channel list.")
                self.failed.emit()
            else:
                self.end_msg = f'Finished loading data stored in {folders_str}'
                self.finished.emit(data_object)


class DataLoader(QtCore.QObject):
    finished = QtCore.Signal()
    """
    The class that coordinate the loading of data using multiple threads. The
    code inside has to be encapsulated in a class because a connection between
    a signal and a receiver is automatically disconnected when either of them
    is deleted (e.g. goes out of scope).
    """

    def __init__(self):
        super().__init__()
        self.running = False
        self.thread: Optional[QtCore.QThread] = None
        self.worker: Optional[DataLoaderWorker] = None

    def init_loader(self, data_type: str,
                    tracking_box: QtWidgets.QTextBrowser,
                    is_multiplex: bool,
                    list_of_dir: List[Union[str, Path]],
                    list_of_rt130_paths: List[Union[str, Path]],
                    req_wf_chans: Union[List[str], List[int]] = [],
                    req_soh_chans: List[str] = [],
                    gap_minimum: Optional[float] = None,
                    read_start: float = 0,
                    read_end: float = constants.HIGHEST_INT,
                    include_mp123: bool = False,
                    include_mp456: bool = False,
                    rt130_waveform_data_req: bool = False,
                    rt130_log_files: List[Path] = [],
                    include_masspos_in_soh_messages: bool = False):
        """
        Initialize the data loader. Construct the thread and worker and connect
        them together. Separated from the actual loading of the data to allow
        the main window a chance to connect its slots to the data loader.

        :param data_type: the type of data being loaded. 'RT130' for RT130 data
            and 'Centaur', 'Pegasus', and 'Q330' for MSeed data.
        :param tracking_box: the widget used to display tracking info
        :param list_of_dir: list of directories selected by users
        :param list_of_rt130_paths: list of rt130 directories selected by users
        :param req_wf_chans: list of requested waveform channels
        :param req_soh_chans: list of requested SOH channel
        :param read_start: the time before which no data is read
        :param read_end: the time after which no data is read
        :param include_mp123: if mass position channels 1,2,3 are requested
        :param include_mp456: if mass position channels 4,5,6 are requested
        :param rt130_log_files: list of paths to RT130 log files selected by
            the user
        :param include_masspos_in_soh_messages: whether to include
            mass-position data in RT130 SOH messages
        """
        if self.running:
            # TODO: implement showing an error window
            print('Already running')
            return False

        self.running = True
        self.thread = QtCore.QThread()
        self.worker = DataLoaderWorker(
            data_type,
            tracking_box,
            is_multiplex,
            list_of_dir,
            list_of_rt130_paths,
            req_wf_chans=req_wf_chans,
            req_soh_chans=req_soh_chans,
            gap_minimum=gap_minimum,
            read_start=read_start,
            read_end=read_end,
            include_mp123=include_mp123,
            include_mp456=include_mp456,
            rt130_waveform_data_req=rt130_waveform_data_req,
            rt130_log_files=rt130_log_files,
            include_masspos_in_soh_messages=include_masspos_in_soh_messages,
            parent_thread=self.thread
        )

        self.worker.moveToThread(self.thread)

    def connect_worker_signals(self):
        """
        Connect the signals of the data loader to the appropriate slots.
        All connections with a signal of the data loader made after this method
        is called will be ignored.
        """
        # Connection order from https://realpython.com/python-pyqt-qthread
        self.thread.started.connect(self.worker.run)

        self.worker.finished.connect(self.thread.quit)
        self.worker.failed.connect(self.thread.quit)
        self.worker.stopped.connect(self.thread.quit)

        self.thread.finished.connect(self.load_end)
        self.thread.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.finished)
        self.thread.finished.connect(self.thread.deleteLater)

        self.worker.button_dialog.connect(self.create_button_dialog)

    def load_data(self):
        """
        Start the data loading thread.
        """
        self.thread.start()

    @QtCore.Slot()
    def load_end(self):
        """
        Cleans up after data loading ended. Called even if the loading fails or
        is stopped.

        Currently does the following:
            - Set running state of self to False
        """
        log_type = LogType.INFO
        if self.worker.process_failed:
            log_type = LogType.ERROR
        display_tracking_info(self.worker.tracking_box,
                              self.worker.end_msg, log_type)
        self.running = False

    @QtCore.Slot()
    def create_button_dialog(self, msg: str, button_labels: List[str]):
        """
        Create a modal dialog with buttons. Show the dialog and send the user's
        choice to the data object being created.

        :param msg: the instruction shown to the user
        :type msg: str
        :param button_labels: the list of labels that are shown on the buttons
        :type button_labels: List[str]
        """
        chosen_idx = create_multi_buttons_dialog(
            msg, button_labels, has_abort=False)
        self.worker.button_chosen.emit(chosen_idx)
