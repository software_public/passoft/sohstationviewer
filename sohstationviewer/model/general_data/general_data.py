from __future__ import annotations
import os
from pathlib import Path
from typing import Optional, Union, List, Tuple, Dict
import traceback

from obspy import UTCDateTime

from PySide6 import QtCore
from PySide6 import QtWidgets

from sohstationviewer.controller.util import \
    display_tracking_info, get_valid_file_count, validate_file, validate_dir
from sohstationviewer.view.plotting.gps_plot.gps_point import GPSPoint
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.model.general_data.general_data_helper import \
    retrieve_data_time_from_data_dict, retrieve_gaps_from_data_dict, \
    combine_data, sort_data, squash_gaps, reset_data
from sohstationviewer.view.create_muti_buttons_dialog import (
    create_multi_buttons_dialog)


class ProcessingDataError(Exception):
    def __init__(self, msg):
        self.message = msg


class ThreadStopped(Exception):
    """
    An exception that is raised when the user requests for the data loader
    thread to be stopped.
    """
    def __init__(self, *args, **kwargs):
        self.args = (args, kwargs)


class GeneralData():
    def __init__(self, data_type,
                 tracking_box: Optional[QtWidgets.QTextBrowser] = None,
                 is_multiplex: bool = False, list_of_dir: List[str] = [],
                 list_of_rt130_paths: List[Path] = [],
                 req_wf_chans: Union[List[str], List[int]] = [],
                 req_soh_chans: List[str] = [],
                 gap_minimum: float = None,
                 read_start: Optional[float] = UTCDateTime(0).timestamp,
                 read_end: Optional[float] = UTCDateTime().timestamp,
                 include_mp123zne: bool = False,
                 include_mp456uvw: bool = False,
                 rt130_waveform_data_req: bool = False,
                 rt130_log_files: List[Path] = [],
                 include_masspos_in_soh_messages: bool = False,
                 creator_thread: Optional[QtCore.QThread] = None,
                 notification_signal: Optional[QtCore.Signal] = None,
                 pause_signal: Optional[QtCore.Signal] = None,
                 on_unittest: bool = False,
                 *args, **kwargs):
        """
        Super class for different data type to process data from data files

        :param data_type: type of the object
        :param tracking_box: widget to display tracking info
        :param is_multiplex: flag showing if a file have more than one channel
        :param list_of_dir: list of paths to the folders of data
        :param list_of_rt130_paths: path to the folders of RT130 data
        :param req_wf_chans: requested waveform channel list
        :param req_soh_chans: requested SOH channel list
        :param read_start: requested start time to read
        :param read_end: requested end time to read
        :param include_mp123zne: if mass position channels 1,2,3 are requested
        :param include_mp456uvw: if mass position channels 4,5,6 are requested
        :param rt130_waveform_data_req: flag for RT130 to read waveform data
        :param rt130_log_files: list of paths to log files chosen by the user
        :param include_masspos_in_soh_messages: whether to include mass
            position data in the displayed SOH messages of an RT130 data set
        :param creator_thread: the thread the current DataTypeModel instance is
            being created in. If None, the DataTypeModel instance is being
            created in the main thread
        :param notification_signal: signal used to send notifications to the
            main thread. Only not None when creator_thread is not None
        :param pause_signal: signal used to notify the main thread that the
            data loader is paused.
        """
        self.data_type = data_type
        self.tracking_box = tracking_box
        self.is_multiplex = is_multiplex
        self.list_of_dir = list_of_dir
        self.list_of_rt130_paths = list_of_rt130_paths
        self.req_soh_chans = req_soh_chans
        self.req_wf_chans = req_wf_chans
        self.gap_minimum = gap_minimum
        self.read_start = read_start
        self.read_end = read_end
        self.include_mp123zne = include_mp123zne
        self.include_mp456uvw = include_mp456uvw
        self.rt130_waveform_data_req = rt130_waveform_data_req
        self.rt130_log_files = rt130_log_files
        self.include_masspos_in_soh_messages = include_masspos_in_soh_messages
        self.on_unittest = on_unittest
        if creator_thread is None:
            err_msg = (
                'A signal is not None while running in main thread'
            )
            assert notification_signal is None, err_msg
            assert pause_signal is None, err_msg
            self.creator_thread = QtCore.QThread()
        else:
            self.creator_thread = creator_thread
        self.notification_signal = notification_signal
        self.pause_signal = pause_signal

        """
        processing_log: record the progress of processing
        """
        self.processing_log: List[Tuple[str, LogType]] = []
        """
        data_set_ids: list of all data_set_ids
        """
        self.data_set_ids = []

        DataSetID = Union[Tuple[str, str], str]

        """
        log_texts: dictionary of content of text files by filenames
        """
        self.log_texts: Dict[str, str] = {}
        # Look for description in data_structures.MD
        self.log_data = {'TEXT': []}  # noqa
        self.waveform_data = {}
        self.soh_data = {}
        self.mass_pos_data = {}
        """
        data_time: time range of data sets:
        """
        self.data_time: Dict[DataSetID, List[float]] = {}

        """
        The given data may include more than one data set which is station_id
        in mseed or (unit_id, exp_no) in reftek. User are allow to choose which
        data set to be displayed
        selected_data_set_id: id of the data set to be displayed
        """
        self.selected_data_set_id: Optional[str] = None
        """
        gaps: gaps info in dict:
        """
        self.gaps: Dict[DataSetID, List[List[float]]] = {}

        self._pauser = QtCore.QSemaphore()
        self.pause_response = None

        self.gps_points: List[GPSPoint] = []

    def select_data_set_id(self) -> Union[str, Tuple[str, str]]:
        """
        Get the data_set_id for the data set to process.
        :return: id of the selected data set
        """
        pass

    def processing_data(self):
        if self.creator_thread.isInterruptionRequested():
            raise ThreadStopped()
        self.read_folders()
        self.selected_data_set_id = self.select_data_set_id()

        self.fill_empty_data()
        if self.creator_thread.isInterruptionRequested():
            raise ThreadStopped()
        self.finalize_data()

    def read_folders(self, folders) -> None:
        """
        Read data from given folders to create data dicts which are
            attributes of current class
        """
        count = 0
        total = get_valid_file_count(folders)
        for folder in folders:
            count = self.read_folder(folder, total, count)

    def read_folder(self, folder: str, total: int, count: int) -> int:
        """
        Read data from current folder.

        :param folder: folder to read data from
        :param total: total of all valid files
        :param count: total of files that have been processed before this
            folder to keep track of progress
        :return count: total of files that have been processed after this
            folder to keep track of progress
        """
        if not os.path.isdir(folder):
            raise ProcessingDataError(f"Path '{folder}' not exist")

        for path, sub_dirs, files in os.walk(folder):
            try:
                validate_dir(path)
            except Exception as e:
                # skip Information folder
                self.track_info(str(e), LogType.WARNING)
                continue
            for file_name in files:
                if self.creator_thread.isInterruptionRequested():
                    raise ThreadStopped()

                path2file = Path(path).joinpath(file_name)
                if not validate_file(path2file, file_name):
                    continue
                count += 1
                try:
                    self.read_file(path2file, file_name, count, total)
                except Exception:
                    fmt = traceback.format_exc()
                    self.track_info(f"Skip file {path2file} can't be read "
                                    f"due to error: {str(fmt)}",
                                    LogType.WARNING)
        return count

    def read_file(self, path2file: Path, file_name: str,
                  count: int, total: int) -> None:
        """
        Read data or text from file
        :param path2file: absolute path to file
        :param file_name: name of file
        :param count: total number of file read
        :param total: total number of all valid files
        """
        pass

    def finalize_data(self):
        """
        This function should be called after all folders finish reading to
            + Sort all data traces in time order
            + Combine traces in data and split at gaps > gap_minimum
            + Apply convert_factor to avoid using flags to prevent double
                applying convert factor when plotting
            + Retrieve gaps from data_dicts
            + Retrieve data_time from data_dicts
            + Change data time with default value that are invalid for plotting
                to read_start, read_end.
        """
        if self.selected_data_set_id is None:
            return

        self.track_info("Finalizing...", LogType.INFO)

        self.sort_all_data()
        self.combine_all_data()

        self.retrieve_gaps_from_data_dicts()
        self.retrieve_data_time_from_data_dicts()
        if self.selected_data_set_id not in self.data_time.keys():
            self.data_time[self.selected_data_set_id] = \
                [self.read_start, self.read_end]

    def select_diff_data_set_id(self):
        """
        The pop-up allow user to choose one of the available data_set_id for
        different data set. This function is based on the assumption that
        finalizing_data() has been performed for all data sets after they are
        loaded.
        """
        msg = "Please select one of the following data set IDs:"
        ret = create_multi_buttons_dialog(
            msg, self.data_set_ids, has_abort=True)
        if ret == -1:
            return False
        self.selected_data_set_id = self.data_set_ids[ret]
        return True

    def track_info(self, text: str, type: LogType) -> None:
        """
        Display tracking info in tracking_box.
        Add all errors/warnings to processing_log.
        :param text: message to display
        :param type: type of message (error/warning/info)
        """
        # display_tracking_info updates a QtWidget, which can only be done in
        # the main thread. So, if we are running in a background thread
        # (i.e. self.creator_thread is not None), we need to use signal slot
        # mechanism to ensure that display_tracking_info is run in the main
        # thread.
        if self.notification_signal is None:
            display_tracking_info(self.tracking_box, text, type)
        else:
            self.notification_signal.emit(self.tracking_box, text, type)
        if type != LogType.INFO:
            self.processing_log.append((text, type))

    def pause(self) -> None:
        """
        Pause the thread this DataTypeModel instance is in. Works by trying
        to acquire a semaphore that is not available, which causes the thread
        to block.

        Note: due to how this is implemented, each call to pause will require
        a corresponding call to unpause. Thus, it is inadvisable to call this
        method more than once.

        Caution: not safe to call in the main thread. Unless a background
        thread releases the semaphore, the whole program will freeze.
        """
        self._pauser.acquire()

    @QtCore.Slot()
    def unpause(self):
        """
        Unpause the thread this DataTypeModel instance is in. Works by trying
        to acquire a semaphore that is not available, which causes the thread
        to block.

        Caution: due to how this is implemented, if unpause is called before
        pause, the thread will not be paused until another call to pause is
        made. Also, like pause, each call to unpause must be matched by another
        call to pause for everything to work.
        """
        self._pauser.release()

    @QtCore.Slot()
    def receive_pause_response(self, response: object):
        """
        Receive a response to a request made to another thread and unpause the
        calling thread.

        :param response: the response to the request made
        """
        self.pause_response = response
        self.unpause()

    @classmethod
    def get_empty_instance(cls) -> GeneralData:
        """
        Create an empty data object. Useful if a DataTypeModel instance is
        needed, but it is undesirable to load a data set. Basically wraps
        __new__().

        :return: an empty data object
        :rtype: DataTypeModel
        """
        return cls.__new__(cls)

    def sort_all_data(self):
        """
        Sort traces by startTmEpoch on all data: waveform_data, mass_pos_data,
            soh_data.
        Reftek's soh_data won't be sorted here. It has been sorted by time
            because it is created from log data which is sorted in
            prepare_soh_data_from_log_data()
        """
        for data_set_id in self.data_set_ids:
            sort_data(self.waveform_data[data_set_id])
            sort_data(self.mass_pos_data[data_set_id])
            try:
                sort_data(self.soh_data[data_set_id])
            except KeyError:
                # Reftek's SOH trace doesn't have startTmEpoch and
                # actually soh_data consists of only one trace
                pass

    def combine_all_data(self):
        for data_set_id in self.data_set_ids:
            combine_data(data_set_id, self.waveform_data, self.gap_minimum)
            combine_data(data_set_id, self.mass_pos_data, self.gap_minimum)
            if self.__class__.__name__ == "MSeed":
                combine_data(data_set_id, self.soh_data, self.gap_minimum)

    def retrieve_gaps_from_data_dicts(self):
        """
        Getting gaps from each data_dicts then squash all related gaps
        """
        for data_set_id in self.data_set_ids:
            self.gaps[data_set_id] = []
            if self.gap_minimum is None:
                continue
            retrieve_gaps_from_data_dict(data_set_id,
                                         self.soh_data, self.gaps)
            retrieve_gaps_from_data_dict(data_set_id,
                                         self.mass_pos_data, self.gaps)
            retrieve_gaps_from_data_dict(data_set_id,
                                         self.waveform_data, self.gaps)

            self.gaps[data_set_id] = squash_gaps(self.gaps[data_set_id])

    def retrieve_data_time_from_data_dicts(self):
        """
        Going through each data_dict to update the data_time to be
            [min of startTimeEpoch, max of endTimeEpoch] for each station.
        """
        for data_set_id in self.data_set_ids:
            retrieve_data_time_from_data_dict(
                data_set_id, self.soh_data, self.data_time)
            retrieve_data_time_from_data_dict(
                data_set_id, self.mass_pos_data, self.data_time)
            retrieve_data_time_from_data_dict(
                data_set_id, self.waveform_data, self.data_time)

    def fill_empty_data(self):
        """
        Filling an empty_dict into station with no data added in data_dicts
        """
        for data_set_id in self.data_set_ids:
            if data_set_id not in self.soh_data:
                self.soh_data[data_set_id] = {}
            if data_set_id not in self.waveform_data:
                self.waveform_data[data_set_id] = {}
            if data_set_id not in self.mass_pos_data:
                self.mass_pos_data[data_set_id] = {}
            if data_set_id not in self.log_data:
                self.log_data[data_set_id] = {}

    def reset_all_selected_data(self):
        """
        Remove all data_set_ids created in the plotting process.
        This function is to replace deepcopy which uses more memory.
        """
        reset_data(self.selected_data_set_id, self.soh_data)
        reset_data(self.selected_data_set_id, self.waveform_data)
        reset_data(self.selected_data_set_id, self.mass_pos_data)
