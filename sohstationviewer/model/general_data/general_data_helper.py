from typing import List, Dict, Optional, Union, Tuple
import numpy as np
import os
from pathlib import Path


def _check_related_gaps(min1: float, max1: float,
                        min2: float, max2: float,
                        index: int, checked_indexes: List[int]):
    """
    Check if the passing ranges overlapping each other and add indexes to
        checked_indexes.

    :param min1: start of range 1
    :param max1: end of range 1
    :param min2: start of range 2
    :param max2: end of range 2
    :param index: index of gap being checked
    :param checked_indexes: list of gaps that have been checked

    :return: True if the two ranges overlap each other, False otherwise
    """
    if ((min1 <= min2 <= max1) or (min1 <= max2 <= max1)
            or (min2 <= min1 <= max2) or (min2 <= max1 <= max2)):
        # range [min1, max1] and [min2, max2] have some part overlap each other
        checked_indexes.append(index)
        return True
    else:
        return False


def squash_gaps(gaps: List[List[float]]) -> List[List[float]]:
    """
    Compress gaps from different channels that have time range related to
    each other to the ones with outside boundary (min start, max end)
    or (min end, max start) in case of overlap.
    :param gaps: list of gaps of multiple channels:
        [[start, end],], [[start, end],]
    :return: squashed_gaps: all related gaps are squashed extending to the
        outside start and end [[min start, max end], [max start, min end]]

    """
    gaps = sorted(gaps, key=lambda x: x[0])
    squashed_gaps = []
    checked_indexes = []

    for idx, g in enumerate(gaps):
        if idx in checked_indexes:
            continue
        squashed_gaps.append(g)
        checked_indexes.append(idx)
        overlap = g[0] >= g[1]
        for idx_, g_ in enumerate(gaps):
            if idx_ in checked_indexes:
                continue
            if not overlap:
                if g_[0] >= g_[1]:
                    continue
                if _check_related_gaps(g[0], g[1], g_[0], g_[1],
                                       idx_, checked_indexes):
                    squashed_gaps[-1][0] = min(g[0], g_[0])
                    squashed_gaps[-1][1] = max(g[1], g_[1])
                else:
                    break
            else:
                if g_[0] < g_[1]:
                    continue
                if _check_related_gaps(g[1], g[0], g_[1], g_[0],
                                       idx_, checked_indexes):
                    squashed_gaps[-1][0] = max(g[0], g_[0])
                    squashed_gaps[-1][1] = min(g[1], g_[1])

    return squashed_gaps


def sort_data(sta_data_dict: Dict) -> None:
    """
    Sort data in 'traces_info' of each channel by 'startTmEpoch' order
    :param sta_data_dict: data of a station
    """
    for chan_id in sta_data_dict:
        traces_info = sta_data_dict[chan_id]['tracesInfo']
        sta_data_dict[chan_id]['tracesInfo'] = sorted(
            traces_info, key=lambda i: i['startTmEpoch'])


def retrieve_data_time_from_data_dict(
        selected_data_set_id: Union[str, Tuple[str, str]],
        data_dict: Dict, data_time: Dict[str, List[float]]) -> None:
    """
    Going through each channel in each station to get data_time for each
        station which is [min of startTimeEpoch, max of endTimeEpoch] among
        the station's channels.
    :param selected_data_set_id: the key of the selected data set
    :param data_dict: the given data_dict
    :param data_time: data by sta_id
    """
    selected_data_dict = data_dict[selected_data_set_id]
    for c in selected_data_dict:
        dtime = [selected_data_dict[c]['startTmEpoch'],
                 selected_data_dict[c]['endTmEpoch']]

        if selected_data_set_id in data_time.keys():
            data_time[selected_data_set_id][0] = min(
                data_time[selected_data_set_id][0], dtime[0])
            data_time[selected_data_set_id][1] = max(
                data_time[selected_data_set_id][1], dtime[1])
        else:
            data_time[selected_data_set_id] = dtime


def retrieve_gaps_from_data_dict(
        selected_data_set_id: Union[str, Tuple[str, str]],
        data_dict: Dict,
        gaps: Dict[str, List[List[float]]]) -> None:
    """
    Create each station's gaps by adding all gaps from all channels
    :param selected_data_set_id: the id of the selected data set
    :param data_dict: given stream
    :param gaps: gaps list by key
    """
    selected_data_dict = data_dict[selected_data_set_id]
    for c in selected_data_dict.keys():
        cgaps = selected_data_dict[c]['gaps']
        if cgaps != []:
            gaps[selected_data_set_id] += cgaps


def combine_data(selected_data_set_id: Union[str, Tuple[str, str]],
                 data_dict: Dict, gap_minimum: Optional[float]) -> None:
    """
    Traverse through traces in each channel, add to gap list if
        delta >= gap_minimum with delta is the distance between
        contiguous traces.
    Combine sorted data using concatenate, which also change data ot ndarray
        and update startTmEpoch and endTmEpoch.
    :param selected_data_set_id: the id of the selected data set
    :param data_dict: dict of data
    :param gap_minimum: minimum length of gaps to be detected
    """
    selected_data_dict = data_dict[selected_data_set_id]
    for chan_id in selected_data_dict:
        channel = selected_data_dict[chan_id]
        traces_info = channel['tracesInfo']
        if 'gaps' in channel:
            # gaps key is for mseed data only
            for idx in range(len(traces_info) - 1):
                curr_end_tm = traces_info[idx]['endTmEpoch']
                next_start_tm = traces_info[idx + 1]['startTmEpoch']
                delta = abs(curr_end_tm - next_start_tm)
                if gap_minimum is not None and delta >= gap_minimum:
                    # add gap
                    gap = [curr_end_tm, next_start_tm]
                    selected_data_dict[chan_id]['gaps'].append(gap)
        if len(traces_info) == 0:
            channel['tracesInfo'] = [{'data': np.array([]),
                                      'times': np.array([])}]
            continue
        channel['startTmEpoch'] = min([tr['startTmEpoch']
                                       for tr in traces_info])
        channel['endTmEpoch'] = max([tr['endTmEpoch'] for tr in traces_info])

        data_list = [tr['data'] for tr in traces_info]
        times_list = [tr['times'] for tr in traces_info]
        channel['tracesInfo'] = [{
            'startTmEpoch': channel['startTmEpoch'],
            'endTmEpoch': channel['endTmEpoch'],
            'data': np.concatenate(data_list),
            'times': np.concatenate(times_list)
        }]


def reset_data(selected_data_set_id: Union[str, Tuple[str, str]],
               data_dict: Dict):
    """
    Remove all keys created in the plotting process for the given data dict
    :param selected_data_set_id: the id of the selected data set
    :param data_dict: data of the selected key
    """
    try:
        selected_data_dict = data_dict[selected_data_set_id]
    except KeyError:
        return
    for chan_id in selected_data_dict:
        selected_data_dict[chan_id]['fullData'] = False
        del_keys = ['chan_db_info', 'times', 'data', 'ax', 'ax_wf']
        for k in del_keys:
            try:
                del selected_data_dict[chan_id][k]
            except KeyError:
                pass


def read_text(path2file: Path) -> Union[bool, str]:
    """
    Read text file and add to log_data under channel TEXT.
        + Raise exception if the file isn't a text file
        + Remove empty lines in content
    :param path2file: str - absolute path to text file
    :param file_name: str - name of text file
    :param text_logs: holder to keep log string, refer to
        DataTypeModel.__init__.log_data['TEXT']
    """
    try:
        with open(path2file, 'r') as file:
            content = file.read().strip()
    except UnicodeDecodeError:
        return

    if content != '':
        # skip empty lines
        no_empty_line_list = [
            line for line in content.splitlines() if line]
        no_empty_line_content = os.linesep.join(no_empty_line_list)

        log_text = "\n\n** STATE OF HEALTH: %s\n" % path2file.name
        log_text += no_empty_line_content
    else:
        log_text = ''
    return log_text
