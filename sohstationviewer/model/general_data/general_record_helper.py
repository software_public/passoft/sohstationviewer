import struct


class Unpacker:
    """
    A wrapper around struct.unpack() to unpack binary data without having to
    explicitly define the byte order in the format string. Also restrict the
    type of format to str and buffer to bytes.
    """
    def __init__(self, byte_order_char: str = '') -> None:
        self.byte_order_char = byte_order_char

    def unpack(self, format: str, buffer: bytes):
        """
        Unpack a string of bytes into a tuple of values based on the given
        format. Use the same format as struct.unpack(), just without the byte
        order character. This is also compatible with formats that contain
        the byte order character, but is slightly slower in those cases.
        :param format: the format used to unpack the byte string
        :param buffer: the byte string
        :return: a tuple containing the unpacked values.
        """
        # Use try-except instead of an if statement to make this runs faster.
        # This method is only called internally, so the only time the except
        # clause
        # This method is called a lot, so speeding it up brings some decent
        # speed up to the program.
        try:
            format = self.byte_order_char + format
            return struct.unpack(format, buffer)
        except struct.error as e:
            default_byte_order_chars = ('@', '=', '>', '<', '!')
            if format.startswith(default_byte_order_chars):
                format = self.byte_order_char + format[:1]
                return struct.unpack(format, buffer)
            else:
                raise e
