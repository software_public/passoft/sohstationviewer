from __future__ import annotations
from typing import Callable, Tuple, List, Union, Set, Dict, TYPE_CHECKING

from sohstationviewer.conf import constants
from sohstationviewer.controller.util import (
    get_time_6, get_time_4, get_val, rtn_pattern)
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.model.reftek_data.log_info_helper import (
    remove_question_marks)

if TYPE_CHECKING:
    from sohstationviewer.model.reftek_data.reftek import RT130


class LogInfo():

    def __init__(self, parent: RT130,
                 track_info: Callable[[str, LogType], None], log_text: str,
                 data_set_id: Tuple[str, LogType],
                 read_start: float, read_end: float,
                 req_data_streams: List[int],
                 req_soh_chans: List[str], is_log_file: bool = False):
        """
        Help to extract channel data from LogText which include SOH Message and
        Event message.
        :param parent: reftek object that calls LogInfo
        :param track_info: to track data processing
        :param log_text: SOH and Event messages in time order
        :param data_set_id: ID of the data set including unit_id and exp_no
        :param read_start: requested start time to read
        :param read_end: requested end time to read
        :param req_data_streams: requested data stream ID,
        :param req_soh_chans: requested data stream ID
        :param is_log_file: flag indicate if this is a log file
        """
        self.parent = parent
        self.track_info = track_info
        self.log_text = log_text
        self.data_set_id = data_set_id
        self.unit_id, self.exp_no = data_set_id
        self.req_data_streams = req_data_streams
        self.no_question_req_soh_chans = remove_question_marks(req_soh_chans)
        self.is_log_file = is_log_file
        self.read_start = read_start
        self.read_end = read_end
        """
        track_year to add year to time since year time not include year after
        header.
        yearChanged: if doy=1, track_year is added 1, y_added is marked
        this has happened to only added 1 once.
        """
        self.y_added = False
        self.track_year = 0
        if self.unit_id >= "9000":
            self.model = "RT130"
        else:
            self.model = "72A"
        self.max_epoch = 0
        self.min_epoch = constants.HIGHEST_INT
        self.chans: Dict = self.parent.soh_data[self.data_set_id]
        self.cpu_vers: Set[str] = set()
        self.gps_vers: Set[str] = set()
        self.extract_info()

    def read_evt(self, line: str) -> Union[Tuple[float, int], bool]:
        """
        Read EVT info from line for a specific datastream (DS)
        :param line: a line of evt message
        :return epoch: epoch time of message
        :return DS: index of data stream
            for epoch, using trigger time (TT) if available or
            first sample time (FST) if available, otherwise return 0, 0
        min_epoch and max_epoch are updated.
        """
        # Ex: DAS: 0108 EV: 2632 DS: 2 FST = 2001:253:15:13:59:768
        #       TT =2001:253:15:13:59:768 NS: 144005 SPS: 40 ETO: 0
        parts = line.split()
        data_stream = int(parts[5])
        if (self.req_data_streams == ['*'] or
                data_stream in self.req_data_streams):
            try:
                if parts[8].startswith("00:000"):
                    if parts[11].startswith("00:000"):
                        return -1, 0
                    epoch, _ = get_time_6(parts[11])
                else:
                    epoch, _ = get_time_6(parts[8])
            except AttributeError:
                self.parent.processing_log.append((line, LogType.ERROR))
                return False
            if epoch > 0:
                self.min_epoch = min(epoch, self.min_epoch)
                self.max_epoch = max(epoch, self.max_epoch)
            else:
                return 0, 0
        else:
            return 0, 0
        return epoch, data_stream

    def read_sh_header(self, line: str) -> Union[float, bool]:
        """
        :param line: a line of evt message
        :return epoch: time for state of health header
        min_epoch and max_epoch are updated.
        y_added is reset to false to allow adding 1 to track_year
        If different unit_id is detected, give warning and skip reading.
        """
        # Ex: State of Health  01:251:09:41:35:656   ST: 0108
        parts = line.split()
        try:
            epoch, self.track_year = get_time_6(parts[3])
        except AttributeError:
            self.parent.processing_log.append(line, LogType.ERROR)
            return False
        self.y_added = False         # reset yAdded
        unit_id = parts[5].strip()
        if unit_id != self.unit_id:
            msg = ("The State Of Health messages for DAS %s were being "
                   "read, but now there is information for DAS %s in "
                   "the same State Of Health messages.\n"
                   "Skip reading State Of Health." % (self.unit_id, unit_id))
            self.track_info(msg, LogType.ERROR)
            False
        return epoch

    def simple_read(self, line: str, header_epoch: float
                    ) -> Union[Tuple[List[str], float], bool]:
        """
        Read parts and epoch from an SOH line
        :param line: a line of evt message
        :param header_epoch: the epoch time gotten from the SOH header
        :return parts: parts of line with space delim
        :return epoch: time when info is recorded
        max_epoch is updated with the epoch time.
        """
        # Ex: 186:21:41:35 <content>
        parts = line.split()
        try:
            epoch = get_time_4(parts[0], header_epoch)
        except AttributeError:
            self.parent.processing_log.append(line, LogType.ERROR)
            return False
        return parts, epoch

    def read_int_clock_phase_err(self, line: str, header_epoch: float
                                 ) -> Union[bool, Tuple[float, float]]:
        """
        Read internal clock phase error
        :param line: str - a line of evt message
        :param header_epoch: the epoch time gotten from the SOH header
        :return epoch: float - time when info is recorded
        :return error: float - time of ICP error in microseconds
        """
        # Ex: 253:19:41:42 INTERNAL CLOCK PHASE ERROR OF 4823 USECONDS
        ret = self.simple_read(line, header_epoch)
        if not ret:
            return False
        parts, epoch = ret
        error = float(parts[-2])
        if parts[-1].startswith("SEC"):
            error *= 1000000.0
        return epoch, error

    def read_bat_tem_bkup(self, line: str, header_epoch: float
                          ) -> Union[bool, Tuple[float, float, float, float]]:
        """
        Read battery voltage, temperature, backup voltage
        :param line: str - a line of evt message
        :param header_epoch: the epoch time gotten from the SOH header
        :return epoch: float - time when info is recorded
        :return volts: float - battery voltage.
        :return temp: float - temperature of battery in Celsius
        :return bkup_v: float - backup voltage.
            Available for RT130. For 72A, 0.0 is assigned
        """
        # 72A's:
        # Ex: 186:14:33:58 BATTERY VOLTAGE = 13.6V, TEMPERATURE = 26C
        # RT130:
        # Ex: 186:14:33:58 BATTERY VOLTAGE = 13.6V, TEMPERATURE = 26C,
        #    BACKUP = 3.3V
        parts, epoch = self.simple_read(line, header_epoch)
        try:
            volts = get_val(parts[4])
            temp = get_val(parts[7])
        except AttributeError:
            self.parent.processing_log.append(line, LogType.ERROR)
            return False
        if self.model == "RT130":
            bkup_v = get_val(parts[10])
        else:
            bkup_v = 0.0
        return epoch, volts, temp, bkup_v

    def read_disk_usage(self, line: str, header_epoch: float
                        ) -> Union[bool, Tuple[float, float, float]]:
        """
        Read disk usage
        :param line: str - a line of evt message
        :param header_epoch: the epoch time gotten from the SOH header
        :return epoch: float - time when info is recorded
        :return disk: int -  disk number
        :return val: int - memory used in disk
            Available for RT130. For 72A, 0 is assigned
        """
        # RT130:
        # Ex: 186:14:33:58 DISK 1: USED: 89744 AVAIL:...
        # Ex: 186:14:33:58 DISK 2* USED: 89744 AVAIL:...
        parts, epoch = self.simple_read(line, header_epoch)
        try:
            disk = get_val(parts[2])
            val = get_val(parts[4])
        except AttributeError:
            self.parent.processing_log.append(line, LogType.ERROR)
            return False
        return epoch, disk, val

    def read_dsp_clock_diff(self, line: str, header_epoch: float
                            ) -> Union[bool, Tuple[float, float]]:
        """
        Read DPS clock difference
        :param line: str - a line of evt message
        :param header_epoch: the epoch time gotten from the SOH header
        :return epoch: float - time when info is recorded
        :return total: float - total difference time in milliseconds
        """
        # Ex: 245:07:41:45 DSP CLOCK DIFFERENCE: 0 SECS AND -989 MSECS
        parts, epoch = self.simple_read(line, header_epoch)
        try:
            secs = get_val(parts[4])
            msecs = get_val(parts[-2])
        except AttributeError:
            self.parent.processing_log.append(line, LogType.ERROR)
            return False
        total = abs(secs) * 1000.0 + abs(msecs)
        if secs < 0.0 or msecs < 0.0:
            total *= -1.0
        return epoch, total

    def read_defs(self, line: str) -> Union[bool, float]:
        """
        Read definitions' time. Currently, only read Station Channel Definition
        Based on user requested, may use ["STATION", "DATA", "CALIBRATION"]
        :param line: str - a line of evt message
        :return epoch: float - time of the definition
        """
        # Ex: Station Channel Definition   01:330:19:24:42:978    ST: 7095
        # Ex: Data Stream Definition   01:330:19:24:42:978    ST: 7095
        # Ex: Calibration Definition   01:330:19:24:42:978    ST: 7095
        # Ex: STATION CHANNEL DEFINITION  2020:066:19:00:56:000   ST: 92E9
        parts = line.split()
        # Lines from a .log file may be just the first time the parameters were
        # saved until the DAS is reset, so if this is a log file then use the
        # current SOH time for plotting the points, instead of what is in the
        # message line.
        if parts[0] in ["STATION", "DATA", "CALIBRATION", "OPERATING"]:
            if self.is_log_file is False:
                try:
                    epoch, _ = get_time_6(parts[-3])
                except AttributeError:
                    self.parent.processing_log.append(line, LogType.ERROR)
                    return False
            else:
                epoch = self.max_epoch
        else:
            return False
        # These will not be allowed to set TimeMin since the RT130's save
        # multiple  copies of the parameters in the log files (one per day),
        # but they all have the date/timestamp of the first time the parameters
        # were written (unless a new copy is written because the parameters
        # were changed, of course).
        return epoch

    def read_cpu_ver(self, line: str) -> str:
        """
        Read version of CPU software
        :param line: str - a line of evt message
        :return cpu_ver: str - version of CPU software
        """
        # Ex: 341:22:05:41 CPU SOFTWARE V03.00H  (72A and older 130 FW)
        # Ex: 341:22:05:41 REF TEK 130  (no version number at all)
        # Ex: 341:22:05:41 Ref Tek 130  2.8.8S (2007:163)
        # Ex: 341:22:05:41 CPU SOFTWARE V 3.0.0 (2009:152)
        parts = line.split()
        if parts[1].startswith("CPU"):
            cpu_ver = " ".join(parts[3:])
        elif parts[1].upper().startswith("REF"):
            # There may not be any version info in this line:
            # Ex: REF TEK 130...then nothing
            # but accessing non-existant InParts doesn't matter.
            # cpu_ver will just be "".
            cpu_ver = " ".join(parts[4:])
        return cpu_ver

    def read_gps_ver(self, line: str) -> str:
        """
        Read version of GPS firmware
        :param line: str - a line of evt message
        :return gps_ver: str - version of GPS firmware
        """
        parts = line.split()
        ver_parts = [p.strip() for p in parts]
        if "GPS FIRMWARE VERSION:" in line:
            # 130 Ex: 291:19:54:29 GPS FIRMWARE VERSION: GPS 16-HVS Ver. 3.20
            gps_ver = ' '.join(ver_parts[4:])
        else:
            # 72A: Ex: 159:15:41:05 GPS: V03.30    (17JAN2000)
            gps_ver = " ".join(ver_parts[2:])
        return gps_ver

    def read_soh_line(self, line: str, line_idx: int, header_epoch: float):
        """
        Read an SOH line and store the parsed data point.
        :param line: the line being read
        :param line_idx: the index of the line being read
        :param header_epoch: the time of the SOH header in epoch time
        """
        if "INTERNAL CLOCK PHASE ERROR" in line:
            ret = self.read_int_clock_phase_err(line, header_epoch)
            if ret:
                epoch, err = ret
                self.add_chan_info('Clk Phase Err', epoch, err, line_idx)

        elif "POSSIBLE DISCREPANCY" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Discrepancies', epoch, 1, line_idx)

        elif "BATTERY VOLTAGE" in line:
            ret = self.read_bat_tem_bkup(line, header_epoch)
            if ret:
                epoch, volts, temp, bkup_v = ret
                self.add_chan_info('Battery Volt', epoch, volts, line_idx)
                self.add_chan_info('DAS Temp', epoch, temp, line_idx)
                # bkup_v: x<3, 3<=x<3.3, >3.3
                self.add_chan_info('Backup Volt', epoch, bkup_v, line_idx)

        elif all(x in line for x in ['DISK', 'USED']):
            ret = self.read_disk_usage(line, header_epoch)
            if ret:
                epoch, disk, val = ret
                self.add_chan_info(
                    f'Disk Usage{int(disk)}', epoch, val, line_idx
                )

        elif "AUTO DUMP CALLED" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Dump Called/Comp', epoch, 1, line_idx)
        elif "AUTO DUMP COMPLETE" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Dump Called/Comp', epoch, 0, line_idx)

        elif "DSP CLOCK SET" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Jerks/DSP Sets', epoch, 1, line_idx)
        elif 'JERK' in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Jerks/DSP Sets', epoch, 0, line_idx)

        elif "DSP CLOCK DIFF" in line:
            ret = self.read_dsp_clock_diff(line, header_epoch)
            if ret:
                epoch, total = ret
                self.add_chan_info('DSP Clock Diff', epoch, total, line_idx)

        elif "ACQUISITION STARTED" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('ACQ On/Off', epoch, 1, line_idx)
        elif "ACQUISITION STOPPED" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('ACQ On/Off', epoch, 0, line_idx)

        elif "NETWORK LAYER IS UP" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Net Up/Down', epoch, 0, line_idx)
        elif "NETWORK LAYER IS DOWN" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Net Up/Down', epoch, 1, line_idx)

        elif "MASS RE-CENTER" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Mass Re-center', epoch, 0, line_idx)

        elif any(x in line for x in ["SYSTEM RESET", "FORCE RESET"]):
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Reset/Power-up', epoch, 0, line_idx)
        elif "SYSTEM POWERUP" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Reset/Power-up', epoch, 1, line_idx)

        # ================= GPS ==================================
        elif "EXTERNAL CLOCK IS UNLOCKED" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS Lk/Unlk', epoch, -1, line_idx)
        elif "EXTERNAL CLOCK IS LOCKED" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS Lk/Unlk', epoch, 1, line_idx)
        elif "EXTERNAL CLOCK CYCLE" in line:
            # GPS Clock Power
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS Lk/Unlk', epoch, 0, line_idx)

        elif any(x in line for x in ["EXTERNAL CLOCK POWER IS TURNED ON",
                                     "EXTERNAL CLOCK WAKEUP",
                                     "GPS: POWER IS TURNED ON",
                                     "GPS: POWER IS CONTINUOUS"]
                 ):
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS On/Off/Err', epoch, 1, line_idx)
        elif any(x in line for x in ["EXTERNAL CLOCK POWER IS TURNED OFF",
                                     "EXTERNAL CLOCK SLEEP",
                                     "GPS: POWER IS TURNED OFF",
                                     "NO EXTERNAL CLOCK"]
                 ):
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS On/Off/Err', epoch, 0, line_idx)
        elif "EXTERNAL CLOCK ERROR" in line:
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('GPS On/Off/Err', epoch, -1, line_idx)

        # ================= VERSIONS ==============================
        elif any(x in line for x in ["REF TEK", "CPU SOFTWARE"]):
            cpu_ver = self.read_cpu_ver(line)
            self.cpu_vers.add(cpu_ver)

        elif any(x in line for x in ["GPS FIRMWARE VERSION:", "GPS: V"]):
            gps_ver = self.read_gps_ver(line)
            self.gps_vers.add(gps_ver)

        # ================= ERROR/WARNING =========================
        elif any(x in line for x in ["ERROR:", "NO VALID DISK", "FAILED"]):
            # These lines with "ERROR:" are generated by programs like
            # ref2segy and do not have any time associated with them so
            # just use whatever time was last in Time.
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Error/Warning', epoch, 1, line_idx)

        elif "WARNING" in line:
            # Warings come in lines with the time from the RT130 and lines
            # created by programs without times.
            if rtn_pattern(line[:12]) == "000:00:00:00":
                epoch = self.simple_read(line, header_epoch)[1]
                if epoch:
                    self.add_chan_info('Error/Warning', epoch, 0, line_idx)
            else:
                # Just use whatever time was last in Time.
                # The 2 means that the dots will be a little different.
                self.add_chan_info('Error/Warning', header_epoch, -1, line_idx)

        elif (all(x in line for x in ["BAD", "MAKER"]) or
              any(x in line for x in ["ERTFS", "IDE BUSY"])):
            epoch = self.simple_read(line, header_epoch)[1]
            if epoch:
                self.add_chan_info('Error/Warning', epoch, 0, line_idx)

    def add_chan_info(self, chan_id: str, t: float, d: float, idx: int
                      ) -> None:
        """
        Add information to channel's tracesInfo
        {
            unitID: serial number of device
            expNo: experiment number
            times: list of epoch time
            data: list of channel's values
            logIdx: list of indexes of SOH message line corresponding to
                time and data
        }
        :param chan_id: ID of channel
        :param t: epoch time of data
        :param d: value of data
        :param idx: index of SOH message line
        """
        if self.no_question_req_soh_chans and not chan_id.startswith(
                tuple(self.no_question_req_soh_chans)):
            return

        if chan_id not in self.chans:
            self.chans[chan_id] = {
                'reftek': True,
                'samplerate': 1,
                'chanID': chan_id,
                'visible': True,
                'tracesInfo': [{
                    'unitID': self.unit_id,
                    'expNo': self.exp_no,
                    'times': [],
                    'data': [],
                    'logIdx': []
                }]
            }
        if t < self.read_start or t > self.read_end:
            # data outside reading time range
            return
        self.min_epoch = min(t, self.min_epoch)
        self.max_epoch = max(t, self.max_epoch)
        self.chans[chan_id]['tracesInfo'][0]['times'].append(t)
        self.chans[chan_id]['tracesInfo'][0]['data'].append(d)
        self.chans[chan_id]['tracesInfo'][0]['logIdx'].append(idx)

    def extract_info(self) -> None:
        """
        Extract data from each line of log string to add to
        SOH channels' tracesInfo using add_chan_info()
        """
        lines = [ln.strip() for ln in self.log_text.splitlines()]
        is_in_soh_packet = False
        current_soh_packet_epoch = 0

        for idx, line in enumerate(lines):

            if line == '':
                is_in_soh_packet = False
                continue
            line = line.upper()

            if 'FST' in line:
                ret = self.read_evt(line)
                if ret is not False:
                    epoch, data_stream = ret
                    if epoch > 0:
                        chan_name = 'Event DS%s' % data_stream
                        self.add_chan_info(chan_name, epoch, 1, idx)
                    elif epoch == 0:
                        self.parent.processing_log.append(
                            (line, LogType.WARNING))
                    else:
                        self.parent.processing_log.append(
                            (line, LogType.ERROR))

            elif "DEFINITION" in line:
                epoch = self.read_defs(line)
                if epoch:
                    self.add_chan_info('SOH/Data Def', epoch, 0, idx)

            elif line.startswith("STATE OF HEALTH"):
                is_in_soh_packet = True
                epoch = self.read_sh_header(line)
                if epoch:
                    current_soh_packet_epoch = epoch
                    self.add_chan_info('SOH/Data Def', epoch, 1, idx)
            elif is_in_soh_packet:
                self.read_soh_line(line, idx, current_soh_packet_epoch)
