def remove_question_marks(req_soh_chan):
    """
    Remove the question marks in the channel request list, req_soh_chan
    :param req_soh_chan: soh channel request list
    :return no_question_req_soh_chan: the channel request list with the
        question marks removed
    """
    no_question_req_soh_chan = []
    for idx, req in enumerate(req_soh_chan):
        if req.endswith('?'):
            no_question_req_soh_chan.append(req_soh_chan[idx][:-1])
        else:
            no_question_req_soh_chan.append(req_soh_chan[idx])
    return no_question_req_soh_chan
