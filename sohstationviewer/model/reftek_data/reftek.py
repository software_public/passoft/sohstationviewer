"""
RT130 object to hold and process RefTek data
"""
import datetime
from pathlib import Path
from typing import Union, List, Tuple, Dict
import traceback
import numpy as np
from obspy import UTCDateTime
from obspy.core import Stream

from sohstationviewer.conf import constants
from sohstationviewer.conf.constants import SOFTWARE_VERSION
from sohstationviewer.model.reftek_data.reftek_reader.core import \
    DecimatedReftek130
from sohstationviewer.model.reftek_data.reftek_reader.log_file_reader import (
    LogFileReader, process_mass_poss_line, LogReadError,
)
from sohstationviewer.view.util.enums import LogType

from sohstationviewer.model.general_data.general_data import (
    GeneralData, ThreadStopped, ProcessingDataError,
)
from sohstationviewer.model.general_data.general_data_helper import read_text

from sohstationviewer.model.reftek_data.reftek_helper import (
    check_reftek_header, read_reftek_stream, check_time_in_file,
    retrieve_gaps_from_stream_header, get_total_data_points
)
from sohstationviewer.model.reftek_data.reftek_reader import core, soh_packet
from sohstationviewer.model.reftek_data.log_info import LogInfo


def format_timestamp_as_mass_pos_time(timestamp: float) -> str:
    """
    Format a UTC timestamp in the format of a mass-position time in SOH
    messages.
    :param timestamp: the UTC timestamp to format
    :return: the formatted time string
    """
    # We trim the last three character because the %f format character has a
    # millisecond precision (6 characters), while the time format we use only
    # has a microsecond precision (3 characters).
    return UTCDateTime(timestamp).strftime('%Y:%j:%H:%M:%S.%f')[:-3]


format_timestamp_as_mass_pos_time = np.vectorize(
    format_timestamp_as_mass_pos_time, otypes=[str]
)


class RT130(GeneralData):
    """
    read and process reftek file into object with properties can be used to
    plot SOH data, mass position data, waveform data and gaps
    """

    def __init__(self, *args, **kwarg):
        self.EH = {}
        super().__init__(*args, **kwarg)
        """
        req_data_streams: list of data streams requested by user is kept
            in self.req_wf_chans to be compliant with mseed when passing to
            object. But assign to this name to be closer to actual meaning.
        """
        self.req_data_streams: List[Union[int, str]] = self.req_wf_chans
        """
        rt130_waveform_data_req: flag to create waveform data according to
            req_data_stream
        """
        self.rt130_waveform_data_req: bool = kwarg['rt130_waveform_data_req']
        """
        stream_header_by_data_set_id_chan: stream header by data_set_id and
            chan_id to get data_set_id list, gaps by data_set_id,
            nets by data_set_id, channels by data_set_id
        """
        self.stream_header_by_data_set_id_chan: Dict[
            str, Dict[str, Stream]] = {}
        """
        gaps_by_data_set_id_chan: gap list for each data_set_id/chan_id to
            separate data at gaps, overlaps
        """
        DataSetID = Union[str, Tuple[str, str]]
        GapsByChannel = Dict[str, List[List[int]]]
        self.gaps_by_data_set_id_chan: Dict[DataSetID, GapsByChannel] = {}
        """
        found_data_streams: list of data streams found to help inform user
            why the selected data streams don't show up
        """
        self.found_data_streams: List[int] = []

        self.processing_data()

    def processing_data(self):
        if self.creator_thread.isInterruptionRequested():
            raise ThreadStopped()
        # We separate the reading of log files and real data sets because their
        # formats are very different.
        if self.rt130_log_files:
            self.read_log_files()
        else:
            self.read_folders()

        self.data_set_ids = sorted(list(set(
            list(self.soh_data.keys()) +
            list(self.mass_pos_data.keys()) +
            list(self.waveform_data.keys()))))

        if self.creator_thread.isInterruptionRequested():
            raise ThreadStopped()
        self.finalize_data()

        self.selected_data_set_id = self.select_data_set_id()

    def finalize_data(self):
        """
        This function should be called after all folders finish reading to
            + create soh_data from log_data
            + check not found data stream to give user a warning if needed
            + other tasks in super().finalize_data()
        """
        self.track_info("Finalizing...", LogType.INFO)
        self.track_info(
            "Prepare SOH data from log data", LogType.INFO)
        self.prepare_soh_data_from_log_data()

        if self.req_data_streams != ['*']:
            not_found_data_streams = [ds for ds in self.req_data_streams
                                      if ds not in self.found_data_streams]
            if not_found_data_streams != []:
                msg = (f"No data found for data streams: "
                       f"{', '.join(map(str, not_found_data_streams))}")
                self.processing_log.append((msg, LogType.WARNING))

        self.sort_all_data()
        self.combine_all_data()

        retrieve_gaps_from_stream_header(
            self.stream_header_by_data_set_id_chan,
            self.gaps_by_data_set_id_chan,
            self.gaps, self.gap_minimum, self.read_start, self.read_end)

        for data_set_id in self.data_time:
            if self.data_time[data_set_id] == [constants.HIGHEST_INT, 0]:
                # this happens when there is text or ascii only in the data
                self.data_time[data_set_id] = [self.read_start, self.read_end]

        for data_set_id in self.log_data:
            if data_set_id == 'TEXT':
                continue
            soh_header = ''
            soh_footer = ''
            soh_messages = self.log_data[data_set_id]['SOH']
            # We want to include a log file header only when there is not
            # already one.
            possible_programs = ['logpeek', 'SOHStationViewer', 'rt2ms']
            message_has_header = any(soh_messages[0].startswith(program)
                                     for program
                                     in possible_programs)
            if not message_has_header:
                current_time = datetime.datetime.now().ctime()
                soh_header = (f'SOHStationViewer: v{SOFTWARE_VERSION} '
                              f'Run time (UTC): {current_time}\n')
            if self.include_masspos_in_soh_messages:
                soh_footer += '\n\nMass-positions:\n'
                mass_pos_lines = []
                soh_messages = self.log_data[data_set_id]['SOH']
                for chan in self.mass_pos_data[data_set_id]:
                    chan_data = self.mass_pos_data[data_set_id][chan]
                    # We combined all data into one trace above
                    trace = chan_data['tracesInfo'][0]
                    times = format_timestamp_as_mass_pos_time(trace['times'])
                    data = trace['data'].astype(times.dtype)
                    formatted_lines = [
                        f'LPMP {time} {chan[-1]} {mass_position}'
                        for (time, mass_position)
                        in zip(times, data)
                    ]
                    mass_pos_lines.extend(formatted_lines)
                soh_footer += '\n'.join(mass_pos_lines)
            soh_messages[0] = soh_header + soh_messages[0] + soh_footer

    def read_log_files(self):
        """
        Read data from self.rt130_log_files and store it in self.log_data
        """
        for log_file in self.rt130_log_files:
            reader = LogFileReader(log_file)
            try:
                reader.read()
            except UnicodeDecodeError:
                msg = (f'The log file {log_file.name} cannot be read '
                       f'because it is not a text file.')
                self.track_info(msg, LogType.ERROR)
                if len(self.rt130_log_files) == 1:
                    # If there is only one log file chosen, we want to notify
                    # the main window of the reason of the failure. This is
                    # most conveniently done through a raised exception.
                    raise LogReadError(msg)
                continue
            file_key = (reader.station_code, reader.experiment_number)
            self.populate_cur_data_set_id_for_all_data(file_key)
            # We are creating the value for both keys 'SOH' and 'EHET' in this
            # method (unlike how RT130 is usually read), so we only need to do
            # one check.
            if 'EHET' not in self.log_data[file_key]:
                self.log_data[file_key]['EHET'] = [(1, reader.eh_et_lines)]
                self.log_data[file_key]['SOH'] = [(1, reader.soh_lines)]
            else:
                # Just in case we are reading multiple files with the same key.

                # We are going to assume that the log files are read in order.
                # That makes dealing with multiple files a lot easier. We can
                # always sort the processed SOH data if this assumption is
                # wrong.
                key_file_count = self.log_data[file_key]['EHET'][-1][0] + 1
                self.log_data[file_key]['EHET'].append(
                    (key_file_count, reader.eh_et_lines)
                )
                self.log_data[file_key]['SOH'].append(
                    (key_file_count, reader.soh_lines)
                )
            self.process_mass_pos_log_lines(file_key, reader.masspos_lines)

    def process_mass_pos_log_lines(self, data_set_id: Tuple[str, str],
                                   masspos_lines: List[str]):
        """
        Process mass-position log lines and store the result in
        self.masspos_data.

        :param data_set_id: the current data set id
        :param masspos_lines: the mass-position lines to process
        """
        # Mass-position channels is suffixed by a number from 1 to 6.
        processed_masspos_data = process_mass_poss_line(masspos_lines)
        for i, (times, data) in enumerate(processed_masspos_data, start=1):
            if len(data) == 0:
                continue
            masspos_chan = f'MassPos{i}'
            trace = {'startTmEpoch': times[0], 'endTmEpoch': times[-1],
                     'data': data, 'times': times}
            if masspos_chan not in self.mass_pos_data[data_set_id]:
                if masspos_chan[-1] in ['1', '2', '3']:
                    is_visible = self.include_mp123zne
                else:
                    is_visible = self.include_mp456uvw
                self.mass_pos_data[data_set_id][masspos_chan] = (
                    {'tracesInfo': [],
                     'visible': is_visible}
                )
                self.mass_pos_data[data_set_id][masspos_chan]['samplerate'] = 0
            trace['startTmEpoch'] = times[0]
            trace['endTmEpoch'] = times[-1]
            traces = self.mass_pos_data[data_set_id][masspos_chan][
                'tracesInfo']
            traces.append(trace)

    def read_folders(self) -> None:
        """
        Read data from list_of_dir or list_of_rt130_paths for soh,
            mass position and waveform.
        """
        if self.list_of_rt130_paths != []:
            folders = self.list_of_rt130_paths
        else:
            folders = self.list_of_dir
        super().read_folders(folders)

    def read_file(self, path2file: Path, file_name: str,
                  count: int, total: int) -> None:
        """
        Read data or text from file.
        Skip reading data if data belong to data stream that isn't requested.
        Data stream can be detected based on folder structure:
          [YYYYDOY]/[das_serial_number]/[data_stream]/[filename]
        Use check_time_in_file to compare the file's time with read_start and
        read end to decide including this file in processing or not.

        :param path2file: absolute path to file
        :param file_name: name of file
        :param count: total number of file read
        :param total: total number of all valid files
        """
        if count % 50 == 0:
            self.track_info(
                f"Read {count}/{total} files", LogType.INFO)
        log_text = read_text(path2file)
        if log_text is not None:
            self.log_texts['TEXT'].append(log_text)
            return
        if self.req_data_streams != ['*']:
            # filter non selected data stream
            ds = path2file.parts[-2]
            if ds in ['1', '2', '3', '4', '5', '6', '7', '8']:
                # Check if folder name match format of data stream's folder
                if int(ds) not in self.req_data_streams:
                    # Check if data stream is required
                    return
        if not check_time_in_file(path2file, self.read_start, self.read_end):
            return
        self.read_reftek_130(path2file)

    def select_data_set_id(self) -> Tuple[str, str]:
        """
        :return selected_data_set_id:
            (device's serial number, experiment_number)
            the selected data_set_id from available data_set_ids of
            all data dicts.
        + If there is only one data_set_id, return it.
        + If there is more than one, show all data_set_ids, let user choose one
         to return.
        """
        data_set_ids = self.data_set_ids
        total_data_points = get_total_data_points(self.soh_data)
        total_data_points += get_total_data_points(self.waveform_data)
        total_data_points += get_total_data_points(self.mass_pos_data)

        if total_data_points == 0:
            msg = 'No required data found for the data set.'
            raise ProcessingDataError(msg)

        selected_data_set_id = data_set_ids[0]

        self.track_info(f'Select data set ID {selected_data_set_id}',
                        LogType.INFO)
        return selected_data_set_id

    def read_reftek_130(self, path2file: Path) -> bool:
        """
        From the given file:
            + Read SOH data from file with SH packets,
            + read event info, mass position and index waveform (data stream)
                file from file with EH or ET packets

        :param path2file: absolute path to file
        """
        try:
            rt130 = DecimatedReftek130.from_file(path2file)
        except Exception:
            fmt = traceback.format_exc()
            self.track_info(f"Skip file {path2file} can't be read "
                            f"due to error: {str(fmt)}", LogType.WARNING)
            return
        unique, counts = np.unique(rt130._data["packet_type"],
                                   return_counts=True)
        nbr_packet_type = dict(zip(unique, counts))

        if b"SH" in nbr_packet_type:
            self.read_sh_packet(rt130)
        if b"EH" in nbr_packet_type or b"ET" in nbr_packet_type:
            self.read_eh_or_et_packet(rt130)
        return True

    def read_sh_packet(self, rt130: DecimatedReftek130) -> None:
        """
        Use soh_packet library to read file with SH packet for soh data
        to append tuple (time, log string) to
        log_data[self.cur_data_set_id][SOH]

        :param rt130: RT130 object of an SOH file.
        """
        data = rt130._data
        for ind, d in enumerate(data):
            cur_data_set_id = (d['unit_id'].decode(),
                               f"{d['experiment_number']}")
            self.populate_cur_data_set_id_for_all_data(cur_data_set_id)
            current_packet = soh_packet.SOHPacket.from_data(d)
            if getattr(current_packet, 'from_old_firmware', False):
                old_firmware_message = (
                    'It looks like this data set comes from an RT130 with '
                    'firmware version earlier than 2.9.0.',
                    LogType.INFO
                )
                if old_firmware_message not in self.processing_log:
                    self.processing_log.append(old_firmware_message)
            logs = soh_packet.SOHPacket.from_data(d).__str__()
            if 'SOH' not in self.log_data[cur_data_set_id]:
                self.log_data[cur_data_set_id]['SOH'] = []
            self.log_data[cur_data_set_id]['SOH'].append((d['time'], logs))

    def read_eh_or_et_packet(self, rt130: DecimatedReftek130) -> None:
        """
        Files that contents EH or ET packets are data stream (DS and
            mass position (DS 9) files.
        Name of file is name of packets' DS + 1.
        To be consistent with filename, check DS by the real value + 1
        The DS to be selected from GUI is also this value.
        There can be at most 1 - 9 DSs.

        DSs that aren't selected will be excluded first.
        Then data will be retrieved for log_data, mass_pos_data and
            waveform data.

        :param rt130: RT130 object of a data stream file.
        """
        if len(rt130._data) == 0:
            return
        data_stream = rt130._data['data_stream_number'][0] + 1
        if (self.req_data_streams != ['*'] and
                data_stream not in self.req_data_streams + [9]):
            return
        self.found_data_streams.append(data_stream)
        # suppose all data in a reftek file have the same data_set_id
        cur_data_set_id = (rt130._data[0]['unit_id'].decode(),
                           f"{rt130._data[0]['experiment_number']}")
        self.populate_cur_data_set_id_for_all_data(cur_data_set_id)
        if data_stream != 9:
            self.get_ehet_in_log_data(rt130, cur_data_set_id)
        self.get_mass_pos_data_and_waveform_data(
            rt130, data_stream, cur_data_set_id)

    def get_ehet_in_log_data(self, rt130: DecimatedReftek130,
                             cur_data_set_id: Tuple[str, str]) -> None:
        """
        Read event header info to add to log_data['EHET']

        :param rt130: RT130 object
        :param cur_data_set_id: current data_set_id of data set:
        """
        ind_ehet = [ind for ind, val in
                    enumerate(rt130._data["packet_type"])
                    if val in [b"EH", b"ET"]]
        nbr_dt_samples = sum(
            [rt130._data[ind]["number_of_samples"]
             for ind in range(0, len(rt130._data))
             if ind not in ind_ehet])

        log_data = []
        for index in ind_ehet:
            d = rt130._data[index]
            logs = core.EHPacket(d).eh_et_info(nbr_dt_samples)
            if 'EHET' not in self.log_data[cur_data_set_id]:
                self.log_data[cur_data_set_id]['EHET'] = []
            item = (d['time'], logs)
            if item not in log_data:
                # Prevent duplicated item in a file caused by EH and ET having
                # the same info
                log_data.append(item)
        self.log_data[cur_data_set_id]['EHET'] += log_data

    def get_mass_pos_data_and_waveform_data(
            self, rt130: DecimatedReftek130, data_stream: int,
            cur_data_set_id: Tuple[str, str]) -> None:
        """
        Get mass_pos_data for the current data_set_id in DS 9.
        Get waveform_data for the current data_set_id in DS 1-8
        Read header to find the available trace indexes to decide to read
            deeply for real data.

        :param rt130: RT130 object
        :param data_stream: index of data stream (1-8)
        :param cur_data_set_id: current data_set_id of data set
        """
        if data_stream == 9:
            cur_data_dict = self.mass_pos_data[cur_data_set_id]
        elif self.rt130_waveform_data_req:
            cur_data_dict = self.waveform_data[cur_data_set_id]
        else:
            return

        avail_trace_indexes = check_reftek_header(
            rt130, cur_data_set_id, self.read_start, self.read_end,
            self.stream_header_by_data_set_id_chan,
            cur_data_dict, self.data_time[cur_data_set_id],
            self.include_mp123zne, self.include_mp456uvw)
        if avail_trace_indexes == []:
            return
        read_reftek_stream(
            rt130, avail_trace_indexes, cur_data_dict,
            self.include_mp123zne, self.include_mp456uvw)

    def prepare_soh_data_from_log_data(self) -> None:
        """
        Process SOH and event log_data to create SOHdata
        """
        for k in self.log_data:
            if self.creator_thread.isInterruptionRequested():
                raise ThreadStopped()
            if k == 'TEXT':
                continue
            if k not in self.data_time:
                self.data_time[k] = [constants.HIGHEST_INT, 0]
            self.soh_data[k] = {}
            logs = []
            for pkt_type in ['SOH', 'EHET']:
                if pkt_type == 'EHET':
                    # according to the above looping list, EHET will be
                    # at the bottom of log_str
                    logs += '\n\nEvents:'
                try:
                    # sort log data according to time (x[0])
                    self.log_data[k][pkt_type] = sorted(
                        self.log_data[k][pkt_type], key=lambda x: x[0])
                    for log in self.log_data[k][pkt_type]:
                        logs += log[1]
                        if pkt_type == 'SOH':
                            logs += '\n'
                except KeyError:
                    pass
            log_str = ''.join(logs)
            self.log_data[k] = {'SOH': [log_str]}
            log_obj = LogInfo(
                self, self.track_info, log_str, k,
                self.read_start, self.read_end,
                self.req_data_streams, self.req_soh_chans)
            self.data_time[k][0] = min(log_obj.min_epoch, self.data_time[k][0])
            self.data_time[k][1] = max(log_obj.max_epoch, self.data_time[k][1])
            for c_name in self.soh_data[k]:
                c = self.soh_data[k][c_name]['tracesInfo'][0]
                tr = {
                    'times': np.array(c['times']),
                    'data': np.array(c['data']),
                    'logIdx': np.array(c['logIdx']),
                    'startTmEpoch': self.data_time[k][0],
                    'endTmEpoch': self.data_time[k][1]
                }
                self.soh_data[k][c_name]['tracesInfo'] = [tr]

    def populate_cur_data_set_id_for_all_data(
            self, cur_data_set_id: Tuple[str, str]) -> None:
        """
        Set up new data set's id for all data

        :param cur_data_set_id: current processing data_set_id:
            DAS SN, experiment number
        """
        if cur_data_set_id not in self.log_data:
            self.log_data[cur_data_set_id] = {}
            self.mass_pos_data[cur_data_set_id] = {}
            self.waveform_data[cur_data_set_id] = {}
            self.gaps[cur_data_set_id] = []
            self.data_time[cur_data_set_id] = [constants.HIGHEST_INT, 0]
            self.stream_header_by_data_set_id_chan[cur_data_set_id] = {}
