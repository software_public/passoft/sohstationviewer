from __future__ import annotations

from typing import List

"""
Routines building upon obspy.io.reftek.packet.
Redefine packet header (PACKET) based on rt130 manual.
Handles all state of health (SOH) packets:
- SH: State-Of-Health Packet
- SC: Station/Channel Parameter Packet
- OM: Operating Mode Parameter Packet
- DS: Data Stream Parameter Packet
- AD: Auxiliary Data Parameter Packet
- CD: Calibration Parameter Packet
- FD: Filter Description Packet

Maeva Pourpoint IRIS/PASSCAL
"""

import obspy.io.reftek.packet as obspy_rt130_packet
import numpy as np
import warnings

from obspy import UTCDateTime
from obspy.core.compatibility import from_buffer
from obspy.io.reftek.util import (bcd, bcd_hex, _decode_ascii,
                                  bcd_julian_day_string_to_nanoseconds_of_year,
                                  bcd_16bit_int, _parse_long_time,
                                  _get_nanoseconds_for_start_of_year)


class Reftek130UnpackPacketError(ValueError):
    pass


AD_DS_RECORDING_DEST = ["RAM", "Disk", "Ethernet", "Serial"]
CD_MAX_NBR_STRUCT = 4
DS_MAX_NBR_ST = 4
SC_MAX_NBR_CHA = 5

# Packet header for rt130 state of health packets.
# Tuples are:
#  - field name
#  - dtype during initial reading
#  - conversion routine (if any)
#  - dtype after conversion
PACKET = [
    ("packet_type", "|S2", None, "S2"),
    ("experiment_number", np.uint8, bcd, np.uint8),
    ("year", np.uint8, bcd, np.uint8),
    ("unit_id", (np.uint8, 2), bcd_hex, "S4"),
    ("time", (np.uint8, 6), bcd_julian_day_string_to_nanoseconds_of_year, np.int64),  # noqa: E501
    ("byte_count", (np.uint8, 2), bcd_16bit_int, np.uint16),
    ("packet_sequence", (np.uint8, 2), bcd_16bit_int, np.uint16),
    ("payload", (np.uint8, 1008), None, (np.uint8, 1008))]

PACKET_INITIAL_UNPACK_DTYPE = np.dtype([(name, dtype_initial)
                                        for name, dtype_initial, converter,
                                        dtype_final in PACKET])
PACKET_FINAL_DTYPE = np.dtype([(name, dtype_final) for name, dtype_initial,
                               converter, dtype_final in PACKET])

# name, length (bytes) and converter routine for packet payload
SH_PAYLOAD = {
    "reserved": (8, _decode_ascii),
    "information": (1000, _decode_ascii)}

SC_PAYLOAD = {
    "experiment_number_sc": (2, _decode_ascii),
    "experiment_name": (24, _decode_ascii),
    "experiment_comment": (40, _decode_ascii),
    "station_number": (4, _decode_ascii),
    "station_name": (24, _decode_ascii),
    "station_comment": (40, _decode_ascii),
    "das_model": (12, _decode_ascii),
    "das_serial": (12, _decode_ascii),
    "experiment_start": (14, _decode_ascii),
    "time_clock_type": (4, _decode_ascii),
    "time_clock_sn": (10, _decode_ascii),
    "sc_info": (730, None),
    "reserved": (76, _decode_ascii),
    "implement_time": (16, _parse_long_time)}

SC_INFO = {
    "_number": (2, _decode_ascii),
    "_name": (10, _decode_ascii),
    "_azimuth": (10, _decode_ascii),
    "_inclination": (10, _decode_ascii),
    "_x_coordinate": (10, _decode_ascii),
    "_y_coordinate": (10, _decode_ascii),
    "_z_coordinate": (10, _decode_ascii),
    "_xy_unit": (4, _decode_ascii),
    "_z_unit": (4, _decode_ascii),
    "_preamp_gain": (4, _decode_ascii),
    "_sensor_model": (12, _decode_ascii),
    "_sensor_serial": (12, _decode_ascii),
    "_comments": (40, _decode_ascii),
    "_adjusted_nominal_bit_weight": (8, _decode_ascii)}

OM_PAYLOAD = {
    "_72A_power_state": (2, _decode_ascii),
    "recording_mode": (2, _decode_ascii),
    "disk_reserved_1": (4, _decode_ascii),
    "auto_dump_on_ET": (1, _decode_ascii),
    "disk_reserved_2": (1, _decode_ascii),
    "auto_dump_threshold": (2, _decode_ascii),
    "_72A_power_down_delay": (4, _decode_ascii),
    "disk_wrap": (1, _decode_ascii),
    "disk_reserved_3": (1, _decode_ascii),
    "_72A_disk_power": (1, _decode_ascii),
    "_72A_terminator_power": (1, _decode_ascii),
    "disk_retry": (1, _decode_ascii),
    "disk_reserved_4": (11, _decode_ascii),
    "_72A_wakeup_reserved_1": (2, _decode_ascii),
    "_72A_wakeup_start_time": (12, _decode_ascii),
    "_72A_wakeup_duration": (6, _decode_ascii),
    "_72A_wakeup_repeat_interval": (6, _decode_ascii),
    "_72A_wakeup_number_intervals": (2, _decode_ascii),
    "_72A_wakeup_reserved_2": (484, _decode_ascii),
    "reserved": (448, _decode_ascii),
    "implement_time": (16, _parse_long_time)}

DS_PAYLOAD = {
    "ds_info": (920, None),
    "reserved": (72, _decode_ascii),
    "implement_time": (16, _parse_long_time)}

DS_INFO = {
    "_number": (2, _decode_ascii),
    "_name": (16, _decode_ascii),
    "_recording_destination": (4, _decode_ascii),
    "_reserved_1": (4, _decode_ascii),
    "_channels_included": (16, _decode_ascii),
    "_sample_rate": (4, _decode_ascii),
    "_data_format": (2, _decode_ascii),
    "_reserved_2": (16, _decode_ascii),
    "_trigger_type": (4, _decode_ascii),
    "_trigger_description": (162, None)}

DS_TRIGGER = {
    "CON": {
        "RecordLength": (8, _decode_ascii),
        "StartTime": (14, _decode_ascii),
        "Reserved": (140, _decode_ascii)},
    "CRS": {
        "TriggerStreamNo": (2, _decode_ascii),
        "PretriggerLength": (8, _decode_ascii),
        "RecordLength": (8, _decode_ascii),
        "Reserved": (144, _decode_ascii)},
    "EVT": {
        "TriggerChannels": (16, _decode_ascii),
        "MinimumChannels": (2, _decode_ascii),
        "TriggerWindow": (8, _decode_ascii),
        "PretriggerLength": (8, _decode_ascii),
        "PosttriggerLength": (8, _decode_ascii),
        "RecordLength": (8, _decode_ascii),
        "Reserved1": (8, _decode_ascii),
        "STALength": (8, _decode_ascii),
        "LTALength": (8, _decode_ascii),
        "MeanRemoval": (8, _decode_ascii),
        "TriggerRatio": (8, _decode_ascii),
        "DetriggerRatio": (8, _decode_ascii),
        "LTAHold": (4, _decode_ascii),
        "LowPassCornerFreq": (4, _decode_ascii),
        "HighPassCornerFreq": (4, _decode_ascii),
        "Reserved2": (52, _decode_ascii)},
    "EXT": {
        "PretriggerLength": (8, _decode_ascii),
        "RecordLength": (8, _decode_ascii),
        "Reserved": (146, _decode_ascii)},
    "LEV": {
        "Level": (8, _decode_ascii),
        "PretriggerLength": (8, _decode_ascii),
        "RecordLength": (8, _decode_ascii),
        "LowPassCornerFreq": (4, _decode_ascii),
        "HighPassCornerFreq": (4, _decode_ascii),
        "Reserved": (130, _decode_ascii)},
    "TIM": {
        "StartTime": (14, _decode_ascii),
        "RepeatInterval": (8, _decode_ascii),
        "Intervals": (4, _decode_ascii),
        "Reserved1": (8, _decode_ascii),
        "RecordLength": (8, _decode_ascii),
        "Reserved2": (120, _decode_ascii)},
    "TML": {
        "StartTime01": (14, _decode_ascii),
        "StartTime02": (14, _decode_ascii),
        "StartTime03": (14, _decode_ascii),
        "StartTime04": (14, _decode_ascii),
        "StartTime05": (14, _decode_ascii),
        "StartTime06": (14, _decode_ascii),
        "StartTime07": (14, _decode_ascii),
        "StartTime08": (14, _decode_ascii),
        "StartTime09": (14, _decode_ascii),
        "StartTime10": (14, _decode_ascii),
        "StartTime11": (14, _decode_ascii),
        "RecordLength": (8, _decode_ascii)}}

AD_PAYLOAD = {
    "marker": (2, _decode_ascii),
    "channels": (16, _decode_ascii),
    "sample_period": (8, _decode_ascii),
    "data_format": (2, _decode_ascii),
    "record_length": (8, _decode_ascii),
    "recording_destination": (4, _decode_ascii),
    "reserved": (952, _decode_ascii),
    "implement_time": (16, _parse_long_time)}

CD_PAYLOAD = {
    "_72A_start_time": (14, _decode_ascii),
    "_72A_repeat_interval": (8, _decode_ascii),
    "_72A_number_intervals": (4, _decode_ascii),
    "_72A_length": (8, _decode_ascii),
    "_72A_step_onoff": (4, _decode_ascii),
    "_72A_step_period": (8, _decode_ascii),
    "_72A_step_size": (8, _decode_ascii),
    "_72A_step_amplitude": (8, _decode_ascii),
    "_72A_step_output": (4, _decode_ascii),
    "_72A_reserved": (48, _decode_ascii),
    "_130_autocenter": (64, None),
    "_130_signal": (112, None),
    "_130_sequence": (232, None),
    "reserved": (470, _decode_ascii),
    "implement_time": (16, _parse_long_time)}

CD_130_AUTOCENTER = {
    "_sensor": (1, _decode_ascii),
    "_enable": (1, _decode_ascii),
    "_reading_interval": (4, _decode_ascii),
    "_cycle_interval": (2, _decode_ascii),
    "_level": (4, _decode_ascii),
    "_attempts": (2, _decode_ascii),
    "_attempts_interval": (2, _decode_ascii)}

CD_130_SIGNAL = {
    "_sensor": (1, _decode_ascii),
    "_enable": (1, _decode_ascii),
    "_reserved": (2, _decode_ascii),
    "_duration": (4, _decode_ascii),
    "_amplitude": (4, _decode_ascii),
    "_signal": (4, _decode_ascii),
    "_step_interval": (4, _decode_ascii),
    "_step_width": (4, _decode_ascii),
    "_sine_frequency": (4, _decode_ascii)}

CD_130_SEQUENCE = {
    "_sequence": (1, _decode_ascii),
    "_enable": (1, _decode_ascii),
    "_reserved_1": (2, _decode_ascii),
    "_start_time": (14, _decode_ascii),
    "_interval": (8, _decode_ascii),
    "_count": (2, _decode_ascii),
    "_record_length": (8, _decode_ascii),
    "_sensor": (4, _decode_ascii),
    "_reserved_2": (18, _decode_ascii)}

FD_PAYLOAD = {
    "fd_info": (992, None),
    "implement_time": (16, _parse_long_time)}

FD_INFO = {
    "_filter_block_count": (1, int),
    "_filter_ID": (1, _decode_ascii),
    "_filter_decimation": (1, int),
    "_filter_scalar": (1, int),
    "_filter_coeff_count": (1, int),
    "_packet_coeff_count": (1, int),
    "_coeff_packet_count": (1, int),
    "_coeff_format": (1, bcd),
    "_coeff": (984, None)}


class SOHPacket(obspy_rt130_packet.Packet):
    """Class used to define shared tools for the SOH packets"""

    _headers = ('experiment_number', 'unit_id', 'byte_count',
                'packet_sequence', 'time')

    @staticmethod
    def from_data(data: np.ndarray) -> SOHPacket:
        """
        Checks for valid packet type identifier and returns appropriate
        packet object
        """
        packet_type = data['packet_type'].decode("ASCII", "ignore")
        if packet_type == "SH":
            return SHPacket(data)
        elif packet_type == "SC":
            return SCPacket(data)
        elif packet_type == "OM":
            return OMPacket(data)
        elif packet_type == "DS":
            return DSPacket(data)
        elif packet_type == "AD":
            return ADPacket(data)
        elif packet_type == "CD":
            return CDPacket(data)
        elif packet_type == "FD":
            return FDPacket(data)
        else:
            msg = "Can not create Reftek SOH packet for packet type '{}'"
            raise NotImplementedError(msg.format(packet_type))

    @staticmethod
    def time_tag(time: UTCDateTime):
        return "{:04d}:{:03d}:{:02d}:{:02d}:{:02d}:{:03d}".format(time.year,
                                                                  time.julday,
                                                                  time.hour,
                                                                  time.minute,
                                                                  time.second,
                                                                  time.microsecond)  # noqa: E501

    @property
    def packet_tagline(self) -> str:
        return "\n"


class SHPacket(SOHPacket):
    """Class used to parse and generate string representation for SH packets"""

    def __init__(self, data: np.ndarray) -> None:
        self._data = data
        payload = self._data["payload"].tobytes()
        start_sh = 0
        for name, (length, converter) in SH_PAYLOAD.items():
            data = payload[start_sh:start_sh + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("SH packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_sh = start_sh + length

    def __str__(self) -> str:
        info = []
        # info.append(self.packet_tagline)
        packet_soh_string = ("\nState of Health  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time)[2:],
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        info.append("\n" + self.information.strip())
        return info


class SCPacket(SOHPacket):
    """Class used to parse and generate string representation for SC packets"""

    def __init__(self, data: np.ndarray) -> None:
        # Station/Channel payload
        self._data = data
        payload = self._data["payload"].tobytes()
        start_sc = 0
        for name, (length, converter) in SC_PAYLOAD.items():
            data = payload[start_sc:start_sc + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("SC packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_sc = start_sc + length
        # Detailed info for each channel - Up to 5 channels
        start_info = 0
        for ind_sc in range(1, SC_MAX_NBR_CHA + 1):
            for name, (length, converter) in SC_INFO.items():
                data = self.sc_info[start_info:start_info + length]
                if converter is not None:
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("SC packet, channel info, wrong conversion "
                               "routine for input variable : {}".format(name))
                        warnings.warn(msg)
                        data = ''
                name = "sc" + str(ind_sc) + name
                setattr(self, name, data)
                start_info = start_info + length

    def __str__(self) -> str:
        info = []
        packet_soh_string = ("\nStation Channel Definition  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)
        info.append("\n Experiment Number = " + self.experiment_number_sc)
        info.append("\n Experiment Name = " + self.experiment_name)
        info.append("\n  Comments - " + self.experiment_comment)
        info.append("\n Station Number = " + self.station_number)
        info.append("\n Station Name = " + self.station_name)
        info.append("\n  Station Comments - " + self.station_comment)
        info.append("\n DAS Model Number = " + self.das_model)
        info.append("\n DAS Serial Number = " + self.das_serial)
        info.append("\n Experiment Start Time = " + self.experiment_start)
        info.append("\n Time Clock Type = " + self.time_clock_type)
        info.append("\n Clock Serial Number = " + self.time_clock_sn)
        for ind_sc in range(1, SC_MAX_NBR_CHA + 1):
            channel_number = getattr(self, 'sc' + str(ind_sc) + '_number')
            if channel_number.strip():
                info.append("\n  Channel Number = " + channel_number)
                info.append("\n     Name - " + getattr(self, 'sc' + str(ind_sc) + '_name'))  # noqa: E501
                info.append("\n     Azimuth - " + getattr(self, 'sc' + str(ind_sc) + '_azimuth'))  # noqa: E501
                info.append("\n     Inclination - " + getattr(self, 'sc' + str(ind_sc) + '_inclination'))  # noqa: E501
                info.append("\n     Location")
                info.append("\n     X - " + getattr(self, 'sc' + str(ind_sc) + '_x_coordinate'))  # noqa: E501
                info.append("  Y - " + getattr(self, 'sc' + str(ind_sc) + '_y_coordinate'))  # noqa: E501
                info.append("  Z - " + getattr(self, 'sc' + str(ind_sc) + '_z_coordinate'))  # noqa: E501
                info.append("\n     XY Units - " + getattr(self, 'sc' + str(ind_sc) + '_xy_unit'))  # noqa: E501
                info.append("  Z Units - " + getattr(self, 'sc' + str(ind_sc) + '_z_unit'))  # noqa: E501
                info.append("\n     Preamplifier Gain = " + getattr(self, 'sc' + str(ind_sc) + '_preamp_gain'))  # noqa: E501
                info.append("\n     Sensor Model - " + getattr(self, 'sc' + str(ind_sc) + '_sensor_model'))  # noqa: E501
                info.append("\n     Sensor Serial Number - " + getattr(self, 'sc' + str(ind_sc) + '_sensor_serial'))  # noqa: E501
                info.append("\n     Volts per Bit = " + getattr(self, 'sc' + str(ind_sc) + '_adjusted_nominal_bit_weight'))  # noqa: E501
                info.append("\n     Comments - " + getattr(self, 'sc' + str(ind_sc) + '_comments'))  # noqa: E501
        return info

    def get_info(self, infos: List[List]) -> List[List]:
        """
        Compile relevant information - unit id, reference channel, network
        code, station code, component code, gain and implementation time - for
        given SC packet
        """
        implement_time = UTCDateTime(ns=self.implement_time)
        for ind_sc in range(1, SC_MAX_NBR_CHA + 1):
            channel_number = getattr(self, 'sc' + str(ind_sc) + '_number')
            if channel_number.strip():
                gain = getattr(self, 'sc' + str(ind_sc) + '_preamp_gain')
                if gain is None:
                    msg = ("No gain available for parameter packet "
                           "implemented at {} - refchan {}"
                           .format(implement_time, channel_number))
                    warnings.warn(msg)
                    continue
                # order: #das, refchan, netcode, station, channel[3],
                # gain, implement_time
                info = [self.unit_id.decode(),
                        channel_number.strip(),
                        self.experiment_number_sc.strip(),
                        self.station_name.strip(),
                        getattr(self, 'sc' + str(ind_sc) + '_name').strip(),
                        gain.strip(),
                        implement_time]
                if info not in infos:
                    infos.append(info)
        return infos


class OMPacket(SOHPacket):
    """Class used to parse and generate string representation for OM packets"""

    def __init__(self, data: np.ndarray) -> None:
        self._data = data
        payload = self._data["payload"].tobytes()
        start_om = 0
        for name, (length, converter) in OM_PAYLOAD.items():
            data = payload[start_om:start_om + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("OM packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_om = start_om + length

    def __str__(self) -> str:
        info = []
        # info.append(self.packet_tagline)
        packet_soh_string = ("\nOperating Mode Definition  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),  # noqa: E501
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)
        info.append("\n  Operating Mode 72A Power State " + self._72A_power_state)  # noqa: E501
        info.append("\n  Operating Mode Recording Mode " + self.recording_mode)
        info.append("\n  Operating Mode Auto Dump on ET " + self.auto_dump_on_ET)  # noqa: E501
        info.append("\n  Operating Mode Auto Dump Threshold " + self.auto_dump_threshold)  # noqa: E501
        info.append("\n  Operating Mode 72A Power Down Delay " + self._72A_power_down_delay)  # noqa: E501
        info.append("\n  Operating Mode Disk Wrap " + self.disk_wrap)
        info.append("\n  Operating Mode 72A Disk Power " + self._72A_disk_power)  # noqa: E501
        info.append("\n  Operating Mode 72A Terminator Power " + self._72A_terminator_power)  # noqa: E501
        info.append("\n  Operating Mode 72A Wake Up Start Time " + self._72A_wakeup_start_time)  # noqa: E501
        info.append("\n  Operating Mode 72A Wake Up Duration " + self._72A_wakeup_duration)  # noqa: E501
        info.append("\n  Operating Mode 72A Wake Up Repeat Interval " + self._72A_wakeup_repeat_interval)  # noqa: E501
        info.append("\n  Operating Mode 72A Number of Wake Up Intervals " + self._72A_wakeup_number_intervals)  # noqa: E501
        return info


class DSPacket(SOHPacket):
    """Class used to parse and generate string representation for DS packets"""

    def __init__(self, data: np.ndarray) -> None:
        # Data Stream payload
        self._data = data
        payload = self._data["payload"].tobytes()
        start_ds = 0
        for name, (length, converter) in DS_PAYLOAD.items():
            data = payload[start_ds:start_ds + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("DS packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_ds = start_ds + length
        # Detailed info for each stream - Up to 4 streams
        start_info = 0
        for ind_ds in range(1, DS_MAX_NBR_ST + 1):
            for name, (length, converter) in DS_INFO.items():
                data = self.ds_info[start_info:start_info + length]
                if converter is not None:
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("DS packet, data stream info, wrong conversion "
                               "routine for input variable: {}".format(name))
                        warnings.warn(msg)
                        data = ''
                name = "ds" + str(ind_ds) + name
                setattr(self, name, data)
                start_info = start_info + length
            # Detailed info for each trigger
            trigger_type = getattr(self, "ds" + str(ind_ds) + "_trigger_type").strip()  # noqa: E501
            if trigger_type in DS_TRIGGER:
                start_trigger = 0
                for name, (length, converter) in DS_TRIGGER[trigger_type].items():  # noqa: E501
                    data = getattr(self, "ds" + str(ind_ds) + "_trigger_description")[start_trigger:start_trigger + length]  # noqa: E501
                    if converter is not None:
                        try:
                            data = converter(data)
                        except ValueError:
                            msg = ("DS packet, trigger info, wrong conversion "
                                   "routine for input variable: {}"
                                   .format(name))
                            warnings.warn(msg)
                            data = ''
                    name = "ds" + str(ind_ds) + "_trigger_" + name
                    setattr(self, name, data)
                    start_trigger = start_trigger + length
            elif trigger_type.strip():
                msg = ("Trigger type {:s} not found".format(trigger_type))
                warnings.warn(msg)

    def __str__(self) -> str:
        info = []
        info.append(self.packet_tagline)
        packet_soh_string = ("\nData Stream Definition  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),  # noqa: E501
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)
        for ind_ds in range(1, DS_MAX_NBR_ST + 1):
            stream_number = getattr(self, "ds" + str(ind_ds) + "_number")
            if stream_number.strip():
                recording_dest = [AD_DS_RECORDING_DEST[ind_rd]
                                  for ind_rd, val in enumerate(getattr(self, "ds" + str(ind_ds) + "_recording_destination"))  # noqa: E501
                                  if val.strip()]
                info.append(" ".join(["\n  Data Stream",
                                      stream_number,
                                      getattr(self, "ds" + str(ind_ds) + "_name"),  # noqa: E501
                                      ", ".join(recording_dest)]))
                channels = getattr(self, "ds" + str(ind_ds) + "_channels_included")  # noqa: E501
                channel_nbr = [str(ind_chan) for ind_chan, val in enumerate(channels, 1) if val.strip()]  # noqa: E501
                info.append("\n  Channels " + ", ".join(channel_nbr))
                info.append("\n  Sample rate " + getattr(self, "ds" + str(ind_ds) + "_sample_rate") + " samples per second")  # noqa: E501
                info.append("\n  Data Format " + getattr(self, "ds" + str(ind_ds) + "_data_format"))  # noqa: E501
                trigger_type = getattr(self, "ds" + str(ind_ds) + "_trigger_type").strip()  # noqa: E501
                info.append("\n  Trigger Type " + trigger_type)
                if trigger_type in DS_TRIGGER:
                    for key in DS_TRIGGER[trigger_type].keys():
                        if "reserved" not in key.lower():
                            trigger_info = getattr(self, "ds" + str(ind_ds) + "_trigger_" + key)  # noqa: E501
                            if trigger_info:
                                if trigger_type == "EVT" and key == "TriggerChannels":  # noqa: E501
                                    channel_nbr = [str(ind_chan) for ind_chan, val in enumerate(trigger_info, 1) if val.strip()]  # noqa: E501
                                    info.append(" ".join(["\n     Trigger", key, ", ".join(channel_nbr)]))  # noqa: E501
                                else:
                                    info.append(" ".join(["\n     Trigger", key, trigger_info]))  # noqa: E501
        return info

    def get_info(self, infos: List[List]) -> List[List]:
        """
        Compile relevant information - reference data stream, band and
        instrument codes, sample rate and implementation time - for given DS
        packet
        """
        implement_time = UTCDateTime(ns=self.implement_time)
        for ind_ds in range(1, DS_MAX_NBR_ST + 1):
            stream_number = getattr(self, "ds" + str(ind_ds) + "_number")
            if stream_number.strip():
                samplerate = getattr(self, "ds" + str(ind_ds) + "_sample_rate")
                if samplerate is None:
                    msg = ("No sampling rate available for parameter packet "
                           "implemented at {} - refstrm {}"
                           .format(implement_time, stream_number))
                    warnings.warn(msg)
                    continue
                # order: refstrm, channel[0:2], samplerate, implement_time
                info = [stream_number.strip(),
                        getattr(self, "ds" + str(ind_ds) + "_name").strip(),
                        samplerate.strip(),
                        implement_time]
                if info not in infos:
                    infos.append(info)
        return infos


class ADPacket(SOHPacket):
    """Class used to parse and generate string representation for AD packets"""

    def __init__(self, data: np.ndarray) -> None:
        self._data = data
        payload = self._data["payload"].tobytes()
        start_ad = 0
        for name, (length, converter) in AD_PAYLOAD.items():
            data = payload[start_ad:start_ad + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("AD packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_ad = start_ad + length

    def __str__(self) -> str:
        info = []
        # info.append(self.packet_tagline)
        packet_soh_string = ("\nAuxiliary Data Parameter  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),  # noqa: E501
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)
        channel_nbr = [str(ind_chan) for ind_chan, val in
                       enumerate(self.channels, 1) if val.strip()]
        info.append("\n  Channels " + ", ".join(channel_nbr))
        info.append("\n  Sample Period " + self.sample_period)
        info.append("\n  Data Format " + self.data_format)
        info.append("\n  Record Length " + self.record_length)
        recording_dest = [AD_DS_RECORDING_DEST[ind_rd] for ind_rd, val
                          in enumerate(self.recording_destination)
                          if val.strip()]
        info.append("\n  Recording Destination " + ", ".join(recording_dest))
        return info


class CDPacket(SOHPacket):
    """Class used to parse and generate string representation for CD packets"""

    def __init__(self, data: np.ndarray) -> None:
        # Calibration parameter payload
        self._data = data
        payload = self._data["payload"].tobytes()
        start_cd = 0
        for name, (length, converter) in CD_PAYLOAD.items():
            data = payload[start_cd:start_cd + length]
            if converter is not None:
                try:
                    data = converter(data)
                except ValueError:
                    msg = ("CD packet, wrong conversion routine for input "
                           "variable: {}".format(name))
                    warnings.warn(msg)
                    data = ''
            setattr(self, name, data)
            start_cd = start_cd + length

        start_info_ac = 0
        start_info_sig = 0
        start_info_seq = 0
        for ind_cd in range(1, CD_MAX_NBR_STRUCT + 1):
            # Detailed info for 130 Sensor Auto-Center - Up to 4 structures
            for name, (length, converter) in CD_130_AUTOCENTER.items():
                data = self._130_autocenter[start_info_ac:start_info_ac + length]  # noqa: E501
                if converter is not None:
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("CD packet, auto-center info, wrong conversion "
                               "routine for input variable: {}".format(name))
                        warnings.warn(msg)
                        data = ''
                name = "cd_130_autocenter_" + str(ind_cd) + name
                setattr(self, name, data)
                start_info_ac = start_info_ac + length
            # Detailed info for 130 Sensor Calibration Signal
            # Up to 4 structures
            for name, (length, converter) in CD_130_SIGNAL.items():
                data = self._130_signal[start_info_sig:start_info_sig + length]
                if converter is not None:
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("CD packet, calibration signal info, wrong "
                               "conversion routine for input variable: {}"
                               .format(name))
                        warnings.warn(msg)
                        data = ''
                name = "cd_130_signal_" + str(ind_cd) + name
                setattr(self, name, data)
                start_info_sig = start_info_sig + length
            # Detailed info for 130 Sensor Calibration Sequence
            # Up to 4 structures
            for name, (length, converter) in CD_130_SEQUENCE.items():
                data = self._130_sequence[start_info_seq:start_info_seq + length]  # noqa: E501
                if converter is not None:
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("CD packet, calibration sequence info, wrong "
                               "conversion routine for input variable: {}"
                               .format(name))
                        warnings.warn(msg)
                        data = ''
                name = "cd_130_sequence_" + str(ind_cd) + name
                setattr(self, name, data)
                start_info_seq = start_info_seq + length

    def __str__(self) -> str:
        info = []
        # info.append(self.packet_tagline)
        packet_soh_string = ("\nCalibration Definition  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),  # noqa: E501
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)

        if self._72A_start_time.split():
            info.append("\n  72A Calibration Start Time " + self._72A_start_time)  # noqa: E501
            info.append("\n  72A Calibration Repeat Interval " + self._72A_repeat_interval)  # noqa: E501
            info.append("\n  72A Calibration Intervals " + self._72A_number_intervals)  # noqa: E501
            info.append("\n  72A Calibration Length " + self._72A_length)
            info.append("\n  72A Calibration Step On/Off " + self._72A_step_onoff)  # noqa: E501
            info.append("\n  72A Calibration Step Period " + self._72A_step_period)  # noqa: E501
            info.append("\n  72A Calibration Step Size " + self._72A_step_size)
            info.append("\n  72A Calibration Step Amplitude " + self._72A_step_amplitude)  # noqa: E501
            info.append("\n  72A Calibration Step Output " + self._72A_step_output)  # noqa: E501

        for ind_cd in range(1, CD_MAX_NBR_STRUCT + 1):
            autocenter_sensor = getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_sensor')  # noqa: E501
            if autocenter_sensor.strip():
                info.append("\n  130 Auto Center Sensor " + autocenter_sensor)
                info.append("\n  130 Auto Center Enable " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_enable'))  # noqa: E501
                info.append("\n  130 Auto Center Reading Interval " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_reading_interval'))  # noqa: E501
                info.append("\n  130 Auto Center Cycle Interval " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_cycle_interval'))  # noqa: E501
                info.append("\n  130 Auto Center Level " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_level'))  # noqa: E501
                info.append("\n  130 Auto Center Attempts " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_attempts'))  # noqa: E501
                info.append("\n  130 Auto Center Attempt Interval " + getattr(self, "cd_130_autocenter_" + str(ind_cd) + '_attempts_interval'))  # noqa: E501

        for ind_cd in range(1, CD_MAX_NBR_STRUCT + 1):
            signal_sensor = getattr(self, "cd_130_signal_" + str(ind_cd) + '_sensor')  # noqa: E501
            if signal_sensor.strip():
                info.append("\n  130 Calibration Sensor " + signal_sensor)
                info.append("\n  130 Calibration Enable " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_enable'))  # noqa: E501
                info.append("\n  130 Calibration Duration " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_duration'))  # noqa: E501
                info.append("\n  130 Calibration Amplitude " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_amplitude'))  # noqa: E501
                info.append("\n  130 Calibration Signal " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_signal'))  # noqa: E501
                info.append("\n  130 Calibration Step Interval " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_step_interval'))  # noqa: E501
                info.append("\n  130 Calibration Step Width " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_step_width'))  # noqa: E501
                info.append("\n  130 Calibration Sine Frequency " + getattr(self, "cd_130_signal_" + str(ind_cd) + '_sine_frequency'))  # noqa: E501

        for ind_cd in range(1, CD_MAX_NBR_STRUCT + 1):
            sequence_sensor = getattr(self, "cd_130_sequence_" + str(ind_cd) + '_sensor')  # noqa: E501
            if sequence_sensor.strip():
                info.append("\n  130 Calibration Sequence " + sequence_sensor)
                info.append("\n  130 Calibration Sequence Enable " + getattr(self, "cd_130_sequence_" + str(ind_cd) + '_enable'))  # noqa: E501
                info.append("\n  130 Calibration Sequence Start Time " + getattr(self, "cd_130_sequence_" + str(ind_cd) + '_start_time'))  # noqa: E501
                info.append("\n  130 Calibration Sequence Interval " + getattr(self, "cd_130_sequence_" + str(ind_cd) + '_interval'))  # noqa: E501
                info.append("\n  130 Calibration Sequence Count " + getattr(self, "cd_130_sequence_" + str(ind_cd) + '_count'))  # noqa: E501
                info.append("\n  130 Calibration Sequence Record Length " + getattr(self, "cd_130_sequence_" + str(ind_cd) + '_record_length'))  # noqa: E501
        return info


class FDPacket(SOHPacket):
    """Class used to parse and generate string representation for FD packets"""

    def __init__(self, data: np.ndarray) -> None:
        # Filter description payload
        self._data = data
        self.from_old_firmware = False
        payload = self._data["payload"]
        start_fd = 0
        for name, (length, converter) in FD_PAYLOAD.items():
            data = payload[start_fd:start_fd + length]
            if converter is not None and data.size != 0:
                try:
                    data = converter(data.tobytes())
                except ValueError:
                    if name == 'implement_time':
                        msg = (f"FD packet, failed to convert input variable: "
                               f"{name}. It looks like this data set comes "
                               f"from an RT130 with firmware version earlier "
                               f"than 2.9.0."
                               )
                        self.from_old_firmware = True
                        # We have to set FDPacket.implement_time to None
                        # because setting it to an empty string causes
                        # FDPacket.time_tag to fail, which renders the data set
                        # unreadable.
                        data = None
                    else:
                        msg = (f"FD packet, wrong conversion routine for input"
                               f" variable: {name}")
                        data = ''
                    warnings.warn(msg)
            setattr(self, name, data)
            start_fd = start_fd + length
        # Detailed info for filter block(s) (fb)
        # Warning - rt130 manual section on FD packets is not very clear.
        # The following code needs to be further tested if FD packets
        # contains several filter blocks or if packet coefficient count
        # in a given filter block is superior to 248 32-bit coefficients.
        # Test data including these scenarios is however lacking.
        start_info = 0
        nbr_fbs = self.fd_info[0] + 1
        setattr(self, 'nbr_fbs', nbr_fbs)
        if nbr_fbs > 1:
            msg = ("FD packet contains more than one filter block - "
                   "Handling of additional block has not been fully tested")
            warnings.warn(msg)
        for ind_fb in range(1, nbr_fbs + 1):
            for name, (length, converter) in FD_INFO.items():
                if name == "_coeff":
                    coeff_size = int(getattr(self, "fb" + str(ind_fb) + "_coeff_format")[0] / 8)  # noqa: E501
                    setattr(self, "fb" + str(ind_fb) + '_coeff_size', coeff_size)  # noqa: E501
                    length = getattr(self, "fb" + str(ind_fb) + "_packet_coeff_count") * coeff_size  # noqa: E501
                data = self.fd_info[start_info:start_info + length]
                if converter is not None:
                    if converter is _decode_ascii:
                        data = data.tobytes()
                    try:
                        data = converter(data)
                    except ValueError:
                        msg = ("FD packet, filter block info, wrong "
                               "conversion routine for input variable: {}"
                               .format(name))
                        warnings.warn(msg)
                        data = ''
                name = "fb" + str(ind_fb) + name
                setattr(self, name, data)
                start_info = start_info + length

    def __str__(self) -> str:
        info = []
        # info.append(self.packet_tagline)
        packet_soh_string = ("\nFilter Description  {:s}   ST: {:s}"
                             .format(self.time_tag(self.time),  # noqa: E501
                                     self.unit_id.decode()))
        info.append(packet_soh_string)
        implement_time_tag = self.time_tag(
            UTCDateTime(self.implement_time / 10 ** 9)
        )
        info.append("\n Implemented = " + implement_time_tag)
        for ind_fb in range(1, self.nbr_fbs + 1):
            info.append("\n     Filter Block Count " + str(getattr(self, "fb" + str(ind_fb) + "_filter_block_count")))  # noqa: E501
            info.append("\n     Filter ID " + getattr(self, "fb" + str(ind_fb) + "_filter_ID"))  # noqa: E501
            info.append("\n     Filter Decimation " + str(getattr(self, "fb" + str(ind_fb) + "_filter_decimation")))  # noqa: E501
            info.append("\n     Filter Scalar " + str(getattr(self, "fb" + str(ind_fb) + "_filter_scalar")))  # noqa: E501
            info.append("\n     Filter Coefficient Count " + str(getattr(self, "fb" + str(ind_fb) + "_filter_coeff_count")))  # noqa: E501
            info.append("\n     Filter Packet Coefficient Count " + str(getattr(self, "fb" + str(ind_fb) + "_packet_coeff_count")))  # noqa: E501
            info.append("\n     Filter Coefficient Packet Count " + str(getattr(self, "fb" + str(ind_fb) + "_coeff_packet_count")))  # noqa: E501
            info.append("\n     Filter Coefficient Format " + str(getattr(self, "fb" + str(ind_fb) + "_coeff_format")[0]))  # noqa: E501
            info.append("\n     Filter Coefficients:")
            start_coeff = 0
            for inf_coeff in range(0, getattr(self, "fb" + str(ind_fb) + "_packet_coeff_count")):  # noqa: E501
                length = getattr(self, "fb" + str(ind_fb) + '_coeff_size')  # noqa: E501
                coeff = getattr(self, "fb" + str(ind_fb) + "_coeff")[start_coeff:start_coeff + length]  # noqa: E501
                start_coeff = start_coeff + length
                twosCom_bin = ''.join([self.twosCom_dec2bin(x, 8) for x in coeff])  # noqa: E501
                coeff_dec = self.twosCom_bin2dec(twosCom_bin, length * 8)
                info.append("  " + str(coeff_dec))
        return info

    @staticmethod
    def twosCom_bin2dec(bin_: str, digit: int):
        while len(bin_) < digit:
            bin_ = '0' + bin_
        if bin_[0] == '0':
            return int(bin_, 2)
        else:
            return -1 * (int(''.join('1' if x == '0' else '0' for x in bin_), 2) + 1)  # noqa: E501

    @staticmethod
    def twosCom_dec2bin(dec: int, digit: int):
        if dec >= 0:
            bin_ = bin(dec).split("0b")[1]
            while len(bin_) < digit:
                bin_ = '0' + bin_
            return bin_
        else:
            bin_ = -1 * dec
            return bin(dec - pow(2, digit)).split("0b")[1]


def _initial_unpack_packets_soh(bytestring: bytes) -> np.ndarray:
    """
    First unpack data with dtype matching itemsize of storage in the reftek
    file, than allocate result array with dtypes for storage of python
    objects/arrays and fill it with the unpacked data.
    """
    if not len(bytestring):
        return np.array([], dtype=PACKET_FINAL_DTYPE)

    if len(bytestring) % 1024 != 0:
        tail = len(bytestring) % 1024
        bytestring = bytestring[:-tail]
        msg = ("Length of data not a multiple of 1024. Data might be "
               "truncated. Dropping {:d} byte(s) at the end.").format(tail)
        warnings.warn(msg)
    data = from_buffer(
        bytestring, dtype=PACKET_INITIAL_UNPACK_DTYPE)
    result = np.empty_like(data, dtype=PACKET_FINAL_DTYPE)

    for name, dtype_initial, converter, dtype_final in PACKET:
        if converter is None:
            result[name][:] = data[name][:]
        else:
            try:
                result[name][:] = converter(data[name])
            except Exception as e:
                raise Reftek130UnpackPacketError(str(e))
    # time unpacking is special and needs some additional work.
    # we need to add the POSIX timestamp of the start of respective year to the
    # already unpacked seconds into the respective year..
    result['time'][:] += [_get_nanoseconds_for_start_of_year(y)
                          for y in result['year']]
    return result
