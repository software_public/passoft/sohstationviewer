# -*- coding: utf-8 -*-

"""Top-level package for from_rt2ms."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2021.238'
