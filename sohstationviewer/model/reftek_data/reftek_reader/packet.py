#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Suggested updates to obspy.io.reftek.packet:
- Change type of EH_PAYLOAD['sampling_rate'] from int to float.
- Add eh_et_info method to EHPacket class. This method compiles EH and ET info
  and returns a list of strings used to write the rt2ms log file.

Maeva Pourpoint IRIS/PASSCAL
"""
from typing import List

import numpy
import obspy.io.reftek.packet as obspy_rt130_packet

from obspy import UTCDateTime
from obspy.io.reftek.util import (
    _decode_ascii, _parse_long_time, _16_tuple_ascii, _16_tuple_float,
    _16_tuple_int,
)
from sohstationviewer.model.reftek_data.reftek_reader.soh_packet import (
    SOHPacket)


class Reftek130UnpackPacketError(ValueError):
    pass


eh_et_payload_last_field_start = 88
eh_et_payload_last_field_size = 16

# The payload start is based on the start of the payload, so we have to add 24
# to compensate for the size of the header and extended header.
eh_et_payload_end_in_packet = (
        eh_et_payload_last_field_start + eh_et_payload_last_field_size + 24
)

# name, offset, length (bytes) and converter routine for EH/ET packet payload
# Trimmed to only include the parts used elsewhere for the sake of better
# performance.
EH_PAYLOAD = {
    "station_name_extension": (35, 1, _decode_ascii),
    "station_name": (36, 4, _decode_ascii),
    "sampling_rate": (64, 4, float),
    "trigger_time": (72, 16, _parse_long_time),
    "first_sample_time": (
        eh_et_payload_last_field_start, eh_et_payload_last_field_size,
        _parse_long_time),
}

obspy_rt130_packet.EH_PAYLOAD = {
    "trigger_time_message": (0, 33, _decode_ascii),
    "time_source": (33, 1, _decode_ascii),
    "time_quality": (34, 1, _decode_ascii),
    "station_name_extension": (35, 1, _decode_ascii),
    "station_name": (36, 4, _decode_ascii),
    "stream_name": (40, 16, _decode_ascii),
    "_reserved_2": (56, 8, _decode_ascii),
    "sampling_rate": (64, 4, float),
    "trigger_type": (68, 4, _decode_ascii),
    "trigger_time": (72, 16, _parse_long_time),
    "first_sample_time": (88, 16, _parse_long_time),
    "detrigger_time": (104, 16, _parse_long_time),
    "last_sample_time": (120, 16, _parse_long_time),
    "channel_adjusted_nominal_bit_weights": (136, 128, _16_tuple_ascii),
    "channel_true_bit_weights": (264, 128, _16_tuple_ascii),
    "channel_gain_code": (392, 16, _16_tuple_ascii),
    "channel_ad_resolution_code": (408, 16, _16_tuple_ascii),
    "channel_fsa_code": (424, 16, _16_tuple_ascii),
    "channel_code": (440, 64, _16_tuple_ascii),
    "channel_sensor_fsa_code": (504, 16, _16_tuple_ascii),
    "channel_sensor_vpu": (520, 96, _16_tuple_float),
    "channel_sensor_units_code": (616, 16, _16_tuple_ascii),
    "station_channel_number": (632, 48, _16_tuple_int),
    "_reserved_3": (680, 156, _decode_ascii),
    "total_installed_channels": (836, 2, int),
    "station_comment": (838, 40, _decode_ascii),
    "digital_filter_list": (878, 16, _decode_ascii),
    "position": (894, 26, _decode_ascii),
    "reftek_120": (920, 80, None)}


class EHPacket(obspy_rt130_packet.EHPacket):
    def __init__(self, data: numpy.ndarray) -> None:
        """
        Reimplement __init__ to change a different value for EH_PAYLOAD.
        This should be the cleanest way to do it, seeing as any other way I
        can think of modify EH_PAYLOAD in the original file, which can have
        consequences that are not readily apparent.

        :param data: the data of an EH packet. For more information, refer to
            obspy.io.reftek.packet.PACKET_FINAL_DTYPE.
        """
        self._data = data
        payload = self._data["payload"].tobytes()
        for name, (start, length, converter) in EH_PAYLOAD.items():
            data = payload[start:start + length]
            if converter is not None:
                data = converter(data)
            setattr(self, name, data)

    def __str__(self, compact: bool = False) -> str:
        if compact:
            sta = (self.station_name.strip() +
                   self.station_name_extension.strip())
            info = ("{:04d} {:2s} {:4s} {:2d} {:4d} {:4d} {:2d} {:2s} "
                    "{:5s}  {:4s}        {!s}").format(
                self.packet_sequence, self.type.decode(),
                self.unit_id.decode(), self.experiment_number,
                self.byte_count, self.event_number,
                self.data_stream_number, self.data_format.decode(),
                sta, str(self.sampling_rate)[:4], self.time)
        else:
            info = []
            for key in self._headers:
                value = getattr(self, key)
                if key in ("unit_id", "data_format"):
                    value = value.decode()
                info.append("{}: {}".format(key, value))
            info.append("-" * 20)
            for key in sorted(EH_PAYLOAD.keys()):
                value = getattr(self, key)
                if key in ("trigger_time", "detrigger_time",
                           "first_sample_time", "last_sample_time"):
                    if value is not None:
                        value = UTCDateTime(ns=value)
                info.append("{}: {}".format(key, value))
            info = "{} Packet\n\t{}".format(self.type.decode(),
                                            "\n\t".join(info))
        return info

    def eh_et_info(self, nbr_DT_samples: int) -> List[str]:
        """
        Compile EH and ET info to write to log file.
        Returns list of strings.
        Formatting of strings is based on earlier version of rt2ms.
        """
        info = []
        trigger_time = SOHPacket.time_tag(UTCDateTime(ns=self.trigger_time))
        first_sample_time = SOHPacket.time_tag(
            UTCDateTime(ns=self.first_sample_time))  # noqa: E501
        packet_tagline2 = ("\nDAS: {:s} EV: {:04d} DS: {:d} FST = {:s} TT = "
                           "{:s} NS: {:d} SPS: {:.1f} ETO: 0"
                           .format(self.unit_id.decode(),
                                   self.event_number,
                                   self.data_stream_number + 1,
                                   first_sample_time,
                                   trigger_time,
                                   nbr_DT_samples,
                                   self.sampling_rate))
        info.append(packet_tagline2)
        return info
