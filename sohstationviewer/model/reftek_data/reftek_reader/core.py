from __future__ import annotations

import numpy
from numpy._typing import NDArray

from sohstationviewer.model.reftek_data.reftek_reader import soh_packet

"""
Suggested updates to obspy.io.reftek.core:
- modify section of the code that deals with upacking data
  with '16' and '32' encodings to account for number of samples in each data
  packet. Mass position channels data are '16' encoded.

Maeva Pourpoint IRIS/PASSCAL
"""

import copy
from pathlib import Path
from typing import Optional, Union

import obspy.io.reftek.core as obspy_rt130_core
import warnings

import numpy as np

from obspy import Trace, Stream, UTCDateTime
from obspy.core.util.obspy_types import ObsPyException

from sohstationviewer.model.reftek_data.reftek_reader.packet import EHPacket


class DiscontinuousTrace(Trace):
    """
    Extension of obspy.Trace that changes the way time data is handled when
    reading data using the method from logpeek/qpeek.
    """

    def __init__(self, *args, times: np.ndarray, **kwargs):
        super().__init__(*args, **kwargs)
        self._times = times

    def times(self, type: str = "relative",
              reftime: Optional[UTCDateTime] = None) -> np.ndarray:
        """
        Override Trace.times(). Returns a numpy array of stored times data,
        modified based on the argument "type".
        :param type: the type of times data to return. For more information,
            refer to Trace.times(). Note: this method does not implement
            types 'utcdatetime' and 'matplotlib' because they are not going
            to be useful.
        :param reftime: the time used as a reference point when getting
            relative time. If None, the start time of the trace is used as
            the reference point.
        :return: the requested array of time data, modified based on the type
            requested.
        """
        if type == 'utcdatetime' or type == 'matplotlib':
            raise NotImplementedError
        elif type == 'relative':
            if reftime is None:
                return self._times - self.stats.starttime.timestamp
            else:
                return self._times - reftime.timestamp
        elif type == 'timestamp':
            return self._times


class Reftek130Exception(ObsPyException):
    pass


class DecimatedReftek130(obspy_rt130_core.Reftek130):
    """
    Child class of obspy.Reftek that reads waveform data similar to logpeek for
    better performance.
    """

    @staticmethod
    def _decimate_waveform_data(
            data: NDArray[obspy_rt130_core.PACKET_FINAL_DTYPE]):
        """
        Decimate the waveform data of an RT130 file in-place. Works by grabbing
        only one data point from each packet.
        :param data: the waveform data of an RT130 file
        """
        first_packet_payload = data['payload'][0].copy()
        last_packet_payload = data['payload'][-1].copy()

        if data[0]['data_format'] == b'16':
            data_points = data['payload'][:, :2]
            # The data is stored in a big-endian order.
            # Merge the two bytes in each data point into a two-byte number.
            data_points = data_points.view(np.dtype('>i2'))
            # Sign extend the two-byte numbers into four-byte numbers. This is
            # done to match the byte length of other data formats.
            data_points = data_points.astype(np.dtype('>i4'))
            # Convert each four-byte number into four bytes to match Obspy's
            # payload format.
            data_points = data_points.view('>u1')
        else:
            if data[0]['data_format'] in [b'C0', b'C1', b'C2', b'C3']:
                # The first 40 bytes in the payload are filler, so we skip past
                # them. Then, we grab the last data point in the packet, which
                # is stored in bytes 8 to 12 (exclusive) of the actual data. We
                # could also have used the first data point in the packet,
                # which is stored in bytes 4 to 8 (exclusive) of the actual
                # data, but experiments have indicated that using the last data
                # point leads to a better-looking plot.
                data_points = data['payload'][:, 48:52].copy()
            else:
                data_points = data['payload'][:, :4].copy()
        data['payload'][:, :4] = data_points
        data['payload'][:, 4:] = 0

        if data['packet_type'][0] == b'EH':
            data['payload'][0] = first_packet_payload
        if data['packet_type'][-1] == b'ET':
            data['payload'][-1] = last_packet_payload

    @staticmethod
    def from_file(file: Union[str, Path]) -> DecimatedReftek130:
        """
        Read data from an RT130 file and save it in a Reftek130 object.
        :param file: the RT130 file to read
        :return: a Reftek130 object that stores the data in file
        """
        rt = DecimatedReftek130()
        rt._filename = file

        # SOH and waveform packets are read into different formats, so we have
        # to handle them separately. The two formats are encoded in the PACKET
        # constants in obspy.io.reftek.packet (waveform packets) and
        # model.reftek_data.reftek_reader.soh_packet (SOH packets).
        infile = open(file, 'rb')
        # Because SOH and waveform data are stored in separate files, we
        # only need to look at the first packet type
        first_packet_type = infile.read(2)
        infile.seek(0)

        if first_packet_type in [b'EH', b'ET', b'DT']:
            data = obspy_rt130_core.Reftek130.from_file(file)._data
            DecimatedReftek130._decimate_waveform_data(data)
        else:
            data = soh_packet._initial_unpack_packets_soh(infile.read())
        rt._data = data
        return rt

    def to_stream(self, network: str = "", location: str = "",
                  include_mp123: bool = False, include_mp456: bool = False,
                  headonly: bool = False, verbose: bool = False,
                  sort_permuted_package_sequence: bool = False) -> Stream:
        """
        Create an obspy.Stream object that holds the data stored in this
            Reftek130 object.

        :type headonly: bool
        :param headonly: Determines whether or not to unpack the data or just
            read the headers.
        """
        if not len(self._data):
            msg = "No packet data in Reftek130 object (file: {})"
            raise Reftek130Exception(msg.format(self._filename))
        self.check_packet_sequence_and_sort(sort_permuted_package_sequence)
        self.check_packet_sequence_contiguous()
        self.drop_not_implemented_packet_types()
        if not len(self._data):
            msg = ("No packet data left in Reftek130 object after dropping "
                   "non-implemented packets (file: {})").format(self._filename)
            raise Reftek130Exception(msg)
        st = Stream()
        for event_number in np.unique(self._data['event_number']):
            data = self._data[self._data['event_number'] == event_number]
            # we should have exactly one EH and one ET packet, truncated data
            # sometimes misses the header or trailer packet.
            eh_packets = data[data['packet_type'] == b"EH"]
            et_packets = data[data['packet_type'] == b"ET"]
            if len(eh_packets) == 0 and len(et_packets) == 0:
                msg = ("Reftek data (file: {}) contain data packets without "
                       "corresponding header or trailer packet."
                       .format(self._filename))
                raise Reftek130Exception(msg)
            if len(eh_packets) > 1 or len(et_packets) > 1:
                msg = ("Reftek data (file: {}) contain data packets with "
                       "multiple corresponding header or trailer packets."
                       .format(self._filename))
                raise Reftek130Exception(msg)
            if len(eh_packets) != 1:
                msg = ("No event header (EH) packets in packet sequence. "
                       "File ({}) might be truncated.".format(self._filename))
                warnings.warn(msg)
            if len(et_packets) != 1:
                msg = ("No event trailer (ET) packets in packet sequence. "
                       "File ({}) might be truncated.".format(self._filename))
                warnings.warn(msg)
            # use either the EH or ET packet, they have the same content (only
            # trigger stop time is not in EH)
            if len(eh_packets):
                eh = EHPacket(eh_packets[0])
            else:
                eh = EHPacket(et_packets[0])
            header = {
                "unit_id": self._data['unit_id'][0],
                "experiment_number": self._data['experiment_number'][0],
                "network": network,
                "station": (eh.station_name +
                            eh.station_name_extension).strip(),
                "location": location, "sampling_rate": eh.sampling_rate,
                "reftek130": eh._to_dict()}
            delta = 1.0 / eh.sampling_rate
            delta_nanoseconds = int(delta * 1e9)
            inds_dt = data['packet_type'] == b"DT"
            data_channels = np.unique(data[inds_dt]['channel_number'])
            for channel_number in data_channels:
                inds = data['channel_number'] == channel_number
                # channel number of EH/ET packets also equals zero (one of the
                # three unused bytes in the extended header of EH/ET packets)
                inds &= data['packet_type'] == b"DT"
                packets = data[inds]

                # split into contiguous blocks, i.e. find gaps. packet sequence
                # was sorted already..
                endtimes = (
                        packets[:-1]["time"] +
                        packets[:-1]["number_of_samples"].astype(np.int64) *
                        delta_nanoseconds)
                # check if next starttime matches seamless to last chunk
                # 1e-3 seconds == 1e6 nanoseconds is the smallest time
                # difference reftek130 format can represent, so anything larger
                # or equal means a gap/overlap.
                time_diffs_milliseconds_abs = np.abs(
                    packets[1:]["time"] - endtimes) / 1000000
                gaps = time_diffs_milliseconds_abs >= 1
                if np.any(gaps):
                    gap_split_indices = np.nonzero(gaps)[0] + 1
                    contiguous = np.array_split(packets, gap_split_indices)
                else:
                    contiguous = [packets]

                for packets_ in contiguous:
                    starttime = packets_[0]['time']

                    if headonly:
                        sample_data = np.array([], dtype=np.int32)
                        npts = packets_["number_of_samples"].sum()
                    else:
                        # The payload stores the first data point of each
                        # packet, encoded as a numpy array of 4 1-byte numbers.
                        # Due to the way the payload is encoded during the
                        # reading process and a quirk of 2-complement binary
                        # numbers (namely, appending a negative number with 1s
                        # does not change its value), we do not have to care
                        # about the actual encoding type of the stored packets.
                        sample_data = np.ascontiguousarray(
                            packets_['payload'][:, :4])
                        sample_data = sample_data.view(np.dtype('>i4'))
                        sample_data = sample_data.squeeze(axis=-1)
                        npts = sample_data.size
                    tr = DiscontinuousTrace(
                        data=sample_data, header=copy.deepcopy(header),
                        times=(packets_['time'] / 10 ** 9).round(3)
                    )
                    # The plotting process needs to know about the number of
                    # points stored in the trace. However, tr.stats use the
                    # stored npts to calculate some other metadata, so we can't
                    # store that information there. As a compromise, we keep
                    # tr.stats.npts the same, while storing the actual number
                    # of data points in the trace in another part of tr.stats.
                    tr.stats.npts = packets_['number_of_samples'].sum()
                    tr.stats.actual_npts = npts
                    # channel number is not included in the EH/ET packet
                    # payload, so add it to stats as well..
                    tr.stats.reftek130['channel_number'] = channel_number
                    tr.stats.starttime = UTCDateTime(ns=starttime)
                    DS = self._data['data_stream_number'][0] + 1
                    if DS != 9:
                        tr.stats.channel = "DS%s-%s" % (DS, channel_number + 1)
                    else:
                        if not include_mp123 and channel_number < 3:
                            continue
                        if not include_mp456 and channel_number > 2:
                            continue
                        tr.stats.channel = "MassPos%s" % (channel_number + 1)
                        # check if endtime of trace is consistent
                    st += tr
        return st
