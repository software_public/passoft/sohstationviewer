import numpy as np
from typing import Tuple, List, Dict, Optional, Union

from obspy.core import Trace
from obspy.core import Stream
from obspy import UTCDateTime

from sohstationviewer.model.reftek_data.reftek_reader.core import (
    DiscontinuousTrace, DecimatedReftek130)
from sohstationviewer.model.general_data.general_data_helper import squash_gaps


def check_time_in_file(path2file, read_start, read_end):
    """
    Since each file contains data inside 1 day and read_start and read_end is
    the beginning of a day, this function only read the first few bytes of a
    files to get the date of the whole file to decide if this file will be
    included in processing or not.

    :param path2file: path of data file
    :param read_start: time to start reading data from selected by user
    :param read_end: time to end reading data selected by user
    """
    with open(path2file, 'rb') as file:
        try:
            packet = file.read(8)
            # Converting BCD to string of numbers
            # Byte 3 contains the last two digits of the year
            YY = "%02X" % int.from_bytes(
                packet[3:4], byteorder='big', signed=True)

            # Converting BCD to string of numbers
            # Bytes 6 and the first half of byte 7 contain a 3-digit of DOY.
            # DOY_plus includes 4 digits from bytes 6 and 7,
            # where the first 3 digits represent the DOY.
            DOY_plus = "%04X" % int.from_bytes(
                packet[6:8], byteorder='big', signed=True)
            DOY = DOY_plus[0:3]

            # strptime() might take long to process
            # but it is only used once per file.
            epoch = UTCDateTime.strptime(f"{YY}:{DOY}", "%y:%j").timestamp
            if epoch < read_start or epoch >= read_end:
                return False
        except Exception:
            # Can't check time, return True to continue
            return True
    return True


def check_reftek_header(
        rt130: DecimatedReftek130, cur_data_set_id: Tuple[str, str],
        starttime: UTCDateTime, endtime: UTCDateTime,
        stream_header_by_data_set_id_chan: Dict[str, Dict[str, Stream]],
        cur_data_dict: Dict, cur_data_time: List[float],
        include_mp123zne: bool, include_mp456uvw: bool):
    """
    Read mseed headers of a file from the given rt130 object
        to check for time, create stream_header for retrieving gaps later.
        Requested data stream has been checked before passing to this function.

    :param rt130: RT130 object to get data stream from
    :param cur_data_set_id: Tuple of DAS serial number, experiment number of
        the current file.
    :param starttime: start of read data to skip reading actual data if not
        in range
    :param endtime: end of read data to skip reading actual data if not
        in range
    :param stream_header_by_data_set_id_chan: dict of stream header by data set
        id, chan to get gaps later
    :param cur_data_dict: waveform_data/mass_pos_data of the current
        data_set_id
    :param cur_data_time: data_time of the current data_set_id
    :param include_mp123zne: if mass position channels 1,2,3 are requested
    :param include_mp456uvw: if mass position channels 4,5,6 are requested
    """
    stream = DecimatedReftek130.to_stream(
        rt130,
        include_mp123=include_mp123zne,
        include_mp456=include_mp456uvw,
        headonly=True,
        verbose=False,
        sort_permuted_package_sequence=True)

    avail_trace_indexes = []
    for index, trace in enumerate(stream):
        chan_id = trace.stats['channel'].strip()
        samplerate = trace.stats['sampling_rate']
        if chan_id not in cur_data_dict:
            cur_data_dict[chan_id] = {'tracesInfo': [],
                                      'samplerate': samplerate,
                                      'visible': True}
        if trace.stats.npts == 0:
            #  this trace isn't available to prevent bug when creating memmap
            #  with no data
            continue
        if (starttime <= trace.stats['starttime'] <= endtime or
                starttime <= trace.stats['endtime'] <= endtime):
            avail_trace_indexes.append(index)

            if chan_id not in stream_header_by_data_set_id_chan[
                    cur_data_set_id]:
                stream_header_by_data_set_id_chan[
                    cur_data_set_id][chan_id] = Stream()
            stream_header_by_data_set_id_chan[
                cur_data_set_id][chan_id].append(trace)

            cur_data_time[0] = min(
                trace.stats['starttime'].timestamp, cur_data_time[0])
            cur_data_time[1] = max(
                trace.stats['endtime'].timestamp, cur_data_time[1])

    return avail_trace_indexes


def read_reftek_stream(
        rt130: DecimatedReftek130,
        avail_trace_indexes: List[int], cur_data_dict: Dict,
        include_mp123zne: bool, include_mp456uvw: bool):
    """
    Read traces of a file from the given rt130 object for the index in
        avail_trace_indexes.

    :param rt130: RT130 object to get data stream from
    :param avail_trace_indexes: index of traces to get
    :param cur_data_dict: waveform_data/mass_pos_data of the current
        data_set_id
    :param include_mp123zne: if mass position channels 1,2,3 are requested
    :param include_mp456uvw: if mass position channels 4,5,6 are requested
    """
    # TODO: rewrite reftek to read stream with start and end time
    stream = DecimatedReftek130.to_stream(
        rt130,
        include_mp123=include_mp123zne,
        include_mp456=include_mp456uvw,
        headonly=False,
        verbose=False,
        sort_permuted_package_sequence=True)
    for index in avail_trace_indexes:
        trace = stream[index]
        chan_id = trace.stats['channel'].strip()
        traces_info = cur_data_dict[chan_id]['tracesInfo']
        tr = read_mseed_trace(trace)
        traces_info.append(tr)


def read_mseed_trace(
        trace: Union[Trace, DiscontinuousTrace]) -> Dict:
    """
    Read reftek info in the format of mseed trace

    :param trace: mseed trace
    :return tr: dict of trace's info in which data and times are kept
    """
    tr = {}
    tr['chanID'] = trace.stats.channel
    tr['startTmEpoch'] = trace.stats.starttime.timestamp
    tr['endTmEpoch'] = trace.stats.endtime.timestamp
    tr['samplerate'] = trace.stats.sampling_rate
    if hasattr(trace.stats, 'actual_npts'):
        tr['size'] = trace.stats.actual_npts
    else:
        tr['size'] = trace.stats.npts
    """
    trace time start with 0 => need to add with epoch starttime
    times and data have type ndarray
    """
    tr['times'] = trace.times() + trace.stats['starttime'].timestamp
    if trace.stats.channel.startswith('MassPos'):
        tr['data'] = _convert_reftek_masspos_data(trace.data)
    else:
        tr['data'] = trace.data
    return tr


def _convert_reftek_masspos_data(data: np.ndarray) -> Dict:
    """
    Calculate real value for mass possition

    :param data: mass position data
    :return data that has been converted from 16-bit signed integer in which
        32767= 2 ** 16/2 - 1 is the highest value of 16-bit two's complement
        number. The value is also multiplied by 10 for readable display.
    (According to 130_theory.pdf: Each channel connects to a 12-bit A/D
    converter with an input range of +/- 10V. These channel are read
    once per second as left-justified, 2's-compliment, 16 bit values.)

    """
    return np.round_(data / 32767.0 * 10.0, 1)


def retrieve_gaps_from_stream_header(
        streams: Dict[str, Dict[str, Stream]],
        gaps_by_data_set_id_chan: Dict[Union[str, Tuple[str, str]],
                                       Dict[str, List[List[int]]]],
        gaps: Dict[str, List[List[float]]],
        gap_minimum: Optional[float],
        read_start: Optional[float],
        read_end: Optional[float]) -> None:
    """
    Retrieve gaps by data_set_id from stream_header_by_data_set_id_chan

    :param streams: dict of stream header by data_set_id, chan
    :param gaps_by_data_set_id_chan: gaps list by data_set_id and channel id
    :param gaps: gaps list by data_set_id
    :param gap_minimum: minimum length of gaps to be detected
    :param read_start: start time of data to be read
    :param read_end: end time of data to be read
    """
    for data_set_id in streams:
        sta_gaps = []
        gaps_by_data_set_id_chan[data_set_id] = {}
        if gap_minimum is None:
            continue
        for chan_id in streams[data_set_id]:
            stream = streams[data_set_id][chan_id]
            gaps_in_stream = stream.get_gaps()
            gaps_by_data_set_id_chan[data_set_id][chan_id] = stream_gaps = [
                [g[4].timestamp, g[5].timestamp] for g in gaps_in_stream
                if _check_gap(g[4], g[5], read_start, read_end, gap_minimum)]

            sta_gaps += stream_gaps
        gaps[data_set_id] = squash_gaps(sta_gaps)


def _check_gap(t1: float, t2: float, start: float, end: float,
               gap_minimum: float) -> bool:
    """
    Check if a part of the given gap in the given time range and the gap is
        greater than gap_minimum
    :param t1: start of given gap
    :param t2: end of given gap
    :param start: start of time range
    :param end: end of given time range
    :param gap_minimum: minimum length of gaps to be detected
    :return: True if check is satisfied, False otherwise
    """
    t1 = t1.timestamp
    t2 = t2.timestamp
    return (abs(t2 - t1) > gap_minimum and
            (start <= min(t1, t2) <= end or start <= max(t1, t2) <= end))


def get_total_data_points(data_dict: Dict) -> int:
    """
    Get total data points in the given data_dict

    :param data_dict: given data_dict
    :return total_data_points: total data points in the given data_dict
    """
    total_data_points = 0
    for k in data_dict:
        total_data_points += sum(
            len(data_dict[k][c]['tracesInfo'][0]['times'])
            for c in data_dict[k]
        )

    return total_data_points
