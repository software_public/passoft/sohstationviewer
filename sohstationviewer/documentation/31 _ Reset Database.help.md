
## Reset Database to Default State
If users have a need to reset database back to the default state when SOH View
is released. They can do that from menu Commands - Reset Database to Default.
After confirm their will, database will be reset back to its default state.

<br />
<br />
<img alt="Reset Database Menu" src="images/reset_database/reset_database_menu.jpg" width="400" />
<br />