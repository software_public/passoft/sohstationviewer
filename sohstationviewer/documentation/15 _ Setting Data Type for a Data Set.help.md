## Manually giving data type for data set that SOHView cannot identify its datatype
When a data set has no signature channels,  SOHView can't decide its
data type. In the file info box, 'Unknown' will be set as data type for
the dataset. User can't be able to add any channels of this data set to
database.
<br />
<br />
<img alt="Unknown Data Type in File Info Box" src="images/setting_data_type/unknown_data_type_in_file_info_box.jpg" width="250" />
<br />

To set a data type for this data set, right-click on a channel in the figure to
see the context menu. Click on "Set up Data Type for Data Set"
<br />
<br />
<img alt="Select Setup Data Type Context Menu" src="images/setting_data_type/select_setup_data_type_context_menu.jpg" width="300" />
<br />

The Setup Data Type dialog will pop up allowing user to choose one data type from
existing data type in database or create new data type to apply to this data set.
<br />
<br />
<img alt="Setup Data Type Box" src="images/setting_data_type/setup_data_type_box.jpg" width="250" />
<br />

To select from existing data type in database, check the radio button "Use existing data type".
Then select one data type from the data type combo box under that.
<br />
<br />
<img alt="Select From Existing Data Type" src="images/setting_data_type/select_from_existing_data_type.jpg" width="250" />
<br />

If the data type user want to use isn't exist, check the radio button "Create new data type".
Then type the new data type to the text box under that. The new data type entered will be added into
database when SUBMIT button is clicked.
<br />
<br />
<img alt="Create New Data Type" src="images/setting_data_type/create_new_data_type.jpg" width="250" />
<br />

To close box without doing anything, click CANCEL.

To apply changes, user will click on SUBMIT button.

If new data type has been entered, a confirmation dialog will pop up for user to confirm it.
<br />
<br />
<img alt="Confirm adding New Data Type" src="images/setting_data_type/confirm_adding_new_data_type.jpg" width="350" />
<br />

If user select from existing data type, a confirmation box wil pop up for user to confirm setting the data type.
<br />
<br />
<img alt="Confirm setting Data Type for Data Set" src="images/setting_data_type/confirm_setting_data_type_for_data_set.jpg" width="350" />
<br />

New data type will be updated in file info box.
<br />
<br />
<img alt="New Data Type updated in File Info Box" src="images/setting_data_type/new_data_type_updated_in_file_info_box.jpg" width="300" />
<br />

User can also see the new data type in Database - Add/Edit Data Types table.
<br />
<br />
<img alt="New Data Type Added" src="images/setting_data_type/new_data_type_added.jpg" width="650" />
<br />

Now the action allowing user to add channel shows up in the context menu.
<br />
<br />
<img alt="Add New Channel Context Menu" src="images/setting_data_type/add_new_channel_context_menu.jpg" width="300" />
<br />

