<br />
<img alt="Whole Section" src="images/select_soh/whole_section.png" width="270" />
<br />

# Selecting SOH Channels to Plot

---------------------------
---------------------------

From time to time, there might be a need to plot only a limited set of SOH
channels. In order to accommodate this usage, SOHViewer provides a robust
method to select the list of SOH channels to plot.

## Selecting all SOH channels
To read all SOH channels in the selected data set, check the checkbox "All SOH".

<br />
<br />
NOTE: The reading for this selection won't include channels with sample rate 
greater than 1. In order to include them in the plot, please refer to the 
sections below.
<br />
<br />
<img alt="Select all SOH channels" src="images/select_soh/all_soh_checkbox.png" height="35" />
<br />

If this checkbox is unchecked, a user-defined list of SOH channels will
be used when reading the data set.
<br />
<br />
<img alt="Current preferred SOH list" src="images/select_soh/current_soh_list.png" height="35" />
<br />

## Get a preferred SOH channel list to read
Click button "Pref" to open the "SOH Channel Preferences" dialog. Custom lists
of SOH channels to plot can be created and edited here.
<br />
<br />
<img alt="SOH Channel Preferences Dialog" src="images/select_soh/soh_channel_preferences.png" height="670" />
<br />

### SOH channel list

Each row in the table at the center of the SOH Channel Preferences dialog 
contains a list of SOH channels.
<br />
<br />
<img alt="A Row of SOH List" src="images/select_soh/full_row.png" height="85" />
<br />

Some channel lists are included by default for convenience. They are not 
editable, and can't be deleted. They are styled differently to reflect this.
<br />
<br />
<img alt="Default Lists of SOH channels" src="images/select_soh/default_channel_list.png" height="130" />
<br />

### Select an SOH channel list

Check the radio button (1) at the beginning of each row to select the list of 
channels that will be plotted. The selection will also happen when a cell in 
the table is selected.

### Create a new preferred list of channels

+ Add name of the list to the Name box (2)
+ Select a Data Type, which represents a datalogger (3)
+ Add preferred channels to Preferred SOH List box (4).  Channels are separated
by commas.
    * The list of channels can be edited directly in box (4). If needed, click on the EDIT button (5) to open a separate dialog for editing.
+ Click the CLR button (6) to clear a whole row.


### Automatically generate a list of preferred channels

**There are two ways to automatically create a preferred list of SOH channels:**
+ Add DB channels button: all SOH channels for the selected datalogger will be 
added to the Preferred SOH List box of the selected row.
+ Scan Channel from MSeed Data button: the program will scan for all available 
SOH channels in the selected MSeed data set. If no data set or an RT130 data 
set is selected, this button will be disabled. More details about this 
functionality is given below.
<br />

#### Scanning Channels from MSeed Data
When a scan is initiated by clicking on the Scan Channel from MSeed Data button, 
the program will read through all the files in the data set to gather 
information about the data set. This includes the Data Type, SOH channels, 
mass-position channels, waveform channels, and the start and end time of the data.

Once the scan is finished, the gathered information is displayed. The Preferred 
SOH List box is populated with SOH channels. Below the buttons, the mass-position 
channels, waveform channels, and non-waveform channels with sample rate greater 
than 1 are displayed in the info box. By default, the channels with sample rate 
greater than 1 are not included in the Preferred SOH List box because they are 
generally not SOH channel. They can be added manually if there is a need to plot 
them.
<br />
<br />
<img alt="Result of scanning for channels from MSeed data" src="images/select_soh/channel_scan_result.png" height="550"/>
<br />
<br />
Below the info that contains the non-SOH channels, the stations (call Data Set 
ID), and start and end time of the data are displayed. In some cases, there are 
multiple stations in a data set. If this happens, there will be an option to 
switch between the stations. The information displayed will be updated to reflect 
this change.
In the figure below, the first data set is selected.
<br />
<br />
<img alt="Channel scan result, multiple stations, first station" src="images/select_soh/scan_finished_multiple_stations_1.png" height="350"/>
<br />
<br />
In the figure below, the second data set is selected.
<br />
<br />
<img alt="Channel scan result, multiple stations, second station" src="images/select_soh/scan_finished_multiple_stations_2.png" height="350"/>
<br />

NOTE: The end time of the data is only displayed if the data is multiplexed.
<br />


### Save Preferred SOH List
+ Save button: Save all changes to database
+ "Save - Add to Main" button: Save all changes to database and make the 
selected channel list the one to be used when plotting. The name of the list 
will be displayed in the main window. The dialog will also be closed.
<br />
<br />