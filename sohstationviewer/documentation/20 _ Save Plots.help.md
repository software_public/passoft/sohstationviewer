# Save Plots

---------------------------
---------------------------

## Step 1:  click 'Save Plot'
To save the plot currently displayed a plotting window,  click 'Save Plot'.

* Saving State-of-Health plots
<br />
<br />
<img alt="Save SOH" src="images/save_plots/save_button_soh.png" height="40" />
<br />

* Saving Raw data plots
<br />
<img alt="Save Waveform" src="images/save_plots/save_button_wf.png" height="70" />
<br />
* 
* Saving Time-power-square plots:
To save the plot currently displayed on the current tab of tps dialog, click "Save Current Tab".
<br />
<br />
<img alt="Save TPS" src="images/save_plots/save_button_tps.png" height="90" />
<br />
<br />


If the current color mode is black,  user will be asked to continue or cancel 
to change mode before saving the image.

<br />
<br />
<img alt="Want to change color mode?" src="images/save_plots/question_on_changing_black_mode.png" height="150" />
<br />

* If user click 'Cancel'.  The process of saving plots will be canceled for user 
to change mode before restarting saving plots again.
* If user click 'Continue'.  The process of saving plots will be continue and the 
image will be saved in black mode.
<br />

---------------------------
## Step 2:  Edit file path and select image's format
Step 1 will bring the 'Save Plot' dialog up.

<br />
<br />
<img alt="Select Image Format dialog" src="images/save_plots/save_file_dialog.png" height="300" />
<br />

+ The default path to save the image file is preset in (1) text box.  If user 
wants to change the path,  click on 'Save Directory' button to open file dialog 
for changing path.
+ The default filename to save the image is preset in (2) text box.  User can 
change the filename in this box.
+ Inside red oval (3) are the radio buttons to select image format to save 
file.
+ For 'PNG' format,  user can change DPI which is the resolution of the 
image.  Other formats are vector formats which don't require resolution.

Then user can click 'CANCEL' to cancel saving plot or click 'SAVE PLOT' to save
the current plots to file.
<br />
<br />