## Add a new Channel
A new channel can be added directly to database
(Look at View - Edit Database Table)

When plotting, if a channel can't be found in database, its label text will be changed
to orange color and DEFAULT will prefix it to show that the channel will be plotted
with default plot type which is green line.
<br />
<br />
<img alt="Add New Channel Context Menu" src="images/add_edit_single_channel/default_channel.jpg" width="350" />
<br />

To add the channel to database, right click on the channel to see the context menu
and click "Add NEW Channel xxx" in which xxx is the channel name
<br />
<br />
<img alt="Add New Channel Context Menu" src="images/add_edit_single_channel/add_new_channel_menu.jpg" width="300" />
<br />

A dialog will pop up for user to add info for the channel. EDIT PARAMETER and
SAVE CHANNEL button will only be enable if a parameter differ from Default is
selected.
<br />
<br />
<img alt="Add New Channel Dialog" src="images/add_edit_single_channel/add_new_channel_dialog.jpg" width="500" />
<br />

Normally user shouldn't edit parameter because it will affect how to plot other channels
that use the same parameter. But in special case, they can edit the parameter or just simply
want to review how the parameter is plotted by clicking on EDIT PARAMETER. Please
refer to Edit Single Parameter section for the instruction.

When done with all the info of the channel, user can click on SAVE CHANNEL to add
the channel to database and replot the channel. If the channel showing nothing,
look at the parameter editor to see how it is plotted to figure out the problem.
<br />

## Edit a single channel
A channel that has database information will have label text in green.
If user want to edit how a channel is displayed, again, right click for context
menu and select
<br />
<br />
<img alt="Add New Channel Dialog" src="images/add_edit_single_channel/edit_channel_menu.jpg" width="300" />
<br />

A dialog will pop up for user to edit info for the channel. Default parameter won't be
allowed to select.
<br />
<br />
<img alt="Add New Channel Dialog" src="images/add_edit_single_channel/edit_channel_dialog.jpg" width="500" />
<br />

When done with all the info of the channel, user can click on SAVE CHANNEL to add
the changes to the channel to database and replot the channel. If the channel
showing nothing, look at the parameter editor to see how it is plotted to figure out
the problem.

To be easy to edit select a parameter for the channel, user should open Database -
Add/Edit Parameter dialog for view of all parameters exist in SOHViewer.
<br />
<br />