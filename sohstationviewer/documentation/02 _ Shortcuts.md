# Shortcuts

<br />

Below is a list of available keyboard shortcuts for commonly used operations and
forms.

<br />

| Shortcut | Combination |
| -------- | ----------- |
| Ctrl + I | Open Documentation Viewer                               |
| Ctrl + F | Open folder (Change data directory to another location) |
