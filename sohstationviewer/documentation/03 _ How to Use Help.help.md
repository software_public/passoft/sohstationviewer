

# How to Use Help

---------------------------
---------------------------

## How to open
Go to Menu - Help and click on "Documentation".

<br />

---------------------------
## User interface

<br />

### Search box
The box to enter search text is placed at the top of the dialog.

<br />

### Search Through All Documents button
The button to search through all documents for search text located on the
right of Search box.

<br />

### Toolbar

+ <img alt="Navigate to Table of Contents" src="images/help/table_contents.png" width="30" />    **Navigate to Table of Contents**: 
Go to 'Table of Contents' page.

+ <img alt="Recreate Table of Contents" src="images/help/recreate_table_contents.png" width="30" />    **Recreate Table of Contents**: 
If links in 'Table of Contents' are broken,  the page can be recreated by clicking this button.

+ <img alt="Search Previous" src="images/help/search_previous.png" width="30" />    **Search Previous**: 
Highlight the previous found text and the view roll to its position.

+ <img alt="Search Next" src="images/help/search_next.png" width="30" />    **Search Next**: 
Highlight the next found text and the view roll to its position.

+ <img alt="Search Through All Documents" src="images/help/search_results.png" width="30" />    **Navigate to Search Through All Documents**: 
Go to 'Search Through All Documents' result page. 

+ <img alt="Save Current Help Document" src="images/help/save.png" width="30" />    **Save Current Help Document**: 
Open a file dialog in the '~/Documents/' directory, which will prompt user to provide a name for saving the current help document. 

<br />

### Document list
Is the list of all help documents located on the left of Help Dialog.

<br />

### Document View
Occupies the largest section of Help Dialog located on the right to display
the content of the current selected document.

<br />

---------------------------
## How to search in Help Dialog

<br />

### Search for a text on the current document
Type a searched text, the first text found will be highlighted in the Document
View and the view scrolls to its location.

User can traverse back and forth the view to look for previous or next search
text using **Search Previous** or **Search Next** button.

<br />

### Search for all documents that contain the searched text
Type a searched text, click the button 'Search Through All Documents'.

A list of all documents that contain the searched text will be created in
a document called 'Search Results' which will be brought to Document View.

Click on the link of a document in 'Search Results' will bring it to 
Document View.

User can go back to 'Search Results' document any time by clicking 
on button <img alt="Search Next" src="images/help/search_results.png" width="30" /> on the toolbar.

<br />
