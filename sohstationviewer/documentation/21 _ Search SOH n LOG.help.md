# Search Messages
<br />
<br />
<img alt="Search Dialog" src="images/search_messages/search_dialog.png" width="700" />
<br />
<br />
---------------------------
---------------------------

# How to open
Search Messages dialog will open by itself after a data set are done with reading and plotting.

If it is closed by any reason,  user can open it by go to Menu - Forms and click on "Search Messages"

<br />

---------------------------
# User interface

<br />

## Search box
The box to enter search text is placed at the top of the dialog.

<br />

## Toolbar

+ <img alt="To Selected" src="images/search_messages/to_selected.png" width="30" />    **To Selected**: The current table rolls back to the current selected line.

+ <img alt="Search Previous" src="images/search_messages/search_previous.png" width="30" />    **Search Previous**: The current table rolls to the previous line that contains the searched text.

+ <img alt="Search Next" src="images/search_messages/search_next.png" width="30" />    **Search Next**: The current table rolls to the next line that contains the searched text.

+ <img alt="Save" src="images/search_messages/save.png" width="30" />    **Save**: Save the content of the table to a text file.

<br />

## Tabs

+ **Search SOH Lines** tab: to list all lines that includes searched text with
its channel 
<br />
<br />
<img alt="Search SOH Lines" src="images/search_messages/search_soh_lines.png" width="700" />
<br />
<br />

+ **Processing Logs** tab: to list all logs of data and plotting processing 
with its log type and color of each line depends on its log type.
<br />
<br />
<img alt="Processing Log" src="images/search_messages/processing_log.png" width="700" />
<br />
<br />

+ Each of the following tab is for an SOH log channels
<br />
<br />
<img alt="An SOH Log Channel" src="images/search_messages/soh_log_channel.png" width="700" />
<br />
<br />
 **MSeed**: For a channel that are wrapped in MSeed format, the log messages of the channel will be
nested in tab of the channel name. For a text log file under the folder of a station name, the log will only
be displayed when the station is selected and the tab name will be 'TXT'. For a text log file that isn't 
under any station name folder, the log will be display for any selected station and the tab name will be 'TEXT'.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**RT130**: All the SOH messages 
of the selected set will be displayed under tab 'SOH'.
<br />


## Info box
The box at the bottom to display the info of the selected line.

<br />

---------------------------
# How to search

<br />

## Search for a text 
Type a searched text,  the searched text will be highlighted through the table
and the current table scrolls to the first line that contains the searched text.

User can traverse back and forth the current tab to look for previous or next
search text using **Search Previous** or **Search Next** button.

<br />

## Filter all lines with searched text
The first tab **Search SOH Lines** is for filtering all lines that contains
the searched text.

<br />

## Interaction with RT130 SOH channel's data point
When user click on a clickable data point on a SOH channel of RT130,  SOH tab
will be focused and the line corresponding to the data point will be brought
up to view.

<br />
<br />