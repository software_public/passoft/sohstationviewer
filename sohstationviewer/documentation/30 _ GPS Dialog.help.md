# GPS Dialog

---------------------------
---------------------------

## How to open
The GPS dialog can be opened by using the Commands > GPS Plotter option in the
menu bar.

<img alt="Open GPS dialog using the Commands menu"
src="images/gps_dialog/open_gps_dialog.jpeg" height="100"/>

<br />

---------------------------
## User interface

<img alt="Open GPS dialog using the Commands menu"
src="images/gps_dialog/gps_dialog_ui.jpeg" height="500"/>

### Export directory selector

Composed of a button to select the directory to export the GPS data to and a
textbox displaying that directory.

### GPS plot window
The window where the GPS points extracted from a data set will be plotted.  The
style of the plot depends on the background color chosen in the main window.

<br />

### Button bar

Contains three buttons:

+ **Read/Plot**: Read the GPS data and then plot it.
+ **Export**: Export the GPS points into a tab-separated file.
+ **Close**: Close the GPS dialog.

<br />

### Notification/info box

The box to display notifications or the information of a GPS data poin,

<br />

---------------------------
## How to use

<br />

### Plotting GPS data of a data set

Once a data set has been loaded,  its GPS data can be plotted by clicking the
Read/Plot button.

Each GPS data point is plotted as a small rectangle.

Once the plotting finishes,  a message is displayed in the notification with
the number of data points plotted.

Furthermore,  the range of latitude and longitude of the GPS data is displayed
on the plot.

<br />

### Displaying a GPS data point's information 

When the GPS data finished plotting, clicking on a point on the plot will
display its data in the notification box.

<br />

### Exporting the GPS data

By clicking the Export button,  the GPS data will be exported to a file.  The
name of the exported file is the name of the data set suffixed with
```.gps.dat```.  By default,  the file will be in the same directory as the data
set.  A sample directory structure is given below.
```
Data/
├─ data.data/
└─ data.data.gps.dat
```

The directory the export file is stored in can be changed by clicking on the
**Export Directory** button.

<br />
<br />
<img alt="Change export directory" src="images/gps_dialog/click_on_export_directory_button.jpeg" height="500"/>
<br />
<br />
<img alt="Change export directory" src="images/gps_dialog/after_click_on_export_directory_button.jpeg" height="600"/>
<br />
