# Select Main Data Directory
* To change Main Data Directory to where folders for different data set are nested, 
click button 'Main Data Directory' (1). A file dialog will pop up to select the main 
main directory.
* The path of the main directory will be displayed in (2) box.
* All folders under the main directory will be displayed in the directory list (3).
* 
<br />
<img alt="From Memory Card checkbox" src="images/read_data/select_main_data_directory.png" height="300" />
<br />
<br />

# Read data from selected folders:
After selecting the option needed, users have two ways to read data from selected folders:
* Double-click on one folder in the directory list box, the reading will be started
* Select one by left-click on a folder in the directory list box 
or select more than one folder by ctrl + left-click on different folders in 
the list box, then click button 'Read Files' to start reading
* 
<br />
<img alt="From Memory Card checkbox" src="images/read_data/read_files_button.png" height="200" />
<br />
<br />

# Read data from external memory card:
To read data from a memory card,  navigate to the desired drive by clicking on 
'Main Data Directory' button and check the checkbox "From Memory Card".

Directory List box will display "Memory Card". Click 'Read Files' to read data 
from the memory card. *selecting 'Memory Card' in the directory list box isn't 
necessary *

There are different kinds of memory cards that can be used in SOHViewer,  eg.  SD 
card,  CF card or flash drive.
<br />
<br />
<img alt="From Memory Card checkbox" src="images/read_data/from_memory_card_checkbox.png" height="50" />
<br />

## Baler data: data/ or sdata/
Baler data has data/ folder for main data and sdata/ folder for backup
data.  When Baler memory card is selected,  the radio buttons for data/ and sdata/
are enabled for user to select one of the folder to read from.

<br />
<br />
<img alt="data/sdata radio buttons" src="images/read_data/data_sdata_radio_buttons.png" height="60" />
<br />

If a Baler folder is selected to read like a normal data folder,  a dialog will 
pop up allow user to select one of data/ or sdata/ folder to read.

<br />
<br />
<img alt="data/sdata selecting dialog" src="images/read_data/data_sdata_selecting_dialog.png" height="170" />
<br />

## RT130
When an RT130 folder is selected as the Main Data Directory,  the serial numbers of the 
RT130 DASs will be listed for user to select for reading.

<br />
<br />
<img alt="DAS in CF Card" src="images/read_data/das_in_cf_card.png" height="120" />
<br />

Note: DASs list will be displayed in the File List box no matter the From Data
Card checkbox is checked or not.
<br />

# Limit time period to plot
<br />
<img alt="DAS in CF Card" src="images/read_data/date_editor_calendar.png" height="440" />
<br />

If only a specific time period in a data set needs to be plotted, the program can 
limit plotting to only that time period. This is done by using the From and To Date 
editors, located near the top right corner of the main window. In order to select 
a date, click on the dropdown button. A calendar will be displayed, and the date 
can be chosen. If the day of year is desired, check the Show DOY box and the day 
of year will be displayed for each day in the calendar. 

NOTE: the format the chosen date is shown in can be changed by changing the date 
format in the Options menu (see the Options Menu section).
<br />
<br />
