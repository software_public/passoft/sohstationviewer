## Edit a Single Parameter
It is recommended to edit parameters in Database - Add/Edit Parameters dialog.
However if user click on EDIT PARAMETER in Add/Edit a Single Channel dialog,
they can view or edit parameter from there.

A dialog will pop up for user to edit info for the parameter. Default parameter won't be
allowed to select.
<br />
<br />
<img alt="Edit Single Parameter Dialog" src="images/edit_single_parameter/edit_parameter_dialog.jpg" width="400" />
<br />
<br />

Notice that if the height is different to the current channel's height, to apply the
new height, user need to RePlot all the channels.
