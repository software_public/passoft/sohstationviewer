# Search List of Directories

---------------------------
---------------------------

## User Interface

The search bar is located above the list of directories in the main data
directory.  When there is text in the search bar,  a button that clears the
search will show up.

---------------------------

## How to use

Whenever the content of the search bar is updated,  the list of current
directory will be filtered.  The result is displayed above the directory list
as a list of directories that match the search text,  prefaced with the text 
"Found files:".

<br />
<br />
<img alt="Search results" src="images/search_directories/search_result.jpeg"
 height="280" />
<br />

There are two methods to clear the search.  The first one is to click on the
clear button that appears on the right edge of the search bar.  The second one
is to delete the content of the search bar.  Either way,  the directory list is
reset and the search bar is cleared.

<br />
<br />
<img alt="Clear search" src="images/search_directories/clear_button_pressed.jpg"
 height="280" />
<br />

