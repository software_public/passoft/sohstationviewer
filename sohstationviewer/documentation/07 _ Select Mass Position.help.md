# Selecting Mass Position
Users can either select mass position according to group of channels 1,2,3 or 4,5,6
or both by checking the checkboxes as displayed.
<br />
<br />
<img alt="Select Mass Position" src="images/select_masspos/select_masspos.png" height="30" />
<br />

---------------------------
---------------------------

## Warning when the selected mass position channels not found
If the selected mass position channel cannot be found,  a warning will be
created in Processing Logs.
<br />
<br />
<img alt="Select Mass Position" src="images/select_masspos/warning_req_masspos_not_exist.png" height="70" />
<br />