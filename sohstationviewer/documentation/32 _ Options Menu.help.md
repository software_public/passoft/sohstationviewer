## Options Menu

<br />
<br />
<img alt="Reset Database Menu" src="images/options_menu/options_menu.jpg" width="500" />
<br />
<br />

### Warn big file size

User can set checked or unchecked the option "Warn big file size" to select the
option before doing the task Read Files.

Once "Warn Big file size" is checked, the size of the data set will be checked
to see if the data needed to be processed is big and it will take a long time to
process it. In that case, a warning will pop up for user to continue with the
selection or can change the selected channels to be plotted. Taking off reading
waveform channels for plotting TPS and RAW data is the effective way to speed
up the processing.

<br />
<br />
<img alt="Big Size Warning" src="images/options_menu/big_size_warning.jpg" width="280" />
<br />
<br />

### Mass Position Colors

User can select mass position color from two options: regular and trillium.

<br />
<br />
<img alt="Mass Position Colors" src="images/options_menu/mass_position_colors.jpg" width="600" />
<br />
<br />

+ Regular:
  * Value <= 0.5: Cyan
  * 0.5 < Value <= 2.0: Green
  * 2.0 < Value <= 4.0: Yellow
  * 4.0 < Value <= 7.0: Red
  * Value > 7.0: Magenda

+ Trillium:
  * Value <= 0.5: Cyan
  * 0.5 < Value <= 1.8: Green
  * 1.8 < Value <= 2.4: Yellow
  * 2.4 < Value <= 3.5: Red
  * Value > 3.5: Magenda

When processing the data, SOHViewer will use the selected mass position colors
to plot mass position channels.
User can select the other option then click on RePlot to apply the new choice
in plotting.

<br />
<br />

### Add Mass Positions to RT130 SOH Messages

This is the option to add mass positions' information to the RT130's SOH messages.

This option needs to be selected before data is read and the mass position can be
seen at the end of Search Messages dialog - SOH tab.

<br />
<br />

### Date Format

Allow user to select the format to be displayed for date on plotting figure and
From, To dates.

<br />
<br />
<img alt="Date Format" src="images/options_menu/date_format.jpg" width="620" />
<br />

There are three options to select from:

+ YYYY-MM-DD: Ex. 2023-03-01
+ YYYY-DOY: Ex. 2023-201
+ YYYYMMMDD: Ex. 2023MAR01
+ 
<br />
<br />


### Base Font Size

Allow user to select the font size of texts displayed in the plotting. This
option has to be selected before reading data or replot data.

<br />
<br />
<img alt="Base Plot Font Size" src="images/options_menu/base_plot_font_size.jpg" width="530" />
<br />
