## Setting Gap Length
To display gaps,  add minimum length of gaps in Minimum Gap Length box.  If no
length set,  gap bar will not show up.
<br />
<br />
<img alt="Select Mass Position" src="images/gaps/gap_length_box.png" height="30" />
<br />

---------------------------
---------------------------

## Color
Gap will be displayed in red color.
<br />
<br />
<img alt="Gap Color" src="images/gaps/gap.png" height="60" />
<br />

Overlap will be displayed in orange color.
<br />
<br />
<img alt="Overlap" src="images/gaps/overlap.png" height="60" />
<br />