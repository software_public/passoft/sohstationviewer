## Hide, show, reorder channels
To Select an ordered list of SOH channels to plot before reading the data set, refer
to "Select SOH" section.

Once all channels are plotted, user can reorder, hide or show SOH channels or
waveform channel by right-clicking anywhere on a channel and select "Select Channels
to show" from context menu.
<br />
<br />
<img alt="Select Channel to show from Context Menu" src="images/hide_show_reorder_channels/select_context_menu.jpg" width="250" />
<br />

A "Show/Hide and Reorder Channels" dialog will pop up.

As stated in the dialog, user can show channels by checking the checkboxes in front of
the channels and hide channels by unchecking the checkboxes. In the picture, HH2 and VM2
are unchecked and will be hidden when APPLY is clicked.
<br />
<br />
<img alt="Check to show, uncheck to hide channels" src="images/hide_show_reorder_channels/check_to_show_uncheck_to_hide.jpg" height="300" />
<br />

User can also drag and drop to reorder the channels. In the picture, HHZ has been moved to
the top of the list and it will show on top of other channels in the figure when APPLY is clicked.
<br />
<br />
<img alt="Reordered channels" src="images/hide_show_reorder_channels/reorder_channels.jpg" height="300" />
<br />

Note that mass position channels are plotted in order already and won't be reorderable.

The following is the result of hide/show, reorder tasks.
<br />
<br />
<img alt="Reordered channels" src="images/hide_show_reorder_channels/hide_show_reorder_result.jpg" width="600" />
<br />

