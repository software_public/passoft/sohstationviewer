# Ruler
Ctrl/Cmd + Click on a point in a plotting, a yellow ruler will show up with time on top.

Rulers will also show up on other plotting dialog at the corresponding position 

On TPS plotting a yellow square box will show up to mark the corresponding data points on
all the TPS channels. Ctr/cmd + Click on TPS plot will work the same.

<br />
<br />
<img alt="Ruler" src="images/ruler_and_zooming/ruler.png" width="600" />
<br />
<br />

---------------------------
---------------------------

# Zooming
<br />

## Zoom in
Shift + Click on a plot to mark the first zooming point, then Shift + click again to mark 
the second zooming point to zoom the area between two zooming points.

The zooming task will be performed in the area corresponding to zooming triggering 
dialog.

For TPS, zooming won't be performed but mark with 2 orange bars. Zooming can't 
be triggered from TPS dialog.

<br />
<br />
<img alt="Ruler" src="images/ruler_and_zooming/tps_zooming.png" width="600" />
<br />
<br />

## Zoom out
Shift + Click on the left side of a plot to zoom out one level, which means
undoing one zoom in. 
<br />
<br />