## ValueColor Widget
ValueColor Widget is a widget to edit colors for plotting channels.
It can be found in Database - Parameter table or Edit Parameter dialog.

The following is Parameter table dialog
<br />
<br />
<img alt="Edit/Add/Delete Parameters Dialog" src="images/edit_value_color_edit/edit_add_delete_parameters.jpg" width="750" />
<br />

The following is Edit Parameter dialog
<br />
<br />
<img alt="Edit Single Parameter Dialog" src="images/edit_value_color_edit/edit_parameter_dialog.jpg" width="400" />
<br />

When selecting a new plot type for the parameter,

 + If the current value color string isn't compatible with the plot type,
the default value color string will show up in ValueColor Widget.
 + If there's no value color string show up in ValueColor Widget,
colors aren't editable for the selected plot type.

<br />
<br />

## Edit ValueColor

To edit ValueColor, click on pencil icon on the right of the widget. Then
a dialog corresponding to the selected plot type will pop up to edit the value
color string.

The general buttons in the dialog to edit value color string:

 + Select Color: To edit Color. The color selected will be displayed in the box on
its left.
 + SAVE COLORS: Create new value color string according to the setting, close the
dialog and display the new string in the ValueColor widget.
 + CANCEL: Discard changes and close the dialog

<br />
<br />

### Dialog to edit linesDots plot type's value color string

<br />
<br />
<img alt="Dialog to edit linesDots plot type's value color string" src="images/edit_value_color_edit/line_dots_value_color.jpg" width="400" />
<br />

Three items are defined for this plot type.

 + Line color is required. The channel will be plotted as a line through all
data points of the channel. Line color defines the color of the channel's line.
The label on the right will display the total of all data points in the selected view.
 + Dot color is optional. Selected by setting checked on the checkbox Included
in front of Dot. If included, a dot will be displayed at each data points.
 + Zero color is optional. Selected by setting checked on the checkbox Included
in front of Zero. If included, the zero value data point won't be included in the
line/dot but display as a dot separately with the defined color. There will be
three labels on the right of the channel. The top one displays the total of data
points with value greater than 0. The middle one displays the total of data points
with value 0. The top one displays the total of data oints with value less than 0.

<br />
<br />

### Dialog to edit upDownDots plot type's value color string

<br />
<br />
<img alt="Dialog to edit upDownDots plot type's value color string" src="images/edit_value_color_edit/up_down_value_color.jpg" width="400" />
<br />

Two items are defined for this plot type to plot the channel with two values: 1 and 0

 + Up color is required. All data points with value 1 will be plotted as dots above the
center line with the defined color for Up.
 + Down color is required. All data points with value 0 will be plotted as dots under the
center line with the defined color for Down.

<br />
<br />

### Dialog to edit triColorLines plot type's value color string

<br />
<br />
<img alt="Dialog to edit triColorLines plot type's value color string" src="images/edit_value_color_edit/tri_color_lines_value_color.jpg" width="400" />
<br />

Three base lines will be plotted to display this plot type.

Three items are defined for this plot type to plot the channel with three values: 1, 0 and -1

 + 1 color is required. All data points with value 1 will be plotted as dots on the
first line with the defined color for '1'.
 + 0 color is required. All data points with value 0 will be plotted as dots on the
second line with the defined color for '0'.
 + -1 color is required. All data points with value -1 will be plotted as dots on the
third line with the defined color for '-1'.

<br />
<br />

### Dialog to edit multiColorDotsEqualOnUpperBound plot type's value color string

<br />
<br />
<img alt="Dialog to edit multiColorDotsEqualOnUpperBound plot type's value color string"
     src="images/edit_value_color_edit/multi_color_dots_equal_on_upper_bound_value_color.jpg" width="550"
/>
<br />

User can define up to 7 colors corresponding to 7 ranges of values in this plot type.

All data points are plotted in the center line with colors represents its values.

 + If the Include checkbox in the first row is checked, the range less than or equal
to the defined value will be plotted with the defined color. If it is unchecked,
the color in the range won't be plotted, and the 'Select Color' button of the row
will be disabled.
 + Any of the middle rows define the color for the range defined on that row. It
is required to add value in the increasing order and not skipping rows. Warning
will be given if the rules are broken.
 + The greatest value will be added automatically to the last row to define the
color greater than the greatest value. However it is optional to include it or not.

<br />
<br />

### Dialog to edit multiColorDotsEqualOnLowerBound plot type's value color string

<br />
<br />
<img alt="Dialog to edit multiColorDotsEqualOnLowerBound plot type's value color string"
     src="images/edit_value_color_edit/multi_color_dots_equal_on_lower_bound_value_color.jpg" width="550"
/>
<br />

User can define up to 7 colors corresponding to 7 ranges of values in this plot type.

All data points are plotted in the center line with colors represents its values.

 + If the Include checkbox in the first row is checked, the range less than
the defined value will be plotted with the defined color. If it is unchecked,
the color in the range won't be plotted, and the 'Select Color' button of the row
will be disabled.
 + Any of the middle rows define the color for the range defined on that row. It
is required to add value in the increasing order and not skipping rows. Warning
will be given if the rules are broken.
 + The greatest value will be added automatically to the last row to define the
color equal to the greatest value. However it is optional to include it or not.

<br />
<br />
