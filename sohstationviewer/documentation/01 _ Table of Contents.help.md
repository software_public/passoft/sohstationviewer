# SOHViewer Documentation

Welcome to the SOHViewer documentation. Here you will find usage guides and other useful information in navigating and using this software.

On the left-hand side you will find a list of currently available help topics.

If the links of the Table of Contents are broken, click on Recreate Table of Content <img src='recreate_table_contents.png' height=30 /> to rebuild it.

The home button can be used to return to this page at any time.

# Table of Contents

+ [Table of Contents](01%20_%20Table%20of%20Contents.help.md)

+ [How to Use Help](03%20_%20How%20to%20Use%20Help.help.md)

+ [Search List of Directories](04%20_%20Search%20List%20of%20Directories.help.md)

+ [Read Data](05%20_%20Read%20Data.help.md)

+ [Select SOH](06%20_%20Select%20SOH.help.md)

+ [Select Mass Position](07%20_%20Select%20Mass%20Position.help.md)

+ [Select Waveforms](08%20_%20Select%20Waveforms.help.md)

+ [Gap Display](09%20_%20Gap%20Display.help.md)

+ [RT130 Log Files](10%20_%20RT130%20Log%20Files.help.md)

+ [TPS Dialog](11%20_%20TPS%20Dialog.help.md)

+ [Display Data Point Info](12%20_%20Display%20Data%20Point%20Info.help.md)

+ [Ruler and Zooming](13%20_%20Ruler%20and%20Zooming.help.md)

+ [Hide, show, reorder channels](14%20_%20Hide,%20show,%20reorder%20channels.help.md)

+ [Setting Data Type for a Data Set](15%20_%20Setting%20Data%20Type%20for%20a%20Data%20Set.help.md)

+ [Add or Edit a Single Channel](16%20_%20Add%20or%20Edit%20a%20Single%20Channel.help.md)

+ [Edit a Single Parameter](17%20_%20Edit%20a%20Single%20Parameter.help.md)

+ [Edit valueColor for a Parameter](18%20_%20Edit%20valueColor%20for%20a%20Parameter.help.md)

+ [Plot Different Data Set ID](19%20_%20Plot%20Different%20Data%20Set%20ID.help.md)

+ [Save Plots](20%20_%20Save%20Plots.help.md)

+ [Search SOH n LOG](21%20_%20Search%20SOH%20n%20LOG.help.md)

+ [GPS Dialog](30%20_%20GPS%20Dialog.help.md)

+ [Reset Database](31%20_%20Reset%20Database.help.md)

+ [Options Menu](32%20_%20Options%20Menu.help.md)

+ [View-Edit Database Tables](33%20_%20View-Edit%20Database%20Tables.help.md)

+ [Firmware Notice](40%20_%20Firmware%20Notice.help.md)

