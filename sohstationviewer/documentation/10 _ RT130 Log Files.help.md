# Reading RT130 Log Files

SOHStationViewer inherits the ability to read and generate log files from
logpeek.  Log files contain the state of health data,  event information,  and
(optionally) mass-position data of an RT130 data set.  They are useful for
remote troubleshooting of RT130 stations due to the ease of sending them over
the internet.  You can generate log files using logpeek,  rt2ms,  or this
program.

---------------------------
---------------------------

## Reading RT130 log files

In order to read RT130 log files,  check the log checkbox next to the file
search bar.  After doing this,  the program will display a list of log files.
You can read a log file the same way you read a directory.  Currently,  only
files with the .log extension are counted as log files.

<br />
<br />
<img alt="Search results" src="images/log_files/toggle_log_checkbox.png"
 height="240" />
<br />

When reading log files, the option to read from memory cards is disabled. 

<br />
<br />
<img alt="Search results" src="images/log_files/disabled_from_memory_card.png"
 height="280" />
<br />

The list of log files can be filtered using the search bar.

<br />
<br />
<img alt="Search results" src="images/log_files/search_log_files.png"
 height="240" />
<br />

---------------------------

## Generating RT130 log files

RT130 log files can be generated using the Search Messages dialog.  This is
accomplished by saving the SOH table of an RT130 data set.  The Search Messages
dialog saves a table in a file with extension .log,  which means that the saved
log file will be recognized in SOHStationViewer by default.  For information
about saving a table in the Search Messages dialog,  refer to the "Search SOH n
Log" section.

Refer to the "Options Menu" section for information to add mass position to 
RT130 SOH messages.
