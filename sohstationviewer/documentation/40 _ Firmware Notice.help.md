# Firmware notices

---------------------------
---------------------------

## Old Pegasus firmwares

Some old Pegasus firmwares record a number of SOH channels in different units. 
These channels included:
+ VEI: microvolts in old firmware, millivolts in new firmware
+ VDT: microdegrees C in old firmware, centidegrees C in new firmware
+ VM1-6: microvolts in old firmware, millivolts in new firmware

Currently, SOHViewer only supports new versions of the Pegasus firmware. If the 
plotted values of the channels listed above are higher than expected, the data 
is recorded using old firmwares. This problem can be fixed by editing the 
affected channels' conversion factor to reflect the old units (refer to the 
View-Edit Database Tables section).
