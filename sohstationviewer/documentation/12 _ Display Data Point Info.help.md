# Display info
Click on any data point to display the info of the data point in the info box 
at the bottom of each plotting dialog
<br />
<br />

## Mseed SOH data

<br />
<br />
<img alt="MSeed SOH data info" src="images/display_data_point_info/mseed_soh_data.png" width="600" />
<br />
<br />

## Waveform data

<br />
<br />
<img alt="Waveform data info" src="images/display_data_point_info/waveform_data.png" width="600" />
<br />
<br />

## TPS data
Click on one tps channel will display info at the same position for all tps channels no matter 
the tps channels are in the same tab or in different tabs.

<br />
<br />
<img alt="Waveform data info" src="images/display_data_point_info/tps_data.png" width="600" />
<br />
<br />

## RT130 SOH data
Click on one SOH data point will highlight the SOH message corresponding to that line.

<br />
<br />
<img alt="MSeed SOH data info" src="images/display_data_point_info/rt130_soh_data.png" width="600" />
<br />
<br />
