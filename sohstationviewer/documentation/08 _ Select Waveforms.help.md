<img alt="Whole Section" src="images/select_waveform/whole_section.png" width="290" />
<br />

# Selecting Waveforms

---------------------------
---------------------------

## Selecting all waveform channels
Check the checkbox "All Waveform Channels"
<br />
<br />
<img alt="Select all waveform channels" src="images/select_waveform/check_all_waveform_channels.png" height="40" />
<br />

## Selecting no waveform channels
Unchecked the checkbox "All Waveform Channels",  unchecked all "DSs" 
checkboxes,  clear textbox "MSeed Wildcards".
<br />
<br />
<img alt="Select no waveform channels" src="images/select_waveform/check_none_waveform_channels.png" width="300" />
<br />

## Selecting data streams for Reftek
Check some of "DSs" checkboxes with the index corresponding to the data
streams you want to select.
<br />
<br />
<img alt="Select data streams" src="images/select_waveform/select_data_stream.png" width="300" />
<br />

If some "DSs" are checked,  but data type isn't Reftek,  a warning will be created, 
"Checked data streams will be ignored for none-RT130 data type."

## Selecting mseed's waveform
User can add different wildcards separated with commas to MSeed Wildcard
textbox.  For example: \*, LH*, L*1.

<br />
<img alt="Select MSeed wildcards" src="images/select_waveform/mseed_wildcards.png" width="300" />
<br />

If some wildcard are added,  but data type is Reftek,  a warning will be created, "Checked data streams will be ignored
for RT130 data type."

## Displaying waveform channels
TPS needs to be checked to display Time-Power-Squared of waveform.
RAW needs to be checked to display actual signal of waveform.
+ <img alt="TPS" src="images/select_waveform/select_TPS.png" height="30" />: to diplay Time-Power-Squared of the selected waveform data 
+ <img alt="RAW" src="images/select_waveform/select_RAW.png" height="30" />: and check RAW to display the actual selected waveform data.
<br />

If any of waveform is checked but no TPS or RAW is checked,
+ For RT130, the program will read event of the selected data stream.
+ For MSeed, the program will pop up message request user to clear waveform selection or select either TPS or RAW.