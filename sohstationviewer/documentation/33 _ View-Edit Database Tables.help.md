# View/Edit Database Tables 

To view/edit database a table,  select the table in database menu.
<br />
<br />
<img alt="Database Menu" src="images/database_tables/database_menu.png" width="250" />
<br />

---------------------------
---------------------------
## Plot Types Table

List all types of plotting and description.  This table is readonly.
<br />
<br />
<img alt="Plot Types Table" src="images/database_tables/plot_types.jpg" width="750" />
<br />

+ Fields' meaning:
    + Plot Type: Name of the plotting type
    + Description: Describing how the channel will look like and the format
of the ValueColors if applicable.
<br />
<br />


## Data Types Table

List all data types available in which Default is the datatype for DEFAULT 
channel to draw any channels that aren't defined in database.
<br />
<br />
<img alt="Data Types Table" src="images/database_tables/data_types.jpg" width="750" />
<br />
<br />


## Parameters Table

List all parameters available.
<br />
<br />
<img alt="Parameters Table" src="images/database_tables/parameters.jpg" width="750" />
<br />

+ Fields' meaning:
  * No.: Line number,  start with 0.
  * Param: Parameter's name.
  * Plot Type: Define how to plot the parameter.  Description of Plot Type
can be found in Plot Types Table.
  * ValueColors: mapping between values/dot/line/up/down and color,  or
simply color of dots in the plots. When Plot Type is changed, if the current valueColors in
database is in incorrect format, default valueColors will be used. To edit Value Color, click
on the pencil sign on the right of ValueColors widget.
  * Height: Height of plot in compare with other plots.
  * Reset to org. DB: Button to reset the changes in the current DB to backup (original) DB.
  * Delete: button to delete a row from the
+ For each color mode available, a row in the database has a separate value for
ValueColors.  In order to change between the color mode, use the selector at
the bottom right of the dialog.  The color mode chosen when the Parameters table
is opened is the one chosen in the main window.  The ValueColor widget has the background
color matching with the selected background color so user can see the real effect of the
selected color on the background.
<br />
<br />

<img alt="Parameters Table" src="images/database_tables/parameters_change_color_mode.jpeg" width="750" />

<br />


## Channels Table

List all channels available for the selected Data Type on the left-top corner.
<br />
<br />
<img alt="A Row of SOH List" src="images/database_tables/channels.jpg" width="750" />
<br />

+ Fields' meaning:
    + No.: Line number, start with 0.
    + Channel: Channel's name.
    + Label: Displayed with the channel to explain the meaning of the channel.
    + Param: To describe what kind of parameter the channel is. How the channel is plotted depends on this field.
    + ConvertFactor: To convert from value read to actual display.
    + Unit: The unit of the displayed value for the channel.
    + FixPoint: Decimal point to display the value.
+ Channels that aren't defined in database will be draw with DEFAULT channel defined in Data Type Default.
<br />
<br />
<img alt="DEFAULT channel" src="images/database_tables/default_channel.png" width="750" />
<br />


## Common Widgets
+ ADD ROW: Button to add a new row to the table,  click on ADD ROW button.
+ SAVE CHANGES: To save any changes to database.  Each row will be validated before saved.
+ RESET: To reset changes during open time.
<br />
<br />
<img alt="ADD, SAVE, RESET buttons" src="images/database_tables/add-save-reset.jpg" width="550" />
<br />
+ Reset to org. DB: To reset the changes in the current DB to backup (original) DB on a row.
This button is only enabled if the row has a backup row and the backup row is different from
the content of the row in current DB.  Once Reset is performed,  the button will turn to 
Undo Reset button which allows user to change the row back to its current saved content in
the database.  Changes made by this button is only saved to DB if SAVE CHANGES is clicked.
<br />
<br />
<img alt="Reset to org. DB buttons" src="images/database_tables/reset-undoreset.jpg" width="750" />
<br />
+ DELETE: To delete a row from database and remove it from GUI.  The button will only be enabled
if the row have no constrain with other tables.  Once Delete is performed, the button will turn
to Undo button which allows user to add the row back to the GUI.  Changes made by this button is only
saved to DB if SAVE CHANGES is clicked.
<br />
<br />
<img alt="DEFAULT channel" src="images/database_tables/undo-delete.jpg" width="750" />
<br />
<br />