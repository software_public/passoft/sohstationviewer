## Plotting different data set id
There is a chance that the selected data set has more than one id defined by 
station id in MSeed data or (serial number, network code) in Reftek.

<br />

Button "Plot Different Data Set ID" has been implemented to allow user select a 
different data set id after data from selected files are plotted.

<br />
<br />
This button is disabled by default.
<br />
<br />
<img alt="Plot Different Data Set ID button disabled" src="images/plot_different_data_set_id/plot_different_data_set_id_button_disabled.png" height="60" />
<br />
<br />

After data are loaded, if the data set has more than one id, user will be 
asked to select one data set id to plot.  User will click on one of the data set id 
buttons to plot a data set.
<br />
<br />
<img alt="Select One Data Set ID to Display dialog" src="images/plot_different_data_set_id/select_one_data_set_id_to_display_dlg.png" height="120" />
<br />
<br />

The button "Plot Different Data Set ID" will be enabled.
<br />
<br />
<img alt="Plot Different Data Set ID button enabled" src="images/plot_different_data_set_id/plot_different_data_set_id_button_enabled.png" height="60" />
<br />
<br />

Now user can click on the button to access a dialog that allows user to select 
another data set id for plotting. User can click on one of the id buttons to plot  
or click Abort to cancel.
<br />
<br />
<img alt="Select a Different Data Set ID to Replot" src="images/plot_different_data_set_id/select_a_different_data_set_id_to_replot_dlg.png" height="110" />
<br />
<br />