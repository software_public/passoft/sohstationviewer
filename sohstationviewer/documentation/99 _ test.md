Document Title?
===
# Testing markdown features supported by Qt
To show this file in help, change extension to ".help.md"
[Links to other documents?](01 _ Table of Contents.md)
1. Numbered List
    1. Sublist
    2. Another Entry
        1. Another Sublist

* Bulleted list
    * Sublist
        * Another Sublist

# Section
## Subsection
### Sub-subsection
#### Subsections all the way down

*Emphasis*

**Bold**

***Bold-Emphasis***

~strikethrough?~

> This is a quote

```
printf("%s\n", codeFences.doTheyWork ? "Success!" : "Oof.");
```

```c
printf("%s\n", syntaxHighlighting.doesItWork ? "Success!" : "Oof.");
```

---
^ This is a horizontal line

v This is an image
![An Image?](recreate_table_contents.png)

---
Another horizontal line

Is there a footnote? [^1]

[^1]: Maybe.

Definition Lists
: Do they work?

Task Lists? 
- [ ] Yes
- [ ] No
- [x] Maybe

==Highlighting==? Nope.

Subscripts? x~0 Nope.

Superscripts? ax^2 + bx + c = 0. Also Nope.

Html?
x<sub>0</sub> Yup.
x<sup>2</sup> Yup.

Tables?

~Not supported.~
Just kidding, they totally are, and I just didn't do it right.

| Is this | a table?     |
| ------- | ------------ |
| Perhaps | Perhaps not. |

