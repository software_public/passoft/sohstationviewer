# Plotting TPS
Each block of TPS shows the square root of the average of the squares of 
each 5 minute of data.  Each TPS bar consists of 288 blocks bar representing a 
UT day. One TPS channel channel consists of multi TPS bars depends on the time 
range the channel cover.

Each 5-minute block is color-coded according to the average amplitude 
during the period following the Color range code at the end of this document.
<br />

---------------------------
---------------------------
# TPS Dialog

<br />
<img alt="TPS Dialog" src="images/tps/tps_dialog.png" width="1000" />
<br />

TPS are plotted in tab(s).

* If total days <= 180: all channels are plotted in one tab.
* If total days > 180: each channel will be plotted in a separate tab.
<br />
---------------------------
---------------------------
# TPS Color Range
<br />
<br />
<img alt="Change TPS Color Range" src="images/tps/tps_color_range.png" height="130" />
<br />

## Selecting Color Range
User can select the desired color range from the dropdown list in the above picture.  There are 4 different 
color ranges in the list: Antarctica,  Low,  Medium and High
<br />

## Applying the selected Color Range
* Click button "RePlot Current Tab" to apply new selected color range to TPS plots in the current tab.
* Click button "RePlot All Tabs" to apply new selected color range to TPS plots in all tabs.
<br />

## Color range code
**Antarctica**
* Dark Grey/Black: there was no data for that period or the average really 
was 0.0 (not likely)
* Dark blue: the average was +/-10 counts
* Cyan: +/-100 counts
* Green: +/-1,000 counts
* Yellow: +/-10,000 counts
* Red: +/-100,000 counts
* Magenta: +/-1,000,000 counts
* Light Grey/White: > +/-1,000,000 counts
<br />

**Low**
* Dark Grey/Black: there was no data for that period or the average 
really was 0.0 (not likely)
* Dark blue: the average was +/-20 counts
* Cyan: +/-200 counts 
* Green: +/-2,000 counts
* Yellow: +/-20,000 counts
* Red: +/-200,000 counts
* Magenta: +/-2,000,000 counts
* Light Grey/White: > +/-2,000,000 counts
<br />

**Medium**
* Dark Grey/Black: there was no data for that period or the average really
was 0.0 (not likely)
* Dark blue: the average was +/-50 counts
* Cyan: +/-500 counts
* Green: +/-5,000 counts
* Yellow: +/-50,000 counts
* Red: +/-500,000 counts
* Magenta: +/-5,000,000 counts
* Light Grey/White: > +/-5,000,000 counts
<br />

**High**
* Dark Grey/Black: there was no data for that period or the average really
was 0.0 (not likely)
* Dark blue: the average was +/-80 counts
* Cyan: +/-800 counts
* Green: +/-8,000 counts
* Yellow: +/-80,000 counts 
* Red: +/-800,000 counts
* Magenta: +/-8,000,000 counts
* Light Grey/White: > +/-8,000,000 counts

<br />


