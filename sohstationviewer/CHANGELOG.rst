=======
History
=======

2023.1.0.0
--------

* First release

2023.1.0.1
--------

* Fixed a problem with SOHViewer not fitting on smaller resolutions

2024.2.0.0
--------

* Migrated GUI library from PySide2 to PySide6
* Database editor is now functional
* RT130's mass-position data (i.e. data stream 9 data) now does not generate
an Event channel
* Input to minimum gap length editor is now constrained to only accept numeric
input
* Improved the ability to calculate the size of a data set
* General bug fixes

2024.2.1.0
--------

* Add reset button to database editors
* Add ability to add new channels to the database from the plot
* Add ability to edit a channel's plotting information from the plot
* Change the size of the main information box
* Bug fixes

2024.3.0.0
--------
* Improve the performance of the program
* Add ability to print a help document to PDF
* Let users hide/show and reorder plotted channels
* Update editor for value colors in database editor
* Add backup database and ability to revert to it
* Add ability to revert a row to the value in the backup database in the
database editor
* Shows information about a gap when it is clicked
* Show external devices when selecting the data directory in Linux
* Add version information to various places
* Disable replot button during various times
* Add ability to add a data type to data sets without detected one
* Improve how TPS plots look
* Improve the UI of the database editor
* Add support for VST channel in Pegasus data
* Update the colors in the plot for white background
* Bug fixes

2024.3.0.1
--------
* Fix a bug that happens when building on some Linux machines
* Bux fixes

2024.3.1.0
--------
* Improve performance for RT130 data sets when only a subset of all data
streams is read.
* Add the ability to zoom out in the plot.
* Add unused SOH channels (VGT, VPH, VPO, VPP, VPV) for Pegasus datalogger to
the database. These channels are used for an unsupported model of the Pegasus
datalogger.
* Plot points with value 255 in the VAN channel for Pegasus datalogger in gray
instead of red. This distinguish them from points with value 2.
* Limit the zoom range. This fixes a bug that happens when the zoom range is
too small.
* Remove support for matplotlib versions below 3.6.2.
* Pause support for numpy versions 2.0 and above. Support will resume when all
dependencies have been updated to support numpy 2.0 and above.
* Fix the button to plot a different station in the same data set not being
enabled when needed.
* Fix default rows in the channel preference dialog being converted to
non-default rows on save.
* Fix artifacts showing up in the plot with some field laptops.
* Fix problems reading RT130 data sets caused by choosing specific data streams
to read.
* Fix TPS plot being broken for data sets with high-amplitude waveforms.

2024.4.0.0
--------
* Add a changelog viewer in the about window.
* Add ability to change the text font size in the plots.
* Add ability to change the size of plotted points in some plot types.
* Add ability to revert changes when editing the value-color map of a plot
* Improved performance when reading RT130 data by not reading data outside of the selected time range.

2024.4.0.1
--------
* Fix bug caused by PySide6 version 6.8

2025.1.0.0
--------
* Add support for Q8 dataloggers
* Change how the data type of a dataset is detected
