from typing import Dict, List

from PySide6 import QtWidgets, QtCore
from PySide6.QtWidgets import (
    QDialog, QListWidget, QListWidgetItem, QAbstractItemView, QLabel,
    QPushButton
)
from PySide6.QtCore import Qt


class SelectChanelsToShowDialog(QDialog):
    def __init__(self, parent):
        """
        Dialog to select channels to display and reorder them by dragging and
        dropping list items.

        :param parent: the parent widget
        """
        super(SelectChanelsToShowDialog, self).__init__()
        self.parent = parent

        # reorderablable_chan_list_widget: list widget where channels can be
        # reordered by dragging and dropping, used to display SOH and waveform
        # channels
        self.reorderable_chan_list_widget = self.create_chan_list_widget(
            self.parent.plotting_data1, self.parent.pref_order, True
        )

        # unreorderable_chan_list_widget: list widget where channels can NOT be
        # reordered, used to display mass position channels
        self.unreorderable_chan_list_widget = self.create_chan_list_widget(
            self.parent.plotting_data2, [], False
        )

        self.cancel_btn = QPushButton('CANCEL', self)
        self.apply_btn = QPushButton('APPLY', self)

        self.setup_ui()
        self.connect_signals()

    @staticmethod
    def create_chan_list_widget(channel_dict: Dict,
                                pref_order: List[str],
                                reorderable: bool):
        """
        Create a checkbox for each channel in channel_dict with the order
        according to pref_order if any.

        :param channel_dict: dictionary of channel data
        :param pref_order: list of preferred order of channels to be plotted
        :param reorderable: flag for chan_list to be able to drag and drop for
            reordering
        """
        chan_list_widget = QListWidget()
        chan_order = sorted(channel_dict.keys())    # to order masspos
        if pref_order:
            chan_order = pref_order

        for chan_id in chan_order:
            if chan_id not in channel_dict.keys():
                continue
            chan_item = QListWidgetItem(chan_id)
            if channel_dict[chan_id]['visible']:
                chan_item.setCheckState(Qt.CheckState.Checked)
            else:
                chan_item.setCheckState(Qt.CheckState.Unchecked)
            chan_list_widget.addItem(chan_item)
        if reorderable:
            chan_list_widget.setDragDropMode(
                QAbstractItemView.DragDropMode.InternalMove)

        # set height fit to content's height, not use scroll bar
        chan_list_widget.verticalScrollBar().setDisabled(True)
        chan_list_widget.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff)
        chan_list_widget.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff)
        chan_list_widget.setFixedHeight(
            chan_list_widget.count() * chan_list_widget.sizeHintForRow(0)
        )

        return chan_list_widget

    def setup_ui(self) -> None:
        self.setWindowTitle("Show/Hide and Reorder Channels")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        instruct_label1 = QLabel(
            "* Check to show/ uncheck to hide a channel.")
        main_layout.addWidget(instruct_label1)
        if self.reorderable_chan_list_widget.count() > 0:
            instruct_label2 = QLabel(
                "* Drag and drop channels in the first list to reorder.")
            main_layout.addWidget(instruct_label2)
            main_layout.addWidget(self.reorderable_chan_list_widget)

        if self.unreorderable_chan_list_widget.count() > 0:
            label = QLabel("* Mass Position channels won't be reorderable.")
            main_layout.addWidget(label)
            main_layout.addWidget(self.unreorderable_chan_list_widget)

        button_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(button_layout)
        button_layout.addWidget(self.cancel_btn)
        button_layout.addWidget(self.apply_btn)

    def connect_signals(self) -> None:
        self.cancel_btn.clicked.connect(self.close)
        self.apply_btn.clicked.connect(self.apply)

    @staticmethod
    def apply_selection(
            channel_list_widget, plotting_data):
        """
        Set show value according to channel checkboxes' check state
        :param channel_list_widget: the list widget that contains checkboxes
            representing channels' show status
        :param plotting_data: the channel dota in which channels have item show
            to be modified.
        :return new_pref_order: the new preferred order that channels are going
            to be plotted according to.
        """
        new_pref_order = []
        for row in range(channel_list_widget.count()):
            chan_item = channel_list_widget.item(row)
            chan_id = chan_item.text()
            if chan_item.checkState() == Qt.CheckState.Checked:
                plotting_data[chan_id]['visible'] = True
            else:
                plotting_data[chan_id]['visible'] = False
            new_pref_order.append(chan_id)
        return new_pref_order

    @QtCore.Slot()
    def apply(self) -> None:
        """
        Change value of key 'visible' according to the selections then replot
        channels in new order for plotting_data1 in parent widget
        """

        new_pref_order = (
            self.apply_selection(
                self.reorderable_chan_list_widget, self.parent.plotting_data1))
        self.apply_selection(
            self.unreorderable_chan_list_widget, self.parent.plotting_data2)

        self.parent.clear()
        self.parent.plot_channels(
            self.parent.data_object,
            self.parent.data_set_id,
            self.parent.start_tm,
            self.parent.end_tm,
            self.parent.time_ticks_total,
            new_pref_order,
            keep_zoom=True
        )
        self.parent.draw()

        self.close()
