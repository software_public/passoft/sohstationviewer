import sys
import os
from pathlib import PosixPath, Path
from typing import Dict, List, Tuple, Callable, Union, Optional

from PySide6 import QtGui, QtCore, QtWidgets
from PySide6.QtWidgets import QStyle, QAbstractItemView
from PySide6.QtGui import QPalette

from sohstationviewer.view.search_message.highlight_delegate import (
    HighlightDelegate)
from sohstationviewer.view.util.functions import (
    get_soh_messages_for_view, log_str)
from sohstationviewer.view.util.color import set_color_mode
from sohstationviewer.view.util.enums import LogType


class SearchMessageDialog(QtWidgets.QWidget):
    """
    GUI:
        + "Search SOH Lines" tab: to list all lines that includes searched text
            with its channel
        + "Processing Logs" tab: to list all processing logs with its log type
            and color of each line depends on its log type.
        + Each SOH LOG channel has its own tab
    For the last two types of tabs, user can use the buttons in the tool bar
        to process the following tasks:
        + The 1st button: The current table rolls back to the current selected
            line.
        + Type a searched text, the searched text will be highlighted through
            the table and the current table scrolls to the first line that
            contains the searched text.
        + The 2nd button: The current table rolls to the next line that
            contains the searched text.
        + The 3rd button: The current table rolls to the previous line that
            contains the searched text.
        + The 4th button: Save the content of the table to a text file
    Interaction: When user click on a clickable data point on a SOH channel
        of RT130, SOH tab will be focus and the line corresponding to the
        data point will be highlighted and rolled to view.

    Attributes
    ----
    SCREEN_SIZE_SCALAR_X : float
        Specifies how to scale the width of the window, in relation to total
        screen size.
    SCREEN_SIZE_SCALAR_Y : float
        Specifies how to scale the height of the window, in relation to total
        screen size.
    """
    SCREEN_SIZE_SCALAR_X = 0.40
    SCREEN_SIZE_SCALAR_Y = 0.50

    def __init__(self, home_path: str = '.'):
        super().__init__()
        # Get screen dimensions
        geom = QtGui.QGuiApplication.screens()[0].availableGeometry()
        self.setGeometry(10, 10,
                         geom.size().width() * self.SCREEN_SIZE_SCALAR_X,
                         geom.size().height() * self.SCREEN_SIZE_SCALAR_Y
                         )
        self.setWindowTitle("Search Messages")

        """
        images_path: path to the folder containing images
        """
        self.images_path: PosixPath = Path(
            home_path).resolve().joinpath('images')

        # -------------------- PRE-DEFINE WIDGETS ----------------------------
        """
        search_box: QLineEdit - text box to enter a string to search along the
            current table.
        """
        self.search_box: QtWidgets.QLineEdit = None
        """
        tab_widget: QTabWidget - widget to set up tabs for tables.
            All soh tables in this widget will be deleted before setting up new
            tables for new data reading because channels may be different for
            each data set.
        """
        self.tab_widget: QtWidgets.QTabWidget = None
        """
        info_display: QTextEdit - text box to show info of selected line
        """
        self.info_display: QtWidgets.QTextEdit = None
        """
        filter_soh_lines_table: QTableWidget - table to display all lines that
            contains search text
        """
        self.filter_soh_lines_table: QtWidgets.QTableWidget = None
        """
        processing_log_table: QTableWidget - table to display processing log
        """
        self.processing_log_table: QtWidgets.QTableWidget = None
        """
        current_table: QTableWidget - The active table (widget of a tab for
            this dialog)
        """
        self.current_table: QtWidgets.QTableWidget = None
        """
        selected_item: QTableWidgetItem - The last selected cell widget in a
            table. There is only one selected_item over all tables
        """
        self.selected_item: QtWidgets.QTableWidgetItem = None
        """
        active_table_for_new_dataset: QTableWidget - table to be set active
            when a new dataset is load
        """
        self.active_table_for_new_dataset: QtWidgets.QTableWidget = None
        """
        save_button: QToolButton - Button to save log messages on current
            tab in a text file
        """
        self.save_button: QtWidgets.QToolButton = None
        """
        delegate: HighlightDelegate - delegate that help format current table
            items that contain search_text
        """
        self.delegate: HighlightDelegate = None
        # --------------------- PRE-DEFINED OTHER ATTRIBUTES ----------------
        """
        search_rowidx: int - The last row index found during searching
        """
        self.search_rowidx: int = 0
        """
        search_text: str - text to search for
        """
        self.search_text: str = ""
        """
        display_color: {type: color,} - color map for setting background color
            for different types of processing log or
             "state of health"/"Events:" labels
        """
        self.display_color: Dict[str, str] = set_color_mode("W")
        """
        processing_logs: [(message, type),] - record of processing progress
        """
        self.processing_logs: List[(str, str)] = []
        """
        soh_dict: {chan_id: [str,]} - dict of list of soh message lines
            for each soh channel
        """
        self.soh_dict: Dict[str, List[str]] = {}
        """
        soh_tables_dict: {chan_id: QTableWidget,} - dict of table for
            each soh channel
        """
        self.soh_tables_dict: Dict[str, QtWidgets.QTableWidget] = {}

        self.setup_ui()

    def setup_ui(self):
        """
        Set up GUI includes:
            + A search box to enter words for searching
            + A toolbar allow user to go back to last selected, search next,
                search previous, save file
            + Tabs each of which is a table to show message for processing
                log or a state-of-health channel's message
            + A text box to show selected line's text
        """
        # ------------------------- search box ------------------------
        self.search_box = QtWidgets.QLineEdit()
        self.search_box.setTextMargins(7, 7, 7, 7)
        self.search_box.setFixedHeight(40)
        self.search_box.setClearButtonEnabled(True)
        self.search_box.setPlaceholderText("Enter a string to search")
        self.search_box.textChanged.connect(self.on_search_text_changed)
        # -------------- Create tool buttons -------------------------

        navigation = QtWidgets.QToolBar('Navigation')
        self.add_nav_button(
            navigation,
            QtGui.QIcon(
                self.images_path.joinpath('to_selected.png').as_posix()),
            'Scroll to selected line of the current table',
            self.scroll_to_selected)
        self.add_nav_button(
            navigation,
            self.style().standardIcon(QStyle.StandardPixmap.SP_ArrowBack),
            'Scroll to previous search text',
            self.scroll_to_previous_search_text)
        self.add_nav_button(
            navigation,
            self.style().standardIcon(QStyle.StandardPixmap.SP_ArrowForward),
            'Scroll to next search text',
            self.scroll_to_next_search_text)
        self.save_button = self.add_nav_button(
            navigation,
            self.style().standardIcon(
                QStyle.StandardPixmap.SP_DialogSaveButton),
            'Save messages in current tab to text file',
            self.save_messages)

        # --------- display info message ------------------
        self.info_display = QtWidgets.QLineEdit()
        self.info_display.setTextMargins(7, 7, 7, 7)
        self.info_display.setFixedHeight(40)
        self.info_display.setReadOnly(True)

        # --------- set layout -------------------------------------
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.setSpacing(10)
        main_layout.setContentsMargins(5, 5, 5, 5)

        main_layout.addWidget(self.search_box)

        main_layout.addWidget(navigation)

        tab_widget_layout = QtWidgets.QVBoxLayout()
        main_layout.addLayout(tab_widget_layout)

        self.tab_widget = QtWidgets.QTabWidget()
        tab_widget_layout.addWidget(self.tab_widget)
        self.tab_widget.currentChanged.connect(self.set_current_tab)

        self.filter_soh_lines_table = self.create_table(0, 2)
        self.tab_widget.addTab(
            self.filter_soh_lines_table, 'Filter SOH Lines')
        self.processing_log_table = self.create_table(0, 2, [0])
        self.tab_widget.addTab(
            self.processing_log_table, 'Processing Logs')
        main_layout.addWidget(self.info_display)

    def setup_logview(
            self, data_set_id: Union[str, Tuple[str, str]],
            soh_messages: Dict[str, Union[List[str], Dict[str, List[str]]]],
            processing_logs: List[Tuple[str, str]]):
        """
        When a dataset is loaded,
            + processing_logs need to be refilled in processing_log_table
            + Tabs need to be recreated for soh channels since they are
                different for each dataset
            + If there is any info in processing_log_table, it will be
                activated. Otherwise, the first soh table will be activated

        Parameters
        ----
        data_set_id: data set's id
        soh_messages: - info from log channels, soh messages, text file
        processing_logs: record of processing progress
        """
        for i in range(self.tab_widget.count() - 1, 1, -1):
            # delete all soh tabs in self.tab_widget
            widget = self.tab_widget.widget(i)
            self.tab_widget.removeTab(i)
            widget.setParent(None)

        self.soh_tables_dict = {}

        self.add_processing_log_table(processing_logs)
        self.add_soh_tables(data_set_id, soh_messages)
        self.tab_widget.setCurrentWidget(self.active_table_for_new_dataset)

    def add_processing_log_table(self, processing_logs: List[Tuple[str, str]]):
        """
        Adding info to Processing Logs table

        Parameters
        ----
        processing_logs: [(message, type),] - record of processing progress
        """

        self.processing_logs = processing_logs
        if processing_logs == []:
            self.active_table_for_new_dataset = None
        else:
            self.active_table_for_new_dataset = self.processing_log_table

        self.processing_log_table.setRowCount(0)
        count = 0
        processing_logs = (
            [("There are no processing logs to display.", LogType.INFO)]
            if processing_logs == [] else processing_logs)
        for log_line in processing_logs:
            self.add_line(self.processing_log_table, text1=count,
                          text2=log_line[0], log_type=log_line[1])

    def add_soh_tables(
            self, data_set_id: Union[str, Tuple[str, str]],
            soh_messages: Dict[str, Union[List[str], Dict[str, List[str]]]]):
        """
        Adding SOH Channel tables to tab_widget and their info

        Parameters
        ----
        data_set_id: data set's id
        soh_messages: info from log channels, soh messages, text file
        """
        self.soh_dict = get_soh_messages_for_view(data_set_id, soh_messages)
        for chan_id in self.soh_dict:
            self.soh_tables_dict[chan_id] = self.create_table(0, 2, [0])
            if self.active_table_for_new_dataset is None:
                self.active_table_for_new_dataset = self.soh_tables_dict[
                    chan_id]
            self.tab_widget.addTab(self.soh_tables_dict[chan_id], chan_id)
            count = 0
            for log_line in self.soh_dict[chan_id]:
                self.add_line(
                    self.soh_tables_dict[chan_id],
                    text1=str(count),
                    text2=log_line)
                count += 1

    def add_nav_button(self, nav: QtWidgets.QToolBar,
                       icon: QtGui.QIcon,
                       tool_tip_text: str, function: Callable[[], None])\
            -> QtWidgets.QToolButton:
        """
        Add a QToolButton to Navigation QToolBar including: icon, tool tip,
            function to do

        :param nav: Tool bar to add button
        :type nav: QToolBar
        :param icon_pic: Pix map of the picture of the button
        :type icon_pic: QStyle.StandardPixMap
        :param tool_tip_text: Description of what the button will do
        :type tool_tip_text: str
        :param function: The method that perform the button's action
        :type function: method
        :return: The added button
        :rtype:  QtWidgets.QToolButton
        """
        button = QtWidgets.QToolButton()
        button.setIcon(icon)
        button.setToolTip(tool_tip_text)
        button.clicked.connect(function)
        nav.addWidget(button)
        return button

    def add_line(self, table: QtWidgets.QTableWidget,
                 text1: Union[str, int], text2: str,
                 log_type: LogType = LogType.INFO):
        """
        Add a line of message to a row of table and color it according to
            log_type (red for error, orange for warning) or blue header of
            block of soh messages
        Parameters
        ----
        table: QTableWidget - table to add row
        text1: str/int - index of row for soh tables, log_type for
            Processing Logs table, channel id for Search SOH Lines table
        text2: str - content of line
        log_type: LogType - type of log line
        """
        count = table.rowCount()
        table.setRowCount(count + 1)

        # Add data index to column 0
        it = QtWidgets.QTableWidgetItem(text1)
        table.setItem(count, 0, it)

        # Add log message to column 1
        it = QtWidgets.QTableWidgetItem(text2)
        if log_type is not None:
            # set background color for error/warning processing log
            if log_type == LogType.ERROR:
                it.setBackground(QtGui.QColor(self.display_color['error']))
            elif log_type == LogType.WARNING:
                it.setBackground(QtGui.QColor(self.display_color['warning']))

        if "state of health" in text2.lower() or text2 == "Events:":
            # set background color for header of a block of soh messages
            it.setBackground(
                QtGui.QColor(self.display_color['state_of_health']))
        it.setFlags(QtCore.Qt.ItemFlag.ItemIsSelectable |
                    QtCore.Qt.ItemFlag.ItemIsEnabled)
        table.setItem(count, 1, it)

    def create_table(self, rows: int, cols: int, hidden: List[int] = [])\
            -> QtWidgets.QTableWidget:
        """
        Creates and returns a QTableWidget with the specified number
            of rows and columns.
        The first column should be the index of the line
        The second column should be the text of the line

        Parameters
        ----
        rows : int
            Number of rows in the created table
        cols : int
            Number of columns in the created table
        hidden : iterable object
            An iterable containing >= 0 int's. Each integer should
            correspond to the index of a column which should be hidden
            from display.

        Returns
        ----
        QTableWidget
            The constructed QTableWidget
        """
        # add 1 extra column to show scroll bar (+ 1)
        table = QtWidgets.QTableWidget(rows, cols + 1)

        # To prevent selected row not grey out. It still gets faded out, but
        # the color remain blue which is better than grey out.
        p = table.palette()
        p.setBrush(QPalette.Inactive, QPalette.Highlight,
                   p.brush(QPalette.Highlight))
        table.setPalette(p)

        delegate = HighlightDelegate(table, self.display_color)
        table.setItemDelegate(delegate)
        # Hide header cells
        table.verticalHeader().setVisible(False)
        table.horizontalHeader().setVisible(False)

        # Expand horizontally
        table.horizontalHeader().setStretchLastSection(True)

        table.horizontalHeader().setSectionResizeMode(
            1, QtWidgets.QHeaderView.ResizeMode.ResizeToContents)

        # Hide cell grid
        table.setShowGrid(False)

        table.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectionBehavior.SelectRows
        )
        table.setSelectionMode(
            QtWidgets.QAbstractItemView.SelectionMode.SingleSelection
        )
        table.itemClicked.connect(self.on_log_entry_clicked)

        for index in hidden:
            table.setColumnHidden(index, True)

        return table

    def search(self, col: int = 1, direction: str = 'next',
               start_search: bool = False)\
            -> Optional[Tuple[QtWidgets.QTableWidgetItem, int]]:
        """
        Searches all rows of a table for search_text and returns the item that
            contains search_text and its row index

        Parameters
        ----
        col: int - index of the column to search text from
        direction: str - next/previous: show which direction to search
        start_search: bool - if this search start from typing to search box

        Returns
        ----
        item: QTableWidgetItem, row: int
            A QTableWidgetItem is returned if there is an item matching the
            query in table, otherwise None is returned
            Index of the row where search_text is found
        Or None if no text found
        """
        self.delegate.set_current_row(-1)
        self.info_display.setText('')
        self.current_table.viewport().update()

        if self.search_text == '':
            return

        if direction == "next":
            if not start_search:
                self.search_rowidx += 1
            total_rows = self.current_table.rowCount()
            search_range = range(self.search_rowidx, total_rows)
        else:
            search_range = range(self.search_rowidx - 2, -1, -1)
        for row in search_range:
            item = self.current_table.item(row, col)
            if item is None:
                continue
            if self.search_text.lower() in item.text().lower():
                self.delegate.set_current_row(row)
                self.current_table.viewport().update()
                message_index = self.current_table.item(item.row(), 0).text()
                message_text = self.current_table.item(item.row(), 1).text()
                self.info_display.setText(
                    f"Line {message_index}:   {message_text}")
                if direction == "next":
                    row += 1
                return item, row
        if not start_search:
            opposite_direction = 'previous' if direction == 'next' else 'next'
            self.info_display.setText(
                f"'{self.search_text}' not found in '{direction}' direction. "
                f"Try click '{opposite_direction}' instead."
            )
        return

    def _show_log_message(self, item: QtWidgets.QTableWidgetItem):
        """
        Showing the given item on the current table by:
            + Scrolling to the given item on the current table.
            + Set focus on the current table so that the item will
                be highlight instead of grey out.

        Parameters
        ----
        item : QTableWidgetItem
            A valid QTableWidgetIem
        """
        self.current_table.scrollToItem(
            item, QAbstractItemView.ScrollHint.PositionAtTop
        )
        self.current_table.setFocus()

    def show_log_entry_from_log_indexes(self, log_indexes: List[int]):
        """
        This is called when clicking a clickable data point on a SOH channel
        of RT130, list of log row indexes will be passed to this method.
        This method will:
            + set current tab to soh_table_dict['SOH']
            + scroll the first indexed row to top of table and highlight all
             the row in log_indexes

        Parameters
        ----
        log_indexes : The list of indexes of log row to be selected
        """
        if 'SOH' not in self.soh_tables_dict:
            return
        # switch to SOH tab
        self.tab_widget.setCurrentWidget(self.soh_tables_dict['SOH'])
        self.current_table.clearSelection()
        # Allow to select multiple rows
        self.current_table.setSelectionMode(
            QAbstractItemView.SelectionMode.MultiSelection
        )
        # select all rows according to log_indexes
        for idx in log_indexes:
            self.current_table.selectRow(idx + 1)
        # scroll to the first index and place the row at top of the table
        self.current_table.scrollToItem(
            self.current_table.item(log_indexes[0] + 1, 1),
            QAbstractItemView.ScrollHint.PositionAtTop
        )
        # raise the message dialog on top of others
        self.setWindowState(QtCore.Qt.WindowState.WindowActive)
        self.raise_()
        self.activateWindow()
        self.current_table.setFocus()       # focus row to not faded out
        # return back to select single row
        self.current_table.setSelectionMode(
            QAbstractItemView.SelectionMode.SingleSelection
        )

    def show_log_entry_from_data_index(self, data_index: int):
        """
        This is called when clicking a clickable data point on a SOH channel
            of RT130, data_index, which represent for row index in 'SOH' table,
            will be given. This method will:
            + set current tab to soh_table_dict['SOH']
            + make a search on soh_table_dict['SOH']
            + scroll to and highlight the row corresponding to the data point

        Parameters
        ----
        data_index : int
            The index (of the data point that has been loaded from disk)
            to be selected and scrolled to.
        """
        if 'SOH' not in self.soh_tables_dict:
            return
        self.tab_widget.setCurrentWidget(self.soh_tables_dict['SOH'])
        self.search_text = str(data_index)
        self.search_rowidx = 0
        ret = self.search(col=0, start_search=True)
        if ret is None:
            raise ValueError(f'Not found line: {data_index}')
        it, r = ret
        self.on_log_entry_clicked(it)
        self._show_log_message(it)
        self.setWindowState(QtCore.Qt.WindowState.WindowActive)
        self.raise_()
        self.activateWindow()

    @QtCore.Slot()
    def set_current_tab(self):
        """
        When a tab is selected,
            + Reset selected_item
            + Reset search_text to text in search_box because search text.
                may still keep data_index from RT130 SOH data point clicked
                interaction.
            + Set current_table.
            + Redo the search on search_text.
            + Disable save button if table is filter_soh_lines_table, or if
                table is processing_log_table but have no information.
        """
        self.selected_item = None
        self.search_text = self.search_box.text()   # reset data_index
        self.current_table = self.tab_widget.currentWidget()
        self.on_search_text_changed(self.search_text)

        if (self.current_table == self.filter_soh_lines_table or
                (self.current_table == self.processing_log_table and
                 self.processing_logs == [(
                    "There are no processing logs to display.", LogType.INFO)]
                 )):
            self.save_button.setEnabled(False)
        else:
            self.save_button.setEnabled(True)

    @QtCore.Slot()
    def scroll_to_selected(self):
        """
        Scroll back to the selected item on the current_table
        """
        it = self.selected_item
        if not it:
            return
        self._show_log_message(it)

    @QtCore.Slot()
    def scroll_to_previous_search_text(self):
        """
        Scroll to the search_text before the last search roll
        """
        ret = self.search(direction="previous")
        if ret is None:
            return
        self.selected_item, self.search_rowidx = ret
        self.scroll_to_selected()

    @QtCore.Slot()
    def scroll_to_next_search_text(self):
        """
        Scroll to the search_text after the last search roll
        """
        ret = self.search()
        if ret is None:
            return
        self.selected_item, self.search_rowidx = ret
        self.scroll_to_selected()

    @QtCore.Slot()
    def on_log_entry_clicked(self, item: QtWidgets.QTableWidgetItem):
        """
        Set selectRow for item on the current_table, display the info on
            info_display and set selected_item = item

        Parameters
        ----
        item : QTableWidgetItem
            A valid QTableWidgetIem to select
        """
        self.current_table.selectRow(item.row())

        # # Display full text of message
        message_index = self.current_table.item(item.row(), 0).text()
        message_text = self.current_table.item(item.row(), 1).text()
        self.info_display.setText(f"Line {message_index}:   {message_text}")
        self.selected_item = item

    @QtCore.Slot()
    def on_search_text_changed(self, text: str):
        """
        When text in search_box is changed,
            + Set search_text
            + Highlight text in all tables if it is not a search for data_index
            + If the current_table is Processing Logs or a SOH channel table,
                jump to the first search text.
            + If the current_table is Search SOH Lines, filter all SOH lines to
                display the lines contain the search_text only.
        """
        self.search_text = text
        self.delegate = self.current_table.itemDelegate()
        # check to highlight text when searching for text in search_box but
        # not highlight when searching for data_index
        if self.search_box.text() == self.search_text:
            self.delegate.set_filters([text])
            self.current_table.viewport().update()

        if self.current_table != self.filter_soh_lines_table:
            self._jumpto_search_text_on_current_table()
        else:
            self._filter_lines_with_search_text_from_soh_messages()

    def _jumpto_search_text_on_current_table(self):
        """
        Jump to the first search text by search forward for search_text on the
            current_table from top of the table then scroll to that row.
        """
        self.search_rowidx = 0
        ret = self.search(start_search=True)
        if ret is None:
            return
        self.selected_item, self.search_rowidx = ret
        self.current_table.scrollToItem(
            self.selected_item, QAbstractItemView.ScrollHint.PositionAtTop
        )

    def _filter_lines_with_search_text_from_soh_messages(self):
        """
        Filter all SOH lines to display the lines contain the search_text only.
        """
        self.filter_soh_lines_table.setRowCount(0)
        if self.search_text == '':
            return

        for chan_id in self.soh_dict:
            for line in self.soh_dict[chan_id]:
                if self.search_text in line:
                    self.add_line(self.filter_soh_lines_table,
                                  text1=chan_id, text2=line)

    @QtCore.Slot()
    def save_messages(self):
        """
        Save text on the current_table to a text file.
        """
        tab_index = self.tab_widget.currentIndex()
        tab_name = self.tab_widget.tabText(tab_index)
        default_name = os.path.join(QtCore.QDir.homePath(), f'{tab_name}.log')
        file_name = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save File', default_name, "Text (*.log)")[0]
        if file_name == '':
            return
        with open(file_name, 'w') as file:
            if self.current_table == self.processing_log_table:
                file.write('\n'.join(map(log_str, self.processing_logs)))
            else:
                file.write('\n'.join(self.soh_dict[tab_name]))


def main():
    import platform
    import os
    # Enable Layer-backing for MacOs version >= 11
    # Only needed if using the pyside2 library with version>=5.15.
    # Layer-backing is always enabled in pyside6.
    os_name, version, *_ = platform.platform().split('-')
    # if os_name == 'macOS' and version >= '11':
    # mac OSX 11.6 appear to be 10.16 when read with python and still required
    # this environment variable
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'

    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(
        "QTableView::item:selected {"
        "  selection-background-color: #93CAFF;}"
    )
    soh_messages = {
        'TEXT': ['this is a text message\nThis is a different line'],
        '0895': {
            'SOH': ["\n\n**** STATE OF HEALTH: From:2018-02-07T17:48:24.000000Z  To:2018-02-07T17:48:24.000000Z"   # noqa
                    "\n\nVCO Correction: 50.0\nTime of exception: 2018:38:17:48:24:0\nMicro sec: 0\nReception Quality: 0\nException Count: 1", # noqa
                    "\n\n**** STATE OF HEALTH: From:2018-02-07T17:58:35.000000Z  To:2018-02-07T17:58:35.000000Z" # noqa
                    "\n\nVCO Correction: 55.6396484375\nTime of exception: 2018:38:17:58:35:0\nMicro sec: 0\nReception Quality: 0\nException Count: 1"]}}  # noqa

    processing_logs = [
        ("info line 1", LogType.INFO),
        ("warning line 1", LogType.WARNING),
        ("info line 2", LogType.INFO),
        ("error line 1", LogType.ERROR)
    ]

    wnd = SearchMessageDialog(home_path='../../../')
    wnd.setup_logview('0895', soh_messages, processing_logs)
    wnd.show()
    # wnd.show_log_entry_from_data_index(10)

    sys.exit(app.exec())


if __name__ == '__main__':
    main()
