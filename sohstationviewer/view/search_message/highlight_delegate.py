"""
Credit: https://stackoverflow.com/questions/53353450/how-to-highlight-a-words-in-qtablewidget-from-a-searchlist  # noqa
"""
from typing import List, Dict
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtGui import QPen, QTextCursor, QTextCharFormat, QColor


class HighlightDelegate(QtWidgets.QStyledItemDelegate):
    """
    Help with highlighting search text in SOH Message tables.
    """
    def __init__(self, parent=None, display_color=None):
        super(HighlightDelegate, self).__init__(parent)
        """
        doc: container to format soh table's item
        """
        self.doc: QtGui.QTextDocument = QtGui.QTextDocument(self)
        """
        filters: list of texts to apply text highlighted
        """
        self.filters: List[str] = []
        """
        current_row: the row index of the item to apply border highlighted
        """
        self.current_row: int = -1
        """
        display_color: {type: color,} - color map for setting highlight color
        """
        self.display_color: Dict[str, str] = display_color

    def paint(self, painter: QtGui.QPainter,
              option: QtWidgets.QStyleOptionViewItem,
              index: QtCore.QModelIndex):
        """
        Custom rendering which is a reimplementation of the abstract function
        paint()

        Parameters
        ----
        painter: painter of the table
        option: the item widget
        index: location of the item on the table widget
        """
        painter.save()
        options = QtWidgets.QStyleOptionViewItem(option)
        self.initStyleOption(options, index)
        self.doc.setPlainText(options.text)
        if index.column() != 0:
            # not apply highlight for column 0
            self.apply_highlight(painter, option, index.row())
        options.text = ""
        style = QtWidgets.QApplication.style() if options.widget is None \
            else options.widget.style()
        style.drawControl(QtWidgets.QStyle.ControlElement.CE_ItemViewItem,
                          options, painter)

        context = QtGui.QAbstractTextDocumentLayout.PaintContext()
        if option.state & QtWidgets.QStyle.StateFlag.State_Selected:
            role = QtGui.QPalette.ColorRole.HighlightedText
        else:
            role = QtGui.QPalette.ColorRole.Text
        context.palette.setColor(
            QtGui.QPalette.ColorRole.Text,
            option.palette.color(QtGui.QPalette.ColorGroup.Active, role)
        )

        text_rect = style.subElementRect(
            QtWidgets.QStyle.SubElement.SE_ItemViewItemText, options)

        if index.column() != 0:
            text_rect.adjust(5, 0, 0, 0)

        the_constant = 4
        margin = (option.rect.height() - options.fontMetrics.height()) // 2
        margin = margin - the_constant
        text_rect.setTop(text_rect.top() + margin)

        painter.translate(text_rect.topLeft())
        painter.setClipRect(text_rect.translated(-text_rect.topLeft()))
        self.doc.documentLayout().draw(painter, context)

        painter.restore()

    def apply_highlight(self, painter, option, row):
        """
        Look through doc to find the word matched with all text in filters
        to highlight it.
        """
        cursor = QTextCursor(self.doc)
        cursor.beginEditBlock()
        fmt = QTextCharFormat()
        fmt.setForeground(QColor(self.display_color['highlight']))
        fmt.setFontWeight(700)
        done_task = False
        for f in self.filters:
            highlight_cursor = QTextCursor(self.doc)
            while (not highlight_cursor.isNull() and
                   not highlight_cursor.atEnd()):
                highlight_cursor = self.doc.find(f, highlight_cursor)
                if not highlight_cursor.isNull():
                    highlight_cursor.mergeCharFormat(fmt)
                    if not done_task and row == self.current_row:
                        painter.setPen(
                            QPen(QColor(self.display_color['highlight']), 3))
                        painter.drawRect(option.rect)
                        done_task = True
        cursor.endEditBlock()

    def set_filters(self, filters: List[str]):
        """
        Set texts to highlight

        Parameters
        ----
        filters: list of texts
        """
        self.filters = filters

    def set_current_row(self, current_row):
        self.current_row = current_row
