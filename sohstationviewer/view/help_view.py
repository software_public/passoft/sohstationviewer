import sys
from pathlib import Path
from typing import Tuple, Callable, Union, List, Dict, Optional

from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtPrintSupport import QPrinter
from PySide6.QtWidgets import QStyle

from sohstationviewer.view.util.functions import (
    create_search_results_file, create_table_of_content_file)
from sohstationviewer.view.util.color import set_color_mode
from sohstationviewer.conf import constants as const


class HelpBrowserItemDelegate(QtWidgets.QStyledItemDelegate):
    """
    To help showing names of files in tree list without extension.
    """
    def initStyleOption(self, opt, index):
        super().initStyleOption(opt, index)
        if not index.model().isDir(index):
            base_file_name = index.model().fileInfo(index).baseName()
            try:
                display_file_name = base_file_name.split(" _ ")[1]
            except IndexError:
                print(f"WARNING: Filename '{base_file_name}' in "
                      f"'documentation/' is NOT in the "
                      f"correct format: <order number> _ <text>")
                display_file_name = base_file_name

            opt.text = display_file_name

    def setEditorData(self, ed, index):
        if isinstance(index.model(), QtWidgets.QFileSystemModel):
            if not index.model().isDir(index):
                ed.setText(index.model().fileInfo(index).baseName())
            else:
                super().setEditorData(ed, index)


class HelpBrowser(QtWidgets.QWidget):
    """
    GUI:
        + Search box: to enter text to search for, first search is processed
            with text changed
        + Search through all documents button: To create the links to all
            documents that contain search text. Click on a link will go to
            the link's page with search text highlight at the first position.
        + Table of Contents button: to go to Table of Contents page.
        + Recreate Table of Contents button: to recreate Table of Contents
            when there are any changes in the name of help documents that may
            make links broken or some pages has no links in Table of Contents.
        + Backward arrow button: to search backward on the current page.
        + Forward arrow button: to search forward on the current page.
        + Go to Search Results page: to continue working on Search Results page
        + Document list: tree list located on left side to browse for help
            documents.
        + Document view: the largest text box located on the right side of the
            dialog to display the current selected document's content.

    Attributes
    ----
    SCREEN_SIZE_SCALAR_X : float
        Specifies how to scale the width of the window, in relation to total
        screen size.
    SCREEN_SIZE_SCALAR_Y : float
        Specifies how to scale the height of the window, in relation to total
        screen size.
    TREE_VIEW_SCALAR: float
        Specifies how to scale the width of tree_view, in relation to total
        screen size.
    """

    SCREEN_SIZE_SCALAR_X = 0.75
    SCREEN_SIZE_SCALAR_Y = 0.75
    TREE_VIEW_SCALAR = 0.25

    def __init__(
            self,
            parent: Union[QtWidgets.QWidget, QtWidgets.QMainWindow] = None,
            home_path: str = '.'):
        """
        :param parent: The window that call HelpBrowser object
        :type parent: QWidget/QMainWindow
        :param home_path: relative path to the folder where app is started
        :type home_path: str
        """
        super().__init__(parent)
        """
        home_path: absolute path to the root of the project
        """
        self.home_path = Path(home_path).resolve()
        """
        docdir_path: path to the folder containing the documentations
        """
        self.docdir_path: Path = self.home_path.joinpath('documentation')
        """
        images_path: path to the folder containing images
        """
        self.images_path: Path = self.home_path.joinpath('images')
        """
        contents_table_path: the absolute path to "01 _ Table of Contents.md"
        """
        self.contents_table_path: Path = self.docdir_path.joinpath(
            const.TABLE_CONTENTS)
        """
        search_results_path: the absolute path to "Search Results.md"
        """
        self.search_results_path: Path = self.docdir_path.joinpath(
            'Search Results.md')
        """
        default_pdf_file_name: the default name for pdf file when the current
        help document is saved
        """
        self.default_pdf_file_name: str = ''
        # Get screen dimensions
        geom = QtGui.QGuiApplication.screens()[0].availableGeometry()
        self.setGeometry(10, 10,
                         geom.size().width() * self.SCREEN_SIZE_SCALAR_X,
                         geom.size().height() * self.SCREEN_SIZE_SCALAR_Y
                         )
        self.setWindowTitle("Help Documents")
        # -------------------- PRE-DEFINE WIDGETS ----------------------------
        """
        search_box: text box to enter a string to search along the
            current table.
        """
        self.search_box: Optional[QtWidgets.QLineEdit] = None
        """
        search_all_button: button to perform create list of links of documents
            that contain search text which is called search results
        """
        self.search_all_button: Optional[QtWidgets.QPushButton] = None
        """
        go_to_search_results_button: button to navigate to the search results
        """
        self.go_to_search_results_button: Optional[QtWidgets.QToolButton] = \
            None
        """
        save_button: button to save the current document to a pdf file
        """
        self.save_button: Optional[QtWidgets.QToolButton] = None
        """
        tree_view: tree list of all documents
        """
        self.tree_view: Optional[QtWidgets.QTreeView] = None
        """
        file_system_model: provide data model for document's filesystem
        """
        self.file_system_model: Optional[QtWidgets.QFileSystemModel] = None
        """
        help_view: to display selected document's content
        """
        self.help_view: Optional[QtWidgets.QTextBrowser] = None
        # --------------------- PRE-DEFINED OTHER ATTRIBUTES ----------------
        """
        cursor_list: list of cursors of search text found
        """
        self.cursor_list: List[QtGui.QTextCursor] = None
        """
        current_index: index of the current cursor in cursor_list
        """
        self.current_index: int = 0
        """
        display_color: {type: color,} - color map for setting background color
            for different types of processing log or
             "state of health"/"Events:" labels
        """
        self.display_color: Dict[str, str] = set_color_mode("W")

        """
        highlight_format: to apply to the found text in help view
        """
        self.highlight_format: QtGui.QTextCharFormat = \
            self.get_highlight_format()

        self.setup_ui()

        self.tree_view.setFocus()

    def __del__(self):
        try:
            with open(self.docdir_path.joinpath(const.SEARCH_RESULTS), 'w'):
                pass
        except NameError:
            pass

    def get_highlight_format(self):
        """
        Setting format to apply for search_text found in document

        :return: format to apply for search_text
        :rtype:  QtGui.QTextCharFormat
        """
        text_format = QtGui.QTextCharFormat()
        text_format.setForeground(
            QtGui.QColor(self.display_color['highlight'])
        )
        return text_format

    def setup_ui(self):
        """
        Setting up GUI including
            + A search box and a button to search through all documents
            + A navigation bar to go to Table of Contents, recreate Table of
                Contents, search back and forth, go to Search Results
            + A tree to list all documents and select document to view
            + A view to view the selected documents
        """
        # Searching
        search_layout = QtWidgets.QHBoxLayout()
        self.search_box = QtWidgets.QLineEdit()
        self.search_box.setTextMargins(7, 7, 7, 7)
        self.search_box.setFixedHeight(40)
        self.search_box.setClearButtonEnabled(True)
        self.search_box.setPlaceholderText("Enter a string to search")
        self.search_box.textChanged.connect(self.start_search_on_curr_doc)
        search_layout.addWidget(self.search_box)

        self.search_all_button = QtWidgets.QPushButton(
            "Search through\nAll Documents")
        self.search_all_button.setFixedHeight(50)
        self.search_all_button.setFixedWidth(150)
        self.search_all_button.clicked.connect(self.search_through_all_docs)
        search_layout.addWidget(self.search_all_button)

        navigation = self.create_navigation()

        # Documentation listing
        self.tree_view, self.file_system_model = self.create_tree_view()

        # Help documentation display
        self.help_view = QtWidgets.QTextBrowser()
        # set stylesheet for the selected text in help_view
        self.help_view.setStyleSheet(
            f"selection-background-color:"
            f" {self.display_color['highlight_background']};"
            f"selection-color: {self.display_color['highlight']};")
        self.help_view.sourceChanged.connect(self.on_source_changed)
        split = QtWidgets.QSplitter()
        split.addWidget(self.tree_view)
        split.addWidget(self.help_view)

        # --------- set layout -------------------------------------
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.setSpacing(10)
        main_layout.setContentsMargins(5, 5, 5, 5)

        main_layout.addLayout(search_layout)
        main_layout.addWidget(navigation)
        main_layout.addWidget(split, 2)

        self.load_file(self.contents_table_path.as_posix())

    def create_navigation(self) -> QtWidgets.QToolBar:
        """
        Create navigation bar includes:
            + A button to go back to Table of Contents document
            + A button to recreate TAble of Contents document
            + A button to search backward on the current document
            + A button to search forward on the current document
            + A button to go back to Search Results

        :return: Toolbar that includes necessary buttons
        :rtype:  QtWidgets.QToolBar
        """

        nav_bar = QtWidgets.QToolBar("Navigation")

        self.add_nav_button(
            nav_bar,
            QtGui.QIcon(self.images_path.joinpath(
                'table_contents.png').as_posix()),
            'Navigate to Table of Contents', self.go_table_contents)

        self.add_nav_button(
            nav_bar,
            QtGui.QIcon(self.images_path.joinpath(
                'recreate_table_contents.png').as_posix()),
            'Recreate Table of Contents', self.recreate_table_contents)

        self.add_nav_button(
            nav_bar,
            self.style().standardIcon(QStyle.StandardPixmap.SP_ArrowBack),
            'Search Backward', self.search_backward)

        self.add_nav_button(
            nav_bar,
            self.style().standardIcon(QStyle.StandardPixmap.SP_ArrowForward),
            'Search Forward', self.search_forward)

        self.go_to_search_results_button = self.add_nav_button(
            nav_bar,
            QtGui.QIcon(self.images_path.joinpath(
                'search_results.png').as_posix()),
            'Navigate to Search Results', self.go_search_results)
        self.go_to_search_results_button.setEnabled(False)

        self.save_button = self.add_nav_button(
            nav_bar,
            self.style().standardIcon(
                QStyle.StandardPixmap.SP_DialogSaveButton),
            'Save the Current Document to PDF file', self.save_to_pdf)

        return nav_bar

    def add_nav_button(self, nav: QtWidgets.QToolBar,
                       icon: QtGui.QIcon,
                       tool_tip_text: str, function: Callable[[], None])\
            -> QtWidgets.QToolButton:
        """
        Add a QToolButton to Navigation QToolBar including: icon, tool tip,
            function to do

        :param nav: Tool bar to add button
        :type nav: QToolBar
        :param icon: icon of the button
        :type icon: QtGui.QIcon
        :param tool_tip_text: Description of what the button will do
        :type tool_tip_text: str
        :param function: The method that perform the button's action
        :type function: method
        :return: The added button
        :rtype:  QtWidgets.QToolButton
        """
        button = QtWidgets.QToolButton()
        button.setIcon(icon)
        button.setToolTip(tool_tip_text)
        button.clicked.connect(function)
        nav.addWidget(button)
        return button

    def create_tree_view(self) -> Tuple[QtWidgets.QTreeView,
                                        QtWidgets.QFileSystemModel]:
        """
        Create tree_view associating with file_system_model

        :return: tree view to show list of files in document folder
        :rtype:  QTreeView
        :return: file system model for mechanism to access file when it is
            clicked on tree view
        :rtype:  QFileSystemModel
        """
        tree_view = QtWidgets.QTreeView()

        file_system_model = QtWidgets.QFileSystemModel()
        file_system_icon_provider = QtWidgets.QFileIconProvider()

        file_system_model.setIconProvider(file_system_icon_provider)
        index = file_system_model.setRootPath(self.docdir_path.as_posix())

        file_system_model.setFilter(
            QtCore.QDir.Filter.NoDotAndDotDot | QtCore.QDir.Filter.Files)
        file_system_model.setNameFilters(['*.help.md'])
        file_system_model.setNameFilterDisables(False)  # hide inactive

        tree_view.setItemDelegate(HelpBrowserItemDelegate())

        tree_view.setModel(file_system_model)

        tree_view.setRootIndex(
            index)

        for i in range(1, file_system_model.columnCount()):
            tree_view.hideColumn(i)

        width = self.geometry().size().width()
        tree_view.setMaximumWidth(width * self.TREE_VIEW_SCALAR)

        tree_view.clicked.connect(self.on_tree_view_item_clicked)
        tree_view.setToolTip('Available documentation pages')
        return tree_view, file_system_model

    def load_file(self, file_path: str):
        """
        Load file from file_path to help_view

        :param file_path:  absolute path to a document
        """
        url = QtCore.QUrl.fromLocalFile(file_path)
        self.help_view.setSource(url)
        file_name = file_path.split(' _ ')[1]
        file_name = "SOHViewer Help - " + file_name
        self.default_pdf_file_name = file_name.replace('.help.md', '')

    @QtCore.Slot()
    def on_tree_view_item_clicked(self, index: QtCore.QModelIndex):
        """
        Load file to help_view when a file name is click on tree_view

        :param index: index of the file name
        :type index: QtCore.QModelIndex
        """
        path = self.file_system_model.filePath(index)
        self.load_file(path)

    @QtCore.Slot()
    def go_table_contents(self):
        """
        Select Table of Contents on tree view and bring the file to help view
        """
        self._go_to_file(self.contents_table_path)

    @QtCore.Slot()
    def recreate_table_contents(self):
        """
        Recreate Table of Contents when users see any inconsistent with
            the documents in documentation folder.
        """
        create_table_of_content_file(self.docdir_path)
        QtWidgets.QMessageBox.information(
            self, "Link fixed!!!", "Table of Contents has been recreated.")
        self.help_view.clear()
        self.help_view.setSource(
            QtCore.QUrl(self.contents_table_path.as_posix()))

    @QtCore.Slot()
    def go_search_results(self):
        """
        Bring "Search Results.md" file to view
        """
        self._go_to_file(self.search_results_path)

    @QtCore.Slot(QtCore.QUrl)
    def on_source_changed(self, url: QtCore.QUrl):
        """
        When bringing up a page to help_view from clicking on a link on
            help_view, this slot is implemented to set the current selection
            on tree_view and start Search on that page.
        For Search Results, it's better experience if
            not perform search.

        :param url: The url emit from clicking on a link on help view
        :type url: QtCore.QUrl
        """
        self.tree_view.setCurrentIndex(self.file_system_model.index(
            url.path(), 0))
        if const.SEARCH_RESULTS == url.fileName():
            return
        try:
            self.start_search_on_curr_doc(self.search_box.text(),
                                          from_entering_search_text=False)
        except AttributeError:
            # Error happens because highlight_format not exist when help_view
            # first open
            pass

    @QtCore.Slot()
    def search_backward(self):
        """
        When clicking on "Search Backward" button, the cursor_index will go
            back one on the cursor_list, then highlight text in that cursor.
        """
        self.current_index -= 1
        if self.current_index < 0:
            self.current_index = len(self.cursor_list) - 1

        self.help_view.setTextCursor(self.cursor_list[self.current_index])

    @QtCore.Slot()
    def search_forward(self):
        """
        When clicking on "Search Forward" button, the cursor_index will go
            forsard one on the cursor_list, then highlight text in that cursor.
        """
        self.current_index += 1
        if self.current_index > len(self.cursor_list) - 1:
            self.current_index = 0
        self.help_view.setTextCursor(self.cursor_list[self.current_index])

    @QtCore.Slot(str)
    def start_search_on_curr_doc(self, search_text: str,
                                 from_entering_search_text: bool = True):
        """
        If current page is 'Search Results' the content isn't
            matched with search_text anymore. So, navigate to
            'Table of Contents' if in 'Search Results' page and disable
            'Navigate to Search Results' button if the function is called
            from clicking on go_to_search_results_button
        Build cursor_list which is the list of cursors of all search texts'
            occurrences on the current document on help_view.
        Highlight the first cursor to show the starting of the search to user.

        :param search_text: text to search
        :type search_text: str
        :param from_entering_search_text: flag indicate if the method is called
            from entering text in search box
        :type from_entering_search_text: bool
        """
        if from_entering_search_text:
            if self.help_view.source().fileName() == const.SEARCH_RESULTS:
                self.help_view.setSource(
                    QtCore.QUrl(self.contents_table_path.as_posix()))
            self.go_to_search_results_button.setEnabled(False)

        self.help_view.setTextCursor(QtGui.QTextCursor())
        doc = self.help_view.document()
        self.cursor_list = []
        cursor = QtGui.QTextCursor()
        selections = []
        while 1:
            cursor = doc.find(search_text, cursor)
            if cursor.isNull():
                break
            self.cursor_list.append(cursor)
            sel = QtWidgets.QTextEdit.ExtraSelection()
            sel.cursor = cursor
            sel.format = self.highlight_format
            selections.append(sel)
        self.help_view.setExtraSelections(selections)        # to highlight
        self.current_index = 0
        # to roll to the current index and select text
        if self.cursor_list != []:
            self.help_view.setTextCursor(self.cursor_list[self.current_index])

    @QtCore.Slot()
    def search_through_all_docs(self):
        """
        Create the links to all documents that contain search text.
        Bring up the result to view.
        Allow user to go back to the result page.
        """
        self.go_to_search_results_button.setEnabled(True)

        search_text = self.search_box.text()
        if search_text == '':
            return
        search_results_file = create_search_results_file(
            self.docdir_path, search_text)

        self._go_to_file(search_results_file)

    def _go_to_file(self, filepath):
        """
        + Select filename on tree_view,
        + Bring file's content to help_view
        """
        self.tree_view.setCurrentIndex(self.file_system_model.index(
            filepath.as_posix(), 0, 0))
        # pyside2 need the following line to update tree_view
        # but it causes bug in pyside6
        # self.tree_view.update()
        self.help_view.setSource(QtCore.QUrl(filepath.as_posix()))

    @QtCore.Slot()
    def save_to_pdf(self):
        """
        Save the current document to pdf file.

        A3 size is selected because the current font look well in that. After
        the document is saved, user can print it in the size they want. The
        size of A3 is 11.7 x 16.5. So the width of pictures in the document
        will be cut off if greater than (11.7 - margins).

        Note: the pictures' width in the document must be smaller than 11 in/
            1584px or its content will be cut off.
        """
        home_path = Path.home()
        doc_path = home_path.joinpath("Documents")
        file_path = doc_path.joinpath(self.default_pdf_file_name)
        save_file = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save to PDF', file_path.as_posix())[0]
        if save_file != "":
            filename = save_file
            if not filename.endswith('.pdf'):
                filename = filename + '.pdf'
            printer = QPrinter(
                QPrinter.PrinterMode.PrinterResolution.HighResolution)
            printer.setPageSize(
                QtGui.QPageSize.PageSizeId.A3)
            printer.setColorMode(QPrinter.ColorMode.Color)
            printer.setOutputFormat(QPrinter.OutputFormat.PdfFormat)
            printer.setOutputFileName(filename)
            self.help_view.document().print_(printer)
            msg = f"The current help document has been saved at {filename}"
            QtWidgets.QMessageBox.information(self, "Document Saved", msg)


def main():
    app = QtWidgets.QApplication(sys.argv)

    wnd = HelpBrowser(home_path='../')
    wnd.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
