# colors that are good for both light and dark mode
COLOR = {
    'changedStatus': '#268BD2',             # blue
    'changedStatusBackground': "#e0ecff",   # light blue
}

# Just using RGB for everything since some things don't handle color names
# correctly, like PIL on macOS doesn't handle "green" very well.
# b = dark blue, was the U value for years, but it can be hard to see, so U
#     was lightened up a bit.
# Orange should be #FF7F00, but #DD5F00 is easier to see on a white background
# and it still looks OK on a black background.
# Purple should be A020F0, but that was a little dark.
# "X" should not be used. Including X at the end of a passed color pair (or by
# itself) indicates that a Toplevel or dialog box should use grab_set_global()
# which is not a color.

clr = {"B": "#000000", "C": "#00FFFF", "G": "#00FF00", "M": "#FF00FF",
       "R": "#FF0000", "O": "#FF7F00", "W": "#FFFFFF", "Y": "#FFFF00",
       "E": "#DFDFDF", "A": "#8F8F8F", "K": "#3F3F3F", "U": "#0070FF",
       "N": "#007F00", "S": "#7F0000", "y": "#7F7F00", "u": "#ADD8E6",
       "s": "#FA8072", "p": "#FFB6C1", "g": "#90EE90", "r": "#EFEFEF",
       "P": "#AA22FF", "b": "#0000FF", "o": "#f7e5a8"}

# This is just if the program wants to let the user know what the possibilities
# are.
clr_desc = {"B": "black", "C": "cyan", "G": "green", "M": "magenta",
            "R": "red", "O": "orange", "W": "white", "Y": "yellow",
            "E": "light gray", "A": "gray", "K": "dark gray", "U": "blue",
            "N": "dark green", "S": "dark red", "y": "dark yellow",
            "u": "light blue", "s": "salmon", "p": "light pink",
            "g": "light green", "r": "very light gray", "P": "purple",
            "b": "dark blue", "o": "light orange"}


def set_color_mode(mode):
    """
    get the display_color dict according to mode
    :param mode: "B" or "W"
    """
    display_color = {}
    if mode == "B":
        display_color["background"] = clr["B"]
        display_color['basic'] = clr["W"]
        display_color['sub_basic'] = clr["A"]
        display_color["plot_label"] = clr["C"]
        display_color["time_ruler"] = clr["Y"]
        display_color["zoom_marker"] = "#FFA500"   # to show up on tps's colors
        display_color["warning"] = clr["O"]
        display_color["error"] = clr["R"]
        display_color["state_of_health"] = clr["u"]
        display_color["highlight"] = clr['P']
        display_color["highlight_background"] = clr['u']
    elif mode == "W":
        display_color["background"] = clr["W"]
        display_color["basic"] = clr["B"]
        display_color['sub_basic'] = clr["A"]
        display_color["plot_label"] = clr["B"]
        display_color["time_ruler"] = clr["U"]
        display_color["zoom_marker"] = "#FFA500"   # to show up on tps's colors
        display_color["warning"] = clr["O"]
        display_color["error"] = clr["s"]
        display_color["state_of_health"] = clr["u"]
        display_color["highlight"] = clr['P']
        display_color["highlight_background"] = clr['u']
    return display_color
