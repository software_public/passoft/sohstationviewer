import re

hexcolor_re = '#[0-9A-F]{6}'                        # Ex: #0F3A4C
value_re = r'-?[0-9]\.?[0-9]?'                      # Ex: 0.1, -2.2, 3
le_value_re = rf'<={value_re}'                      # Ex: <=0.1
gt_value_re = rf'{value_re}<'                       # Ex: 0.1<
le_gt_value_re = rf'({le_value_re}|{gt_value_re})'  # Ex: <=0.1|0.1<
l_e_value_re = rf'[=<]{value_re}'                   # Ex: <0.1, =-0.1
size_re = '(:((smaller)|(normal)|(bigger)))?'       # Ex: :smaller

plot_types = {
        'linesDots': {
            "description": (
                "Lines, one color dots. Dot or line/color mapping defined by "
                "ValueColors.\n"
                "Ex: Line:#00FF00|Dot:#FF0000  means\n"
                "   Lines are plotted with color #00FF00\n"
                "   Dots are plotted with color #FF0000\n"
                "If Dot is not defined, dots won't be displayed.\n"
                "If L is not defined, lines will be plotted with color "
                "#00FF00.\n\n"
                "If Zero is defined, this plot type will plot a line through "
                "bottom points (<0) and top points (>0). Zero points will be "
                "plotted in the middle."
                "Ex: Line:#00FF00|Dot:#FF0000|Zero:#0000FF"
            ),
            "plot_function": "plot_lines_dots",
            "value_pattern": re.compile('^(L|D|Z|Line|Dot|Zero)$'),
            "pattern": re.compile(f'^$|^(?:Line|Dot|Zero):{hexcolor_re}$'),
            "default_value_color": "Line:#00CC00"
        },
        'linesSRate': {
            "description": "Lines, one color dots, bitweight info. ",
            "plot_function": "plot_lines_s_rate"
        },
        'linesMasspos': {
            "description": "multi-line mass position, multi-color dots. ",
            "plot_function": "plot_lines_mass_pos"
        },
        'triColorLines': {
            "description": (
                "Three values -1,0,1 in three lines with three "
                "different colors according to valueColors:\n"
                "Ex: -1:#FF0000|0:#00FF00|1:#0000FF  means\n"
                "value = -1  => plot on line y=-1 with #FF0000 color\n"
                "value = 0   => plot on line y=0 with #00FF00 color\n"
                "value = 1 => plot on line y=1 with #0000FF color"),
            "plot_function": "plot_tri_colors",
            "value_pattern": re.compile('^-?[10]?$'),
            "pattern": re.compile(f'^-?[10]:{hexcolor_re}$'),
            "default_value_color": "-1:#FF0000|0:#00CC00|1:#0099DD",
            "total_value_color_required": 3
        },
        'dotForTime': {
            "description": (
                "Dots according to timestamp.\n"
                "Color defined by ValueColors.\n"
                "Ex: Color:#00FF00"),
            "plot_function": "plot_dot_for_time",
            "value_pattern": re.compile('^C|(Color)$'),
            "pattern": re.compile(f'^$|^Color:{hexcolor_re}$'),
            "default_value_color": "Color:#00CC00",
            "total_value_color_required": 1
        },
        'multiColorDotsEqualOnUpperBound': {
            "description": (
                "Multicolor dots in center with value/colors mapping defined "
                "by ValueColors.\n"
                "Ex: <=-1:not plot|<=0:#FF0000|0<:#FF00FF  means:\n"
                "   value <= -1   => not plot\n"
                "   -1 < value <= 0 => plot with #FF0000 color\n"
                "   0 < value  => plot with #FF00FF color\n"
                "If the valueColor has arrow up (or the word 'bigger'),"
                " the point size will be bigger\n"
                "If the valueColor has arrow down (or the word 'smaller'),"
                " the point size will be smaller\n"
            ),
            "plot_function": "plot_multi_color_dots_equal_on_upper_bound",
            "value_pattern": re.compile(rf'^{le_gt_value_re}$'),
            "pattern": re.compile(
                fr'^{le_gt_value_re}:({hexcolor_re}|not plot){size_re}$'
            ),
            "default_value_color": "<=0:#FF0000|0<:#FF00FF"
        },
        'multiColorDotsEqualOnLowerBound': {
            "description": (
                "Multicolor dots in center with value/colors mapping defined "
                "by ValueColors.\n"
                "Ex: <-1:not plot|<0:#FF0000|=0:#FF00FF  means:\n"
                "   value < -1   => not plot\n"
                "   -1 =< value < 0 => plot with #FF0000 color\n"
                "   value >= 0  => plot with #FF00FF color\n"
                "If the valueColor has arrow up (or the word 'bigger'),"
                " the point size will be bigger\n"
                "If the valueColor has arrow down (or the word 'smaller'),"
                " the point size will be smaller\n"
            ),
            "plot_function": "plot_multi_color_dots_equal_on_lower_bound",
            "value_pattern": re.compile(rf'^{l_e_value_re}$'),
            "pattern": re.compile(
                fr'^{l_e_value_re}:({hexcolor_re}|not plot){size_re}$'
            ),
            "default_value_color": "<0:#FF0000|=0:#00CC00"
        },
        'upDownDots': {
            "description": (
                "Two different values: one above the center line, the other "
                "under it. Colors defined by ValueColors.\n"
                "Ex: Down:#FF0000|Up:#00FFFF  means:\n"
                "   value == 1 => plot above center line with #00FFFF color\n"
                "   value == 0 => plot under center line with #FF0000 color"),
            "plot_function": 'plot_up_down_dots',
            "value_pattern": re.compile("^(0|1|Up|Down)$"),
            "pattern": re.compile(f"^(?:Up|Down):{hexcolor_re}$"),
            "default_value_color": "Down:#FF0000|Up:#00FFFF",
            "total_value_color_required": 2
        }
    }
