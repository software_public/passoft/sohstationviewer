from __future__ import annotations

from PySide6 import QtCore
from PySide6.QtGui import QCloseEvent
from PySide6.QtWidgets import QWidget


class DialogAlreadyOpenedError(Exception):
    """
    Error to raise when a child of OneWindowAtATimeDialog already has a window
    shown.
    """
    pass


class OneWindowAtATimeDialog(QWidget):
    """
    A widget type which can only have one instance at a time.

    The implementation of this class is similar to that of a singleton widget.
    The main difference is that when the sole existing instance of this class
    is closed, it is also deleted, allowing another instance to be created.
    """
    current_instance: OneWindowAtATimeDialog = None

    def __init__(self) -> None:
        super().__init__()
        # Allow each database dialog to only have one window open at a time.
        # If we allow a database dialog to have multiple windows open at one
        # time, we would need to somehow sync the windows when one of them
        # update the database. While it is possible to do that (keep a list
        # of all windows and update them when one window updates the database),
        # that would be too much work for a feature that might not be used too
        # much.
        if self.__class__.current_instance:
            raise DialogAlreadyOpenedError()
        self.__class__.current_instance = self
        # If we don't have this line, all dialogs of this type will be kept in
        # memory (even when they are closed) until the program is closed. This
        # creates a memory leak.
        self.setAttribute(QtCore.Qt.WidgetAttribute.WA_DeleteOnClose)

    def closeEvent(self, event: QCloseEvent) -> None:
        """
        When the currently opened window is closed, remove the stored reference
        to it.

        :param event: the event emitted when the currently opened window is
            closed
        """
        self.__class__.current_instance = None
        super().closeEvent(event)
