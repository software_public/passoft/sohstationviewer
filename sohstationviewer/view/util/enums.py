import enum


class LogType(enum.Enum):
    INFO = 0
    WARNING = 1
    ERROR = 2
