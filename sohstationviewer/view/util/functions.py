import re
from pathlib import Path
from typing import Dict, List, Tuple, Union

from sohstationviewer.view.util.enums import LogType
from sohstationviewer.conf import constants as const
from sohstationviewer.conf.constants import (WF_1ST, WF_2ND, WF_3RD)


def is_doc_file(file: Path, include_table_of_contents: bool = False)\
        -> bool:
    """
    Check if file is a document which is an '.help.md' file

    :param file: Absolute path to file
    :type file: Path
    :param include_table_of_contents: if Table of Contents file is considered
        as doc file or not
    :type include_table_of_contents: bool
    :return: True if file is a document, False otherwise
    :rtype: bool
    """
    if not file.is_file():
        return False
    if not file.name.endswith('.help.md'):
        return False
    if not include_table_of_contents and file.name == const.TABLE_CONTENTS:
        return False
    return True


def create_search_results_file(base_path: Path, search_text: str)\
        -> Path:
    """
    Create 'Search Results.md' file if not exist.
    Write file content which includes all links to '.help.md' files of which
        content contains search_text, excluding Table of Contents file.
    Format of each link is [name](URL)

    :param base_path: directory where document files are located
    :type base_path: Path
    :param search_text: text to search in each file
    :type search_text: str
    :return: path to search through file
    :rtype: Path
    """
    all_files = [f for f in list(base_path.iterdir()) if is_doc_file(f)]

    search_header = "# Search results\n\n"
    search_results = ""
    for file in sorted(all_files):
        with open(file) as f:
            content = f.read()
            if search_text in content:
                # space in URL must be replace with %20
                url_name = file.name.replace(" ", "%20")
                base_file_name = file.stem.split('.help')[0]
                try:
                    display_file_name = base_file_name.split(" _ ")[1]
                except IndexError:
                    display_file_name = base_file_name
                search_results += (f"+ [{display_file_name}]"
                                   f"({url_name})\n\n")
    if search_results == "":
        search_notfound = f"Text '{search_text}' not found."
        search_results = search_header + search_notfound
    else:
        search_found = (f"Text '{search_text}' found in the following files:"
                        f"\n\n---------------------------\n\n")
        search_results = search_header + search_found + search_results

    search_through_file = Path(base_path).joinpath(const.SEARCH_RESULTS)
    with open(search_through_file, "w") as f:
        f.write(search_results)
    return search_through_file


def create_table_of_content_file(base_path: Path) -> None:
    """
    Creating Table of Contents which includes all links to '.help.md' files.
    Format of each link is [name](URL)
    This function is added to __main__. So run functions.py to create "Table
        of Contents" file.

    :param base_path: directory where document files are located
    :type base_path: Path
    """
    all_files = [f for f in list(base_path.iterdir())
                 if is_doc_file(f, include_table_of_contents=True)]

    header = (
        "# SOHViewer Documentation\n\n"
        "Welcome to the SOHViewer documentation. Here you will find "
        "usage guides and other useful information in navigating and using "
        "this software.\n\n"
        "On the left-hand side you will find a list of currently available"
        " help topics.\n\n"
        "If the links of the Table of Contents are broken, click on Recreate "
        "Table of Content <img src='recreate_table_contents.png' height=30 /> "
        "to rebuild it.\n\n"
        "The home button can be used to return to this page at any time.\n\n"
        "# Table of Contents\n\n")
    links = ""

    for file in sorted(all_files):
        # space in URL must be replace with %20
        url_name = file.name.replace(" ", "%20")
        base_file_name = file.stem.split('.help')[0]
        try:
            display_file_name = base_file_name.split(" _ ")[1]
        except IndexError:
            display_file_name = base_file_name
        links += f"+ [{display_file_name}]({url_name})\n\n"

    contents = header + links

    contents_table_file = Path(base_path).joinpath(const.TABLE_CONTENTS)
    with open(contents_table_file, "w") as f:
        f.write(contents)
        print(f"{contents_table_file.absolute().as_posix()} has been created.")


def get_soh_messages_for_view(
        data_set_id: Union[str, Tuple[str, str]],
        soh_messages: Dict[str, Union[List[str], Dict[str, List[str]]]]) ->\
        Dict[str, List[str]]:
    """
    Convert SOH message of the selected data_set_id and TEXT log to dict of
    list of str to display

    :param data_set_id: id of selected data set: station_id
        or (experiment_number, serial)
    :param soh_messages: {'TEXT': log_text,
                        data_set_id:{chan_id: sos_messages,},}
    :return: {'TEXT'/chan_id: [str,]
    """

    soh_message_view = {}

    if 'TEXT' in soh_messages.keys() and soh_messages['TEXT'] != []:
        soh_message_view['TEXT'] = []
        for msg_list in soh_messages['TEXT']:
            for msg in msg_list.split('\n'):
                soh_message_view['TEXT'].append(msg)
    if data_set_id in soh_messages.keys():
        for chan_id in soh_messages[data_set_id]:
            soh_message_view[chan_id] = []

            for msg_lines in soh_messages[data_set_id][chan_id]:
                for msg in msg_lines.split('\n'):
                    soh_message_view[chan_id].append(msg)
    return soh_message_view


def log_str(log_info: Tuple[str, LogType]) -> str:
    """
    Convert log_info to string that showing log line in saved file
    :param log_info: tuple include text and type of a log line
    :type log_info: Tuple[str, LogType]
    :return: line of log to save in file
    :rtype: str
    """
    log_text, log_type = log_info
    return f"{log_type.name}: {log_text}"


def check_chan_wildcards_format(wildcards: str):
    """
    Check format of channel's wildcards. Raise exception if invalid.
    :param wildcards: wildcards that are separated with ','.
    :type wildcards: str
    """
    for wc in wildcards.split(','):
        wc = wc.strip()

        if len(wc) == 1 and wc != '*':
            raise Exception(
                f"Request '{wc}' has length=1 which must be '*'.")
        if '**' in wc:
            raise Exception(
                f"Request '{wc}' includes '**' which isn't allowed.")
        if len(wc) > 3:
            raise Exception(
                f"Request '{wc}' has length={len(wc)} > 3 which isn't allowed."
            )
        if len(wc) == 2 and '*' not in wc:
            raise Exception(
                f"Request '{wc}' has length=2 which required one '*'."
            )
        pattern = f"[{WF_1ST}*]"
        if not re.compile(pattern).match(wc[0]):
            raise Exception(
                f"Request '{wc}' has first character not match {pattern}."
            )
        pattern = f"[{WF_3RD}*]"
        if not re.compile(pattern).match(wc[-1]):
            raise Exception(
                f"Request '{wc}' has last character not match {pattern}."
            )
        if len(wc) == 3:
            pattern = f"[{WF_2ND}*]"
            if not re.compile(pattern).match(wc[1]):
                raise Exception(
                    f"Request '{wc}' has second character not match {pattern}."
                )


def check_masspos(mp_data: Dict[str, Dict],
                  selected_data_set_id: Union[tuple, str],
                  include_mp123: bool, include_mp456: bool) -> None:
    """
    Check mass positions channels to raise Exception if requested channels
        aren't included.
    :param mp_data: mass position channels of the selected selected_data_set_id
    :param selected_data_set_id: selected data set id to be used in the
        error message
    :param include_mp123: if mass position channels 1,2,3 are requested
    :param include_mp456: if mass position channels 4,5,6 are requested
    """
    included_mp = []
    for chan_id in mp_data.keys():
        included_mp.append(chan_id[-1])
    req_mp = []

    if include_mp123:
        req_mp += ['1', '2', '3']
    if include_mp456:
        req_mp += ['4', '5', '6']

    not_included_mp = [mp for mp in req_mp if mp not in included_mp]

    if not_included_mp != []:
        raise Exception(f"Data set {selected_data_set_id} doesn't include mass"
                        f" position {','.join(not_included_mp)}")


def extract_netcodes(data_obj):
    """
    Extract information about network codes from data_obj.nets_by_sta which
        is a dictionary with key is a station id and values is all network ids
        for that station.
    This function will return one string for each item showing the network ids
        with the corresponding station. Multiple lines will be sparated with
        "\n\t"

    This function will combine all tuples with the same represent netids
        and return one string for each represent netids separated with "\n\t".
        Ex: nets_by_sta = {'3734': {'XX', 'NA'},
                           '1540': {'XX'}}
            return: "XX,NA (3734)\n\tXX (1540)"
    :param data_obj: the data object to extract file information from
    :return: netid string
    """
    nets = data_obj.nets_by_sta
    net_info_list = []

    for k, v in nets.items():
        net_str = ','.join(sorted(list(v)))
        if len(nets) == 1:
            net_info_list = [net_str]
        else:
            net_info_list.append(f"{net_str} (STA {k})")
    return '\n\t'.join(net_info_list)


def remove_not_found_chans(
        chan_order: List[str], actual_chans: List[str],
        processing_log: List[Tuple[str, LogType]]) -> List[str]:
    """
    Remove channels that are not found in actual_chans from chan_order.

    :param chan_order: list of channels in order that user wants to plot
    :param actual_chans: The actual channel list
    :param processing_log: The log list to keep track with not found channels
    :return: chan_order from which not found channels have been removed.
    """
    not_found_chans = [c for c in chan_order if c not in actual_chans]
    if not_found_chans != []:
        msg = (f"No data found for the following channels: "
               f"{', '.join(not_found_chans)}")
        processing_log.append((msg, LogType.WARNING))
    return [c for c in chan_order if c not in not_found_chans]


def replace_actual_question_chans(
        chan_order: List[str], actual_chans: List[str]) -> List[str]:
    """
    Remove channels end with '?' from chan_order and replace with corresponding
        channels found in actual channels.

    :param chan_order: The list of channel that have channels end with '?'
    :param actual_chans: The actual channel list
    :return: chan_order that have channels end with '?' replaced by actual
        channels.
    """
    question_chans = [c for c in chan_order if c.endswith('?')]
    for qc in question_chans:
        actual_question_chans = [c for c in list(actual_chans)
                                 if qc[:-1] == c[:-1]]
        if actual_question_chans:
            question_idx = chan_order.index(qc)
            chan_order.remove(qc)
            # replace a question channel with the actual channels that it
            # represent for
            chan_order[question_idx:question_idx] = \
                sorted(actual_question_chans)
    return chan_order


if __name__ == '__main__':
    create_table_of_content_file(Path('../../../documentation'))
