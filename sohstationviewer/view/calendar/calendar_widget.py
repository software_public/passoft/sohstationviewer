from PySide6 import QtCore, QtGui, QtWidgets


class CalendarWidget(QtWidgets.QCalendarWidget):
    """
    Widget of a calendar allow user to select date with a checkbox to display
    day of year
    """
    def __init__(self, parent):
        super().__init__(parent)
        """
        toggle_day_of_year: QCheckBox: for user to choose to show day of year
            or not
        """
        self.toggle_day_of_year = None
        """
        _show_day_of_year: boolean - flag showing if need to show day of year
            or not
        """
        self._show_day_of_year = getattr(
            self, 'toggle_day_of_year', None) is None
        self.setup_ui()

    def setup_ui(self):
        """
        Set up the display of the calendar widget with:
        + size
        + add checkbox self.toggle_day_of_year that allow user to choose
            showing DOY or not.
        """
        self.setMinimumWidth(300)
        self.setMinimumHeight(275)

        nav_bar = self.findChild(
            QtWidgets.QWidget, 'qt_calendar_navigationbar'
        )

        if nav_bar:
            self.toggle_day_of_year = QtWidgets.QCheckBox('Show DOY')
            nav_bar.layout().insertWidget(2, self.toggle_day_of_year)

            self.toggle_day_of_year.toggled.connect(self.set_show_day_of_year)
            self.toggle_day_of_year.setCheckable(True)

            palette = self.toggle_day_of_year.palette()
            palette.setColor(QtGui.QPalette.ColorRole.WindowText,
                             QtGui.QColor('white'))
            self.toggle_day_of_year.setPalette(palette)

            self.toggle_day_of_year.show()

            self.set_show_day_of_year(False)

    def show_day_of_year(self):
        """
        Get the value of self._show_day_of_year
        """
        return self._show_day_of_year

    def paintCell(self, painter, rect, date):
        """
        OVERRIDE Qt method.
        Display day of year of the given date at the bottom left of the cell.
        :param painter: painter to paint the widget
        :param rect: rectangle define the position of the widget
        :param date: the specific date of a cell in calendar widget
        """
        super().paintCell(painter, rect, date)

        painter.save()

        if self.show_day_of_year():
            color = (QtGui.QColor('red') if date != self.selectedDate()
                     else QtGui.QColor('white'))
            font = painter.font()
            font.setPointSize(font.pointSize() * 0.9)
            painter.setFont(font)
            painter.setPen(color)
            painter.drawText(rect.bottomLeft(), str(date.dayOfYear()))
        painter.restore()

    @QtCore.Slot(bool)
    def set_show_day_of_year(self, value):
        """
        When the checkbox self.toggle_day_of_year is selected, variable
        self._show_day_of_year will be updated and all cells of the calendar
        widget will be updated with day of year as well
        """
        self._show_day_of_year = value
        self.updateCells()
