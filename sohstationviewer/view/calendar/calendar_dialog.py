from PySide6 import QtWidgets

from sohstationviewer.view.ui.calendar_ui_qtdesigner import Ui_CalendarDialog


class CalendarDialog(QtWidgets.QDialog, Ui_CalendarDialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
