import getpass
import os

from PySide6 import QtCore, QtWidgets


UrlRole = QtCore.Qt.UserRole + 1
EnabledRole = QtCore.Qt.UserRole + 2

EXT_DRIVE_BASE = ['/media', '/run/media']


def get_external_drive_url():
    """
    External drives of Linux machine can be located under:
         /media/<username>
      OR /run/media/<username>
    return url_dict: dict with key is a url of an external drive's path and
        value is the drive's name.
    """
    url_dict = {}
    for base in EXT_DRIVE_BASE:
        if not os.path.isdir(base):
            continue
        username = getpass.getuser()
        external_drive_root = os.path.join(base, username)
        if not os.path.isdir(external_drive_root):
            continue
        for d in os.listdir(external_drive_root):
            full_path = os.path.join(os.path.join(external_drive_root, d))
            if not os.path.isdir(full_path):
                continue
            url_dict[QtCore.QUrl.fromLocalFile(full_path)] = d
    return url_dict


class URLShortcutTextDelegate(QtWidgets.QStyledItemDelegate):
    """
    Help with displaying shortcut text for each drive.
    https://stackoverflow.com/questions/69284292/is-there-an-option-to-rename-a-qurl-shortcut-in-a-sidebar-of-a-qfiledialog  # noqa: E501
    """
    mapping = dict()

    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        url = index.data(UrlRole)
        text = self.mapping.get(url)
        if isinstance(text, str):
            option.text = text
        is_enabled = index.data(EnabledRole)
        if is_enabled is not None and not is_enabled:
            option.state &= ~QtWidgets.QStyle.State_Enabled


class FileDialogForLinuxExternalDrive(QtWidgets.QFileDialog):
    """
    File Dialog that show external drives of linux (Ubuntu, Fedora) on sidebar
    """
    def __init__(self, parent, curr_dir):

        url_dict = get_external_drive_url()
        # only use customized sidebar if len(url_dict)>0
        options = (QtWidgets.QFileDialog.Option.DontUseNativeDialog if url_dict
                   else QtWidgets.QFileDialog.Option.ShowDirsOnly)

        super().__init__(parent, caption="Select Main Data Directory",
                         options=options,
                         fileMode=QtWidgets.QFileDialog.FileMode.Directory)
        if url_dict:
            self.setSidebarUrls(self.sidebarUrls() + list(url_dict.keys()))
            sidebar = self.findChild(QtWidgets.QListView, "sidebar")
            # fit sidebar with content
            sidebar.setMinimumWidth(sidebar.sizeHintForColumn(0))
            delegate = URLShortcutTextDelegate(sidebar)
            delegate.mapping = url_dict
            sidebar.setItemDelegate(delegate)

        self.setDirectory(curr_dir)
