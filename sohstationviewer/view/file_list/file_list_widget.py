from PySide6 import QtWidgets


class FileListItem(QtWidgets.QListWidgetItem):
    """
    Widget to select file and save the absolute path of the file under
    self.file_path variable
    """
    def __init__(self, file_path, parent=None):
        try:
            path = file_path.name
        except AttributeError:
            path = file_path
        super().__init__(path, parent,
                         type=QtWidgets.QListWidgetItem.ItemType.UserType)
        self.file_path = path
