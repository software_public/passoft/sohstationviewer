from PySide6.QtCore import Qt
from PySide6.QtGui import (
    QKeyEvent, QWheelEvent, QContextMenuEvent, QAction,
)
from PySide6.QtWidgets import (
    QDateEdit, QLineEdit, QMenu
)


class DayOfYearSupportedDateTextBox(QDateEdit):
    """
    A variant of QDateEdit that support the string 'DOY' as a display format.
    Currently, the only way to change the date in this widget is to use a
    calendar widget. This is due to the difficulty of the other options (adding
    the capability to edit the DOY format and reimplementing the whole
    QDateEdit class).
    """
    def __init__(self, *args, date=None, parent=None, **kwargs):
        super().__init__(date, parent, *args, **kwargs)
        self.dateChanged.connect(self.on_date_changed)
        # Store the current display format so that we know if 'DOY' is in it.
        self._raw_display_format = ''

    def keyPressEvent(self, event: QKeyEvent):
        """
        Intercept key presses used to edit the content of the widget and ignore
        them. If a key press does not fit this description, pass the key press
        on to the next widget in the chain.

        :param event: the key press event being examined
        """
        ignored_keys = [Qt.Key_Enter, Qt.Key_Return, Qt.Key_Up, Qt.Key_Down,
                        Qt.Key_PageUp, Qt.Key_PageDown, Qt.Key_Delete,
                        Qt.Key_Backspace, Qt.Key_1, Qt.Key_2, Qt.Key_3,
                        Qt.Key_4, Qt.Key_5, Qt.Key_6, Qt.Key_7, Qt.Key_8,
                        Qt.Key_9]
        if event.key() in ignored_keys:
            return
        super().keyPressEvent(event)

    def on_date_changed(self):
        """
        On the stored date being changed, reset the date format so that the
        displayed date can be formatted correctly.
        """
        self.setDisplayFormat(self._raw_display_format)

    def wheelEvent(self, event: QWheelEvent):
        """
        Intercept the event of the mouse wheel being scrolled and ignore it.

        When the mouse wheel is scrolled, the currently selected date component
        is edited. That is why we ignore the event.

        :param event: the mouse wheel event being ignored.
        """
        event.accept()

    def contextMenuEvent(self, event: QContextMenuEvent):
        """
        Create a context menu with the options to copy or select all content
        when there is a right click on the widget.

        While QDateEdit has a built-in context menu, it contains options that
        edit the content of the widget. It is difficult to remove those
        options, so we create a new context menu without them instead.

        :param event: the right click event that requests a context menu
        """
        lineedit: QLineEdit = self.findChild(QLineEdit, 'qt_spinbox_lineedit')
        menu = QMenu(self)
        copy_action = QAction('Copy', menu)
        copy_action.setCheckable(False)
        copy_action.triggered.connect(lineedit.copy())

        select_all_action = QAction('Select all', menu)
        select_all_action.setCheckable(False)
        select_all_action.triggered.connect(self.selectAll)

        menu.addAction(copy_action)
        menu.addAction(select_all_action)
        menu.exec(event.globalPos())

    def displayFormat(self) -> str:
        return self._raw_display_format

    def setDisplayFormat(self, format):
        """
        If 'DOY' is in format, replace it with the day of year of the date
        being stored by the widget. Then, call QDateEdit.setDisplayFormat().
        Done this way because it is a lot easier to replace 'DOY' with the
        appropriate value then to make the widget accept 'DOY' as a format.

        :param format: the date format being set.
        """
        self._raw_display_format = format
        if 'DOY' in format:
            format = format.replace('DOY', f"{str(self.date().dayOfYear())}")
        super().setDisplayFormat(format)
