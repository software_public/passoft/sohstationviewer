import math
import traceback
from pathlib import Path
from typing import List, Optional, Literal, Iterable, Any

from PySide6 import QtWidgets, QtCore
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QLineEdit, QLabel, QHBoxLayout
from matplotlib.axes import Axes
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure
from matplotlib.text import Text

from sohstationviewer.controller.util import display_tracking_info
from sohstationviewer.view.plotting.gps_plot.gps_point import GPSPoint
from sohstationviewer.view.util.enums import LogType


class SuffixedLineEdit(QWidget):
    """
    A one-line text editor that contains a default suffix.
    """
    def __init__(self, suffix: str, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.editor = QLineEdit(self)
        self.editor.setContentsMargins(0, 0, 0, 0)

        self.suffix = QLabel(suffix, self)
        self.setup_ui()

    def setup_ui(self):
        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)
        layout.addWidget(self.editor, 1)
        layout.addWidget(self.suffix)

    def set_text(self, text: str):
        """
        Set the content of this editor.
        :param text: the new content of this editor
        """
        self.editor.setText(text)

    def text(self) -> str:
        """
        Return the content of this editor.
        :return: the content of this editor with the suffix attached
        """
        return self.editor.text() + self.suffix.text()


class GPSWidget(QtWidgets.QWidget):
    """
    Widget that contains the GPS plot.
    """
    def __init__(self, parent: QtWidgets.QWidget,
                 tracking_box: QtWidgets.QTextBrowser):
        super().__init__(parent)
        self.tracking_box = tracking_box
        self.gps_points: Optional[List[GPSPoint]] = None
        # It would be a waste of time to plot points that have the same
        # coordinates (i.e. the same latitude and longitude) so we only plot
        # one point for each coordinate.
        # We use a list instead of a set because matplotlib's pick event only
        # provides the index of a point being clicked, and set doesn't support
        # indexing.
        self.unique_gps_points: Optional[List[GPSPoint]] = None

        self.fig = Figure(figsize=(6, 6), dpi=100, facecolor='#ECECEC')
        self.canvas = Canvas(self.fig)
        self.canvas.mpl_connect('pick_event', self.on_pick_event)

        self.ax: Axes = self.fig.add_axes((0, 0, 1, 1))
        # Force the plot to be square.
        self.ax.set_box_aspect(1)
        # Remove all traces of the axes, leaving only the plot.
        self.ax.set_yticks([])
        self.ax.set_xticks([])
        self.ax.spines['right'].set_visible(False)
        self.ax.spines['left'].set_visible(False)
        self.ax.spines['top'].set_visible(False)
        self.ax.spines['bottom'].set_visible(False)

        # The text on the plot that shows the range of latitude and longitude
        # in meter.
        self.range_text_plot_widget: Optional[Text] = None

        self.background_color = ''
        self.point_color = ''
        self.text_color = ''

        self.set_colors('B')

    def set_colors(self, color_mode: Literal['B', 'W']) -> None:
        """
        Set the colors of the plot based on the chosen color mode in the main
        window.

        :param color_mode: the chosen color mode. Can be either 'B' or 'W'.
        """
        if color_mode == 'B':
            self.background_color = 'dimgray'
            self.point_color = 'white'
            self.text_color = 'white'
        else:
            self.background_color = 'white'
            self.point_color = 'black'
            self.text_color = 'black'
        self.ax.set_facecolor(self.background_color)
        if self.ax.get_lines():
            self.ax.get_lines()[0].set_markerfacecolor(self.point_color)
            self.range_text_plot_widget.set_color(self.text_color)

        self.canvas.draw()
        self.repaint()

    @QtCore.Slot()
    def plot_gps(self) -> None:
        """
        Plot the GPS points onto the canvas. Display the latitude and longitude
        range in meters and adjust the x and y limits of the plot so that the
        axis is square.
        """
        if not self.gps_points:
            msg = 'There is no GPS data to plot.'
            display_tracking_info(self.tracking_box, msg, LogType.ERROR)
            return

        all_latitudes = [point.latitude for point in self.unique_gps_points]
        all_longitudes = [point.longitude for point in self.unique_gps_points]
        max_latitude = max(all_latitudes)
        min_latitude = min(all_latitudes)
        max_longitude = max(all_longitudes)
        min_longitude = min(all_longitudes)

        latitude_range = convert_latitude_degree_to_meter(
            max_latitude - min_latitude
        )
        # qpeek uses the average latitude in this conversion, so we copy it.
        longitude_range = convert_longitude_degree_to_meter(
            max_longitude - min_longitude,
            (max_latitude + min_latitude) / 2
        )

        self.ax.plot(all_longitudes, all_latitudes, linestyle='',
                     markerfacecolor=self.point_color,
                     marker='s', markersize=4,
                     markeredgecolor='black', picker=True, pickradius=4)
        range_text = (f'Lat range: {latitude_range:.2f}m    '
                      f'Long range: {longitude_range:.2f}m')
        self.range_text_plot_widget = self.ax.text(
            0.01, 0.96, range_text, color=self.text_color, size=10,
            transform=self.ax.transAxes
        )

        # Stretch the shorter axis so that it forms a square with the long
        # axis.
        latitude_range = max_latitude - min_latitude
        longitude_range = max_longitude - min_longitude
        if latitude_range != longitude_range:
            if latitude_range > longitude_range:
                longitude_average = (min_longitude + max_longitude) / 2
                min_longitude = longitude_average - latitude_range / 2
                max_longitude = longitude_average + latitude_range / 2
                self.ax.set_xlim(min_longitude, max_longitude)
            else:
                latitude_average = (min_latitude + max_latitude) / 2
                min_latitude = latitude_average - longitude_range / 2
                max_latitude = latitude_average + longitude_range / 2
                self.ax.set_ylim(min_latitude, max_latitude)

        self.canvas.draw()
        self.repaint()
        msg = (f'Plotted {len(self.unique_gps_points)} out of '
               f'{len(self.gps_points)} points.')
        display_tracking_info(self.tracking_box, msg)

    def clear_plot(self) -> None:
        """
        Clear the GPS plot.
        """
        self.ax.clear()
        # Axes.clear() also clear the ticks on the axis, so we have to reset
        # the ticks to make them disappear.
        self.ax.set_yticks([])
        self.ax.set_xticks([])
        if self.range_text_plot_widget is not None:
            self.range_text_plot_widget.remove()
            self.range_text_plot_widget = None
        self.canvas.draw()
        self.repaint()
        self.tracking_box.clear()

    def on_pick_event(self, event) -> Any:
        """
        On a GPS point being picked, display the data of that point on the
        tracking box.

        :param event: the pick event
        """
        # index of the clicked point on the plot
        click_plot_index = event.ind[0]
        picked_point = self.unique_gps_points[click_plot_index]

        # We are plotting the GPS points with North facing up and East facing
        # right(the standard orientation of a map). The sign of the latitude
        # and longitude follows correspondingly.
        lat_dir = 'N' if picked_point.latitude > 0 else 'S'
        long_dir = 'E' if picked_point.longitude > 0 else 'W'
        meta_separator = '&nbsp;' * 7
        loc_separator = '&nbsp;' * 5
        msg = (
            f' Mark: {picked_point.last_timemark}{meta_separator}'
            f'Fix: {picked_point.fix_type}{meta_separator}'
            f'Sats: {picked_point.num_satellite_used}<br>'
            f' Lat: {lat_dir}{abs(picked_point.latitude):.6f}{loc_separator}'
            f'Long: {long_dir}{abs(picked_point.longitude):.6f}{loc_separator}'
            f'Elev: {picked_point.height}{picked_point.height_unit}'
        )
        display_tracking_info(self.tracking_box, msg)


class GPSDialog(QtWidgets.QWidget):
    def __init__(self, parent: QtWidgets.QWidget = None):
        super().__init__()
        self.parent = parent

        self.export_file_extension = '.gps.dat'

        self.data_path: Optional[Path] = None
        # The directory the GPS will be exported to. By default, this will be
        # the folder that contains the data set.
        self.export_dir: Path = Path('')

        self.export_dir_button = QtWidgets.QPushButton("Export Directory")
        self.export_dir_textbox = QtWidgets.QLineEdit()

        self.export_file_name_label = QtWidgets.QLabel("Export File Name:")
        self.export_file_name_textbox = SuffixedLineEdit(
            self.export_file_extension
        )

        self.info_text_browser = QtWidgets.QTextBrowser(self)
        self.plotting_widget = GPSWidget(self, self.info_text_browser)

        self.read_button = QtWidgets.QPushButton('Read/Plot', self)
        self.export_button = QtWidgets.QPushButton('Export', self)
        self.close_button = QtWidgets.QPushButton('Close', self)

        self.setup_ui()

    def setup_ui(self) -> None:
        """
        Set up the user interface of the dialog.
        """
        # The size of the dialog is chosen so that the GPS plot is flush with
        # the edges of the dialog.
        self.setGeometry(300, 300, 449, 579)
        self.setWindowTitle("GPS Plot")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.setSpacing(0)

        export_layout = QtWidgets.QGridLayout()
        export_layout.addWidget(self.export_dir_button, 0, 0)
        export_layout.addWidget(self.export_dir_textbox, 0, 1)
        self.export_dir_textbox.setReadOnly(True)
        export_layout.setSpacing(5)

        export_layout.addWidget(self.export_file_name_label, 1, 0,
                                Qt.AlignmentFlag.AlignCenter)
        self.export_file_name_label.setContentsMargins(4, 0, 0, 0)
        export_layout.addWidget(self.export_file_name_textbox, 1, 1)
        export_layout.setContentsMargins(5, 0, 5, 0)
        export_layout.setSpacing(5)

        main_layout.addLayout(export_layout)

        main_layout.addWidget(self.plotting_widget.canvas)

        # Add a 1-pixel high widget to the main layout so that there is a
        # visible border between the GPS plot and the button. QTextBrowser
        # is used because it is known to have a visible border.
        invisible_widget = QtWidgets.QTextBrowser()
        invisible_widget.setFixedHeight(1)
        main_layout.addWidget(invisible_widget)

        button_layout = QtWidgets.QHBoxLayout()
        button_layout.setContentsMargins(100, 0, 100, 0)
        button_layout.setStretch(1, 3)
        button_layout.setSpacing(10)

        self.close_button.setStyleSheet('QPushButton {color: red;}')

        button_layout.addWidget(self.read_button)
        button_layout.addWidget(self.export_button)
        button_layout.addWidget(self.close_button)

        bottom_layout = QtWidgets.QVBoxLayout()
        bottom_layout.addLayout(button_layout)
        self.info_text_browser.setFixedHeight(45)
        bottom_layout.addWidget(self.info_text_browser)
        main_layout.addLayout(bottom_layout)

        self.connect_signals()

    def connect_signals(self) -> None:
        """
        Connect the signals of the contained widgets to the appropriate slots.
        """
        self.export_dir_button.clicked.connect(self.change_export_directory)

        self.read_button.clicked.connect(self.plotting_widget.plot_gps)
        self.export_button.clicked.connect(self.export_gps_points)
        self.close_button.clicked.connect(self.close)

    @property
    def unique_gps_points(self) -> List[GPSPoint]:
        """
        Get the current list of unique GPS points.

        :return: the current list of unique GPS points.
        """
        return self.plotting_widget.unique_gps_points

    @property
    def gps_points(self) -> List[GPSPoint]:
        """
        Get the current list of all GPS points.

        :return: the current list of all GPS points.
        """
        return self.plotting_widget.gps_points

    @gps_points.setter
    def gps_points(self, points: Iterable[GPSPoint]) -> None:
        """
        Set the stored lists of GPS points. Set both the list of all GPS points
        and the list of unique GPS points.
        :param points: the list of GPS points to be stored.
        """
        self.plotting_widget.gps_points = [point
                                           for point in points
                                           if not point.is_bad_point()]
        self.plotting_widget.unique_gps_points = list(
            {point
             for point in points
             if not point.is_bad_point()}
        )

    def set_colors(self, color_mode: Literal['B', 'W']) -> None:
        """
        Set the color mode of the GPS plots.
        :param color_mode: the chosen color mode. Can be either 'B' or 'W'.
        """
        self.plotting_widget.set_colors(color_mode)

    def clear_plot(self) -> None:
        """
        Clear the GPS plot.
        """
        self.plotting_widget.clear_plot()

    @QtCore.Slot()
    def change_export_directory(self) -> None:
        """
        Show a file selection window and change the GPS data export directory
        based on the folder selected by the user.
        """
        fd = QtWidgets.QFileDialog(self)
        fd.setFileMode(QtWidgets.QFileDialog.FileMode.Directory)
        fd.setDirectory(self.export_dir_textbox.text())
        fd.exec()
        new_path = fd.selectedFiles()[0]
        self.set_export_directory(new_path)

    def set_data_path(self, data_path: str):
        """
        Set the path to the data. Because the initial export path is dependent
        on the data path, it will be set as well.
        :param data_path: the path to the data as a string. Can be either a
            directory or a file.
        """
        data_path = Path(data_path)
        self.data_path = data_path
        self.export_file_name_textbox.set_text(str(data_path.name))
        self.set_export_directory(str(data_path.parent))

    def set_export_directory(self, dir_path: str) -> None:
        """
        Set the GPS data export directory to dir_path.
        :param dir_path: the path to set the data export directory to
        """
        self.export_dir_textbox.setText(dir_path)
        self.export_dir = Path(dir_path)

    def export_gps_points(self) -> None:
        """
        Export the data of the GPS points to a file. The file will be named
        <data set name>.gps.data and will be located in the directory
        self.export_path.

        The first line in the export file contains information about the data
        set. Each subsequent line has the form
        <YYYY-MM-DD HH:MM:SS> <fix type> <no. sats. used> <latitude:.6f> <longitude:.6f> <height:.6f>  # noqa
        and is tab-separated.
        """
        if not self.gps_points:
            msg = 'There is no GPS data to export.'
            display_tracking_info(self.info_text_browser, msg, LogType.ERROR)
            return
        export_file_name = self.export_file_name_textbox.text()
        export_file_path = self.export_dir / export_file_name

        try:
            with open(export_file_path, 'w+') as outfile:
                outfile.write(f'# GPS data points for {self.data_path.name}\n')
                for point in self.gps_points:
                    if point.is_bad_point():
                        continue
                    line = (
                        f'{point.last_timemark}\t{point.fix_type}\t'
                        f'{point.num_satellite_used}\t{point.latitude:+.6f}\t'
                        f'{point.longitude:+.6f}\t{point.height:.6f}\n'
                    )
                    outfile.write(line)
        except OSError:
            err_msg = traceback.format_exc()
            msg = f"Can't export to file due to error: {err_msg}"
            display_tracking_info(self.info_text_browser, msg, LogType.ERROR)
        else:
            msg = f'Successfully exported to {export_file_path}'
            display_tracking_info(self.info_text_browser, msg, LogType.INFO)


# We use the WGS-84's version of the Earth ellipsoid as a reference point for
# converting latitude and longitude differences from degree to meter.
# The constants below are obtained from this website
# https://en.wikipedia.org/wiki/Earth_ellipsoid#Historical_Earth_ellipsoids.
# In order to use other models of the Earth ellipsoid, simply change these
# constants.
POLAR_CIRCUMFERENCE = 40007863
EQUATORIAL_CIRCUMFERENCE = 40075017


def convert_latitude_degree_to_meter(lat: float) -> float:
    """
    Convert the given latitude from degree to meter.

    :param lat: latitude given in degree
    :return: the given latitude converted to meter
    """
    # A whole circumference is 360 degrees, so we can get the length of one
    # degree by dividing the circumference by 360.
    lat_degree_length_in_meter = POLAR_CIRCUMFERENCE / 360
    return lat * lat_degree_length_in_meter


def convert_longitude_degree_to_meter(long: float, lat: float) -> float:
    """
    Convert the given longitude from degree to meter. Need to adjust for
    latitude because the length of a longitude degree changes with the
    latitude.

    :param long: longitude given in degree
    :param lat: the latitude to adjust for
    :return: the given longitude converted to meter
    """
    # A whole circumference is 360 degrees, so we can get the length of one
    # degree by dividing the circumference by 360.
    long_degree_length_in_meter = EQUATORIAL_CIRCUMFERENCE / 360
    adjustment_for_latitude = math.cos(math.radians(lat))
    return long * long_degree_length_in_meter * adjustment_for_latitude
