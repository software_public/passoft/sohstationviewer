import functools
from datetime import datetime
from typing import List, Optional, Dict, NoReturn

import numpy as np
from obspy import UTCDateTime

from sohstationviewer.model.mseed_data.mseed import MSeed
from sohstationviewer.model.reftek_data.reftek import RT130
from sohstationviewer.view.plotting.gps_plot.gps_point import GPSPoint
from sohstationviewer.view.util.enums import LogType


def extract_gps_data_q330(data_obj: MSeed) -> List[GPSPoint]:
    """
    Read LOG channel and extract GPS data stored in Q330 data set.

    :param data_obj: the data object that stores the read data
    """
    extracted_gps_points = []
    for station in data_obj.log_data:
        if station == 'TEXT':
            continue
        if 'LOG' not in data_obj.log_data[station]:
            continue
        # Q330 log data is composed of a list of string, so we combine them
        # into one big string for ease of processing.
        log_str = ''.join(data_obj.log_data[station]['LOG'])
        log_lines = [line for line in log_str.splitlines() if line != '']
        for idx, line in enumerate(log_lines):
            if line == "GPS Status":
                try:
                    # We are assuming that a GPS status report is 12 lines
                    # long, has a specific format, and is followed by a PLL
                    # status report. This method call checks if these
                    # preconditions hold and raise an error if not.
                    check_gps_status_format_q330(log_lines[idx:idx + 13])

                    point = parse_gps_point_q330(log_lines[idx:idx + 12])
                    extracted_gps_points.append(point)
                except ValueError as e:
                    data_obj.processing_log.append((e.args[0], LogType.ERROR))
    return extracted_gps_points


def parse_gps_point_q330(gps_status_lines: List[str]) -> GPSPoint:
    """
    Extract the data from a set of log lines that encode a GPS data point.

    :param gps_status_lines: a list of log lines that encodes a GPS data
        point. Must have a length of 12.
    :return a GPSPoint object that contains data encoded in
        gps_status_lines.
    """
    if len(gps_status_lines) != 12:
        raise ValueError('The number of GPS log lines must be 12.')

    # Last timemark and fix type are always available, so we have to get
    # them before doing anything else.
    last_timemark = gps_status_lines[11][19:]
    fix_type = gps_status_lines[3][10:]

    # If location data is missing, we set them to 0.
    if gps_status_lines[5] == 'Latitude: ':
        return GPSPoint(last_timemark, fix_type, 0, 0, 0, 0, '')

    num_sats_used = int(gps_status_lines[8].split(': ')[1])

    # Height is encoded as a float followed by the unit. We
    # don't know how many characters the unit is composed of,
    # so we have to loop through the height string backward
    # until we can detect the end of the height value.
    height_str: str = gps_status_lines[4].split(': ')[1]
    # Start pass the end of the string and look backward one
    # index every iteration so we don't have to add 1 to the
    # final index.
    current_idx = len(height_str)
    current_char = height_str[current_idx - 1]
    while current_char != '.' and not current_char.isnumeric():
        current_idx -= 1
        current_char = height_str[current_idx - 1]
    height = float(height_str[:current_idx])
    height_unit = height_str[current_idx:]

    # Latitude and longitude are encoded in the format
    # <degree><decimal minute><cardinal direction>. For
    # latitude, <degree> has two characters, while for longitude, <degree>
    # has three.
    # To make the GPS points easier to plot, we convert the latitude and
    # longitude to decimal degree at the cost of possible precision loss
    # due to rounding error.
    raw_latitude = gps_status_lines[5].split(': ')[1]
    lat_degree = int(raw_latitude[:2])
    lat_minute = float(raw_latitude[2:-1]) / 60
    latitude = lat_degree + lat_minute
    if raw_latitude[-1].lower() == 's':
        latitude = -latitude

    raw_longitude = gps_status_lines[6].split(': ')[1]
    long_degree = int(raw_longitude[:3])
    long_minute = float(raw_longitude[3:-1]) / 60
    longitude = long_degree + long_minute
    if raw_longitude[-1].lower() == 'w':
        longitude = -longitude

    gps_point = GPSPoint(last_timemark, fix_type, num_sats_used,
                         latitude, longitude, height, height_unit)
    return gps_point


def check_gps_status_format_q330(status_lines: List[str]) -> None:
    """
    Check if the given set of log lines encode a GPS data point by
    determining if they follow a specific format.

    :param status_lines: a list of log lines. Must have a length of 13.
    """

    if len(status_lines) != 13:
        raise ValueError('The number of possible GPS log lines to check'
                         'must be 13.')
    if status_lines[12].lower() != 'pll status':
        if not status_lines[3] == 'Fix Type: COLD':
            raise ValueError(
                'Q330 log data is malformed: '
                'PLL status does not follow GPS status when fix type is not '
                'cold.'
            )
    if 'fix type' not in status_lines[3].lower():
        raise ValueError(
            'Q330 log data is malformed: '
            'Fix type is not at expected position.'
        )
    if 'height' not in status_lines[4].lower():
        raise ValueError(
            'Q330 log data is malformed: '
            'Height is not at expected position.'
        )
    if 'latitude' not in status_lines[5].lower():
        raise ValueError(
            'Q330 log data is malformed: '
            'Latitude is not at expected position.'
        )
    if 'longitude' not in status_lines[6].lower():
        raise ValueError(
            'Q330 log data is malformed: '
            'Longitude is not at expected position.'
        )
    if 'sat. used' not in status_lines[8].lower():
        raise ValueError(
            'Q330 log data is malformed: '
            'Sat. Used is not at expected position.'
        )
    if 'last gps timemark' not in status_lines[11].lower():
        if not status_lines[3] == 'Fix Type: COLD':
            raise ValueError(
                'Q330 log data is malformed: '
                'Last GPS timemark is not at expected position when fix type '
                'is not cold.'
            )


def get_gps_channel_prefix(data_obj: MSeed, data_type: str) -> Optional[str]:
    """
    Determine the first letter of the GPS channel name for the current data
    set. Only applicable to Centaur and Pegasus data sets. Q330 data sets
    store GPS data in the LOG channel.

    Centaur and Pegasus data sets share the last two character for GPS
    channels. However, Pegasus GPS channels start with an 'V', while
    Centaur GPS channels all start with a 'G'.

    :param data_obj: the data object that stores the read data
    :param data_type: the type of data contained in the current data set
    :return: the first character of the GPS channels. Is 'V' if data_type
         is 'Pegasus', or 'G' if data_type is 'Centaur'
    """
    gps_prefix = None
    if data_type == 'Pegasus':
        gps_prefix = 'V'
    elif data_type == 'Centaur':
        gps_prefix = 'G'
    else:
        gps_suffixes = {'NS', 'LA', 'LO', 'EL'}
        pegasus_gps_channels = {'V' + suffix for suffix in gps_suffixes}
        centaur_gps_channels = {'G' + suffix for suffix in gps_suffixes}

        # Determine the GPS channels by checking if the current data set
        # has all the GPS channels of a data type.
        channels = set(data_obj.soh_data[data_obj.selected_data_set_id].keys())
        if pegasus_gps_channels & channels == pegasus_gps_channels:
            gps_prefix = 'V'
        elif centaur_gps_channels & channels == centaur_gps_channels:
            gps_prefix = 'G'
        else:
            msg = "Can't detect GPS channels."
            data_obj.track_info(msg, LogType.ERROR)
    return gps_prefix


def get_chan_soh_trace_as_dict(data_obj: MSeed, chan: str
                               ) -> Dict[float, List[float]]:
    """
    Get the data of a channel as a dictionary mapping a data point's time to
    its data. Suppose that gps data's spr <= 1.
    :param data_obj: the data object that stores the read data
    :param chan: the channel name
    :return: a dict that maps the times of channel chan to its data
    """
    chan_data = data_obj.soh_data[data_obj.selected_data_set_id][chan]
    traces = chan_data['tracesInfo']
    times = np.hstack([trace['times'] for trace in traces])
    data = np.hstack([trace['data'] for trace in traces])
    data_dict = {}
    for times_point, data_point in zip(times, data):
        data_dict.setdefault(times_point, []).append(data_point)
    return data_dict


def extract_gps_data_pegasus_centaur(data_obj: MSeed, data_type: str
                                     ) -> List[GPSPoint]:
    """
    Extract GPS data of the current data set and store it in
    self.gps_points. Only applicable to Centaur and Pegasus data sets. Q330
    data sets store GPS data in the LOG channel.

    :param data_obj: the data object that stores the read data
    :param data_type: data type of the current data set
    """
    # Caching GPS data in dictionaries for faster access. In the algorithm
    # below, we need to access the data associated with a time. If we leave
    # the times and data in arrays, we will need to search for the index of
    # the specified time in the times array, which takes O(N) time. The
    # algorithm then repeats this step n times, which gives us a total
    # complexity of O(n^2). Meanwhile, if we cache the times and data in
    # a dictionary, we only need to spend O(n) time building the cache and
    # O(n) time accessing the cache, which amounts to O(n) time in total.
    gps_prefix = get_gps_channel_prefix(data_obj, data_type)
    if data_obj.selected_data_set_id is None or gps_prefix is None:
        return []
    gps_chans = {gps_prefix + 'NS', gps_prefix + 'LA', gps_prefix + 'LO',
                 gps_prefix + 'EL'}

    channels = data_obj.soh_data[data_obj.selected_data_set_id].keys()
    if not gps_chans.issubset(channels):
        missing_gps_chans = gps_chans - channels
        missing_gps_chans_string = ', '.join(missing_gps_chans)
        raise ValueError(f"Some GPS channels are missing: "
                         f"{missing_gps_chans_string}.")

    ns_dict = get_chan_soh_trace_as_dict(data_obj, gps_prefix + 'NS')
    la_dict = get_chan_soh_trace_as_dict(data_obj, gps_prefix + 'LA')
    lo_dict = get_chan_soh_trace_as_dict(data_obj, gps_prefix + 'LO')
    el_dict = get_chan_soh_trace_as_dict(data_obj, gps_prefix + 'EL')

    extracted_gps_points = []
    for time, num_sats_used in ns_dict.items():
        # There is no channel for GPS fix type in Pegasus and Centaur data,
        # so we are giving it a dummy value.
        fix_type = 'N/A'
        current_lat = la_dict.get(time, None)
        current_long = lo_dict.get(time, None)
        current_height = el_dict.get(time, None)
        # We are ignoring any point that does not have complete location data.
        # It might be possible to ignore points with missing latitude or
        # longitude, seeing as height is not required to plot a GPS point.
        if (current_lat is None or
                current_long is None or
                current_height is None):
            continue
        # Convert the location data to the appropriate unit. Centaur and
        # Pegasus dataloggers both store latitude and longitude in microdegree,
        # so we only use one conversion factor. On the other hand, Centaur
        # stores elevation in micrometer while Pegasus stores it in centimeter,
        # so we have to use a difference conversion factor for elevation
        # depending on the data type.
        for i, num_sats in enumerate(num_sats_used):
            try:
                lat = current_lat[i] / 1e6
                long = current_long[i] / 1e6
                # The GPS prefix is unique between Pegasus (V) and Centaur (G),
                # so we can use it in place of the data type.
                if gps_prefix == 'V':
                    height_factor = 100
                else:
                    height_factor = 1e6
                height = current_height[i] / height_factor
                height_unit = 'M'
                formatted_time = UTCDateTime(time).strftime(
                    '%Y-%m-%d %H:%M:%S'
                )
                gps_point = GPSPoint(formatted_time, fix_type, num_sats, lat,
                                     long, height, height_unit)
                extracted_gps_points.append(gps_point)
            except IndexError:
                break
    # We only need to loop through one dictionary. If a time is not
    # available for a channel, the GPS data point at that time would be
    # invalid (it is missing a piece of data). Once we loop through a
    # channel's dictionary, we know that any time not contained in that
    # dictionary is not available for the channel. As a result, any time
    # we pass through in the other channels after the first loop would
    # result in an invalid GPS data point. Because we discard any invalid
    # point, there is no point in looping through the dictionary of other
    # channels.
    return extracted_gps_points


def extract_gps_data_q8(data_obj: MSeed, data_type: str) -> List[GPSPoint]:
    """
    Extract GPS data of the current data set and store it in self.gps_points.
    Only applicable to Q8 data sets.

    :param data_obj: the data object that stores the read data
    :param data_type: data type of the current data set
    """
    GPS_CHANS = {'LAT', 'LON', 'LEV', 'LFT', 'LSU'}

    channels = data_obj.soh_data[data_obj.selected_data_set_id].keys()
    if not GPS_CHANS.issubset(channels):
        missing_gps_chans = GPS_CHANS - channels
        missing_gps_chans_string = ', '.join(missing_gps_chans)
        raise ValueError(f"Some GPS channels are missing: "
                         f"{missing_gps_chans_string}.")

    # Caching GPS data in dictionaries for faster access. In the algorithm
    # below, we need to access the data associated with a time. If we leave
    # the times and data in arrays, we will need to search for the index of
    # the specified time in the times array, which takes O(N) time. The
    # algorithm then repeats this step n times, which gives us a total
    # complexity of O(n^2). Meanwhile, if we cache the times and data in
    # a dictionary, we only need to spend O(n) time building the cache and
    # O(n) time accessing the cache, which amounts to O(n) time in total.
    ns_dict = get_chan_soh_trace_as_dict(data_obj, 'LSU')
    la_dict = get_chan_soh_trace_as_dict(data_obj, 'LAT')
    lo_dict = get_chan_soh_trace_as_dict(data_obj, 'LON')
    el_dict = get_chan_soh_trace_as_dict(data_obj, 'LEV')

    extracted_gps_points = []
    for time, num_sats_used in ns_dict.items():
        # We currently don't know how to translate the data in the LFT channel
        # into the actual fix type, so we are giving it a dummy value until we
        # can do so.
        fix_type = 'N/A'
        current_lat = la_dict.get(time, None)
        current_long = lo_dict.get(time, None)
        current_height = el_dict.get(time, None)
        # We are ignoring any point that does not have complete location data.
        # It might be possible to only ignore points with missing latitude or
        # longitude, seeing as height is not required to plot a GPS point.
        if (current_lat is None or
                current_long is None or
                current_height is None):
            continue
        for i, num_sats in enumerate(num_sats_used):
            try:
                # Convert the location data to the appropriate unit. Q8 stores
                # latitude and longitude in microdegrees, and we want them to
                # be in degrees. The unit of the elevation is m, which is what
                # we want so there is no conversion done.
                lat = current_lat[i] / 1e6
                long = current_long[i] / 1e6
                height = current_height[i]
                height_unit = 'M'
                formatted_time = UTCDateTime(time).strftime(
                    '%Y-%m-%d %H:%M:%S'
                )
                gps_point = GPSPoint(formatted_time, fix_type, num_sats, lat,
                                     long, height, height_unit)
                extracted_gps_points.append(gps_point)
            except IndexError:
                break
    # We only need to loop through one dictionary. If a time is not
    # available for a channel, the GPS data point at that time would be
    # invalid (it is missing a piece of data). Once we loop through a
    # channel's dictionary, we know that any time not contained in that
    # dictionary is not available for the channel. As a result, any time
    # we pass through in the other channels after the first loop would
    # result in an invalid GPS data point. Because we discard any invalid
    # point, there is no point in looping through the dictionary of other
    # channels.
    return extracted_gps_points


def extract_gps_data_rt130(data_obj: RT130) -> List[GPSPoint]:
    """
    Retrieve the GPS of the current data set. Works by looking into the log
    of the data set.

    :param data_obj: the data object that stores the read data
    """
    log_lines = data_obj.log_data[data_obj.selected_data_set_id]['SOH']
    log_lines = log_lines[0].split('\n')
    log_lines = [line.strip('\r').strip('') for line in log_lines]
    gps_year = None

    extracted_gps_points = []
    for line in log_lines:
        # The lines of the log that contains the GPS data does not include
        # the year. Instead, we grab it by looking at the header of the
        # state of health packet the GPS line is in.
        # Also, it might be possible to only grab the year once. It would
        # be easy to check if the year change by seeing if the day of year
        # loops back to 1. That approach is a bit more complex, however,
        # so we are not using it.
        if 'State of Health' in line:
            current_time = line.split()[3]
            two_digit_year = current_time[:2]
            gps_year = two_digit_year_to_four_digit_year(two_digit_year)
        if 'GPS: POSITION' in line:
            extracted_gps_points.append(parse_gps_point_rt130(line, gps_year))
    return extracted_gps_points


def parse_gps_point_rt130(gps_line: str, gps_year: str) -> GPSPoint:
    """
    Parse a GPS log line to extract the GPS data point it contains. Needs
    to be given a year because GPS log lines do not contain the year.

    :param gps_line: the GPS log line to parse
    :param gps_year: the year associated with gps_line
    :return: a GPSPoint object that contains the GPS data points stored in
        gps_line
    """
    # RT130 data does not contain GPS fix type and number of satellites
    # used, so we set them to not available.
    fix_type = 'N/A'
    num_sats_used = 'N/A'

    time_str, *_, lat_str, long_str, height_str = gps_line.split()

    time_str = gps_year + ':' + time_str
    time_format = '%Y:%j:%H:%M:%S'
    gps_time = datetime.strptime(time_str, time_format).isoformat(sep=' ')

    # Latitude and longitude are stored in degrees, minutes, and seconds,
    # with each quantity being separated by ':'. The degree part of
    # latitude is stored as three characters, while the degree part of
    # longitude is stored as four characters. The first character of the
    # degree part is the cardinal direction (NS/EW), so we ignore it when
    # calculating latitude and longitude.
    lat_as_list = lat_str.split(':')
    lat_second = float(lat_as_list[2])
    lat_minute = float(lat_as_list[1]) + lat_second / 60
    lat = float(lat_as_list[0][1:]) + lat_minute / 60
    if lat_as_list[0][0] == 'S':
        lat = -lat

    long_as_list = long_str.split(':')
    long_second = float(long_as_list[2])
    long_minute = float(long_as_list[1]) + long_second / 60
    long = float(long_as_list[0][1:]) + long_minute / 60
    if long_as_list[0][0] == 'W':
        long = -long

    # Height is encoded as a signed float followed by the unit. We don't
    # know how many characters the unit is composed of, so we have to loop
    # through the height string backward until we can detect the end of the
    # height value.
    # Start pass the end of the string and look backward one index every
    # iteration so that we don't have to add 1 to the final index.
    current_idx = len(height_str)
    current_char = height_str[current_idx - 1]
    while current_char != '.' and not current_char.isnumeric():
        current_idx -= 1
        current_char = height_str[current_idx - 1]
    height = float(height_str[:current_idx])
    height_unit = height_str[current_idx:]

    return GPSPoint(gps_time, fix_type, num_sats_used, lat, long, height,
                    height_unit)


def two_digit_year_to_four_digit_year(year: str) -> str:
    """
    Convert a year represented by two digits to its representation in 4 digits.
    Follow the POSIX and ISO C standards mentioned by the Python documentation,
    quoted below.

    'When 2-digit years are parsed, they are converted according to the POSIX
    and ISO C standards: values 69–99 are mapped to 1969–1999, and values 0–68
    are mapped to 2000–2068.'

    Raise ValueError if year is outside the 0-99 range (0n counts as n).

    :param year: the 2-digit year as a string
    :return: the 4-digit representation of year as a string
    """
    year_as_int = int(year)
    if not (0 <= year_as_int <= 99):
        raise ValueError(f'Given year {year} is not in the valid range.')
    if int(year) < 69:
        prefix = '20'
    else:
        prefix = '19'
    return f'{prefix}{year:0>2}'


@functools.singledispatch
def extract_gps_data(data_obj) -> NoReturn:
    """
    Extract GPS data from a data set based on the data type contained in the
    data set.

    :param data_obj: the data object that stores the read data
    :return: the list of extracted GPS data points
    """
    raise NotImplementedError('This function only works for subtypes of '
                              'DataTypeModel. Please confirm that you have'
                              'passed in the correct type.')


@extract_gps_data.register(RT130)
def gps_data_rt130(data_obj: RT130) -> List[GPSPoint]:
    return extract_gps_data_rt130(data_obj)


@extract_gps_data.register(MSeed)
def gps_data_mseed(data_obj: MSeed) -> List[GPSPoint]:
    try:
        data_type = data_obj.data_type
    except Exception:
        data_type = 'Unknown'

    if data_type == 'Q330':
        return extract_gps_data_q330(data_obj)
    elif data_type == 'Centaur' or data_type == 'Pegasus':
        return extract_gps_data_pegasus_centaur(data_obj, data_type)
    elif data_type == 'Q8':
        return extract_gps_data_q8(data_obj, data_type)
    else:
        # data_type = "Unknown"
        try:
            gps_data = extract_gps_data_q330(data_obj)
        except KeyError:
            try:
                gps_data = extract_gps_data_pegasus_centaur(
                    data_obj, data_type)
            except AttributeError:
                return []
        return gps_data
