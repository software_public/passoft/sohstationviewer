from typing import NamedTuple, Union


class GPSPoint(NamedTuple):
    """
    The metadata and location data of a GPS data point.
    """
    last_timemark: str
    fix_type: str
    num_satellite_used: Union[float, str]
    latitude: float
    longitude: float
    height: float
    height_unit: str

    def is_bad_point(self) -> bool:
        """
        Return True if this point is a bad point and False otherwise. A point
        is bad if its latitude and longitude are both 0.
        """
        return self.latitude == 0.0 and self.longitude == 0.0

    def __hash__(self):
        return hash((self.latitude, self.longitude))

    def __eq__(self, other):
        try:
            return (self.latitude == other.latitude and
                    self.longitude == other.longitude
                    )
        except AttributeError:
            return False
