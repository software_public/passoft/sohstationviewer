from typing import List, Union, Tuple, Dict
import numpy as np
from copy import copy
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.conf import constants
from sohstationviewer.controller.util import get_val
from sohstationviewer.view.util.color import clr
from sohstationviewer.database.extract_data import get_convert_factor


# TODO: put this in DB
mass_pos_volt_ranges = {"regular": [0.5, 2.0, 4.0, 7.0],
                        "trillium": [0.5, 1.8, 2.4, 3.5]}
mass_pos_color_pallets = {"B": ["C", "G", "Y", "R", "M"],
                          "W": ["C", "G", "Y", "R", "M"]}


def get_masspos_value_colors(
        range_opt: str, chan_id: str, c_mode: str,
        processing_log: List[Tuple[str, LogType]],
        ret_type: str = 'str'
) -> Union[str, List[Tuple[float, str]], None]:
    """
    Create a map between value and color based on given rangeOpt and c_mode to
    display mass position plots.
    :param range_opt: massPosVoltRangeOpt got from Options Menu - MP coloring
        in Main Window to define different values for mass position.
        (regular/trillium)
    :param chan_id: ID of the channel
    :param c_mode: color mode (B/W)
    :param processing_log: list of processing info and type
    :param ret_type: request return type
    :return: [(value, color), (value, color) ...]
        if retType is 'str', return "value:color|value:color"
    """

    if range_opt.lower() not in mass_pos_volt_ranges.keys():
        processing_log.append(
            (
                f"{chan_id}: The current selected Mass Position color range is"
                f" '{range_opt}' isn't allowable. The accept ranges are: "
                f"{', '.join(mass_pos_volt_ranges.keys())}",
                LogType.ERROR
            )
        )
        return
    mass_pos_volt_range = mass_pos_volt_ranges[range_opt]
    mass_pos_color_pallet = mass_pos_color_pallets[c_mode]
    value_colors = []
    for i in range(len(mass_pos_volt_range)):
        if ret_type == 'str':
            value_colors.append(
                "%s:%s" % (mass_pos_volt_range[i], mass_pos_color_pallet[i]))
        else:
            value_colors.append(
                (mass_pos_volt_range[i], mass_pos_color_pallet[i])
            )
        if i == len(mass_pos_volt_range) - 1:
            if ret_type == 'str':
                value_colors.append(
                    "%s:+%s" % (
                        mass_pos_volt_range[i], mass_pos_color_pallet[i + 1]))
            else:
                value_colors.append(
                    (mass_pos_volt_range[i], mass_pos_color_pallet[i + 1]))
    if ret_type == 'str':
        return '|'.join(value_colors)
    return value_colors


def get_categorized_data_from_value_color_equal_on_upper_bound(
        c_data: Dict, chan_db_info: Dict
) -> Tuple[List[List[float]], List[str], List[float]]:
    """
    Separate data points and color using valueColors in which the condition
    will check if value equal to or less than upper bound and greater than
    lower bound as following example
        <=-1:not plot|<=0:#FF0000:bigger|0<:#FF00FF means:
           value <= -1   => not plot
           -1 < value <= 0 => plot with #FF0000 color in the bigger size
           0 < value  => plot with #FF00FF color
    :param c_data: dict of data of the channel which includes down-sampled
        data in keys 'times' and 'data'.
    :param chan_db_info: dict of info of channel from DB
    :return x: list of x to be plotted to get total samples
    :return colors: list of color to be plotted to decide color of total sample
        text.
    :return sizes: list of sizes in pixel to plot for each points
    """
    prev_val = -constants.HIGHEST_INT
    value_colors = chan_db_info['valueColors'].split('|')
    colors = []
    points_list = []
    sizes = []
    for vc in value_colors:
        vcs = vc.split(':')
        v = vcs[0]
        c = vcs[1]
        s_desc = vcs[2] if len(vcs) > 2 else 'normal'
        sizes.append(constants.SIZE_PIXEL_MAP[s_desc])
        if v == '*':
            val = v         # to have some value for pre_val = val
        else:
            val = get_val(v)
        if c == 'not plot':
            prev_val = val
            continue
        colors.append(c)
        # c_data['times'] is a list of one list (after changing to faster data
        # reading) so don't need to loop through but only taking the first
        # element (c_data['times'][0]).
        # Same for c_data['data']
        times, data = c_data['times'][0], c_data['data'][0]
        if v[-1] == '<':
            # Deal with the last value colors section.
            points = [times[i]
                      for i in range(len(data))
                      if val < data[i]]
        elif v == '*':
            points = times
        else:
            points = [times[i]
                      for i in range(len(data))
                      if prev_val < data[i] <= val]
        points_list.append(points)
        prev_val = val
    return points_list, colors, sizes


def get_categorized_data_from_value_color_equal_on_lower_bound(
        c_data: Dict, chan_db_info: Dict
) -> Tuple[List[List[float]], List[str], List[float]]:
    """
    Separate data points and color using valueColors in which the condition
    will check if value equal to or greater than lower bound and less than
    upper bound as the following example:
        <-1:not plot|<0:#FF0000|=0:#FF00FF:smaller  means:
            value < -1   => not plot
            -1 =< value < 0 => plot with #FF0000 color
            value >= 0  => plot with #FF00FF color in a smaller
    :param c_data: dict of data of the channel which includes down-sampled
        data in keys 'times' and 'data'.
    :param chan_db_info: dict of info of channel from DB
    :return x: list of x to be plotted to get total samples
    :return colors: list of color to be plotted to decide color of total sample
        text.
    """
    prev_val = 0
    value_colors = chan_db_info['valueColors'].split('|')
    colors = []
    points_list = []
    sizes = []
    for vc in value_colors:
        vcs = vc.split(':')
        v = vcs[0]
        c = vcs[1]
        s_desc = vcs[2] if len(vcs) > 2 else 'normal'
        sizes.append(constants.SIZE_PIXEL_MAP[s_desc])
        colors.append(c)
        val = get_val(v)
        times, data = c_data['times'][0], c_data['data'][0]
        if v.startswith('='):
            # Deal with the last value colors section.
            points = [times[i]
                      for i in range(len(data))
                      if data[i] >= val]
        else:
            points = [times[i]
                      for i in range(len(data))
                      if prev_val <= data[i] < val]
        points_list.append(points)
        prev_val = val
    return points_list, colors, sizes


def get_colors_sizes_for_abs_y_from_value_colors(
        y: List[float],
        value_colors: List[Tuple[float, str]]) -> \
        Tuple[List[str], List[float]]:
    """
    Map each abs(value) in y with the value in value_colors to get the colors
    for the indexes corresponding to the y's items.
    Sizes is currently similar for all items
    :param y: list of data values
    :param value_colors: list of color with indexes corresponding to y's items
    :return colors: list of colors of markers corresponding to y
    :return sizes: list of sizes of markers corresponding to y
    """
    colors = [None] * len(y)
    sizes = [1.5] * len(y)
    for i in range(len(y)):
        count = -1
        for v, c in value_colors:
            count += 1
            if count <= len(value_colors) - 2:
                if abs(y[i]) <= v:
                    colors[i] = clr[c]
                    break
            else:
                # The last value color
                colors[i] = clr[c]
    return colors, sizes


def apply_convert_factor(data: List[np.ndarray], chan_id: str, data_type: str
                         ) -> List[np.ndarray]:
    """
    Convert data according to convert_factor got from DB

   :param data: list of value array
   :param chan_id: name of channel
   :param data_type: type of data
    """
    convert_factor = get_convert_factor(chan_id, data_type)
    if convert_factor is not None and convert_factor != 1:
        new_data = [convert_factor * copy(data[0])]
        return new_data
    else:
        return data
