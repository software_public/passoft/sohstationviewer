from PySide6 import QtCore

from sohstationviewer.conf import constants as const
from sohstationviewer.view.plotting.plotting_widget.plotting_processor_helper\
    import downsample


class PlottingChannelProcessorSignals(QtCore.QObject):
    finished = QtCore.Signal(dict, str)
    stopped = QtCore.Signal()


class PlottingChannelProcessor(QtCore.QRunnable):
    """
    The class that handles trimming excess data and interfacing with a
    downsampler for a plotting channel.
    """
    def __init__(self, channel_data, channel_id, start_time, end_time):
        super().__init__()
        self.signals = PlottingChannelProcessorSignals()
        # Aliasing the signals for ease of use
        self.finished = self.signals.finished
        self.stopped = self.signals.stopped

        self.stop_requested = False

        self.channel_data: dict = channel_data
        self.channel_id = channel_id

        self.start_time = start_time
        self.end_time = end_time

    def run(self):
        """
        Because of changes that read less data instead of all data in files,
        now data has only one trace. We can assign the times and data in that
        trace to times and data of the channel. Trimming won't be necessary
        anymore.
        """
        if len(self.channel_data['tracesInfo']) == 0:
            self.channel_data['times'] = []
            self.channel_data['data'] = []
            self.finished.emit(self.channel_data, self.channel_id)
            return
        tr = self.channel_data['tracesInfo'][0]
        if 'logIdx' in tr.keys():
            tr_times, tr_data, tr_logidx = downsample(
                tr['times'], tr['data'], tr['logIdx'],
                rq_points=const.CHAN_SIZE_LIMIT)
            self.channel_data['logIdx'] = [tr_logidx]
        else:
            tr_times, tr_data, _ = downsample(
                tr['times'], tr['data'], rq_points=const.CHAN_SIZE_LIMIT)
        self.channel_data['times'] = [tr_times]
        self.channel_data['data'] = [tr_data]

        self.finished.emit(self.channel_data, self.channel_id)

    def request_stop(self):
        """
        Stop processing the data by requesting the downsampler to stop
        running.
        """
        self.stop_requested = True
