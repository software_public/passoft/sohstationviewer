"""
Class of which object is used to plot data
"""
from typing import List, Optional, Union, Tuple
import math
import numpy as np
import matplotlib.text
from matplotlib import pyplot as pl

from PySide6.QtCore import Qt, QTimer, QEvent, QObject
from PySide6 import QtCore, QtWidgets
from PySide6.QtWidgets import QWidget, QApplication, QTextBrowser

from sohstationviewer.conf import constants as const
from sohstationviewer.view.util.color import set_color_mode

from sohstationviewer.view.plotting.plotting_widget.plotting_widget_helper \
    import get_index_from_data_picked, get_total_miny_maxy
from sohstationviewer.view.plotting.plotting_widget.plotting_axes import (
    PlottingAxes
)
from sohstationviewer.view.plotting.plotting_widget.plotting import Plotting
from sohstationviewer.view.save_plot_dialog import SavePlotDialog

from sohstationviewer.controller.plotting_data import (
    format_time, get_disk_size_format
)
from sohstationviewer.controller.util import \
    display_tracking_info, get_formatted_time_delta


class PlottingWidget(QtWidgets.QScrollArea):
    """
    Widget to plot different type of channels on a figure and different
        events to serve user's purpose.
    """

    def __init__(self, parent: Union[QWidget, QApplication],
                 tracking_box: QTextBrowser,
                 name: str,
                 main_window: QApplication) -> None:
        """
        :param parent: widget that contains this plotting
            widget
        :param tracking_box: widget to display tracking info
        :param name: name of the plotting widget to keep track of what
            widget the program is working on
        :param main_window: Main window that keep all parameters set by user
        """
        self.parent = parent
        self.main_window = main_window
        self.name = name
        self.tracking_box = tracking_box
        # =============== declare attributes =======================
        """
         DPI INFO
         actual_dpi: actual dpi of the screen where main_window is located.
             actual_dpi = physical_dpi * device_pixel_ratio
         dpi_y: vertical dpi of the screen
         dpi_x: horizontal dpi of the screen
         """
        self.actual_dpi: float = 100.
        self.dpi_x: float = 25.
        self.dpi_y: float = 30.
        """
        has_data: indicate if there're any data to be plotted
        """
        self.has_data: bool = False
        """
        processing_log: [(str,str] - list of processing info and type
        """
        self.processing_log = []
        """
        time_ticks_total: int - maximum of total major ticks label displayed
        """
        self.time_ticks_total = 5
        """
        min_x: float - left limit of a channel plot
        max_x: float - right limit of a channel plot
        """
        self.min_x = 0
        self.max_x = 0
        """
        zoom_minmax_list: list of x ranges of zooming. The first range is the
        original of x ranges. If there is more than one ranges the plotting has
        been zoomed in.
        """
        self.zoom_minmax_list: List[Tuple[float, float]] = []
        """
        plotting_bot: float - bottom of a current plot, decrease by plot_h
            return from self.get_height() whenever a new plot is added
        """
        self.plotting_bot = const.BOTTOM
        """
        display_color: dict - that defined colors to be used.
        """
        self.display_color = {}
        """
        c_mode: str - 'B'/'W' - color modes black or white
        """
        self.c_mode = 'B'
        """
        fig_height_in: height of figure in inch
        """
        self.fig_height_in = 0
        """
        height_normalizing_factor: factor to convert height size factor to
        normalize in which total of all height ratios is 1
        """
        self.height_normalizing_factor = 0
        """
        bottom: float - y position of the bottom edge of all plots in self.axes
            This is to identify the bottom of rulers
        """
        self.bottom = 0
        """
        gap_bar: matplotlib.axes.Axes - axes where gaps are plotted
        """
        self.gap_bar = None
        """
        gaps: list of gaps of the displayed data set
        """
        self.gaps: List[List[float, float]] = []
        """
        timestamp_bar_top: matplotlib.axes.Axes - axes to show times that
            places at the top of all channel plots
        timestamp_bar_bottom: matplotlib.axes.Axes - axes to show times that
            places at the bottom of all channel plots
        """
        self.timestamp_bar_top = None
        self.timestamp_bar_bottom = None
        """
        date_mode: str - format of display date
        """
        self.date_mode = 'YYYY-MM-DD'
        """
        peer_plotting_widgets: [PlottingWidget,]: list of all PlottingWidget
            objects that need to be synchronized
        """
        self.peer_plotting_widgets = [self]
        """
        plot_total: int - total of plots display on this widget
        """
        self.plot_total = 0
        """
        axes: [matplotlib.axes.Axes, ] - list of axes for plotting channels
        """
        self.axes = []
        """
        ruler: ConnectionPatch - a line go from top to bottom to help
            user see the corresponding positions of different channels.
            It shows up at the mouse x position when  Ctrl/Cmd + left click,
            and disappears when zoom_marker1 shows up or when ESC is pressed.
        """
        self.ruler = None
        """
        zoom_marker1: ConnectionPatch - a line go from top to bottom to help
            mark where the zooming area start. It shows up at the mouse x
            position when Shift + left click, and disappears when plot has
            zoomed in or when ESC is pressed.
        """
        self.zoom_marker1 = None
        """
        zoom_marker2: ConnectionPatch - a line go from top to bottom to help
            mark where the zooming area end. It shows up at the mouse x
            position when  Shift + left click after zoom_marker1 shown up,
            and disappears when plot has zoomed in or when ESC is pressed.
        """
        self.zoom_marker2 = None
        """
        zoom_marker1_shown: bool - flag for sohview to know if zoom_marker1 is
            shown/hidden
        """
        self.zoom_marker1_shown = False
        """
        plotting_data1: dict that includes 'times', 'data', 'ax'-
            first set of data for plotting. It can be either
            soh_data[data_set_id] or waveform_data[data_set_id]
        """
        self.plotting_data1 = {}
        """
        plotting_data2: dict that includes 'times' and 'data'-
            second set of data for plotting. It is mass_pos_data[data_set_id]
        """
        self.plotting_data2 = {}
        """
        title: title of the plotting including data set index and time range
        """
        self.title: str = ''
        # List of SOH message lines in RT130 to display in info box when
        # there're more than 2 lines for one data point clicked
        self.rt130_log_data: Optional[List[str]] = None

        """
        log_idxes: line index of RT130's log messages
        """
        self.log_idxes = None
        # ----------------------------------------------------------------

        QtWidgets.QScrollArea.__init__(self)

        # To avoid artifact show up at the scrollbar position between the
        # plotting of available and unavailable scrollbar, it is set
        # to be always on for vertical and off for horizontal
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        """
        main_widget: QWidget - widget in the scroll area to keep the drawing
            canvas
        """
        self.main_widget = QtWidgets.QWidget(parent)
        """
        plotting_axes: object that helps creating axes for plotting
        """
        self.plotting_axes = PlottingAxes(self, main_window)
        self.plotting_axes.canvas.setParent(self.main_widget)
        self.plotting_axes.canvas.installEventFilter(self)

        self.setWidget(self.main_widget)

        """
        plotting: object that helps with different types of plotting channels
        """
        self.plotting = Plotting(self, self.plotting_axes, main_window)
        """
        new_min_x: store the new minimum time for zooming; used to fix a
        problem where after the first zoom marker is chosen, any ruler that is
        to the left of the first zoom marker does not have text above it.
        """
        self.new_min_x = float('-inf')
        """
        ruler_text: the current text shown above the ruler
        """
        self.ruler_text: Optional[matplotlib.text.Text] = None
        """
        button_press_picked_a_point: whether a button press event triggered a
        pick event. Used to fix a bug where picking a point on the SOH plot of
        an RT130 data set does not bring the SOH messages dialog to the front.
        """
        self.is_button_press_event_triggered_pick_event: bool = False

        self.set_colors('B')

    def init_fig_height_in(self):
        """
        Start figure height in with the size of margin, time_bar, gap_bar
        which aren't included in get_plotting_info()
        """
        top_space_in = const.BASIC_HEIGHT_IN * const.TOP_SPACE_SIZE_FACTOR
        bot_space_in = const.BASIC_HEIGHT_IN * const.BOT_SPACE_SIZE_FACTOR
        time_bar_height_in = const.BASIC_HEIGHT_IN * const.TIME_BAR_SIZE_FACTOR
        plot_none_in = const.BASIC_HEIGHT_IN * const.PLOT_NONE_SIZE_FACTOR
        gap_bar_in_val = const.BASIC_HEIGHT_IN * const.GAP_SIZE_FACTOR
        gap_bar_in = (0 if self.main_window.gap_minimum is None
                      else gap_bar_in_val)
        return (gap_bar_in + plot_none_in +
                top_space_in + bot_space_in +
                2 * time_bar_height_in)

    def set_dpi(self):
        """
        Calculate dpi to correct sizing calculation
        """
        screen = self.screen()
        if screen is not None:
            self.dpi_x = screen.physicalDotsPerInchX()
            self.dpi_y = screen.physicalDotsPerInchY()
            # Using dpi_x as standard
            self.actual_dpi = (screen.physicalDotsPerInchX() *
                               self.devicePixelRatio())

    def set_size(self, view_port_size: Optional[float] = None) -> None:
        """
        Set figure's width and main widget's width to fit the width of the
        view port of the scroll area.

        When there is no channels, height will be set to the height of the
        viewport to cover the whole viewport.

        :param view_port_size: size of viewport
        """
        self.set_dpi()
        if view_port_size is None:
            view_port_size = self.maximumViewportSize()
        view_port_width = view_port_size.width()
        self.plotting_axes.canvas.setFixedWidth(view_port_width)
        self.fig_width_in = view_port_width/self.dpi_x

        self.plotting_axes.fig.set_dpi(self.actual_dpi)

        # set view size fit with the scroll's viewport size
        self.main_widget.setFixedWidth(view_port_size.width())
        if self.plot_total == 0:
            view_port_height = view_port_size.height()
            self.main_widget.setFixedHeight(view_port_height)
            self.plotting_axes.canvas.setFixedHeight(view_port_height)
            fig_height_in = view_port_height/self.dpi_y
            self.plotting_axes.fig.set_figheight(fig_height_in)

    def plotting_preset(self):
        """
        Call set size to apply current view port size to width of figure and
        main widget.

        Set height of figure and main widget according to total of plotting
        channels' heights.

        Vertical_ratio and fig_height_in_ratio are used to spread out plotting.
        However, TPS channels won't need to be spread out.

        Calculate height_normalizing_factor.
        """
        self.set_size()
        # Don't need to spead out channels for TPS
        vertical_ratio = (self.main_window.vertical_ratio
                          if self.name != 'TPS' else 1)
        if self.name != 'TPS':
            self.fig_height_in *= self.main_window.fig_height_in_ratio

        fig_height = math.ceil(self.fig_height_in * self.dpi_y)

        # adjusting main_widget to fit view port
        max_height = max(self.maximumViewportSize().height(), fig_height)
        self.main_widget.setFixedHeight(max_height)
        self.plotting_axes.canvas.setFixedHeight(max_height)
        # If figure width is set before canvas height is set, it will be
        # changed once canvas height is set for the first plotting. But in the
        # second plotting, that problem won't happen so the figure width remain
        # the same as what is set. As the result the figure width will be
        # different between the first plotting and the rest.
        # To avoid that figure's width has to be set after canvas' height.
        self.plotting_axes.fig.set_figwidth(self.fig_width_in)
        # calculate height_normalizing_factor which is the ratio to multiply
        # with height ratio of an item to fit in figure height start from
        # 1 to 0.
        normalize_total = math.ceil(self.fig_height_in / const.BASIC_HEIGHT_IN)
        self.height_normalizing_factor = vertical_ratio / normalize_total

        if fig_height < max_height:
            # to avoid artifact in main widget's section that isn't covered by
            # figure:
            #  + Figure size need to be spread out to the whole view port by
            #  adjusting fig_height_in.
            #  + Plotting items need to be rescaled through
            #  height_normalizing_factor.
            ratio = fig_height / max_height
            self.height_normalizing_factor *= ratio
            self.fig_height_in = math.ceil(self.fig_height_in / ratio)

        self.plotting_axes.fig.set_figheight(self.fig_height_in)
        self.plotting_axes.height_normalizing_factor = \
            self.height_normalizing_factor * vertical_ratio
    # ======================================================================= #
    #                                  EVENT
    # ======================================================================= #

    def get_timestamp(self, event):
        """
        Get timestamp which is xdata value from the coordinate data of the
            event. Normally, xdata is an attribute of event. However, when
            mouse falls into spaces in between axes, event.xdata is None.

        :param event: button_press_event - mouse click event
        """
        x, y = event.x, event.y  # coordinate data
        inv = self.axes[0].transData.inverted()
        # convert to timestamp, bc event.xdata is None in the space bw axes
        xdata = inv.transform((x, y))[0]
        return xdata

    def zoom_bw_markers(self, xdata):
        """
        Zoom the section in between zoom_marker1 and zoom_marker2:
            + set flag zoom_marker1_shown to False
            + set lim to the new curr_min_x and new curr_max_x set by x of
                zoom_marker1 and zoom_marker2
            + hide zoom_marker1 and zoom_marker2

        :param xdata: float - time value in plot
        """
        self.zoom_marker1.set_visible(False)
        self.zoom_marker2.set_visible(False)
        if abs(self.new_min_x - xdata) < 0.001:
            # to prevent weird zoom in
            display_tracking_info(
                self.tracking_box, "Selected range is too small to zoom in.")
            self.plotting_axes.canvas.draw()
            return
        self.zoom_marker1_shown = False
        [self.min_x, self.max_x] = sorted([self.new_min_x, xdata])
        self.set_lim()
        self.plotting_axes.canvas.draw()

    def zoom_out(self):
        """
        Zoom out by setting limit to the previous range when there's at least
            one zoom-in.
        """
        if len(self.zoom_minmax_list) > 1:
            self.min_x, self.max_x = self.zoom_minmax_list[-2]
            self.zoom_minmax_list.pop()
            self.set_lim(is_zoom_in=False)
        self.zoom_marker1_shown = False
        self.zoom_marker1.set_visible(False)
        self.zoom_marker2.set_visible(False)
        self.plotting_axes.canvas.draw()

    def eventFilter(self, target: QObject, event: QEvent) -> bool:
        """
        Reimplements: QObject.eventFilter()

        Filter mouse wheel events that are sent to the matplotlib plot canvas
        and handle them. Pass on all other events.

        Matplotlib plot canvas swallows events. This is fine for some events,
        but mouse wheel events has to be processed by the scroll area for
        scrolling to happen. This method ensures that scroll events get to
        the scroll area before the plot canvas.

        :param target: the object the event is being sent to
        :param event: the event being sent
        :return: True if the event is filtered out, False otherwise
        """
        is_scroll_on_plot = (target.property('is_plot_canvas') is not None and
                             event.type() == QEvent.Type.Wheel)
        if is_scroll_on_plot:
            # QScrollArea can process QWheelEvent with its wheelEvent() method,
            # but it ignores any QWheelEvent it is sent. We call wheelEvent()
            # explicitly to work around this issue.
            self.wheelEvent(event)
            return False
        else:
            return super().eventFilter(target, event)

    def display_gap_info(self, gap_rectangle: pl.Rectangle) -> None:
        """
        Display gap info in tracking box
        :param gap_rectangle: rectangle that displays the clicked gap on figure
        """
        gap_start = gap_rectangle.get_x()
        gap_width = gap_rectangle.get_width()
        gap_end = gap_start + gap_width

        all_gap_starts = list(zip(*self.gaps))[0]
        gap_position = all_gap_starts.index(gap_start) + 1
        fortmat_start_time = format_time(
            gap_start, self.date_mode, 'HH:MM:SS')
        fortmat_end_time = format_time(
            gap_end, self.date_mode, 'HH:MM:SS')

        gap_delta = get_formatted_time_delta(gap_width)
        info_str = (f"Gap {gap_position}:  "
                    f"{fortmat_start_time}  to  "
                    f"{fortmat_end_time}  "
                    f"{gap_delta}")
        info_str = "<pre>" + info_str + "</pre>"
        display_tracking_info(self.tracking_box, info_str)

    def on_pick_event(self, event):
        """
        When click mouse on a clickable data point (dot with picker=True in
            Plotting),
        + Point's info will be displayed in tracking_box
        + If the chan_data has key 'logIdx', raise the Search Messages dialog,
            focus SOH tab, roll to the corresponding line.
        """
        if event.mouseevent.name == 'scroll_event':
            return
        if event.mouseevent.button in ('up', 'down'):
            return
        self.is_button_press_event_triggered_pick_event = True
        artist = event.artist
        self.log_idxes = None

        if isinstance(artist, pl.Rectangle):
            return self.display_gap_info(artist)

        if not isinstance(artist, pl.Line2D):
            return
        ax = artist.axes
        chan_id = ax.chan
        try:
            chan_data = self.plotting_data1[chan_id]
        except KeyError:
            # in case of mass position
            chan_data = self.plotting_data2[chan_id]
        # list of x values of the plot
        x_list = artist.get_xdata()
        # list of y values of the plot
        y_list = artist.get_ydata()

        # index of the clicked point on the plot
        click_plot_index = event.ind[0]

        # time, val of the clicked point
        clicked_time = x_list[click_plot_index]
        clicked_val = y_list[click_plot_index]

        real_idxes = get_index_from_data_picked(
            chan_data, clicked_time, clicked_val)
        if len(real_idxes) == 0:
            display_tracking_info(self.tracking_box, "Point not found.")
            return

        clicked_data = chan_data['data'][0][real_idxes[0]]
        # The data stored by the data object is not converted, so we need to
        # do the conversion before displaying it to the user.
        clicked_data *= chan_data['chan_db_info']['convertFactor']
        if chan_id.startswith('Disk Usage'):
            clicked_data = get_disk_size_format(clicked_data)
        if hasattr(ax, 'unit_bw'):
            clicked_data = ax.unit_bw.format(clicked_data)
        formatted_clicked_time = format_time(
            clicked_time, self.date_mode, 'HH:MM:SS')
        info_str = (f"<pre>Channel: {chan_id}   "
                    f"Point:{click_plot_index + 1}   "
                    f"Time: {formatted_clicked_time}   "
                    f"Value: {clicked_data}</pre>")
        if 'logIdx' in chan_data.keys():
            self.log_idxes = [chan_data['logIdx'][0][idx]
                              for idx in real_idxes]
            if len(real_idxes) > 1:
                info_str = info_str.replace(
                    "</pre>", f"   ({len(real_idxes)} lines)")
                for idx in self.log_idxes:
                    info_str += (
                        "<pre>   " + self.rt130_log_data[idx] + "</pre>")

        display_tracking_info(self.tracking_box, info_str)

    def on_button_press_event(self, event):
        """
        When click mouse on the current plottingWidget, SOHView will loop
            through different plottingWidgets to do the same task for
            interaction:
            + shift+click:  is disregarded if start in TimePowerSquareWidget
               * If click on left side of the plot (xdata<xmin): call zoom out
               * Otherwise: call on_shift_click to do tasks of zoom in
            + ctrl+click or cmd+click in mac: call on_ctrl_cmd_click() to show
                ruler

        :param event: button_press_event - mouse click event
        """
        for w in self.peer_plotting_widgets:
            if not w.has_data:
                continue
            if w.is_working:
                # try to zoom or use ruler while not done plotting
                return

        modifiers = event.guiEvent.modifiers()
        if self.plotting_data1 == {}:
            return
        if self.name == 'TPS':
            if modifiers == QtCore.Qt.KeyboardModifier.ShiftModifier:
                # not start zooming from TPS Widget
                return
            # on_button_press_event() take place after on_pick_event where
            # tps_t was assigned in TPS Widget
            if self.tps_t is None:
                return
            xdata = self.tps_t
            # Reset tps_t here because the reset in on_pick_event won't be
            # reach when no point is clicked.
            self.tps_t = None
        else:
            xdata = self.get_timestamp(event)

        if (self.main_window.tps_check_box.isChecked() and
                modifiers in [QtCore.Qt.KeyboardModifier.ControlModifier,
                              QtCore.Qt.KeyboardModifier.MetaModifier,
                              QtCore.Qt.KeyboardModifier.NoModifier]):
            self.main_window.tps_dlg.set_indexes_and_display_info(xdata)
        for w in self.peer_plotting_widgets:
            if not w.has_data:
                continue

            if modifiers == QtCore.Qt.KeyboardModifier.ShiftModifier:
                if xdata < w.min_x:
                    # click on left of plot
                    w.zoom_marker1_shown = False  # reset zoom in
                    w.zoom_out()
                else:
                    w.on_shift_click(xdata)
            elif modifiers in [QtCore.Qt.KeyboardModifier.ControlModifier,
                               QtCore.Qt.KeyboardModifier.MetaModifier]:
                w.on_ctrl_cmd_click(xdata)
            w.draw()

        if (not self.is_button_press_event_triggered_pick_event or
                modifiers == QtCore.Qt.KeyboardModifier.ShiftModifier):
            # Bring the main window to the front only if no point was picked.
            # In logpeek and qpeek, zooming is always done, event if a point is
            # picked, so we replicate that here.
            self.parent.show()
            self.parent.activateWindow()
            self.parent.raise_()
        else:
            # Reset the flag checking if this button press event triggered a
            # pick event.
            self.is_button_press_event_triggered_pick_event = False

    def on_ctrl_cmd_click(self, xdata):
        """
        On ctrl/cmd + left click
            + hide zoom_marker1
            + connect ruler to follow mouse
            + show ruler

        :param xdata: float - time value of a channel plot
        """

        self.zoom_marker1.set_visible(False)
        self.zoom_marker1_shown = False
        try:
            self.fig.canvas.mpl_disconnect(self.follower)
        except AttributeError:
            pass
        self.set_ruler_visibled(self.ruler, xdata)
        if xdata >= self.min_x:
            ruler_text_content = format_time(xdata, self.parent.date_format,
                                             'HH:MM:SS')
            try:
                # remove ruler_text before creating the new one
                self.ruler_text.remove()
            except AttributeError:
                pass
            self.ruler_text = self.plotting_axes.fig.text(
                xdata, 5000, ruler_text_content,
                verticalalignment='top',
                horizontalalignment='center',
                transform=self.timestamp_bar_top.transData,
                color=self.display_color['time_ruler'],
                size=self.timestamp_bar_top.get_xticklabels()[0].get_fontsize()
            )

    def on_shift_click(self, xdata):
        """
        On shift + left click:
        if click on the left of a plot, do zoom out to the previous range
        if zoom_marker1 not shown yet:
            + connect zoom_marker2 to follow mouse
            + show zoom_marker1
        else:
            + show zoom_marker2
            + zoom data in between 2 zoomMarkers

        Notice that ruler will stay at the same xdata either with zoom in
        or out.

        :param xdata: float - time value of a channel plot
        """
        if not self.zoom_marker1_shown:
            self.set_ruler_visibled(self.zoom_marker1, xdata)
            self.new_min_x = xdata
            self.zoom_marker1_shown = True
        else:
            self.set_ruler_visibled(self.zoom_marker2, xdata)
            self.plotting_axes.canvas.draw()
            QTimer.singleShot(1, lambda: self.zoom_bw_markers(xdata))
            # self.zoom_bw_markers(xdata)

    def set_ruler_visibled(self, ruler, xdata):
        """
        When set ruler visibled, x of positions of top and bottom have to
            follow the given x.
        If ruler is zoom_marker2, set it to follow the x of mouse's position.

        :param ruler: ConnectionPatch - one of ruler, zoom_marker1,
            zoom_marker2 defined in __init__
        :param xdata: float - time value of a channel plot
        """

        ruler.set_visible(True)
        ruler.xy1 = (xdata, 0)
        ruler.xy2 = (xdata, self.bottom)

    def keyPressEvent(self, event):
        """
        OVERRIDE Qt method.
        When press on Escape key, hide all rulers and set False for
            zoom_marker1_shown on all plotting widgets

        Notice: press Escape is the only way to hide ruler and its text.

        :param event: QKeyEvent - event to know what key is pressed
        """
        if event.key() == QtCore.Qt.Key.Key_Escape:
            try:
                self.ruler_text.remove()
                self.ruler_text = None
            except AttributeError:
                pass
            for w in self.peer_plotting_widgets:
                if not w.has_data:
                    continue
                w.set_rulers_invisible()
                w.zoom_marker1_shown = False
                try:
                    w.ruler_text.remove()
                    w.ruler_text = None
                except AttributeError:
                    pass
                w.draw()
        return super(PlottingWidget, self).keyPressEvent(event)

    def set_rulers_invisible(self):
        """
        Hide ruler, zoom_marker1, zoom_marker2
        """
        self.ruler.set_visible(False)
        self.zoom_marker1.set_visible(False)
        self.zoom_marker2.set_visible(False)

    def check_gap_related_to_range(self, gap: List[float],
                                   min_x: float, max_x: float):
        """
        Check if the given gap inside either completely or partly of range
            (min_x, max_x)
        :param gap: a list of a gap's start time and end time
        :param min_x: minimum of range
        :param max_x: maximum of range
        :return: True if the gap is related to the range, False otherwise.
        """
        return (self.min_x <= min(gap) <= self.max_x
                or self.min_x <= max(gap) <= self.max_x
                or min(gap) <= self.min_x <= max(gap)
                or min(gap) <= self.max_x <= max(gap))

    def set_lim(self, first_time=False, is_zoom_in=True):
        """
        + Append to zoom_minmax_list if called from a zoom_in. First time
        plotting is considered a zoom_in to create first x range in the list.
        + Update timestamp bar with new ticks info
        + Update gap_bar by setting xlim and re-calculate gap total.
        + for each axes, set new x_lim, y_lim, label for totals of data points

        :param first_time: bool - flag shows that this set_lim is called the
            fist time for this data set or not.
        :param is_zoom_in: if set_lim comes from zoom_in task
        """
        from_add_remove_channels = False
        if is_zoom_in:
            if first_time and self.zoom_minmax_list:
                self.min_x, self.max_x = self.zoom_minmax_list[-1]
                from_add_remove_channels = True
            else:
                self.zoom_minmax_list.append((self.min_x, self.max_x))

        self.plotting_axes.update_timestamp_bar(self.timestamp_bar_top)
        self.plotting_axes.update_timestamp_bar(self.timestamp_bar_bottom)
        if self.gap_bar is not None:
            self.gap_bar.set_xlim(self.min_x, self.max_x)
            if not first_time:
                new_gaps = [g for g in self.plotting_axes.gaps
                            if self.check_gap_related_to_range(g,
                                                               self.min_x,
                                                               self.max_x)]

                # reset total of samples on the right
                self.gap_bar.center_total_point_lbl.set_text(len(new_gaps))

        for ax in self.axes:
            if hasattr(ax, 'x') and ax.x is None:
                # the plot_none bar is at the end, no need to process
                break

            ax.set_xlim(self.min_x, self.max_x)
            if not first_time or from_add_remove_channels:
                new_min_y = None
                new_max_y = None
                if hasattr(ax, 'x_top'):
                    # plot_up_down_dots
                    new_x_bottom_indexes = np.where(
                        (ax.x_bottom >= self.min_x) &
                        (ax.x_bottom <= self.max_x))[0]
                    ax.bottom_total_point_lbl.set_text(
                        new_x_bottom_indexes.size)
                    new_x_top_indexes = np.where(
                        (ax.x_top >= self.min_x) &
                        (ax.x_top <= self.max_x))[0]
                    ax.top_total_point_lbl.set_text(
                        new_x_top_indexes.size)
                if hasattr(ax, 'x_center'):
                    if not hasattr(ax, 'y_center'):
                        # plot_dot_for_time and plot_multi_color_dots(_xxx)
                        x = ax.x_center
                        new_x_indexes = np.where(
                            (x >= self.min_x) & (x <= self.max_x))[0]
                        ax.center_total_point_lbl.set_text(new_x_indexes.size)
                        continue
                    total_points = 0
                    tr_min_ys = []
                    tr_max_ys = []
                    x, y = ax.x_center, ax.y_center
                    if len(x) == 0:
                        continue
                    if self.min_x > x[-1] or self.max_x < x[0]:
                        continue
                    ret = get_total_miny_maxy(x, y, self.min_x, self.max_x)
                    total_tr_points, tr_min_y, tr_max_y = ret
                    total_points += total_tr_points
                    if tr_min_y is not None:
                        tr_min_ys.append(tr_min_y)
                        tr_max_ys.append(tr_max_y)
                    if tr_min_ys != []:
                        new_min_y = min(tr_min_ys)
                        new_max_y = max(tr_max_ys)
                        # in case total_points == 1, y lim shouldn't be set
                        # again or the plot would be collapsed to one line
                        if total_points > 1:
                            self.plotting_axes.set_axes_ylim(
                                ax, new_min_y, new_max_y, ax.chan_db_info)
                    ax.center_total_point_lbl.set_text(total_points)

    def draw(self):
        """
        Update drawing on the widget.
        """
        try:
            self.plotting_axes.canvas.draw()
            # a bug on mac:
            # not showing updated info until clicking on another window
            # fix by calling repaint()
            self.main_widget.repaint()
        except TypeError:
            pass

    # ######## Functions for outside world #####
    def init_size(self):
        """
        Set widget to fit with the view port when window first open.
        """
        geo = self.maximumViewportSize()
        if self.plot_total == 0:
            # set view size fit with the scroll's view port size
            self.main_widget.setFixedWidth(geo.width())
            self.main_widget.setFixedHeight(geo.height())

    def set_colors(self, mode):
        """
        Set color mode, colors' definition, background color for the graphic.

        param mode: str - 'B'/'W' - color modes black or white
        """
        self.c_mode = mode
        self.display_color = set_color_mode(mode)
        self.main_widget.setStyleSheet(
            f"background-color:{self.display_color['background']}")

    def set_peer_plotting_widgets(self, widgets):
        """
        Set other plotting widgets so that their rulers and zoom task can work
            synchronously.
        :param widgets: [PlottingWidgets,] - List of other plotting widgets
        """
        self.peer_plotting_widgets = widgets

    def save_plot(self, default_name='plot'):
        if self.c_mode != self.main_window.color_mode:
            main_color = const.ALL_COLOR_MODES[self.main_window.color_mode]
            curr_color = const.ALL_COLOR_MODES[self.c_mode]
            msg = (f"Main window's color mode is {main_color}"
                   f" but the mode haven't been applied to plotting.\n\n"
                   f"Do you want to cancel to apply {main_color} mode "
                   f"by clicking RePlot?\n"
                   f"Or continue with {curr_color}?")
            msgbox = QtWidgets.QMessageBox()
            msgbox.setWindowTitle("Color Mode Conflict")
            msgbox.setText(msg)
            msgbox.addButton(QtWidgets.QMessageBox.StandardButton.Cancel)
            msgbox.addButton('Continue',
                             QtWidgets.QMessageBox.ButtonRole.YesRole)
            result = msgbox.exec()
            if result == QtWidgets.QMessageBox.StandardButton.Cancel:
                return
            self.main_window.color_mode = self.c_mode
            if self.c_mode == 'B':
                self.main_window.background_black_radio_button.setChecked(True)
            else:
                self.main_window.background_white_radio_button.setChecked(True)
        if self.c_mode == 'B':
            msg = ("The current background mode is black.\n"
                   "Do you want to cancel to change the background mode "
                   "before saving the plots to file?")
            msgbox = QtWidgets.QMessageBox()
            msgbox.setWindowTitle("Background Mode Confirmation")
            msgbox.setText(msg)
            msgbox.addButton(QtWidgets.QMessageBox.StandardButton.Cancel)
            msgbox.addButton('Continue',
                             QtWidgets.QMessageBox.ButtonRole.YesRole)
            result = msgbox.exec()
            if result == QtWidgets.QMessageBox.StandardButton.Cancel:
                return
        save_plot_dlg = SavePlotDialog(
            self.parent, self.main_window, default_name)
        save_plot_dlg.exec()
        save_file_path = save_plot_dlg.save_file_path
        if save_file_path is None:
            return
        dpi = save_plot_dlg.dpi
        self.plotting_axes.fig.savefig(
            save_file_path,
            bbox_inches='tight',
            dpi=dpi,
            facecolor=self.display_color['background']
        )
        msg = f"Graph is saved at {save_file_path}"
        display_tracking_info(self.tracking_box, msg)

    def clear(self, do_reset_size: bool = True):
        """
        Clear the figure, reset attributes and initiate size if it is requested

        :param do_reset_size: flag to know if reset_size is requested.
        """
        try:
            self.plotting_axes.fig.clear()
        except AttributeError:
            pass
        self.axes = []
        self.plot_total = 0
        if do_reset_size:
            self.init_size()
        try:
            self.draw()
        except AttributeError:
            pass

    def handle_replot_button(self):
        """
        Check if all plotting_widgets are done with plotting before enable
        replot buttons.
        """
        any_is_working = False
        for w in self.peer_plotting_widgets:
            if w.is_working:
                any_is_working = True
                break
        if not any_is_working:
            self.main_window.replot_button.setEnabled(True)
