# Define functions to call processor

from typing import Tuple, Union, Dict, List, Optional

from PySide6 import QtCore

from sohstationviewer.model.general_data.general_data import GeneralData

from sohstationviewer.view.plotting.plotting_widget.plotting_processor import (
    PlottingChannelProcessor)
from sohstationviewer.view.plotting.plotting_widget.plotting_widget import (
    PlottingWidget)
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.util.functions import (
    replace_actual_question_chans, remove_not_found_chans)

from sohstationviewer.controller.util import display_tracking_info
from sohstationviewer.controller.plotting_data import get_title

from sohstationviewer.conf import constants as const

from sohstationviewer.database.extract_data import get_chan_plot_info

from sohstationviewer.view.select_channels_to_show_dialog import \
    SelectChanelsToShowDialog


class MultiThreadedPlottingWidget(PlottingWidget):
    finished = QtCore.Signal()
    stopped = QtCore.Signal()
    notification = QtCore.Signal(str)

    def __init__(self, *args, **kwargs):
        PlottingWidget.__init__(self, *args, **kwargs)
        self.data_processors: List[PlottingChannelProcessor] = []
        # total number of channels found
        self.number_channel_found = 0
        # order of channels to be plotted
        self.pref_order: List[str] = []
        # object of data to be plotted
        self.data_object: Optional[GeneralData] = None
        # id of data set to be plotted
        self.data_set_id: Union[str, Tuple[str, str]] = ''
        self.start_tm: float = 0
        self.end_tm: float = 0

        # Only one data processor can run at a time, so it is not a big problem
        #
        self.thread_pool = QtCore.QThreadPool()

        # Used to ensure that the user cannot read a new data set when we are
        # zooming in on the waveform plot. Also to prevent user to use ruler
        # or zoom_markers while plotting
        self.is_working = False

        self.finished.connect(self.stopped)
        # TODO: Might not need to use a signal here. The only time this signal
        #   emitted is from this class, and the class is always in the main
        #   thread. The only things running in a different thread are the
        #   PlottingChannelProcessor, and they don't use this signal
        self.notification.connect(
            lambda msg: display_tracking_info(self.tracking_box, msg))

    def reset_widget(self):
        """
        Reset the widget in order to plot a new data set.
        """
        self.data_processors = []
        self.is_working = False

    def init_plot(self,
                  d_obj: GeneralData,
                  data_time: List[float],
                  data_set_id: Union[str, Tuple[str, str]],
                  start_tm: float, end_tm: float,
                  time_ticks_total: int, is_waveform: bool):
        """
        Initiate plotting with gaps, top time bar

        :param d_obj: object of data
        :param data_time: start and end time of data
        :param data_set_id: data set's id
        :param start_tm: requested start time to plot
        :param end_tm: requested end time to plot
        :param time_ticks_total: max number of tick to show on time bar
        :param is_waveform: True if this is a WaveformWidget, otherwise False
        """
        self.has_data = False
        self.zoom_marker1_shown = False
        self.processing_log = []  # [(message, type)]
        self.gaps = d_obj.gaps[data_set_id]
        self.gap_bar = None
        self.date_mode = self.main_window.date_format.upper()
        self.fig_height_in = self.init_fig_height_in()
        self.time_ticks_total = time_ticks_total
        self.min_x = max(data_time[0], start_tm)
        self.max_x = min(data_time[1], end_tm)
        self.plot_total = len(self.plotting_data1) + len(self.plotting_data2)
        self.number_channel_found = len(self.plotting_data1)
        name = self.name
        if not is_waveform:
            self.number_channel_found += len(self.plotting_data2)
            name += " DATA OR MASS POSITION"
        if self.number_channel_found == 0:
            self.title = f"NO {name} DATA TO DISPLAY."
            self.processing_log.append(
                (f"No {name} data to display.", LogType.INFO))
        else:
            self.has_data = True
            self.title = get_title(
                data_set_id, self.min_x, self.max_x, self.date_mode)
        self.plotting_axes.height_normalizing_factor = \
            const.DEFAULT_SIZE_FACTOR_TO_NORMALIZE * \
            self.main_window.vertical_ratio
        self.plotting_bot = const.BOTTOM
        self.axes = []

        if self.number_channel_found == 0:
            # set_size and plot timestamp_bar for the title's position
            self.set_size()
            self.timestamp_bar_top = self.plotting_axes.add_timestamp_bar(
                False)
            self.plotting_axes.set_title(self.title, y=5000)
            self.draw()
            return False
        else:
            return True

    def get_plotting_info(
            self, plotting_data: Dict,
            is_plotting_data1: bool = False
    ) -> List[str]:
        """
        Get chan_db_info and channel order for plotting.
        :param plotting_data: dict of data by chan_id
        :param is_plotting_data1: flag to tell if the plotting_data sent is
            plotting_data1
        :return chan_order: order to plot channels in plotting_data
        """
        chan_order = self.pref_order if is_plotting_data1 and self.pref_order \
            else sorted(list(plotting_data.keys()))
        chan_order = replace_actual_question_chans(
            chan_order, list(plotting_data.keys()))
        chan_order = remove_not_found_chans(
            chan_order, list(plotting_data.keys()), self.processing_log)

        not_plot_chans = []
        for chan_id in chan_order:
            chan_db_info = get_chan_plot_info(chan_id,
                                              self.parent.data_type,
                                              self.c_mode)
            if (chan_db_info['height'] == 0 or
                    chan_db_info['plotType'] == ''):
                # not draw
                not_plot_chans.append(chan_id)
                continue
            if 'DEFAULT' in chan_db_info['channel']:
                msg = (f"Channel {chan_id}'s "
                       f"definition can't be found in database. It will"
                       f"be displayed in DEFAULT style.")
                # TODO: give user a way to add it to DB and leave
                #  instruction here
                self.processing_log.append((msg, LogType.WARNING))

            plotting_data[chan_id]['chan_db_info'] = chan_db_info
            chan_height_ratio = \
                chan_db_info['height'] + const.PLOT_SEPARATOR_SIZE_FACTOR
            self.fig_height_in += chan_height_ratio * const.BASIC_HEIGHT_IN
        if not_plot_chans != []:
            msg = (f"The database settings 'plotType' or 'height' show not to "
                   f"be plotted for the following channels: "
                   f"{', '.join(not_plot_chans)}")
            self.processing_log.append((msg, LogType.WARNING))
        return chan_order

    def plotting_preset(self):
        """
        Calling super's plotting_preset() will preset the width and height of
        figure and main_widget.
        Plot top timestamp bar, gap bar and set title.
        """
        super().plotting_preset()
        self.timestamp_bar_top = self.plotting_axes.add_timestamp_bar(top=True)
        self.plotting_axes.set_title(self.title)
        self.plotting_axes.add_gap_bar(self.gaps)

    def create_ax(self, plotting_data, chan_id):
        """
        Create axes for chan_id in plotting_data to add to key ax or ax_wf of
        channel, and keep track of axes themselves in self.axes.

        :param plotting_data: dict of channels data by chan_id
        :param chan_id: name of channel
        """
        if ('LINES' in
                plotting_data[chan_id]['chan_db_info']['plotType'].upper()):
            has_min_max_lines = True
        else:
            has_min_max_lines = False

        plot_h = self.plotting_axes.get_height(
            plotting_data[chan_id]['chan_db_info']['height'])
        ax = self.plotting_axes.create_axes(
            self.plotting_bot, plot_h, has_min_max_lines)

        if self.name == 'SOH':
            plotting_data[chan_id]['ax'] = ax
        else:
            plotting_data[chan_id]['ax_wf'] = ax

        ax.c_data = plotting_data[chan_id]
        ax.chan = chan_id
        self.axes.append(ax)

    def create_plotting_channel_processors(
            self, plotting_data: Dict, chan_order) -> None:
        """
        Create a data processor for each channel data in the order of
        pref_order. If pref_order isn't given, process in order of
        plotting_data.
        :param plotting_data: dict of data by chan_id
        :param chan_order: order to plot channels
        """
        for chan_id in chan_order:
            if 'chan_db_info' not in plotting_data[chan_id]:
                continue
            if not plotting_data[chan_id].get('visible'):
                continue
            # create ax before plotting, or it will be disordered
            # because of threading
            self.create_ax(plotting_data, chan_id)

            channel_processor = PlottingChannelProcessor(
                plotting_data[chan_id], chan_id,
                self.min_x, self.max_x
            )
            self.data_processors.append(channel_processor)
            channel_processor.finished.connect(self.process_channel)
            channel_processor.stopped.connect(self.has_stopped)

    def plot_channels(
            self, d_obj, data_set_id, start_tm=None, end_tm=None,
            time_ticks_total=0, pref_order=[], keep_zoom=False):
        """
        Prepare to plot waveform/SOH/mass-position data:
            + get_plotting_info: get sizing info
            + plotting_preset: preset figure, widget size according to sizing
                info, plot top time bar, gaps bar
            + creating a data processor for each channel, then,
                run the processors.

        :param d_obj: object of data
        :param data_set_id: data set's id
        :param start_tm: requested start time to read
        :param end_tm: requested end time to read
        :param time_ticks_total: max number of tick to show on time bar
        :param pref_order: order of channels to be plotted
        """
        self.data_object = d_obj
        self.data_set_id = data_set_id
        self.start_tm = start_tm
        self.end_tm = end_tm
        self.time_ticks_total = time_ticks_total
        if not keep_zoom:
            self.zoom_minmax_list = []

        if 'VST' in pref_order:
            # pref_order use original name VST to read from file.
            # For plotting, we need the name that will show in the figure.
            vst_index = pref_order.index('VST')
            pref_order.remove('VST')
            for i in range(3, -1, -1):
                pref_order.insert(vst_index, f'VST{i}')
        self.pref_order = pref_order
        if not self.is_working:
            self.reset_widget()
            self.is_working = True
            start_msg = f'Plotting {self.name} data...'
            display_tracking_info(self.tracking_box, start_msg, LogType.INFO)
            ret = self.init_plot(d_obj, data_set_id, start_tm, end_tm,
                                 time_ticks_total)
            if not ret:
                self.draw()
                self.clean_up(has_data=False)
                self.finished.emit()
                return
            chan_order1 = self.get_plotting_info(self.plotting_data1, True)
            chan_order2 = self.get_plotting_info(self.plotting_data2)
            self.pref_order = chan_order1
            self.plotting_preset()

            self.create_plotting_channel_processors(
                self.plotting_data1, chan_order1)
            self.create_plotting_channel_processors(
                self.plotting_data2, chan_order2)

            self.process_channel()

    @QtCore.Slot()
    def process_channel(self, channel_data=None, channel_id=None):
        """
        Process a channel data. If channel_id and channel_data is
        not None, remove the first data processor from the list of processor
        and plot the channel channel_id. No matter what, the next channel
        processor is started. If there is no more channel processor, the widget
        goes into the steps of finalizing the plot.

        Semantically, if this method is called with no argument, the processing
        is being started. Otherwise, if it receives two arguments, a data
        processor just finished its work and is signalling the widget to plot
        the processed channel and start processing the next one.

        This method has been designed to only start one processor at once. This
        is a deliberate choice in order to avoid running out of memory. Due to
        how they are designed, channels processors use a lot of memory for
        large data sets. If multiple processors are run at once, memory usage
        of the program becomes untenable. It has been observed that a 7.5 GB
        data set can cause an out of memory error.

        :param channel_data: the data of channel_id
        :param channel_id: the name of the channel to be plotted
        """
        if channel_id is not None:
            self.data_processors.pop(0)
            self.notification.emit(f'Plotting channel {channel_id}...')
            self.plotting.plot_channel(channel_data, channel_id)
            self.draw()

        try:
            channel_processor = self.data_processors[0]
            self.notification.emit(f'Processing channel '
                                   f'{channel_processor.channel_id}...')
            self.thread_pool.start(channel_processor)
        except IndexError:
            self.notification.emit('Finishing up...')
            self.clean_up()

        except Exception as e:
            print(e)

    def plot_single_channel(self, c_data: Dict, chan_id: str):
        """
        Plot the channel chan_id.

        :param c_data: data of the channel which includes down-sampled
            data in keys 'times' and 'data'
        :param chan_id: name of channel
        """
        pass

    def clean_up(self, has_data: bool = True):
        """
        Clean up after all available channels have been plotted. The
        cleanup steps are as follows.
            Display a finish message
            Reset all internal flags
        :param has_data: flag that shows if there is data or not
        """
        if has_data:
            self.done()
        finished_msg = f'{self.name} plot finished.'

        display_tracking_info(self.tracking_box, finished_msg, LogType.INFO)
        self.is_working = False
        self.handle_replot_button()

    def done(self):
        """
        Finish up the plot after all channels in the data have been plotted.
        Also signal to the main window that the waveform/soh plot is finished
        so that it can update its flags.
        """
        self.axes.append(self.plotting.plot_none())
        self.timestamp_bar_bottom = self.plotting_axes.add_timestamp_bar(
            top=False)
        super().set_lim(first_time=True)
        self.bottom = self.axes[-1].get_ybound()[0]
        self.ruler = self.plotting_axes.add_ruler(
            self.display_color['time_ruler'])
        self.zoom_marker1 = self.plotting_axes.add_ruler(
            self.display_color['zoom_marker'])
        self.zoom_marker2 = self.plotting_axes.add_ruler(
            self.display_color['zoom_marker'])
        self.draw()
        self.finished.emit()

    def request_stop(self):
        """
        Request the widget to stop plotting.
        """
        # The currently running data processor will always be the first one in
        # the list.
        self.data_processors[0].request_stop()
        # Because the processors are started one at a time, we only need to
        # delete all the processors from the queue. However, we need to keep
        # the currently running data processor in memory to ensure that its
        # downsampler terminate all running threads gracefully.
        self.data_processors = [self.data_processors[0]]

    @QtCore.Slot()
    def has_stopped(self):
        """
        The slot that is called when the last channel processor has terminated
        all running background threads.
        """
        display_tracking_info(self.tracking_box,
                              f'{self.name} plot stopped', LogType.INFO)
        self.is_working = False
        self.stopped.emit()

    def select_channels_to_show(self):
        win = SelectChanelsToShowDialog(self)
        win.exec()
