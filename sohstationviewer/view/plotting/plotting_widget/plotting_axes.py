from typing import List, Optional, Dict, Tuple

import numpy as np

from matplotlib.axes import Axes
from matplotlib.text import Text
from matplotlib.patches import ConnectionPatch, Rectangle
from matplotlib.ticker import AutoMinorLocator
from matplotlib import pyplot as pl
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as Canvas)

from sohstationviewer.controller.plotting_data import (
    get_time_ticks, get_unit_bitweight, get_disk_size_format)
from sohstationviewer.view.util.color import clr
from sohstationviewer.conf import constants as const


def split_label(label: str) -> Tuple[str, str]:
    """
    Split label into 2 parts:
    + If there is '-' between channel and detail, split at the hyphen
    + Otherwise split into words

    :param label: label to be split
    :return 2 parts of label
    """
    if '-' in label:
        labels = label.split('-')
        if len(labels) == 2:
            return labels[0], labels[1]
        else:
            return label[0], '-'.join(labels[1:])
    labels = label.split(' ')
    half_of_total_word = int(len(labels) / 2)
    label = ' '.join(labels[0:half_of_total_word])
    sub_label = ' '.join(labels[half_of_total_word:])
    return label, sub_label


class PlottingAxes:
    """
    Class that includes a figure to add axes for plotting and all methods
        related to create axes, ruler, title.
    """
    def __init__(self, parent, main_window):
        """
        :param parent: PlottingWidget - widget to plot channels
        :param main_window: QApplication - Main Window to access user's
            setting parameters
        """
        self.main_window = main_window
        self.parent = parent

        """
        height_normalizing_factor: Normalize factor corresponding to
        size_factor=1
        Basically this factor is calculated based on the total height of all
        plots. However, when there's no channel, height_normalizing_factor
        can't be calculated so DEFAULT_SIZE_FACTOR_TO_NORMALIZE will be used
        to plot timestamp.
        """
        self.height_normalizing_factor: float = 0.
        """
        gaps: list of gaps which is a list of min and max of gaps
        """
        self.gaps: List[List[float]] = []
        """
        fig: figure associate with canvas to add axes for plotting
        """
        # falcecolor=none to make figure transparent, so the background
        # color depends on canvas'
        self.fig = pl.Figure(facecolor='none', figsize=(10, 10))
        self.fig.canvas.mpl_connect('button_press_event',
                                    self.parent.on_button_press_event)
        self.fig.canvas.mpl_connect('pick_event',
                                    self.parent.on_pick_event)

        """
        canvas: the canvas inside main_widget associates with fig
        """
        self.canvas = Canvas(self.fig)
        # Setting a custom property so we can distinguish the plot canvas from
        # other widgets.
        self.canvas.setProperty('is_plot_canvas', True)

        self.canvas.setParent(self.parent.main_widget)
        # canvas background is transparent
        self.canvas.setStyleSheet("background:transparent;")

    def add_timestamp_bar(self, show_bar: bool = True, top: bool = True):
        """
        Set the axes to display timestamp_bar including color, tick size, label

        In case of no data or TPS, timestamp bar won't be displayed
        but still be needed to display title

        :param show_bar: flag for the timestamp_bar to be displayed or not
        :param top: flag indicating this timestamp_bar is located on top
            or bottom to locate tick label.
        """
        height_normalizing_factor = self.height_normalizing_factor
        if self.parent.name == 'TPS':
            # For TPS, vertical_ratio only apply to title and timestamp bar
            height_normalizing_factor *= self.main_window.vertical_ratio

        # For bottom timestamp bar: space from the last plot to the bar
        # For top timestamp bar: space from title to the bar
        self.parent.plotting_bot -= \
            const.TIME_BAR_SIZE_FACTOR * height_normalizing_factor
        if top:
            # add space for title
            margin = const.TOP_SPACE_SIZE_FACTOR * height_normalizing_factor
            if self.parent.name != 'TPS':
                # TPS has timestamp bar not shown, reduce the margin so
                # the total space from edge to the plot is reasonable
                margin /= 3
            self.parent.plotting_bot -= margin

        height = 0.0005 * height_normalizing_factor

        timestamp_bar = self.fig.add_axes(
            [const.PLOT_LEFT_NORMALIZE, self.parent.plotting_bot,
             const.PLOT_WIDTH_NORMALIZE, height]
        )

        if not show_bar:
            timestamp_bar.set_axis_off()
            return timestamp_bar
        timestamp_bar.xaxis.set_minor_locator(AutoMinorLocator())
        timestamp_bar.spines['bottom'].set_color(
            self.parent.display_color['basic'])
        timestamp_bar.spines['top'].set_color(
            self.parent.display_color['basic'])

        if top:
            labelbottom = False
        else:
            labelbottom = True
            # bottom timestamp bar: space from edge to the bar
            self.parent.plotting_bot -= \
                const.BOT_SPACE_SIZE_FACTOR * height_normalizing_factor

        timestamp_bar.tick_params(which='major', length=7, width=2,
                                  pad=5 * self.main_window.vertical_ratio,
                                  direction='inout',
                                  colors=self.parent.display_color['basic'],
                                  labelbottom=labelbottom,
                                  labeltop=not labelbottom)
        timestamp_bar.tick_params(which='minor', length=4, width=1,
                                  direction='inout',
                                  colors=self.parent.display_color['basic'])
        timestamp_bar.set_ylabel('HOURS',
                                 fontweight='bold',
                                 fontsize=self.main_window.base_plot_font_size,
                                 rotation=0,
                                 labelpad=const.HOUR_TO_TMBAR_D,
                                 ha='left',
                                 color=self.parent.display_color['basic'])
        # not show any y ticks
        timestamp_bar.set_yticks([])
        return timestamp_bar

    def update_timestamp_bar(self, timestamp_bar):
        """
        Update major, minor x ticks, tick labels on timestamp_bar based on
        curr_min_x, curr_max_x, date_mode, time_ticks_total, font_size.

        :param timestamp_bar: matplotlib.axes.Axes - axes for timestamp_bar
        """
        times, major_times, major_time_labels = get_time_ticks(
            self.parent.min_x, self.parent.max_x, self.parent.date_mode,
            self.parent.time_ticks_total
        )
        timestamp_bar.axis('on')
        timestamp_bar.set_xticks(times, minor=True)
        timestamp_bar.set_xticks(major_times)
        timestamp_bar.set_xticklabels(
            major_time_labels,
            fontsize=self.main_window.base_plot_font_size + 1)
        timestamp_bar.set_xlim(self.parent.min_x, self.parent.max_x)

    @staticmethod
    def clean_axes(ax: Axes):
        """
        Remove texts and plots on the given axes ax

        :param ax: axes that has texts and plots to be removed
        """
        for chan_plot in ax.chan_plots:
            chan_plot.remove()
        for text in ax.texts:
            text.remove()
        ax.chan_plots = []

    def create_axes(self, plot_b: float, plot_h: float,
                    has_min_max_lines: bool = True) -> Axes:
        """
        Create axes to plot a channel.

        :param plot_b: bottom of the plot
        :param plot_h: height of the plot
        :param has_min_max_lines: flag showing if the plot need min/max lines
        :return ax: axes created
        """

        ax = self.fig.add_axes(
            [const.PLOT_LEFT_NORMALIZE, plot_b,
             const.PLOT_WIDTH_NORMALIZE, plot_h],
            picker=True
        )

        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        if has_min_max_lines:
            ax.spines['top'].set_zorder(const.Z_ORDER['AXIS_SPINES'])
            ax.spines['bottom'].set_zorder(const.Z_ORDER['AXIS_SPINES'])
            ax.spines['top'].set_color(self.parent.display_color['sub_basic'])
            ax.spines['bottom'].set_color(
                self.parent.display_color['sub_basic'])

        # no stick (use timestamp bar as stick)
        ax.set_yticks([])
        ax.set_xticks([])
        # set tick_params for left ticklabel
        ax.tick_params(colors=self.parent.display_color['basic'],
                       width=0,
                       pad=-2,
                       labelsize=self.main_window.base_plot_font_size)
        # transparent background => self.fig will take care of background
        ax.patch.set_alpha(0)
        # prepare chan_plots list to be reference for the plotted lines/dots
        ax.chan_plots = []
        return ax

    def create_sample_no_label(self, ax: Axes, pos_y: float,
                               sample_no: int, color: str) -> Optional[Text]:
        """
        Create label to display sample number
        :param ax: the axes of the label
        :param pos_y: vertical position for the label
        :param sample_no: total of samples
        :param color: color of the text in the label
        """
        if sample_no is None:
            return
        return ax.text(
            1.005, pos_y,
            str(sample_no),
            horizontalalignment='left',
            verticalalignment='center',
            rotation='horizontal',
            transform=ax.transAxes,
            color=color,
            size=self.main_window.base_plot_font_size
        )

    def set_axes_info(self, ax: Axes,
                      sample_no_list: List[int],
                      sample_no_colors: List[str],
                      sample_no_pos: List[float],
                      label: Optional[str] = None,
                      info: str = '',
                      y_list: Optional[np.ndarray] = None,
                      chan_db_info: Optional[Dict] = None):
        """
        Draw plot's title, sub title, sample total label, center line, y labels
        for a channel.

        :param ax:  axes of a channel
        Each of the following 3 lists have 3 items corresponding to 3 positions
        bottom, center, top
        :param sample_no_list: list of totals of different sample groups
        :param sample_no_colors: list of color to display sample numbers
        :param sample_no_pos: list of position to display sample numbers
            [0.05, 0.5, 0.95] are the basic positions.
        :param label: title of the plot. If label is None, use
            chan_db_info['label']
        :param info: additional info to show in sub title which is
            smaller and under title on the left side
        :param y_list: y values of the channel for min/max labels, lines
        :param chan_db_info: info of channel from database
        """
        if label is None:
            label = chan_db_info['label']

        label_color = self.parent.display_color['plot_label']
        if label.startswith("DEFAULT"):
            label_color = self.parent.display_color["warning"]

        info_color = label_color
        if info != '':
            info_color = self.parent.display_color['sub_basic']

        elif self.main_window.base_plot_font_size > 8:
            # When there's no info and font size is big,
            # separate label into 2 lines.
            # The second line will be in the position of info.
            label, info = split_label(label)

        pos_y = 0.4
        if info != '':
            # set info/sub label on left side in the lower part
            ax.text(
                -0.11, 0.4,
                info,
                horizontalalignment='left',
                verticalalignment='top',
                rotation='horizontal',
                transform=ax.transAxes,
                color=info_color,
                size=self.main_window.base_plot_font_size
            )
            pos_y = 0.6
        # set main label on left side on the higher part
        # or whole label on left side in the middle
        ax.text(
            -0.11, pos_y,
            label,
            horizontalalignment='left',
            rotation='horizontal',
            transform=ax.transAxes,
            color=label_color,
            size=self.main_window.base_plot_font_size + 1
        )

        if not sample_no_pos:
            ax.set_yticklabels([])
            return
        # set samples' total on right side
        # bottom
        ax.bottom_total_point_lbl = self.create_sample_no_label(
            ax, sample_no_pos[0], sample_no_list[0], sample_no_colors[0])
        # center
        ax.center_total_point_lbl = self.create_sample_no_label(
            ax, sample_no_pos[1], sample_no_list[1], sample_no_colors[1])
        # top
        ax.top_total_point_lbl = self.create_sample_no_label(
            ax, sample_no_pos[2], sample_no_list[2], sample_no_colors[2])

        if y_list is None:
            # draw center line
            ax.plot([self.parent.min_x, self.parent.max_x],
                    [0, 0],
                    color=self.parent.display_color['sub_basic'],
                    linewidth=0.5,
                    zorder=const.Z_ORDER['CENTER_LINE']
                    )
            ax.spines['top'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
        else:
            if sample_no_list[0] == 0:
                return

            if len(y_list[0]) == 0:
                min_y = 0
                max_y = 0
            else:
                min_y = min([min(y) for y in y_list])
                max_y = max([max(y) for y in y_list])

            ax.spines['top'].set_visible(True)
            ax.spines['bottom'].set_visible(True)
            ax.unit_bw = get_unit_bitweight(
                chan_db_info, self.main_window.bit_weight_opt
            )
            self.set_axes_ylim(ax, min_y, max_y, chan_db_info)

    def set_axes_ylim(self, ax: Axes, org_min_y: float, org_max_y: float,
                      chan_db_info: dict):
        """
        Limit y range in min_y, max_y.
        Set y tick labels at min_y, max_y
        :param ax: axes of a channel
        :param min_y: minimum of y values
        :param max_y: maximum of y values
        :param chan_db_info: info of channel from database
        """
        if chan_db_info['channel'].startswith('Disk Usage'):
            ax.set_yticks([org_min_y, org_max_y])
            min_y_label = get_disk_size_format(org_min_y)
            max_y_label = get_disk_size_format(org_max_y)
            ax.set_yticklabels([min_y_label, max_y_label])
            return

        if chan_db_info['channel'] == 'GPS Lk/Unlk':
            # to avoid case that the channel doesn't have Lk or Unlk
            # preset min, max value so that GPS Clock Power is always in the
            # middle
            org_min_y = -1
            org_max_y = 1
        min_y = round(org_min_y, 7)
        max_y = round(org_max_y, 7)
        if chan_db_info['fixPoint'] == 0 and org_max_y > org_min_y:
            # if fixPoint=0, the format uses the save value created
            # => try to round to to the point that user can see the differences
            for dec in range(2, 8, 1):
                min_y = round(org_min_y, dec)
                max_y = round(org_max_y, dec)
                if max_y > min_y:
                    break
        if max_y > min_y:
            # There are different values for y => show yticks for min, max
            # separately
            ax.set_yticks([min_y, max_y])
            ax.set_yticklabels(
                [ax.unit_bw.format(min_y), ax.unit_bw.format(max_y)])
        if min_y == max_y:
            # All values for y are the same => show only one yticks
            max_y += 1
            ax.set_yticks([min_y])
            ax.set_yticklabels([ax.unit_bw.format(min_y)])
        ax.set_ylim(min_y, max_y)

    def add_gap_bar(self, gaps):
        """
        Draw the axes and labels to display gap_bar right under top timestamp
        bar.
        Update plotting_bot to draw next plot.

        :param gaps: [[float, float], ] - list of [min, max] of gaps
        """
        if self.main_window.gap_minimum is None:
            return
        self.gaps = gaps

        gap_bar_height = self.height_normalizing_factor * const.GAP_SIZE_FACTOR
        self.parent.plotting_bot -= gap_bar_height
        self.parent.gap_bar = self.create_axes(self.parent.plotting_bot,
                                               gap_bar_height * 0.2,
                                               has_min_max_lines=False)

        gap_label = f"GAP({self.main_window.gap_minimum}sec)"
        h = 0.001  # height of rectangle represent gap
        gap_color = clr['W'] if self.main_window.color_mode == 'B'\
            else clr['B']

        self.set_axes_info(self.parent.gap_bar,
                           sample_no_list=[None, len(gaps), None],
                           sample_no_colors=[None, gap_color, None],
                           sample_no_pos=[None, 0.5, None],
                           label=gap_label)
        # draw gaps
        for i in range(len(gaps)):
            x = gaps[i][0]
            # width of rectangle represent gap
            w = gaps[i][1] - gaps[i][0]
            if w > 0:
                # gap
                c = 'red'
            else:
                # overlap
                c = 'orange'
            self.parent.gap_bar.add_patch(
                Rectangle(
                    (x, - h / 2), w, h, color=c, picker=True, lw=0.,
                    zorder=const.Z_ORDER['GAP']
                )
            )

    def get_height(self, plot_height_ratio: float,
                   plot_separator_size_factor: float =
                   const.PLOT_SEPARATOR_SIZE_FACTOR) -> float:
        """
        Calculate new plot's bottom position and return plot's height.

        :param plot_height_ratio: ratio of the plot height
        :param plot_separator_size_factor: ratio of distance between plots
        :return normalize height of the plot
        """
        plot_h_and_space_ratio = plot_height_ratio + plot_separator_size_factor
        self.parent.plotting_bot -= \
            plot_h_and_space_ratio * self.height_normalizing_factor
        return plot_height_ratio * self.height_normalizing_factor

    def add_ruler(self, color):
        """
        Create a line across all channels' plots from top timestamp
            bar to bottom timestamp bar. There are 3 different rulers defined
            in __init__(): self.ruler, self.zoom_marker1, self.zoom_marker2.

        :param color: color of the ruler
        """
        ruler = ConnectionPatch(
            xyA=(0, 0),
            xyB=(0, self.parent.bottom),
            coordsA="data",
            coordsB="data",
            axesA=self.parent.timestamp_bar_top,
            axesB=self.parent.timestamp_bar_bottom,
            color=color,
        )
        ruler.set_visible(False)
        self.parent.timestamp_bar_bottom.add_artist(ruler)
        return ruler

    def set_title(self, title, x=-0.1, y=6000, v_align='bottom'):
        """
        Display title of the data set's plotting based on

        :param title: str - title of the data set's plotting
        :param x: float - horizontal position to the right from the left edge
            of the self.parent.timestamp_bar_top
        :param y: float - vertical position upward from the top edge of
            the self.parent.timestamp_bar_top
        :param v_align: str - vertical alignment of title to the
            self.parent.timestamp_bar_top
        """
        self.fig.text(x, y, title,
                      verticalalignment=v_align,
                      horizontalalignment='left',
                      transform=self.parent.timestamp_bar_top.transAxes,
                      color=self.parent.display_color['basic'],
                      size=self.main_window.base_plot_font_size + 2)
