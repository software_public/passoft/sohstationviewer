import numpy as np
import math


def downsample(times, data, log_indexes=None, rq_points=0):
    """
    Reduce sample rate of times and data so that times and data return has
        the size around the rq_points.
    Since the functions used for downsampling (chunk_minmax()/constant_rate)
        are very slow, the values of data from mean to CUT_FROM_MEAN_FACTOR
        will be removed first. If the size not meet the rq_points, then
        continue to downsample.
    :param times: numpy array - of a waveform channel's times
    :param data: numpy array - of a waveform channel's data
    :param log_indexes: numpy array - of a waveform channel's soh message line
        index
    :param rq_points: int - requested size to return.
    :return np.array, np.array,(np.array) - new times and new data (and new
        log_indexes) with the requested size
    """
    return times, data, log_indexes
    # if times.size <= rq_points:
    #     return times, data, log_indexes
    # create a dummy array for log_indexes. However this way may slow down
    # the performance of waveform downsample because waveform channel are large
    # and have no log_indexes.
    # if log_indexes is None:
    #     log_indexes = np.empty_like(times)
    # data_max = max(abs(data.max()), abs(data.min()))
    # data_mean = abs(data.mean())
    # indexes = np.where(
    #     abs(data - data.mean()) >
    #     (data_max - data_mean) * const.CUT_FROM_MEAN_FACTOR)
    # times = times[indexes]
    # data = data[indexes]
    # log_indexes = log_indexes[indexes]
    # print(len(times))
    # if times.size <= rq_points:
    #     return times, data, log_indexes
    #
    # return chunk_minmax(times, data, log_indexes, rq_points)


def chunk_minmax(times, data, log_indexes, rq_points):
    """
    Split data into different chunks, take the min, max of each chunk to add
        to the data return
    :param times: numpy array - of a channel's times
    :param data: numpy array - of a channel's data
    :param log_indexes: numpy array - of a channel's log_indexes
    :param rq_points: int - requested size to return.
    :return times, data: np.array, np.array - new times and new data with the
        requested size
    """
    final_points = 0
    if times.size <= rq_points:
        final_points += times.size
        return times, data, log_indexes

    if rq_points < 2:
        return np.empty((1, 0)), np.empty((1, 0)), np.empty((1, 0))

    # Since grabbing the min and max from each
    # chunk, need to div the requested number of points
    # by 2.
    chunk_size = rq_points // 2
    chunk_count = math.ceil(times.size / chunk_size)

    if chunk_count * chunk_size > times.size:
        chunk_count -= 1
        # Length of the trace is not divisible by the number of requested
        # points. So split into an array that is divisible by the requested
        # size, and an array that contains the excess. Downsample both,
        # and combine. This case gives slightly more samples than
        # the requested sample size, but not by much.
        times_0 = times[:chunk_count * chunk_size]
        data_0 = data[:chunk_count * chunk_size]
        log_indexes_0 = log_indexes[:chunk_count * chunk_size]

        excess_times = times[chunk_count * chunk_size:]
        excess_data = data[chunk_count * chunk_size:]
        excess_log_indexes = data[chunk_count * chunk_size:]

        new_times_0, new_data_0, new_log_indexes_0 = downsample(
            times_0, data_0, log_indexes_0, rq_points=rq_points
        )

        # right-most subarray is always smaller than
        # the initially requested number of points.
        excess_times, excess_data, excess_log_indexes = downsample(
            excess_times, excess_data, excess_log_indexes,
            rq_points=chunk_count
        )

        new_times = np.zeros(new_times_0.size + excess_times.size)
        new_data = np.zeros(new_data_0.size + excess_data.size)
        new_log_indexes = np.zeros(
            new_log_indexes_0.size + excess_log_indexes.size
        )

        new_times[:new_times_0.size] = new_times_0
        new_data[:new_data_0.size] = new_data_0
        new_log_indexes[:new_log_indexes_0.size] = new_log_indexes_0

        new_times[new_times_0.size:] = excess_times
        new_data[new_data_0.size:] = excess_data
        new_log_indexes[new_log_indexes_0.size:] = excess_log_indexes

        return new_times, new_data, new_log_indexes

    new_times = times.reshape(chunk_size, chunk_count)
    new_data = data.reshape(chunk_size, chunk_count)
    new_log_indexes = log_indexes.reshape(chunk_size, chunk_count)

    min_data_idx = np.argmin(new_data, axis=1)
    max_data_idx = np.argmax(new_data, axis=1)

    rows = np.arange(chunk_size)

    mask = np.zeros(shape=(chunk_size, chunk_count), dtype=bool)
    mask[rows, min_data_idx] = True
    mask[rows, max_data_idx] = True

    new_times = new_times[mask]
    new_data = new_data[mask]
    new_log_indexes = new_log_indexes[mask]
    return new_times, new_data, new_log_indexes
