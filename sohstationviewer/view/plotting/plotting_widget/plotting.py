# class with all plotting functions
from typing import Dict, Optional
import numpy as np
from matplotlib.axes import Axes

from sohstationviewer.view.plotting.plotting_widget.plotting_helper import (
    get_masspos_value_colors,
    get_categorized_data_from_value_color_equal_on_lower_bound,
    get_categorized_data_from_value_color_equal_on_upper_bound
)
from sohstationviewer.view.util.plot_type_info import plot_types
from sohstationviewer.view.util.color import clr
from sohstationviewer.view.plotting.plotting_widget.plotting_helper import (
    get_colors_sizes_for_abs_y_from_value_colors, apply_convert_factor
)
from sohstationviewer.conf import constants


class Plotting:
    """
    Class that includes different methods to plot channels on a figure.
    """
    def __init__(self, parent, plotting_axes, main_window):
        """
        :param parent: PlottingWidget - widget to plot channels
        :param plotting_axes: PlottingAxes - widget that includes a figure
            and methods related to create axes
        :param main_window: QApplication - Main Window to access user's
            setting parameters
        """
        super().__init__()
        self.parent = parent
        self.main_window = main_window
        self.plotting_axes = plotting_axes

    def plot_none(self):
        """
        Plot with nothing needed to show rulers.
        :return ax: matplotlib.axes.Axes - axes of the empty plot
        """
        plot_h = (constants.PLOT_NONE_SIZE_FACTOR *
                  self.parent.height_normalizing_factor)
        self.parent.plotting_bot -= self.parent.height_normalizing_factor * (
            constants.PLOT_NONE_SIZE_FACTOR +
            constants.PLOT_SEPARATOR_SIZE_FACTOR)

        ax = self.plotting_axes.create_axes(
            self.parent.plotting_bot, plot_h, has_min_max_lines=False)

        ax.x = None
        ax.plot([0], [0], linestyle="")
        ax.chan_db_info = None
        return ax

    def plot_multi_color_dots_base(
            self, c_data: Dict, chan_db_info: Dict,
            ax: Optional[Axes] = None, equal_upper: bool = True):
        """
        plot dots in center with colors defined by valueColors in database:

        x position of each dot defined in value in points_list.
        Color of each dot defined in hex in colors list.
        Size of each dot defined in pixel in sizes list.

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param ax: axes to plot channel
        :param equal_upper:
            if True, plot_from_value_color_equal_on_upper_bound will be used
            otherwise, plot_from_value_color_equal_on_lower_bound will be use
        :return: ax in which the channel is plotted
        """
        if equal_upper:
            points_list, colors, sizes = \
                get_categorized_data_from_value_color_equal_on_upper_bound(
                    c_data, chan_db_info)
        else:
            points_list, colors, sizes = \
                get_categorized_data_from_value_color_equal_on_lower_bound(
                    c_data, chan_db_info)
        # flatten point_list to be x
        x = [item for row in points_list for item in row]
        for points, c, s in zip(points_list, colors, sizes):
            # use bar marker instead of dot to avoid overlap when selecting
            # bigger size for dots.
            chan_plot, = ax.plot(
                points, len(points) * [0], linestyle="",
                marker='|', markersize=s, markeredgewidth=.75,
                zorder=constants.Z_ORDER['DOT'],
                color=c, picker=True, pickradius=3)
            ax.chan_plots.append(chan_plot)
        total_samples = len(x)

        if len(colors) != 1:
            sample_no_colors = [None, '#FFFFFF', None]
        else:
            sample_no_colors = [None, colors[0], None]

        self.plotting_axes.set_axes_info(
            ax, sample_no_list=[None, total_samples, None],
            sample_no_colors=sample_no_colors,
            sample_no_pos=[None, 0.5, None],
            chan_db_info=chan_db_info)

        ax.x_center = c_data['times'][0]
        ax.chan_db_info = chan_db_info
        return ax

    def plot_multi_color_dots_equal_on_upper_bound(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Use plot_multi_color_dots_base() to plot channel in which colors are
        identified by plot_from_value_color_equal_on_upper_bound
        """
        return self.plot_multi_color_dots_base(
            c_data, chan_db_info, ax, equal_upper=True)

    def plot_multi_color_dots_equal_on_lower_bound(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Use plot_multi_color_dots_base() to plot channel in which colors are
        identified by plot_from_value_color_equal_on_lower_bound
        """
        return self.plot_multi_color_dots_base(
            c_data, chan_db_info, ax, equal_upper=False)

    def plot_tri_colors(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Plot 3 different values in 3 lines with 3 different colors according
        to valueColors:
            Ex: "-1:#FF0000|0:#00FF00|1:#0000FF"  means
                value = -1  => plot on line y=-1 with #FF0000 color
                value = 0   => plot on line y=0 with #00FF00 color
                value = 1 => plot on line y=1 with #0000FF color

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :return ax: axes of the channel
        """
        value_colors = chan_db_info['valueColors'].split('|')

        sample_no_colors = []
        total_sample_list = []
        for vc in value_colors:
            v, c = vc.split(':')
            sample_no_colors.append(c)
            val = int(v)
            indexes = np.where(c_data['data'][0] == val)[0]
            times = c_data['times'][0][indexes]

            # base line
            line, = ax.plot(
                [self.parent.min_x, self.parent.max_x],
                [val, val],
                color=clr['r'],
                linewidth=0.5,
                zorder=constants.Z_ORDER['CENTER_LINE']
                )
            ax.chan_plots.append(line)
            # dots
            dots, = ax.plot(
                times, len(times) * [val], linestyle="",
                marker='s', markersize=2,
                zorder=constants.Z_ORDER['DOT'],
                color=c, picker=True, pickradius=3)
            ax.chan_plots.append(dots)

            total_sample_list.append(len(times))
            if val == -1:
                ax.x_bottom = times
            elif val == 0:
                ax.x_center = times
            else:
                ax.x_top = times

        self.plotting_axes.set_axes_info(
            ax, sample_no_list=total_sample_list,
            sample_no_colors=sample_no_colors,
            sample_no_pos=[0.05, 0.5, 0.95],
            chan_db_info=chan_db_info)

        ax.chan_db_info = chan_db_info
        return ax

    def plot_up_down_dots(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Plot channel with 2 different values, one above, one under center line.
        Each value has corresponding color defined in valueColors in database.
        Ex: Down:#FF0000|Up:#00FFFF  means
            value == 1 => plot above center line with #00FFFF color
            value == 0 => plot under center line with #FF0000 color

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :return ax: axes of the channel
        """
        val_cols = chan_db_info['valueColors'].split('|')
        # up/down has 2 values: 0, 1 which match with index of points_list
        points_list = [[], []]
        colors = {}
        for vc in val_cols:
            v, c = vc.split(':')
            val = 0 if v == 'Down' else 1
            points = []
            for times, data in zip(c_data['times'], c_data['data']):
                points += [times[i]
                           for i in range(len(data))
                           if data[i] == val]
            points_list[val] = points
            colors[val] = c

        # down dots
        down_dots, = ax.plot(
            points_list[0], len(points_list[0]) * [-0.5], linestyle="",
            marker='s', markersize=2, zorder=constants.Z_ORDER['DOT'],
            color=colors[0], picker=True, pickradius=3)
        ax.chan_plots.append(down_dots)
        # up dots
        up_dots, = ax.plot(
            points_list[1], len(points_list[1]) * [0.5], linestyle="",
            marker='s', markersize=2, zorder=constants.Z_ORDER['DOT'],
            color=colors[1], picker=True, pickradius=3)
        ax.chan_plots.append(up_dots)

        ax.set_ylim(-2, 2)
        self.plotting_axes.set_axes_info(
            ax,
            sample_no_list=[len(points_list[0]), None, len(points_list[1])],
            sample_no_colors=[colors[0], None, colors[1]],
            sample_no_pos=[0.15, None, 0.85],
            chan_db_info=chan_db_info)

        # x_bottom, x_top are the times of data points to be displayed at
        # bottom or top of the plot
        ax.x_bottom = np.array(points_list[0])
        ax.x_top = np.array(points_list[1])

        ax.chan_db_info = chan_db_info
        return ax

    def plot_dot_for_time(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Plot a dot at each time point in the data. The color of the dot is
        specified in the valueColors
        Ex: Color:#00FF00 means the dot will have the color #00FF00

        :param c_data: dict - data of the channel which includes down-sampled
            data in keys 'times' and 'data'. Refer to DataTypeModel.__init__.
            soh_data[data_set_id][chan_id]

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :return ax: axes of the channel
        """
        # Set the default color to black or white depending on the background
        # color of the plot
        if self.main_window.color_mode == 'B':
            color = '#FFFFFF'
        elif self.main_window.color_mode == 'W':
            color = '#000000'
        if chan_db_info['valueColors'] not in [None, 'None', '']:
            color = chan_db_info['valueColors'].strip()[6:]
        x_list = c_data['times']
        total_x = sum([len(x) for x in x_list])
        self.plotting_axes.set_axes_info(
            ax, sample_no_list=[None, total_x, None],
            sample_no_colors=[None, color, None],
            sample_no_pos=[None, 0.5, None],
            chan_db_info=chan_db_info)

        for x in x_list:
            chan_plot, = ax.plot(
                x, [0] * len(x), marker='s', markersize=1.5,
                linestyle='', zorder=constants.Z_ORDER['LINE'],
                color=color, picker=True,
                pickradius=3)
            ax.chan_plots.append(chan_plot)
        ax.x_center = x_list[0]
        ax.chan_db_info = chan_db_info
        return ax

    def plot_lines_dots(
            self, c_data: Dict, chan_db_info: Dict, chan_id: str,
            ax: Optional[Axes] = None, info: str = ''
    ) -> Axes:
        """
        Plot lines with dots at the data points. Colors of dot and lines are
        defined in valueColors in database.
        Ex: Line:#00FF00|Dot:#FF0000|Zero:#0000FF  means
            Lines are plotted with color #00FF00
            Dots are plotted with color #FF0000
            Additional dot with value Zero in color #0000FF (for channel GPS
            Lk/Unlk)
        If Dot is not defined, dots won't be displayed.
        If Line is not defined, lines will be plotted with color G.
        If Zero is not defined, points with value zero will be plotted the same
            as other points.

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :param info: additional info to be displayed on sub-title under
            main-title
        :return ax: axes of the channel
        """
        x_list, y_list = c_data['times'], c_data['data']
        y_list = apply_convert_factor(
            y_list, chan_id, self.main_window.data_type)
        colors = {}
        if chan_db_info['valueColors'] not in [None, 'None', '']:
            color_parts = chan_db_info['valueColors'].split('|')
            for cStr in color_parts:
                obj, c = cStr.split(':')
                colors[obj] = c
        l_color = '#00FF00'         # default Line color
        if 'Line' in colors:
            l_color = colors['Line']

        has_dot = False             # Optional dot
        if 'Dot' in colors:
            d_color = colors['Dot']
            has_dot = True
        else:
            d_color = l_color

        has_zero = False            # Optional zero
        if 'Zero' in colors:
            z_color = colors['Zero']
            has_zero = True

        if chan_id == 'GPS Lk/Unlk':
            info = "GPS Clock Power"

        if has_zero:
            sample_no_list = []

            # compute x_bottom, x_center, x_top for labels of total numbers on
            # the left of the channel
            ax.x_bottom = x_list[0][np.where(y_list[0] < 0)[0]]
            sample_no_list.append(ax.x_bottom.size)
            ax.x_center = x_list[0][np.where(y_list[0] == 0)[0]]
            sample_no_list.append(ax.x_center.size)
            ax.x_top = x_list[0][np.where(y_list[0] > 0)[0]]
            sample_no_list.append(ax.x_top.size)
            sample_no_colors = [d_color, z_color, d_color]
            sample_no_pos = [0.05, 0.5, 0.95]

            # for plotting top & bottom
            top_bottom_index = np.where(y_list[0] != 0)[0]
            x_list = [x_list[0][top_bottom_index]]
            y_list = [y_list[0][top_bottom_index]]

            chan_plot, = ax.plot(
                ax.x_center, [0] * ax.x_center.size,
                marker='s',
                markersize=1.5,
                linestyle='',
                zorder=constants.Z_ORDER['DOT'],
                mfc=z_color,
                mec=z_color,
                picker=True, pickradius=3)
            ax.chan_plots.append(chan_plot)

        else:
            sample_no_list = [None, sum([len(x) for x in x_list]), None]
            sample_no_colors = [None, d_color, None]
            sample_no_pos = [None, 0.5, None]
            ax.x_center = x_list[0]
            ax.y_center = y_list[0]

        self.plotting_axes.set_axes_info(
            ax, sample_no_list=sample_no_list,
            sample_no_colors=sample_no_colors,
            sample_no_pos=sample_no_pos,
            chan_db_info=chan_db_info,
            info=info, y_list=y_list)

        for x, y in zip(x_list, y_list):
            if not has_dot and sample_no_list[1] > 1:
                # When only line, set marker size small to not show dot.
                # But set pick radius bigger to be easier to click on.
                # This only apply when sample_no_list[1] > 1 because
                # when it is 1, need to show dot or nothing will be plotted.
                chan_plot, = ax.plot(
                    x, y, marker='o', markersize=0.01,
                    linestyle='-', linewidth=0.7,
                    zorder=constants.Z_ORDER['LINE'],
                    color=l_color,
                    picker=True, pickradius=3)
            else:
                chan_plot, = ax.plot(
                    x, y, marker='s', markersize=1.5,
                    linestyle='-', linewidth=0.7,
                    zorder=constants.Z_ORDER['LINE'],
                    color=l_color,
                    mfc=d_color,
                    mec=d_color,
                    picker=True, pickradius=3)
            ax.chan_plots.append(chan_plot)

        ax.chan_db_info = chan_db_info
        return ax

    def plot_lines_s_rate(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Plot line only for waveform data channel (seismic data). Sample rate
        unit will be displayed

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :return ax: axes of the channel
        """
        if c_data['samplerate'] >= 1.0:
            info = "%dsps" % c_data['samplerate']
        else:
            info = "%gsps" % c_data['samplerate']
        return self.plot_lines_dots(
            c_data, chan_db_info, chan_id, ax, info=info)

    def plot_lines_mass_pos(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        Plot multi-color dots with grey line for mass position channel.
        Use get_masspos_value_colors() to get value_colors map based on
            Menu - MP Coloring selected from Main Window.

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :return ax: axes of the channel
        """
        value_colors = get_masspos_value_colors(
            self.main_window.mass_pos_volt_range_opt, chan_id,
            self.parent.c_mode, self.parent.processing_log,
            ret_type='tupleList')

        if value_colors is None:
            return

        x_list, y_list = c_data['times'], c_data['data']
        y_list = apply_convert_factor(
            y_list, chan_id, self.main_window.data_type
        )

        total_x = sum([len(x) for x in x_list])

        self.plotting_axes.set_axes_info(
            ax, sample_no_list=[None, total_x, None],
            sample_no_colors=[None, clr['W'], None],
            sample_no_pos=[None, 0.5, None],
            chan_db_info=chan_db_info, y_list=y_list)
        for x, y in zip(x_list, y_list):
            # plot to have artist pl.Line2D to get pick
            ax.plot(
                x, y, linestyle='-', linewidth=0.7,
                color=self.parent.display_color['sub_basic'],
                picker=True, pickradius=3,
                zorder=constants.Z_ORDER['LINE'])
            colors, sizes = get_colors_sizes_for_abs_y_from_value_colors(
                y, value_colors)
            ax.scatter(
                x, y, marker='s', c=colors, s=sizes,
                zorder=constants.Z_ORDER['DOT'])
            # masspos shouldn't be editable for now so don't append to
            # ax.chan_plots. Also scatter is different with ax.plot and
            # should be treated in a different way if want to replot.
        ax.x_center = x_list[0]
        ax.y_center = y_list[0]
        ax.chan_db_info = chan_db_info
        return ax

    def plot_no_plot_type(
            self, c_data: Dict, chan_db_info: Dict,
            chan_id: str, ax: Optional[Axes] = None) -> Axes:
        """
        In case there're no plot type for the parameter selected when editing
        the channel. the channel will be plot with label and no yticklabels and
        no data. If users re-edit before replotting, they can change the param
        for the channel on the figure. Once they already replot, they have to
        go to channel table to change that.

        :param c_data: data of the channel which includes down-sampled
            (if needed) data in keys 'times' and 'data'.
        :param chan_db_info: info of channel from DB
        :param chan_id: name of channel
        :param ax: axes to plot channel
        :return ax: axes of the channel
        """

        self.plotting_axes.set_axes_info(
            ax,
            sample_no_list=[],
            sample_no_colors=[],
            sample_no_pos=[],
            chan_db_info=chan_db_info)

        ax.chan_db_info = chan_db_info
        return ax

    def plot_channel(self, c_data: Dict, chan_id: str) -> None:
        """
        Plot/replot channel for given data

        :param c_data: data of the channel which includes keys 'times' and
            'data'. Refer to general_data/data_structures.MD
        :param chan_id: name of channel
        """
        if len(c_data['times']) == 0:
            return
        if self.parent.name == 'SOH':
            ax = c_data['ax']
        else:
            ax = c_data['ax_wf']

        chan_db_info = c_data['chan_db_info']
        plot_type = chan_db_info['plotType']
        if plot_type in [None, ""]:
            # when edit select a param that has no plot type
            return self.plot_no_plot_type(c_data, chan_db_info, chan_id, ax)
        getattr(
            self, plot_types[plot_type]['plot_function'])(
            c_data, chan_db_info, chan_id, ax)
