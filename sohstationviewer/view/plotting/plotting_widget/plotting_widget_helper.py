from typing import Dict, Optional

import numpy
import numpy as np


def get_index_from_data_picked(
        chan_data: Dict, tm: float, val: float) -> np.ndarray:
    """
    Get index of data picked
    :param chan_data: dict of data to plot that includes 'times', 'data' key
    :param tm: epoch time of a clicked point
    :param val: data value of a clicked point
    :return real_indexes: list index of data point inside np.ndarray found
    """
    np.set_printoptions(suppress=True)

    if chan_data['chan_db_info']['plotType'] == 'upDownDots':
        # actual plotting has value -0.5 or 0.5;
        # the value need to be convert to 0 or 1
        val = 1 if val == 0.5 else 0

    if (chan_data['chan_db_info']['plotType']
            in ["multiColorDotsEqualOnUpperBound",
                "multiColorDotsEqualOnLowerBound", "dotForTime"]):
        # don't check val because the plot's data don't affect the position
        # of the data point and overlap isn't an issue.
        closest_point_idx = np.where(chan_data['times'][0] == tm)[0]
    else:
        # The data is converted during plotting, so the data provided by
        # matplotlib is different from the raw data. In order to fix this
        # problem, we need to convert matplotlib's data back to raw data.
        val = val / chan_data['chan_db_info']['convertFactor']
        # Converting the data back and forth can introduce floating point
        # errors, so we have to use numpy.isclose() instead of the standard
        # standard comparison.
        real_indexes = np.where(
            (chan_data['times'][0] == tm) &
            (numpy.isclose(chan_data['data'][0], val))
        )[0]
        if len(real_indexes) == 0:
            closest_point_idx = real_indexes
        else:
            closest_point_idx = real_indexes[
                np.argmin(np.abs(chan_data['data'][0][real_indexes] - val))
            ]
            # We turn the index into a numpy array with one element in order to
            # conform to the signature of this function.
            closest_point_idx = np.array([closest_point_idx])
    return closest_point_idx


def get_total_miny_maxy(
        x: np.ndarray, y: Optional[np.ndarray],
        min_x: float, max_x: float) -> (int, Optional[float], Optional[float]):
    """
    Identify total points in channel and y_min, y_max to reset the info and
        ylim
    :param x: x list of the plot
    :param y: y list of the plot
    :param min_x: Under limit of x
    :param max_x: Upper limit of x

    :return: total points, new under limit of y, new upper limit of y
    """
    new_x_indexes = np.where((x >= min_x) & (x <= max_x))[0]

    new_x = x[new_x_indexes]
    if new_x.size == 0:
        return 0, None, None

    new_min_x_index = min(new_x_indexes)
    new_max_x_index = max(new_x_indexes)
    new_y = y[new_min_x_index:new_max_x_index + 1]
    new_min_y = min(new_y)
    new_max_y = max(new_y)
    return new_x_indexes.size, new_min_y, new_max_y
