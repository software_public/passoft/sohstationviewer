# Drawing waveform and mass position

from typing import Tuple, Union
from PySide6 import QtCore, QtWidgets

from sohstationviewer.model.general_data.general_data import GeneralData

from sohstationviewer.view.plotting.plotting_widget.\
    multi_threaded_plotting_widget import MultiThreadedPlottingWidget


class WaveformWidget(MultiThreadedPlottingWidget):
    """
    Widget to display waveform and mass position data.
    """
    def __init__(self, *args, **kwargs):
        MultiThreadedPlottingWidget.__init__(self, *args, **kwargs)

    def contextMenuEvent(self, event):
        """
        Create menu showing up when right click mouse to add/edit channel
        """
        if self.number_channel_found == 0:
            return
        context_menu = QtWidgets.QMenu(self)
        select_channels_to_show_action = context_menu.addAction(
            "Select Channels to show")
        select_channels_to_show_action.triggered.connect(
            self.select_channels_to_show)

        context_menu.exec_(self.mapToGlobal(event.pos()))
        self.curr_ax = None         # to make sure curr_ax is clear
        return super(WaveformWidget, self).contextMenuEvent(event)

    def init_plot(self, d_obj: GeneralData,
                  data_set_id: Union[str, Tuple[str, str]],
                  start_tm: float, end_tm: float, time_ticks_total: int):
        """
        Initialize and configure the plot.

        :param d_obj: object of data
        :param data_set_id: data set's id
        :param start_tm: requested start time to read
        :param end_tm: requested end time to read
        :param time_ticks_total: max number of tick to show on time bar
        """
        self.data_object = d_obj
        self.plotting_data1 = (
            d_obj.waveform_data[data_set_id] if data_set_id else {})
        self.plotting_data2 = (
            d_obj.mass_pos_data[data_set_id] if data_set_id else {})
        data_time = d_obj.data_time[data_set_id] if data_set_id else [0, 1]
        return super().init_plot(d_obj, data_time, data_set_id,
                                 start_tm, end_tm,
                                 time_ticks_total, is_waveform=True)


class WaveformDialog(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__()
        """
        Dialog to display waveform and mass position data. Only pop up if
             'All' checkbox next to WFChans is checked or there are channels
             in WFChans textbox for mseed data or at least one of the DS
            checkbox is checked.

        :param parent: QMainWindow/QWidget - the parent widget
        """
        self.parent = parent
        """
        data_type: str - type of data being plotted
        """
        self.data_type = None
        """
         date_format: format for date
        """
        self.date_format: str = 'YYYY-MM-DD'
        self.setWindowTitle("Raw Data Plot")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.setContentsMargins(5, 5, 5, 5)
        main_layout.setSpacing(0)

        """
        info_text_browser: QTextBrowser - to display info text
        """
        self.info_text_browser = QtWidgets.QTextBrowser(self)
        """
        plotting_widget: PlottingWidget - the widget to draw waveform and
            mass position channel
        """
        self.plotting_widget = WaveformWidget(
            self, self.info_text_browser, 'WAVEFORM', self.parent)
        self.plotting_widget.finished.connect(self.plot_finished)

        main_layout.addWidget(self.plotting_widget, 2)

        bottom_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(bottom_layout)
        """
        save_plot_button: save plot in plotting_widget to file
        """
        self.save_plot_button = QtWidgets.QPushButton('Save Plot', self)
        self.save_plot_button.clicked.connect(self.save_plot)
        bottom_layout.addWidget(self.save_plot_button)
        self.info_text_browser.setFixedHeight(60)
        bottom_layout.addWidget(self.info_text_browser)

    def set_data(self, data_type: str, folder_name: str):
        """
        Set data_type and the window's title.

        :param data_type: data type of data being plotted
        :param folder_name: name of the folder of the data set to be
            displayed
        """
        self.data_type = data_type
        self.setWindowTitle("Raw Data Plot %s - %s" % (data_type, folder_name))

    def resizeEvent(self, event):
        """
        OVERRIDE Qt method.
        When WaveformDialog is resized, its plotting_widget need to initialize
            its size to fit the viewport.

        :param event: QResizeEvent - resize event
        """
        self.plotting_widget.init_size()
        return super(WaveformDialog, self).resizeEvent(event)

    @QtCore.Slot()
    def save_plot(self):
        """
        Save the plotting to a file
        """
        self.plotting_widget.save_plot('Waveform-Plot')

    def plot_finished(self):
        self.parent.is_plotting_waveform = False
