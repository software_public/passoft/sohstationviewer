from typing import Dict, Optional, List

import numpy as np
from PySide6 import QtCore

from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_helper import get_tps_for_discontinuous_data


class TimePowerSquaredProcessorSignal(QtCore.QObject):
    finished = QtCore.Signal(str)
    stopped = QtCore.Signal(str)


class TimePowerSquaredProcessor(QtCore.QRunnable):
    def __init__(self, channel_id: str, channel_data: dict, start_time: float,
                 end_time: float, start_5min_blocks: np.ndarray):
        """
        :param channel_id: name of channel
        :param channel_data: data of channel
        :param start_time: start epoch time of data
        :param end_time: end epoch time of data
        :param start_5min_blocks: the list of starts of five minutes blocks for
            full days.
        """
        super().__init__()
        self.channel_id = channel_id
        self.channel_data = channel_data
        self.start_time = start_time
        self.end_time = end_time
        self.start_5min_blocks = start_5min_blocks
        self.signals = TimePowerSquaredProcessorSignal()
        # Flag to indicate whether the processor should stop running and clean
        # up.
        self.stop = False
        self.stop_lock = QtCore.QMutex()

    def trim_waveform_data(self) -> List[Dict]:
        """
        Trim off waveform traces whose times do not intersect the closed
        interval [self.start_time, self.end_time]. Store the traces that are
        not removed in self.trimmed_trace_list.
        """
        data_start_time = self.channel_data['tracesInfo'][0]['startTmEpoch']
        data_end_time = self.channel_data['tracesInfo'][-1]['endTmEpoch']
        if (self.start_time > data_end_time
                or self.end_time < data_start_time):
            return []

        good_start_indices = [index
                              for index, tr
                              in enumerate(self.channel_data['tracesInfo'])
                              if tr['startTmEpoch'] > self.start_time]
        if good_start_indices:
            start_idx = good_start_indices[0]
            if start_idx > 0:
                start_idx -= 1  # start_time in middle of trace
        else:
            start_idx = 0

        good_end_indices = [idx
                            for idx, tr
                            in enumerate(self.channel_data['tracesInfo'])
                            if tr['endTmEpoch'] <= self.end_time]
        if good_end_indices:
            end_idx = good_end_indices[-1]
            if end_idx < len(self.channel_data['tracesInfo']) - 1:
                end_idx += 1  # end_time in middle of trace
        else:
            end_idx = 0
        end_idx += 1  # a[x:y+1] = [a[x], ...a[y]]

        good_indices = slice(start_idx, end_idx)
        return self.channel_data['tracesInfo'][good_indices]

    def run(self) -> Optional[bool]:
        """
        Different from soh_data where times and data are each in one np.array,
        in waveform_data, times and data are each kept in a list of np.memmap
        files along with startTmEpoch and endTmEpoch.
        self.channel_data['startIdx'] and self.channel_data['endIdx'] will be
        used to exclude np.memmap files that aren't in the zoom time range
        (startTm, endTm). Data in np.memmap will be trimmed according to times
        then time-power-square value for each 5 minutes will be calculated and
        saved in channel_data['tps-data']: np.mean(np.square(5m data))

        """
        if 'tps_data' not in self.channel_data:
            self.channel_data['tps_data'] = get_tps_for_discontinuous_data(
                    self.channel_data, self.start_5min_blocks)
        self.signals.finished.emit(self.channel_id)

    def request_stop(self):
        """Request that the processor stops by setting the stop flag."""
        self.stop_lock.lock()
        self.stop = True
        self.stop_lock.unlock()
        self.signals.finished.emit('')
