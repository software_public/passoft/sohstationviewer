import numpy as np
from typing import Dict, Tuple, List

from obspy import UTCDateTime

from sohstationviewer.conf import constants as const


def get_start_5min_blocks(start_tm: float, end_tm: float) \
        -> Tuple[np.ndarray, float]:
    """
    Get the array of the start time of all five minutes for each day start from
    the start of startTm's day and end at the end of endTm's day.

    :param start_tm: float - start time
    :param end_tm: float - end time

    :return start_5min_blocks: array of starts of five minutes blocks for
        full days that cover start_tm and end_tm.
    :return start_first_day: timestamp of the beginning of the min time's day.
    """
    # get start of the first day
    utc_start = UTCDateTime(start_tm)
    start_first_day = UTCDateTime(
        year=utc_start.year,
        month=utc_start.month,
        day=utc_start.day).timestamp

    # get the end of the last day or beginning of the next day
    utc_end = UTCDateTime(end_tm)
    end_last_day = UTCDateTime(
        year=utc_end.year,
        month=utc_end.month,
        day=utc_end.day).timestamp
    if end_last_day < end_tm:
        # If end_tm is in the middle of a day,
        # end of last day is the beginning of the day after
        end_last_day += 86400

    # split time into 5m blocks in between start_first_day and end_last_day
    start_5min_blocks = np.arange(start_first_day,
                                  end_last_day,
                                  const.SEC_5M)
    return start_5min_blocks, start_first_day


def find_tps_tm_idx(
        given_tm: float, start_5min_blocks: List[List[float]],
        start_first_day: float) \
        -> Tuple[int, int]:
    """
    Convert from given real timestamp to tps plot's day index and
    5m block index.

    :param given_tm: real timestamp
    :param start_5min_blocks: the array of starts of five minutes blocks for
        full days.
    :param start_first_day: timestamp of the beginning of the min time's day.

    :return five_minute_idx: index of 5m blocks
    :return day_idx: index of the day the given time belong to in plotting
    """

    time_delta = given_tm - start_first_day
    day_idx = day_delta = int(time_delta // const.SECOND_IN_DAY)
    second_of_day_of_given_tm = time_delta - day_delta * const.SECOND_IN_DAY
    five_minute_idx = int(second_of_day_of_given_tm // const.SEC_5M)
    five_minute_idx_not_exclude_days = time_delta // const.SEC_5M
    if five_minute_idx_not_exclude_days > len(start_5min_blocks) - 1:
        # When the given time fall into the last 5m of  the last day.
        # Although the time 24:00 of the last day belongs
        # to the next days of other cases, but since there is no more days to
        # plot it, it is no harm to set it at the last 5m of the last day.
        five_minute_idx = const.NUMBER_OF_5M_IN_DAY - 1
        day_idx = int(len(start_5min_blocks) / const.NUMBER_OF_5M_IN_DAY) - 1
    return five_minute_idx, day_idx


def get_tps_for_discontinuous_data(
        channel_data: Dict,
        start_5min_blocks: np.ndarray) -> np.ndarray:
    """
    Calculate 2-D array in which each row contains 5m blocks' mean square
    of a day.
    For the blocks that have no data points, use the mean of all square of data
    in the previous and next blocks if they both have data or the mean_square
    will be zero.

    :param channel_data: dictionary that keeps data of a waveform channel
    :param start_5min_blocks: the list of starts of five minutes blocks for
        full days.
    :return: array of mean square of five-minute data that are separated into
        days
    """
    times = channel_data['tracesInfo'][0]['times']
    data = channel_data['tracesInfo'][0]['data']

    # sorting data according to sorted times so overlap/gap won't affect the
    # calculation
    times_sorting_indices = np.argsort(times)
    times = times[times_sorting_indices]
    data = data[times_sorting_indices]

    # split data into each 5m block
    split_at = times.searchsorted(start_5min_blocks[1:])
    split_data = np.split(data, split_at)

    # Pre-allocate mean squares with zeros
    mean_squares = np.zeros_like(start_5min_blocks, dtype=float)

    # Calculate mean_square for each 5m block
    # adding dtype=float64 to prevent integer overflow
    for i, d in enumerate(split_data):
        if len(d) != 0:
            mean_squares[i] = np.mean(np.square(d, dtype='float64'))
        elif ((0 < i < len(split_data) - 1) and
              len(split_data[i - 1]) > 0 and len(split_data[i + 1]) > 0):
            """
            For the blocks that have no data points, use the mean square of
            data in the previous and next blocks if they both have data.
            """
            mean_squares[i] = np.mean(np.square(
                np.hstack((split_data[i - 1], split_data[i + 1])),
                dtype='float64'
            ))
    # reshape 1D mean_quares into 2D array in which each row contains 288 of
    # 5m blocks' mean_squares of a day
    tps_data = mean_squares.reshape((
        int(len(start_5min_blocks) / const.NUMBER_OF_5M_IN_DAY),
        const.NUMBER_OF_5M_IN_DAY
    ))
    return tps_data


def get_tps_time_by_color_for_a_day(x: np.ndarray,
                                    y: np.ndarray,
                                    square_counts: List[int],
                                    colors: List[str]) -> Dict[str, List[int]]:
    """
    Get indexes with related color to plot tps
    :param x: array of index of 5 minutes in a day
    :param y: array of index of tps value for related x
    :param square_counts: list of square count levels
    :param colors: color corresponding to square count ranges
    :return tps_x_by_color: dict with color code as key, tps_time which is the
        index of 5 minutes in a day as value to plot tps
    """
    tps_x_by_color = {}
    for i, color in enumerate(colors):
        if i == 0:
            indexes = np.where(y == square_counts[i])[0]
        elif i < len(colors) - 1:
            indexes = np.where((square_counts[i-1] < y) &
                               (y <= square_counts[i]))
        else:
            indexes = np.where(square_counts[i - 1] < y)
        tps_x_by_color[color] = x[indexes]
    return tps_x_by_color
