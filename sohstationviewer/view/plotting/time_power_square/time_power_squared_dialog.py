# Display time-power-squared values for waveform data
import numpy as np
from math import sqrt
from typing import Union, Tuple, Dict

from PySide6 import QtWidgets, QtCore
from PySide6.QtCore import QEventLoop, Qt
from PySide6.QtGui import QCursor
from PySide6.QtWidgets import QApplication, QTabWidget

from sohstationviewer.controller.util import \
    display_tracking_info, add_thousand_separator
from sohstationviewer.controller.plotting_data import format_time

from sohstationviewer.database.extract_data import \
    get_color_def, get_color_ranges

from sohstationviewer.model.general_data.general_data import GeneralData

from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_widget import TimePowerSquaredWidget

from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_helper import get_start_5min_blocks, find_tps_tm_idx
from sohstationviewer.conf import constants as const


class TimePowerSquaredDialog(QtWidgets.QWidget):
    def __init__(self, parent):
        """
        Dialog to display time power square data for waveform channels. Users
            are allowed to select different Color Ranges (Antarctica, Low,
            Medium and High) for differences values vs. colors map to draw
            time power square for each 5-minute data

        :param parent: QMainWindow/QWidget - the parent widget
        """
        super().__init__()
        self.main_window = parent
        """
        data_type: str - type of data being plotted
        """
        self.data_type = None
        """
         date_format: format for date
        """
        self.date_format: str = 'YYYY-MM-DD'
        """
        min_x: left limit of data plotted
        max_x: right limit of data plotted
        """
        self.min_x: float = 0
        self.max_x: float = 0

        self.setWindowTitle("TPS Plot")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.setContentsMargins(5, 5, 5, 5)
        main_layout.setSpacing(0)

        """
        tps_widget_dict: dict of TPS widgets in TPS tab
        """
        self.tps_widget_dict: Dict[str, TimePowerSquaredWidget] = {}
        """
        tracking_info_text_browser: QTextBrowser - to display info text
        """
        self.info_text_browser = QtWidgets.QTextBrowser(self)
        """
        plotting_tab: tab that contains widgets to draw time-power-square
            for each 5-minute of data
        """
        self.plotting_tab = QTabWidget(self)
        """
        processing_log_msg: processing message for different TPS channels that
            separated by new line.
        """
        self.processing_log_msg: str = ''
        main_layout.addWidget(self.plotting_tab, 2)

        bottom_layout = QtWidgets.QHBoxLayout()
        bottom_layout.addSpacing(20)
        main_layout.addLayout(bottom_layout)

        buttons_layout = QtWidgets.QVBoxLayout()
        bottom_layout.addLayout(buttons_layout)

        # ################ Coordination ################
        # day index of current clicked point
        self.day_idx: int = 0
        # five minute index of current clicked point
        self.five_minute_idx: int = 0
        # current position of vertical scrollbar when the plot is clicked
        self.vertical_scroll_pos: int = 0
        # ################ Color range #################
        color_layout = QtWidgets.QHBoxLayout()
        buttons_layout.addLayout(color_layout)
        color_layout.addWidget(QtWidgets.QLabel("Color Range"))

        """
        color_def: [str,] -  list of color codes in order of values to be
            displayed
        """
        self.color_def = get_color_def()
        """
        sel_square_counts: [int,] - selected time-power-square ranges
        """
        self.sel_square_counts = []
        """
        sel_col_labels: [str,] - color label for each range
        """
        self.sel_col_labels = []
        """
        color_ranges: [str,] - name of color:value map for user to choose
            'Antarctica'/'Low'/'Medium'/'High'
        all_square_counts: [[int,],] - time-power-squared values
        color_label: [str,] - labels that define count range for colors to show
            in the legend of the tps plotting widget
        """
        (self.color_ranges,
         self.all_square_counts,
         self.color_label) = get_color_ranges()

        """
        color_range_choice: QComboBox - dropdown box for user to choose a
            color:value map name
        """
        self.color_range_choice = QtWidgets.QComboBox(self)
        self.color_range_choice.addItems(self.color_ranges)

        self.color_range_choice.setCurrentText('High')
        color_layout.addWidget(self.color_range_choice)

        """
        every_day_5_min_blocks: the list of starts of five minutes blocks for
            full days.
        """
        self.start_5min_blocks: np.ndarray[float] = np.array([])
        """
        start_first_day: timestamp of the beginning of the min time's day
        """
        self.start_first_day: float = 0

        # ##################### Replot button ########################
        self.replot_current_tab_button = QtWidgets.QPushButton(
            "RePlot Current Tab", self)
        buttons_layout.addWidget(self.replot_current_tab_button)

        self.replot_all_tabs_button = QtWidgets.QPushButton(
            "RePlot All Tabs", self)
        buttons_layout.addWidget(self.replot_all_tabs_button)

        # ##################### Save button ##########################
        self.save_current_tab_button = QtWidgets.QPushButton(
            'Save Current Tab', self)
        buttons_layout.addWidget(self.save_current_tab_button)

        self.info_text_browser.setFixedHeight(60)
        bottom_layout.addWidget(self.info_text_browser)

        self.connect_signals()
        self.color_range_changed()

    def set_data(self, data_type: str, folder_name: str):
        """
        Set data_type and the window's title.

        :param data_type: data type of data being plotted
        :param folder_name: name of the folder of the data set to be
            displayed
        """
        self.data_type = data_type
        self.setWindowTitle("TPS Plot %s - %s" % (data_type, folder_name))

    def resizeEvent(self, event):
        """
        OVERRIDE Qt method.
        When TimePowerDialog is resized, its plotting_widget need to initialize
            its size to fit the viewport.

        :param event: QResizeEvent - resize event
        """
        try:
            view_port_size = \
                self.plotting_tab.currentWidget().maximumViewportSize()
            for tps_widget in self.tps_widget_dict.values():
                tps_widget.set_size(view_port_size)
        except AttributeError:
            # resizeEvent might be called when there's no currentWidget
            pass
        return super(TimePowerSquaredDialog, self).resizeEvent(event)

    def connect_signals(self):
        """
        Connect functions to widgets
        """
        self.save_current_tab_button.clicked.connect(self.save_current_tab)
        self.replot_current_tab_button.clicked.connect(self.replot)
        self.replot_all_tabs_button.clicked.connect(
            self.replot_all_tabs)
        self.color_range_choice.currentTextChanged.connect(
            self.color_range_changed)
        self.plotting_tab.currentChanged.connect(self.on_tab_changed)

    @QtCore.Slot()
    def color_range_changed(self):
        """
        When selecting a different color range from the drop down menu, set
            new value for sel_square_counts and sel_col_labels
        """
        color_range = self.color_range_choice.currentText()
        cr_index = self.color_ranges.index(color_range)
        self.sel_square_counts = self.all_square_counts[cr_index]
        self.sel_col_labels = self.color_label[cr_index]

    @QtCore.Slot()
    def save_current_tab(self):
        """
        Save the plotting to a file
        """
        tps_widget = self.plotting_tab.currentWidget()
        tps_widget.save_plot(f'{tps_widget.tab_name}-Plot')

    @QtCore.Slot()
    def on_tab_changed(self):
        """
        When changing to a new tab, move vertical scroll bar to
        vertical_scroll_pos which was set when user click on a tps_widget.
        """
        try:
            tps_widget = self.plotting_tab.currentWidget()
            tps_widget.verticalScrollBar().setValue(self.vertical_scroll_pos)
        except AttributeError:
            # when tabs are all removed, currentWidget is NoneType
            pass

    @QtCore.Slot()
    def replot(self):
        """
        Apply new settings to the current tps widget and replot.
        """
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        display_tracking_info(self.info_text_browser,
                              "Start replot current TPS tab.")
        QApplication.processEvents(QEventLoop.ExcludeUserInputEvents)
        tps_widget = self.plotting_tab.currentWidget()
        self.processing_log_msg = ''
        tps_widget.replot()
        QApplication.restoreOverrideCursor()

    @QtCore.Slot()
    def replot_all_tabs(self):
        """
        Apply new settings to all tps tabs and replot
        """
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

        display_tracking_info(self.info_text_browser,
                              "Start replot all TPS tabs.")
        self.processing_log_msg = ''
        QApplication.processEvents(QEventLoop.ExcludeUserInputEvents)
        for tps_widget in self.tps_widget_dict.values():
            tps_widget.replot()

        QApplication.restoreOverrideCursor()

    def plot_channels(self, d_obj: GeneralData,
                      data_set_id: Union[str, Tuple[str, str]],
                      start_tm: float, end_tm: float):
        """
        Create tabs to plot data channels,
          + If total days <= limit: all channels are plotted in one tab by
            sending a data_dict of all channels to create_tps_widget where
            they will be plotted using tps_widget.plot_channels()
          + If total days > limit: each channel will be plotted in a separate
            tab by sending a data_dict of one channel to create_tps_widget
            where the channel will be plotted using
            tps_widget.plot_channels()
        :param d_obj: object of data
        :param data_set_id: data set's data_set_id
        :param start_tm: requested start time to read
        :param end_tm: requested end time to read
        """
        self.processing_log_msg = ""
        self.min_x = max(d_obj.data_time[data_set_id][0], start_tm)
        self.max_x = min(d_obj.data_time[data_set_id][1], end_tm)
        self.start_5min_blocks, self.start_first_day = get_start_5min_blocks(
            self.min_x, self.max_x)
        for i in range(self.plotting_tab.count() - 1, -1, -1):
            # delete all tps tabs
            widget = self.plotting_tab.widget(i)
            self.plotting_tab.removeTab(i)
            widget.setParent(None)
        self.tps_widget_dict = {}
        if (len(self.start_5min_blocks)/const.NUMBER_OF_5M_IN_DAY
                <= const.DAY_LIMIT_FOR_TPS_IN_ONE_TAB):
            self.main_window.tps_tab_total = 1
            self.create_tps_widget(
                data_set_id, 'TPS', d_obj.waveform_data[data_set_id])
        else:
            self.main_window.tps_tab_total = len(
                d_obj.waveform_data[data_set_id])
            for chan_id in sorted(d_obj.waveform_data[data_set_id].keys()):
                self.create_tps_widget(
                    data_set_id, chan_id,
                    {chan_id: d_obj.waveform_data[data_set_id][chan_id]})

    def create_tps_widget(self, data_set_id, tab_name, data_dict):
        """
        Create a tps widget and add to plotting_tab, then call plot Channels
        to plot all channels in data_dict.
        :param tab_idx: index of tab to decide to call set_size
        :param data_set_id: data_set_id of the selected data set
        :param tab_name: name of the channel that will be plotted in the tab
            or 'TPS' if all channels in waveform_data of the selected data set
            will be plotted. This is used for name of tab and filename when
            saving the plot under the tab
        :param data_dict: dict of channels to be plotted
        """
        tps_widget = TimePowerSquaredWidget(
            data_set_id, tab_name, self, self.info_text_browser,
            'TPS', self.main_window
        )
        # We need to set the colors of the TPS widget when it is created,
        # otherwise it will default to black.
        tps_widget.set_colors(self.main_window.color_mode)
        self.plotting_tab.addTab(tps_widget, tab_name)
        self.tps_widget_dict[tab_name] = tps_widget
        tps_widget.plot_channels(
            data_dict, data_set_id,
            self.start_5min_blocks, self.start_first_day,
            self.min_x, self.max_x)

    def set_indexes_and_display_info(self, timestamp):

        """
        computing five_minute_idx/day_idx and displaying info of all tps
        channels' points.

        :param timestamp: real timestamp value
        """
        # calculate the indexes corresponding to the timestamp
        self.five_minute_idx, self.day_idx = find_tps_tm_idx(
            timestamp, self.start_5min_blocks, self.start_first_day)
        # timestamp in display format
        format_t = format_time(timestamp, self.date_format, 'HH:MM:SS')

        # display the highlighted point's info on all tabs
        info_str = f"<pre>{format_t}:"
        for tps_widget in self.tps_widget_dict.values():
            for chan_id in tps_widget.plotting_data1:
                c_data = tps_widget.plotting_data1[chan_id]
                data = c_data['tps_data'][self.day_idx,
                                          self.five_minute_idx]
                info_str += (f"   {chan_id}:"
                             f"{add_thousand_separator(sqrt(data))}")
        info_str += "  (counts)</pre>"
        display_tracking_info(self.info_text_browser, info_str)
