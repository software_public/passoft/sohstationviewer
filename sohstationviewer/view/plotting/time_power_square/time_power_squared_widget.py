from typing import List, Tuple, Union, Dict, Optional
import numpy as np
from PySide6 import QtCore
from obspy import UTCDateTime
from matplotlib.axes import Axes
from matplotlib.lines import Line2D
from matplotlib.backend_bases import PickEvent

from sohstationviewer.conf import constants as const
from sohstationviewer.controller.plotting_data import (
    get_title, get_day_ticks
)
from sohstationviewer.controller.util import (
    display_tracking_info
)
from sohstationviewer.database.extract_data import (
    get_seismic_chan_label,
)
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.util.color import clr
from sohstationviewer.view.plotting.plotting_widget import plotting_widget
from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_processor import TimePowerSquaredProcessor
from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_helper import \
    find_tps_tm_idx, get_tps_time_by_color_for_a_day


class TimePowerSquaredWidget(plotting_widget.PlottingWidget):
    stopped = QtCore.Signal()
    """
    Widget to display time power square data for waveform channels
    """
    def __init__(self, data_set_id, tab_name, *args, **kwarg):
        """
        :param data_set_id: the id of the data set
        :param tab_name: name to show to identify the tab that content this
            widget
        """
        self.data_set_id = data_set_id
        self.tab_name = tab_name
        """
        rulers: list of squares on each waveform
            channels to highlight the five-minute at the mouse click on
            TimePowerSquaredWidget or the five-minute corresponding to the
            time at the mouse click on other plotting widgets
        """
        self.rulers: List[Line2D] = []
        """
        zoom_marker1s: list of line markers to
            mark the five-minute corresponding to the start of the zoom area
        """
        self.zoom_marker1s: List[Line2D] = []
        """
        zoom_marker2s: list of line markers to
            mark the five-minute corresponding to the end of the zoom area
        """
        self.zoom_marker2s: List[Line2D] = []
        """
        start_5min_blocks: the list of starts of five minutes blocks for
        full days.
        """
        self.start_5min_blocks: np.ndarray[float] = np.array([])
        """
        start_first_day: timestamp of the beginning of the min time's day
        """
        self.start_first_day: float = 0
        """
        tps_t: float - prompt's time on tps's chart to help rulers on other
            plotting widgets to identify their location
        """
        self.tps_t = None
        """
        display_top_ticks: flag to display time ticks on top of ax or not
        """
        self.display_top_ticks: bool = False

        self.tps_processors: List[TimePowerSquaredProcessor] = []
        # The list of all channels that are processed.
        self.channels = []
        # The list of channels that have been processed.
        self.processed_channels = []
        # To prevent user to use ruler or zoom_markers while plotting
        self.is_working = False
        # The post-processing step does not take too much time so there is no
        # need to limit the number of threads that can run at once.
        self.thread_pool = QtCore.QThreadPool()
        self.finished_lock = QtCore.QMutex()

        super().__init__(*args, **kwarg)

    def resizeEvent(self, event):
        # resizeEvent might not reach to unfocused tabs.
        # parent.view_port_size is set to set size for those tabs' components.
        self.parent.view_port_size = self.maximumViewportSize()
        return super(TimePowerSquaredWidget, self).resizeEvent(event)

    def get_plot_name(self):
        """
        Show TPS following tab names if needed in any tracking info message
        """
        return (self.tab_name if self.tab_name == 'TPS'
                else self.tab_name + " TPS")

    def init_fig_height_in(self):
        return const.BASIC_HEIGHT_IN * (
                const.TOP_SPACE_SIZE_FACTOR/3 +
                const.BOT_SPACE_SIZE_FACTOR +
                const.TPS_LEGEND_SIZE_FACTOR +
                const.TPS_SEPARATOR_SIZE_FACTOR)

    def start_message_and_reset_processing_states(self, start_msg: str):
        """
        Display start message and reset processing states for a new plotting.

        :param start_msg: the starting message to show user that new plotting
            has been started.
        """
        self.plotting_bot = const.BOTTOM
        self.processed_channels = []
        self.channels = []
        self.tps_processors = []
        self.processing_log = []  # [(message, type)]
        self.parent.processing_log_msg += start_msg + '\n'
        self.display_top_ticks = True
        display_tracking_info(
            self.tracking_box, self.parent.processing_log_msg)

    def plot_channels(self, data_dict: Dict,
                      data_set_id: Union[str, Tuple[str, str]],
                      start_5min_blocks: np.ndarray,
                      start_first_day,
                      min_x: float, max_x: float):
        """
        Recursively plot each TPS channels for waveform_data.
        :param data_dict: dict of all channels to be plotted
        :param data_set_id: data set's id
        :param start_5min_blocks: the list of starts of five minutes
            blocks for full days.
        :param min_x: left limit of data plotted
        :param max_x: right limit of data plotted
        """
        self.has_data = False
        self.zoom_marker1_shown = False
        self.is_working = True
        self.display_top_ticks = True
        self.plotting_data1 = data_dict
        self.fig_height_in = self.init_fig_height_in()
        self.min_x = min_x
        self.max_x = max_x
        self.plot_total = len(self.plotting_data1)
        self.start_5min_blocks = start_5min_blocks
        self.start_first_day = start_first_day
        self.start_message_and_reset_processing_states(
            f'Plotting {self.get_plot_name()} ...')
        self.gap_bar = None

        self.date_mode = self.main_window.date_format.upper()
        if self.plotting_data1 == {}:
            self.title = "NO WAVEFORM DATA TO DISPLAY TPS."
            self.processing_log.append(
                ("No WAVEFORM data to display TPS.", LogType.INFO))
        else:
            self.has_data = True
            self.title = get_title(
                data_set_id, self.min_x, self.max_x, self.date_mode)
        # With TPS, not apply vertical ratio to channels,
        # only for timestamp bar and title
        self.plotting_axes.height_normalizing_factor = \
            const.DEFAULT_SIZE_FACTOR_TO_NORMALIZE
        self.plotting_bot = const.BOTTOM
        self.axes = []
        if self.plotting_data1 == {}:
            # set_size and plot timestamp_bar for the title's position
            self.set_size()
            self.timestamp_bar_top = self.plotting_axes.add_timestamp_bar(
                False)
            self.plotting_axes.set_title(self.title, y=0)
            self.is_working = False
            self.draw()
            self.clean_up(None)
            return
        self.get_plotting_info()
        self.plotting_preset()
        self.create_plotting_channel_processors()

    def get_plotting_info(self):
        """
        Calculate height_ratio and add to c_data to be used to identify height
        for TPS plotting.
        Add value to fig_height_in corresponding to the tps channels plotted
        """
        for chan_id in self.plotting_data1:
            c_data = self.plotting_data1[chan_id]
            total_days = (len(self.start_5min_blocks) /
                          const.NUMBER_OF_5M_IN_DAY)
            c_data['height_ratio'] = total_days/1.7
            chan_height_ratio = \
                c_data['height_ratio'] + const.TPS_SEPARATOR_SIZE_FACTOR
            self.fig_height_in += chan_height_ratio * const.BASIC_HEIGHT_IN

    def plotting_preset(self):
        """
        Set the current widget to be the active tab to have the correct view
        port size before calling super's plotting_preset() will preset the
        width and height of figure and main_widget.
        Plot top timestamp bar but not show to anchor the title.
        """
        # set self to be the current widget to have the correct view port size
        self.parent.plotting_tab.setCurrentWidget(self)
        super().plotting_preset()
        self.timestamp_bar_top = self.plotting_axes.add_timestamp_bar(
            show_bar=False, top=True
        )
        self.plotting_axes.set_title(self.title, y=1000)

    def create_plotting_channel_processors(self):
        for chan_id in sorted(self.plotting_data1.keys()):
            c_data = self.plotting_data1[chan_id]

            plot_h = self.plotting_axes.get_height(
                plot_height_ratio=c_data['height_ratio'],
                plot_separator_size_factor=const.TPS_SEPARATOR_SIZE_FACTOR)
            ax = self.create_axes(self.plotting_bot, plot_h)

            c_data['ax'] = ax
            self.channels.append(chan_id)
            channel_processor = TimePowerSquaredProcessor(
                chan_id, c_data, self.min_x, self.max_x,
                self.start_5min_blocks
            )
            channel_processor.signals.finished.connect(self.channel_done)
            channel_processor.signals.stopped.connect(self.channel_done)
            self.tps_processors.append(channel_processor)

        # Because the widget determine if processing is done by comparing the
        # lists of scheduled and finished channels, if a channel runs fast
        # enough that it finishes before any other channel can be scheduled,
        # it will be the only channel executed. To prevent this, we tell the
        # threadpool to only start running the processors once all channels
        # have been scheduled.
        for processor in self.tps_processors:
            self.thread_pool.start(processor)

    @QtCore.Slot()
    def channel_done(self, chan_id: str):
        """
        Slot called when a TPS processor is finished. Plot the TPS data of
        channel chan_id if chan_id is not an empty string and add chan_id to
        the list of processed of channels. If the list of processed channels
        is the same as the list of all channels, notify the user that the
        plotting is finished and add finishing touches to the plot.

        If chan_id is the empty string, notify the user that the plotting has
        been stopped.

        :param chan_id: the name of the channel whose TPS data was processed.
            If the TPS plot is stopped before it is finished, this will be the
            empty string
        """
        self.finished_lock.lock()
        if chan_id != '':
            ax = self.plot_channel(self.plotting_data1[chan_id], chan_id)
            self.axes.append(ax)
        self.processed_channels.append(chan_id)
        if len(self.processed_channels) == len(self.channels):
            self.clean_up(chan_id)
        self.finished_lock.unlock()

    def clean_up(self, chan_id: Optional[str]) -> None:
        """
        Clean up after all available waveform channels have been stopped or
        plotted. The cleanup steps are as follows.
            Display a finished message
            Add finishing touches to the plot
            Emit the stopped signal of the widget

        :param chan_id: channel name in str or None if no data, '' if stop
        """
        if chan_id is None:
            msg = f'{self.get_plot_name()} has no data.'
        elif chan_id == '':
            msg = f'{self.get_plot_name()} stopped.'
        else:
            msg = f'{self.get_plot_name()} finished.'
            self.done()
        if (self.parent.processing_log_msg.startswith('Plotting') or
                self.parent.processing_log_msg.startswith('RePlotting')):
            # reset to have the finish messages
            self.parent.processing_log_msg = ''
        if msg:
            self.parent.processing_log_msg += msg + "\n"
            display_tracking_info(
                self.tracking_box, self.parent.processing_log_msg)

        self.stopped.emit()

    def done(self):
        """Add finishing touches to the plot and display it on the screen."""
        self.set_legend()
        self.set_lim_markers()
        self.draw()
        self.is_working = False
        self.handle_replot_button()

    def plot_channel(self, c_data: str, chan_id: str) -> Axes:
        """
        TPS is plotted in lines of small rectangular, so called bars.
        Each line is a day so - y value is the order of days
        Each bar is data represent for 5 minutes so x value is the order of
            five minute in a day
        If there is no data in a portion of a day, the bars in the portion
            will have grey color.
        For the five minutes that have data, the color of the bars will be
            based on mapping between tps value of the five minutes against
            the selected color range.

        This function draws each 5 minute with the color corresponding to
        value and create ruler, zoom_marker1, and zoom_marker2 for the channel.

        :param c_data: dict - data of waveform channel which includes keys
            'times' and 'data'. Refer to general_data/data_structures.MD
        :param chan_id: str - name of channel
        :return ax: axes of the channel
        """
        ax = c_data['ax']
        ax.spines[['right', 'left', 'top', 'bottom']].set_visible(False)
        ax.text(
            -0.15, 1,
            f"{get_seismic_chan_label(chan_id)} {c_data['samplerate']}sps",
            horizontalalignment='left',
            verticalalignment='top',
            rotation='horizontal',
            transform=ax.transAxes,
            color=self.display_color['plot_label'],
            size=self.main_window.base_plot_font_size + 2
        )

        zoom_marker1 = ax.plot(
            [], [], marker='|', markersize=5, markeredgewidth=1.5,
            markeredgecolor=self.display_color['zoom_marker'],
            zorder=const.Z_ORDER['TPS_MARKER'])[0]
        self.zoom_marker1s.append(zoom_marker1)

        zoom_marker2 = ax.plot(
            [], [], marker='|', markersize=5, markeredgewidth=1.5,
            markeredgecolor=self.display_color['zoom_marker'],
            zorder=const.Z_ORDER['TPS_MARKER'])[0]
        self.zoom_marker2s.append(zoom_marker2)

        ruler = ax.plot(
            [], [], marker='s', markersize=4,
            markeredgecolor=self.display_color['time_ruler'],
            zorder=const.Z_ORDER['TPS_MARKER'],
            markerfacecolor='None')[0]
        self.rulers.append(ruler)

        # --------------------------- PLOT TPS -----------------------------#
        x = np.array([i for i in range(const.NUMBER_OF_5M_IN_DAY)])
        square_counts = self.parent.sel_square_counts  # square counts range
        color_codes = self.parent.color_def  # colordef
        start_year_indexes = []
        start_year_labels = []
        for day_idx, y in enumerate(c_data['tps_data']):
            # set grid line before a year start
            tps_t = UTCDateTime(
                self.start_5min_blocks[day_idx * const.NUMBER_OF_5M_IN_DAY])
            if (tps_t.month, tps_t.day) == (1, 1) and day_idx != 0:
                start_year_indexes.append(-day_idx + 0.5)
                start_year_labels.append(tps_t.year)
            # draw tps for a day
            tps_x_by_color_for_a_day = get_tps_time_by_color_for_a_day(
                x, y, square_counts, color_codes)
            for color, tps_x in tps_x_by_color_for_a_day.items():
                ax.plot(
                    tps_x, [- day_idx] * len(tps_x), linestyle="",
                    marker='s', markersize=2.2, color=clr[color],
                    zorder=const.Z_ORDER['DOT'])

        # extra to show highlight square
        ax.set_ylim(-(c_data['tps_data'].shape[0] + 1), 1)
        # show separation year tick labels
        ax.set_yticks(start_year_indexes)
        ax.set_yticklabels(start_year_labels,
                           fontsize=self.main_window.base_plot_font_size,
                           color=self.display_color['basic'])
        return ax

    def set_legend(self):
        """
        Plot one dot for each color and assign label to it. The dots are
            plotted outside of xlim to not show up in plotting area. xlim is
            set so that it has some extra space to show full highlight square
            of the ruler.
        ax.legend will create one label for each dot.
        """
        # set height of legend and distance bw legend and upper ax
        plot_h = self.plotting_axes.get_height(
            plot_height_ratio=const.TPS_LEGEND_SIZE_FACTOR,
            plot_separator_size_factor=const.TPS_SEPARATOR_SIZE_FACTOR)
        ax = self.plotting_axes.canvas.figure.add_axes(
            [const.TPS_LEFT_NORMALIZE, self.plotting_bot,
             const.TPS_WIDTH_NORMALIZE, plot_h],
            picker=True
        )
        ax.axis('off')
        ax.patch.set_alpha(0)
        c_labels = self.parent.sel_col_labels
        clrs = self.parent.color_def  # colordef
        for idx in range(len(c_labels)):
            # draw a dot out of xlim so it isn't displayed in plotting area
            ax.plot([300], [1], linestyle="",
                    markerfacecolor=clr[clrs[idx]],
                    marker='o',
                    label=c_labels[idx],
                    markeredgecolor=self.display_color['basic'],
                    markersize=.3,
                    alpha=0.8,
                    zorder=1)
        # extra to show highlight square
        ax.set_xlim(-2, const.NUMBER_OF_5M_IN_DAY + 1)
        ax.legend(loc="upper left", framealpha=0.2,
                  markerscale=25,
                  labelcolor=self.display_color['basic'])

    def create_axes(self, plot_b, plot_h):
        """
        Create axes for 288 of 5m in a day in which minor tick for every hour,
            major tick for every 4 hour

        :param plot_b: float - bottom of the plot
        :param plot_h: float - height of the plot
        :return ax: matplotlib.axes.Axes - axes of tps of a waveform channel
        """
        ax = self.plotting_axes.canvas.figure.add_axes(
            [const.TPS_LEFT_NORMALIZE, plot_b,
             const.TPS_WIDTH_NORMALIZE, plot_h],
            picker=True
        )
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.xaxis.grid(True, which='major',
                      color=self.display_color['basic'], linestyle='-')
        ax.xaxis.grid(True, which='minor',
                      color=self.display_color['sub_basic'], linestyle='-')
        # show year separation lines
        ax.yaxis.grid(True, which='major',
                      color=self.display_color['basic'], linestyle='-')
        ax.set_yticks([])

        times, major_times, major_time_labels = get_day_ticks()
        ax.set_xticks(times, minor=True)
        ax.set_xticks(major_times)
        ax.set_xticklabels(major_time_labels,
                           fontsize=self.main_window.base_plot_font_size,
                           color=self.display_color['basic'])
        if self.display_top_ticks:
            # Show time ticks on both top and bottom of the first ax.
            # By default it only show on the bottom of the axes.
            ax.tick_params(axis="x", bottom=True, top=True, labelbottom=True,
                           labeltop=True)
            self.display_top_ticks = False

        # show separation year ticks on both left and right
        ax.tick_params(axis="y",
                       left=True, labelleft=True,
                       right=True, labelright=True)

        # extra to show highlight square
        ax.set_xlim(-2, const.NUMBER_OF_5M_IN_DAY + 1)
        ax.patch.set_alpha(0)
        return ax

    def on_pick_event(self, event: PickEvent):
        """
        This function is called when a point is selected.

        To avoid redundant process, return for scroll event, or any modifiers
        other than ctrl/cmd/no modifier.
        https://matplotlib.org/stable/users/explain/figure/event_handling.html#event-attributes  # noqa: E501

        This function help set indexes in tps_dialog so all other tabs will use
        the same indexes.

        :param event: event when object of canvas is selected.
            The event happens before button_press_event.
        """
        # Set tps_t None in case the function is return before tps_t is
        # calculated, then other function that look for tps_t will know.
        self.tps_t = None
        if event.mouseevent.name == 'scroll_event':
            return
        if event.mouseevent.button in ('up', 'down'):
            return
        modifiers = event.guiEvent.modifiers()
        if modifiers not in [QtCore.Qt.KeyboardModifier.ControlModifier,
                             QtCore.Qt.KeyboardModifier.MetaModifier,
                             QtCore.Qt.KeyboardModifier.NoModifier]:
            # skip modifiers other than Ctrl or command
            return

        if event.artist in self.axes:
            self.parent.vertical_scroll_pos = self.verticalScrollBar().value()
            xdata = event.mouseevent.xdata
            if xdata is None:
                return
            # Because of the thickness of the marker, round help cover the part
            # before and after the point
            xdata = round(xdata)
            if xdata < 0 or xdata > 287:
                # Ignore when clicking outside xrange
                return
            # clicked point's x value is the 5m index in a day
            five_minute_index = xdata

            # day start at a new integer number, so any float between one day
            # to the next will be considered belong to that day.
            ydata = int(event.mouseevent.ydata)

            total_days = (len(self.start_5min_blocks)
                          // const.NUMBER_OF_5M_IN_DAY)
            if ydata > 0 or ydata < - (total_days - 1):
                # Ignore when clicking outside yrange
                return
            # Clicked point's y value is corresponding to the day index but
            # negative because days are plotted from top to bottom.
            day_index = abs(ydata)

            try:
                # Assign tps_t to be used as xdata or real timestamp
                # from plotting_widget.button_press_event() (super class)
                self.tps_t = (day_index * const.SECOND_IN_DAY +
                              five_minute_index * const.SEC_5M +
                              self.start_first_day)

            except IndexError:
                # exclude the extra points added to the 2 sides of x axis to
                # show the entire highlight box
                pass

    def on_ctrl_cmd_click(self, timestamp):
        """
        If Ctrl/Cmd + click on this widget, on_pick_event() will be called
        first and set self.tps_data.

        on_button_press_event() can use xdata, if called from waveform or soh
        widget, or tps_data, if called from tps widget, as real timestamp to
        pass to tps_dlg.set_indexes_and_display_info() for computing
        five_minute_idx/day_idx and displaying info of all tps channels' points
        The above tasks are placed in tps_dlg so that it won't be processed
        repeatedly.

        on_button_press_event() also loops through the tps_widgets to call this
        function to place the highlighting box (ruler) for each of them
        according to the indexes calculated in the tps_dlg.

        :param timestamp: real timestamp value to be consistent with
            on_ctrl_cmd_click in other plotting_widgets.
        """
        self.zoom_marker1_shown = False
        # locate the ruler on each channel in the tab to highlight the point
        for rl in self.rulers:
            rl.set_data([self.parent.five_minute_idx], [-self.parent.day_idx])

    def on_shift_click(self, xdata):
        """
        Shift + right click on other plot widget, this function will be called
            to show marker for place when it is zoomed.
            On the fist of zoom_marker, make ruler disappeared, set min_x.
            On the second of zoom_maker, call set_lim_markers to mark the new
                limit.

        :param xdata: float - time value in other plot
        """

        if not self.zoom_marker1_shown:
            self.set_rulers_invisible()
            self.min_x = xdata
            self.zoom_marker1_shown = True
        else:
            [self.min_x, self.max_x] = sorted(
                [self.min_x, xdata])
            self.set_lim_markers()
            self.zoom_marker1_shown = False

    def zoom_out(self):
        """
        Zoom out by setting limit to the previous range when there's at least
            one zoom-in.
        """
        if len(self.zoom_minmax_list) > 1:
            self.min_x, self.max_x = self.zoom_minmax_list[-2]
            self.zoom_minmax_list.pop()
            self.set_lim_markers(is_zoom_in=False)

    def set_rulers_invisible(self):
        """
        Clear data for self.rulers to make them disappeared.
        """
        for rl in self.rulers:
            rl.set_data([], [])

    def set_lim_markers(self, is_zoom_in=True):
        """
        + Append to zoom_minmax_list if called from a zoom_in. First time
        plotting is considered a zoom_in to create first x range in the list.

        + Find x index (which index in five minutes of a day) and
        y index (which day) of self.min_x and self.min_y, and set data for
        all markers in self.zoom_marker1s and self.zoom_marker2s.

        :param is_zoom_in: if set_lim comes from zoom_in task
        """
        if is_zoom_in:
            self.zoom_minmax_list.append((self.min_x, self.max_x))

        five_minute_idx, day_idx = find_tps_tm_idx(self.min_x,
                                                   self.start_5min_blocks,
                                                   self.start_first_day)
        for zm1 in self.zoom_marker1s:
            zm1.set_data([five_minute_idx], [-day_idx])
        five_minute_idx, day_idx = find_tps_tm_idx(self.max_x,
                                                   self.start_5min_blocks,
                                                   self.start_first_day)
        for zm2 in self.zoom_marker2s:
            zm2.set_data([five_minute_idx], [-day_idx])

    def request_stop(self):
        """Request all running channel processors to stop."""
        for processor in self.tps_processors:
            processor.request_stop()

    def replot(self):
        """
        Reuse tps_data calculated in the first plotting to replot
        with new color range selected.
        """
        # Not reset the size of main_widget so the size from original plotting
        # that fit all channels will be maintain.
        self.clear(do_reset_size=False)
        self.start_message_and_reset_processing_states(
            f'RePlotting {self.get_plot_name()} ...')
        self.set_colors(self.main_window.color_mode)
        title = get_title(
            self.data_set_id, self.min_x, self.max_x, self.date_mode)
        self.timestamp_bar_top = self.plotting_axes.add_timestamp_bar(
            show_bar=False)
        self.plotting_axes.set_title(title)
        self.create_plotting_channel_processors()
