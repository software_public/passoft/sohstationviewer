# Drawing State-Of-Health channels and mass position

from typing import Tuple, Union, Optional
from matplotlib.axes import Axes
from matplotlib.backend_bases import MouseButton
from PySide6 import QtWidgets, QtCore

from sohstationviewer.model.general_data.general_data import GeneralData

from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.plotting.plotting_widget.\
    multi_threaded_plotting_widget import MultiThreadedPlottingWidget
from sohstationviewer.view.db_config.add_edit_single_channel_dialog import \
    AddEditSingleChannelDialog
from sohstationviewer.view.db_config.data_type_selector_dialog \
    import DataTypeSelectorDialog


class SOHWidget(MultiThreadedPlottingWidget):
    """
       Widget to display soh and mass position data.
       """

    def __init__(self, *args, **kwargs):
        MultiThreadedPlottingWidget.__init__(self, *args, **kwargs)
        """
        curr_ax: current axes to be edited
        """
        self.curr_ax: Optional[Axes] = None

    def on_button_press_event(self, event):
        """
        When right-clicking on a plot with no Keyboard pressed,
        set self.curr_ax to the ax of that plot.
        """
        modifiers = event.guiEvent.modifiers()
        if modifiers != QtCore.Qt.KeyboardModifier.NoModifier:
            return super().on_button_press_event(event)

        x = event.xdata
        if x is None:
            # when clicking outside of the plots
            self.curr_ax = None
        else:
            if event.button == MouseButton.RIGHT:
                # RIGHT click
                self.curr_ax = event.inaxes
                self.parent.raise_()
            else:
                # LEFT click
                self.curr_ax = None
                if self.log_idxes is not None:
                    # For Reftek, need to hightlight the corresponding
                    # SOH message lines based on the log_idxes of the clicked
                    # point
                    self.parent.search_message_dialog.show()
                    try:
                        self.parent.search_message_dialog. \
                            show_log_entry_from_log_indexes(self.log_idxes)
                    except ValueError as e:
                        QtWidgets.QMessageBox.warning(self, "Not found",
                                                      str(e))
                else:
                    self.parent.raise_()

    def _create_data_type_action(self, context_menu: QtWidgets.QMenu):
        """
        Create context menu's action to set up data type for data set.

        :param context_menu: context menu when right-clicking on a channel
        """
        setup_data_type_action = context_menu.addAction(
            "Set up Data Type for Data Set")
        setup_data_type_action.triggered.connect(self.setup_data_type)

    def _create_channel_action(self, context_menu: QtWidgets.QMenu):
        """
        Create context menu's action to add a new channel or edit the current
        channel
        :param context_menu: context menu when right-clicking on a channel
        """
        warning_action_str = ''
        add_edit_action_str = ''
        if '?' in self.curr_ax.chan_db_info['dbChannel']:
            warning_action_str = (
                f"Channel '{self.curr_ax.chan_db_info['channel']} '"
                "can't be edited because it has '?' in its DB name, "
                f"{self.curr_ax.chan_db_info['dbChannel']}.")

        elif 'DEFAULT' in self.curr_ax.chan_db_info['label']:
            add_edit_action_str = f"Add NEW Channel {self.curr_ax.chan}"

        else:
            add_edit_action_str = f"Edit Channel {self.curr_ax.chan}"

        if add_edit_action_str != '':
            add_edit_chan_action = context_menu.addAction(add_edit_action_str)
            add_edit_chan_action.triggered.connect(self.add_edit_channel)

        if warning_action_str != '':
            context_menu.addAction(warning_action_str)

    def contextMenuEvent(self, event):
        """
        Create menu showing up when right click mouse to add/edit channel
        """
        if self.number_channel_found == 0:
            return
        context_menu = QtWidgets.QMenu(self)

        select_channels_to_show_action = context_menu.addAction(
            "Select Channels to show")
        select_channels_to_show_action.triggered.connect(
            self.select_channels_to_show)

        if not hasattr(self.curr_ax, 'chan_db_info'):
            # If curr_ax has no chan_db_info, it doesn't belong to a channel.
            context_menu.exec_(self.mapToGlobal(event.pos()))
            self.curr_ax = None  # to make sure curr_ax is clear
            return super(SOHWidget, self).contextMenuEvent(event)

        if self.parent.data_type == 'Unknown':
            self._create_data_type_action(context_menu)
        else:
            self._create_channel_action(context_menu)

        context_menu.exec_(self.mapToGlobal(event.pos()))
        self.curr_ax = None         # to make sure curr_ax is clear
        return super(SOHWidget, self).contextMenuEvent(event)

    def init_plot(self, d_obj: GeneralData,
                  data_set_id: Union[str, Tuple[str, str]],
                  start_tm: float, end_tm: float, time_ticks_total: int):
        """
        Initiate plotting with gaps, top time bar

        :param d_obj: object of data
        :param data_set_id: data set's id
        :param start_tm: requested start time to read
        :param end_tm: requested end time to read
        :param time_ticks_total: max number of tick to show on time bar
        """
        self.plotting_data1 = (
            d_obj.soh_data[data_set_id] if data_set_id else {})
        self.plotting_data2 = (
            d_obj.mass_pos_data[data_set_id] if data_set_id else {})
        self.rt130_log_data = None
        if self.data_object.data_type == 'RT130':
            try:
                self.rt130_log_data = (
                    d_obj.log_data[data_set_id]['SOH'][0].split('\n'))
            except KeyError:
                pass
        channel_list = (
            d_obj.soh_data[data_set_id].keys() if data_set_id else [])
        data_time = (
            d_obj.data_time[data_set_id] if data_set_id else [0, 1])
        ret = super().init_plot(d_obj, data_time, data_set_id,
                                start_tm, end_tm,
                                time_ticks_total, is_waveform=False)
        if not ret:
            return False

        not_found_chan = [c for c in channel_list
                          if c not in self.plotting_data1.keys()]
        if len(not_found_chan) > 0:
            msg = (f"The following channels is in Channel Preferences but "
                   f"not in the given data: {not_found_chan}")
            self.processing_log.append((msg, LogType.WARNING))
        return True

    def add_edit_channel(self):
        win = AddEditSingleChannelDialog(
            self.parent,
            self.plotting,
            self.curr_ax.chan,
            self.parent.data_type,
            self.curr_ax
        )
        win.exec()
        self.draw()

    def setup_data_type(self):
        win = DataTypeSelectorDialog(self.main_window)
        win.exec()
