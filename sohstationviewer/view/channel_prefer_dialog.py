from typing import Dict, List, Union, Optional, Tuple
from pathlib import Path

from PySide6 import QtWidgets, QtCore, QtGui
from PySide6.QtCore import Qt
from PySide6.QtGui import QColor
from PySide6.QtWidgets import (
    QDialogButtonBox, QDialog, QPlainTextEdit,
    QMainWindow, QTableWidgetItem, QMessageBox,
)

from sohstationviewer.database.process_db import (
    execute_db, trunc_add_db, execute_db_dict)
from sohstationviewer.database.extract_data import get_data_types

from sohstationviewer.controller.processing import read_mseed_channels
from sohstationviewer.controller.util import display_tracking_info
from sohstationviewer.controller.plotting_data import format_time

from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.util.one_instance_at_a_time import \
    OneWindowAtATimeDialog

INSTRUCTION = """
Select the list of SOH channels to be used in plotting.\n
Edit lists of channels to be read in the Preferred SOH List field.\n
Click "Save - Add to Main" to save the changes and add the selected list to \
main window.
"""
TOTAL_ROW = 20

COL = {'sel': 0, 'name': 1, 'dataType': 2,
       'preferredSOHs': 3, 'edit': 4, 'clr': 5}


class InputDialog(QDialog):
    def __init__(self, parent=None, text=''):
        super().__init__(parent)
        self.resize(500, 300)
        self.text_box = QPlainTextEdit(self)
        self.text_box.setPlainText(text)

        button_box = QDialogButtonBox(
            QDialogButtonBox.StandardButton.Ok |
            QDialogButtonBox.StandardButton.Cancel,
            self
        )

        layout = QtWidgets.QFormLayout(self)
        layout.addRow(self.text_box)
        layout.addWidget(button_box)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

    def get_input(self):
        return self.text_box.toPlainText()


class ChannelPreferDialog(OneWindowAtATimeDialog):
    def __init__(self, parent: QMainWindow, list_of_dir: List[Path]):
        """
        Dialog to create lists of preferred SOH channels that users want to
            select for plotting.
        User can either fill in manually, get all channels for selected data
            type from DB or scan the current selected folder for the list.

        :param parent: widget that calls this plotting
            widget
        :param list_of_dir: list of absolute paths of data sets
        """
        super(ChannelPreferDialog, self).__init__()
        self.parent = parent
        self.list_of_dir = list_of_dir
        self.setWindowTitle("SOH Channel Preferences")
        self.setGeometry(100, 100, 1100, 800)
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setContentsMargins(7, 7, 7, 7)
        self.setLayout(main_layout)

        main_layout.addWidget(QtWidgets.QLabel(INSTRUCTION))
        # ===================== soh_list_table_widget =========================
        """
        soh_list_table_widget: the table to display/edit different
            preferred channel id list
        """
        self.soh_list_table_widget: Union[QtWidgets.QTableWidget, None] = None
        """
        avail_data_types: list of available data types in DB
        """
        self.avail_data_types: List[str] = self.get_data_types(
            include_default=False)
        """
        curr_row: current row
        """
        self.curr_row: int = -1

        # ========================== buttons ============================
        """
        add_db_chan_btn: Button to add all channels for selected
            data type from DB to add to the current row
        """
        self.add_db_chan_btn: Union[QtWidgets.QPushButton, None] = None
        """
        scan_chan_btn: Button to scan through all channels from list_of_dir
            folder for the list of channels and data type to add
            to the current row
        """
        self.scan_chan_btn: Union[QtWidgets.QPushButton, None] = None
        """
        save_btn: Button to save the selected preferred channel list to DB
        """
        self.save_btn: Union[QtWidgets.QPushButton, None] = None
        """
        save_add_main_btn: Button to save the selected preferred channel list
            to DB, close the dialog and add to
            parent.curr_pref_soh_list_name_txtbox
        """
        self.save_add_main_btn: Union[QtWidgets.QPushButton, None] = None
        """
        close_btn: button to close the dialog and do nothing
        """
        self.close_btn: Union[QtWidgets.QPushButton, None] = None

        self.changed = False
        # ======================= pre-define ==========================
        """
        soh_list_name_item: text box widget for name of preferred channel
            list
        """
        self.soh_list_name_item: Union[QtWidgets.QLineEdit, None] = None
        """
        data_type_combobox: dropdown list to select datatype for
            preferred channel list
        """
        self.data_type_combobox: Union[QtWidgets.QComboBox, None] = None
        """
        soh_list_item: text box for preferred channel list
        """
        self.soh_list_item: Union[QTableWidgetItem, None] = None
        """
        clear_btn: QPushButton - button to clear the preferred channel
        """
        self.clear_btn: Union[QtWidgets.QPushButton, None] = None
        """
        data_type: str - data_type of the current preferred channel
        """
        self.data_type: Union[str, None] = None
        """
        row_sel_radio_btns: list of radio buttons to select rows
        """
        self.row_sel_radio_btns: List[QtWidgets.QRadioButton] = []
        """
        channel_info: dictionary that stores chan_ids scanned from files for
            different categories and stores data time, following each
            data_set_id and keeps radio buttons to switch between data_set_id.
        """
        self.channel_info = {}
        # ============================ GUI ===============================
        """
        chan_ids_row_radio_button_group: group for radio buttons of chan_ids
            rows
        """
        self.chan_ids_row_radio_button_group = QtWidgets.QButtonGroup(self)
        """
        data_set_id_radio_button_group: group for radio buttons to select
            data set id
        """
        self.data_set_id_radio_button_group = QtWidgets.QButtonGroup(self)
        """
        data_set_id_radio_button_dict: radio buttons to select data_set_id
        """
        self.data_set_id_radio_button_dict: Dict[
            str, QtWidgets.QRadioButton] = {}
        self.create_soh_list_table_widget()
        main_layout.addWidget(self.soh_list_table_widget, 1)
        button_layout = self.create_buttons_section()
        main_layout.addLayout(button_layout)
        """
        tracking_info_text_browser: to display tracking info
            when scanning for available channel list from given folder
        """
        self.tracking_info_text_browser: QtWidgets.QTextBrowser = \
            QtWidgets.QTextBrowser(self)
        self.tracking_info_text_browser.setFixedHeight(80)
        main_layout.addWidget(self.tracking_info_text_browser)
        """
        data_set_ids_layout: layout for radio buttons representing data set ids
        """
        self.data_set_id_layout = QtWidgets.QHBoxLayout()
        self.data_set_id_layout.addWidget(QtWidgets.QLabel("Data Set ID:"))
        main_layout.addLayout(self.data_set_id_layout)
        """
        data_time_label: label for displaying data time.
        """
        self.data_time_Label = QtWidgets.QLabel("Data Time:")
        main_layout.addWidget(self.data_time_Label)

    def create_buttons_section(self):
        """
        Create the row of buttons
        """
        h_layout = QtWidgets.QHBoxLayout()
        self.add_db_chan_btn = QtWidgets.QPushButton(
            self, text='Add Channels from DB')
        self.add_db_chan_btn.clicked.connect(self.add_db_channels)
        h_layout.addWidget(self.add_db_chan_btn)

        self.scan_chan_btn = QtWidgets.QPushButton(
            self, text='Scan Channels from MSeed Data')
        self.scan_chan_btn.clicked.connect(self.scan_channels)
        if self.list_of_dir == [] or self.parent.data_type == "RT130":
            self.scan_chan_btn.setEnabled(False)
        h_layout.addWidget(self.scan_chan_btn)

        self.save_btn = QtWidgets.QPushButton(self, text='Save')
        self.save_btn.clicked.connect(self.save)
        h_layout.addWidget(self.save_btn)

        self.save_add_main_btn = QtWidgets.QPushButton(
            self, text='Save - Add to Main')
        self.save_add_main_btn.clicked.connect(self.save_add_to_main_and_close)
        h_layout.addWidget(self.save_add_main_btn)

        self.close_btn = QtWidgets.QPushButton(self, text='Cancel')
        self.close_btn.clicked.connect(self.close)
        h_layout.addWidget(self.close_btn)
        return h_layout

    def create_soh_list_table_widget(self):
        """
        Create self.soh_list_table_widget to view and edit different lists of
            preferred channels
        """
        self.soh_list_table_widget = QtWidgets.QTableWidget(self)
        self.soh_list_table_widget.cellClicked.connect(self.cell_clicked)
        self.soh_list_table_widget.itemChanged.connect(self.input_changed)
        self.soh_list_table_widget.setColumnCount(6)

        col_headers = ['', 'Name', 'DataType', 'Preferred SOH List',
                       'Edit', 'Clear']
        self.soh_list_table_widget.setHorizontalHeaderLabels(col_headers)
        header = self.soh_list_table_widget.horizontalHeader()
        header.setSectionResizeMode(
            QtWidgets.QHeaderView.ResizeMode.ResizeToContents
        )
        header.setSectionResizeMode(
            3, QtWidgets.QHeaderView.ResizeMode.Stretch
        )

        self.soh_list_table_widget.setRowCount(TOTAL_ROW)
        self.avail_data_types = get_data_types()
        for row_idx in range(TOTAL_ROW):
            self.add_row(row_idx)
        self.update_data_table_widget_items()

    def cell_clicked(self, row, col):
        """
        Turn the radio button of row to checked and set the row selected
        :param row: row id
        :type row: int
        :param col: column id
        :type col: int
        """
        self.row_sel_radio_btns[row].setChecked(True)
        self.curr_sel_changed(row)

    @QtCore.Slot()
    def add_row(self, row_idx):
        """
        Add a row to self.soh_list_table_widget

        :param row_idx: row index
        """
        self.row_sel_radio_btns.append(QtWidgets.QRadioButton())
        self.chan_ids_row_radio_button_group.addButton(
            self.row_sel_radio_btns[row_idx])
        self.row_sel_radio_btns[row_idx].clicked.connect(
            lambda checked: self.curr_sel_changed(row_idx))
        self.soh_list_table_widget.setCellWidget(
            row_idx, COL['sel'], self.row_sel_radio_btns[row_idx])

        name_item = QtWidgets.QTableWidgetItem()
        self.soh_list_table_widget.setItem(row_idx, COL['name'], name_item)

        data_type_combo_box = QtWidgets.QComboBox(self)
        data_type_combo_box.textActivated.connect(
            lambda: self.cell_clicked(row_idx, 2))
        data_type_combo_box.currentIndexChanged.connect(self.input_changed)
        data_type_combo_box.addItems(['Unknown'] + self.avail_data_types)
        data_type_combo_box.setCurrentIndex(-1)
        self.soh_list_table_widget.setCellWidget(
            row_idx, COL['dataType'], data_type_combo_box)

        soh_list_item = QtWidgets.QTableWidgetItem()
        self.soh_list_table_widget.setItem(
            row_idx, COL['preferredSOHs'], soh_list_item)

        edit_button = QtWidgets.QPushButton(self, text='EDIT')
        edit_button.clicked.connect(lambda arg: self.edit_soh_list(row_idx))
        self.soh_list_table_widget.setCellWidget(
            row_idx, COL['edit'], edit_button)

        del_button = QtWidgets.QPushButton(self, text='CLR')
        del_button.clicked.connect(lambda arg: self.clear_soh_list(row_idx))
        self.soh_list_table_widget.setCellWidget(
            row_idx, COL['clr'], del_button)

    @QtCore.Slot()
    def input_changed(self):
        """
        Set self.changed to True if there is any info have been edit to
            display a warning message be for processing DB
        """
        self.changed = True

    def update_data_table_widget_items(self):
        """
        Update the content of self.soh_list_table_widget based on data read
            from DB
        """
        soh_list_rows = self.get_soh_list_rows()
        # first row
        self.soh_list_table_widget.cellWidget(0, COL['sel']).setChecked(True)
        self.curr_sel_changed(0)

        count = 0
        for r in soh_list_rows:
            self.soh_list_table_widget.cellWidget(
                count, COL['sel']).setChecked(
                True if r['current'] == 1 else False)
            self.soh_list_table_widget.item(
                count, COL['name']).setText(r['name'])
            self.soh_list_table_widget.cellWidget(
                count, COL['dataType']).setCurrentText(r['dataType'])
            self.soh_list_table_widget.item(
                count, COL['preferredSOHs']).setText(r['preferredSOHs'])

            if r['isDefault'] == 1:
                self.set_default_row(count)

            if r['current'] == 1:
                self.curr_sel_changed(count)
                self.soh_list_table_widget.selectRow(count)
            count += 1
        self.update()

    def set_default_row(self, row_idx):
        """
        Set row at row_idx to default in which,
          + Edit and clear button are disabled and greyed out.
          + All other widgets except for the select radio button are disabled
            but not greyed out.
        """

        row_name_table_item = (self.soh_list_table_widget.item(row_idx,
                                                               COL['name']))
        row_name_table_item.setFlags(
            Qt.ItemFlag.ItemIsSelectable | Qt.ItemFlag.ItemIsEnabled
        )
        # Style the row name table item to make it clear that the row is not
        # editable.
        row_name_table_item.setBackground(QColor(210, 240, 255))
        row_name_table_item.setForeground(QColor(100, 100, 100))

        data_type_widget = self.soh_list_table_widget.cellWidget(
            row_idx, COL['dataType'])
        data_type_widget.setEnabled(False)
        # Only want it to be unchangeable but still look active so
        # the text color is changed to black instead of being grey as default
        # for disable column box.
        palette = data_type_widget.palette()
        palette.setColor(QtGui.QPalette.ColorRole.Text, 'black')
        palette.setColor(QtGui.QPalette.ColorRole.ButtonText, 'black')
        data_type_widget.setPalette(palette)

        preferred_channels_table_item = self.soh_list_table_widget.item(
            row_idx, COL['preferredSOHs']
        )
        preferred_channels_table_item.setFlags(
            Qt.ItemFlag.ItemIsSelectable | Qt.ItemFlag.ItemIsEnabled
        )

        preferred_channels_table_item.setBackground(QColor(210, 240, 255))
        preferred_channels_table_item.setForeground(QColor(100, 100, 100))

        # Buttons Edit and Clear are disable showing that this row is a default
        # row and can't be editable.
        self.soh_list_table_widget.cellWidget(
            row_idx, COL['edit']).setEnabled(False)
        self.soh_list_table_widget.cellWidget(
            row_idx, COL['clr']).setEnabled(False)

    def get_row(self, row_idx):
        """
        Get content of a self.soh_list_table_widget's row

        :param row_idx: int - index of a row
        """
        soh_list_name_item = self.soh_list_table_widget.item(
            row_idx, COL['name'])
        data_type_combobox = self.soh_list_table_widget.cellWidget(
            row_idx, COL['dataType'])
        soh_list_item = self.soh_list_table_widget.item(
            row_idx, COL['preferredSOHs'])
        clear_widget = self.soh_list_table_widget.cellWidget(
            row_idx, COL['clr'])
        return (soh_list_name_item,
                data_type_combobox,
                soh_list_item,
                clear_widget)

    @QtCore.Slot()
    def curr_sel_changed(self, row_idx):
        """
        When check the radio button of a row, set self.curr_row with new
            row_idx and get content of the row.

        :param row_idx: int - index of a row
        """
        if self.row_sel_radio_btns[row_idx].isChecked():
            self.curr_row = row_idx
            (self.soh_list_name_item, self.data_type_combobox,
             self.soh_list_item, self.clear_btn) = self.get_row(row_idx)

    @QtCore.Slot()
    def edit_soh_list(self, row_idx):
        soh_list_item = self.soh_list_table_widget.item(row_idx,
                                                        COL['preferredSOHs'])
        edit_dialog = InputDialog(text=soh_list_item.text())
        if edit_dialog.exec():
            soh_list_item.setText(edit_dialog.get_input())

    @QtCore.Slot()
    def clear_soh_list(self, row_idx):
        """
        When 'Clear' button at the end of a row is click. Confirm with user and
            then clear the content of soh_list_item in that row.

        :param row_idx: int - index of a row
        """
        (soh_list_name_item, data_type_combobox,
         soh_list_item, clear_widget) = self.get_row(row_idx)
        if soh_list_item.text().strip() != "":
            msg = ("Are you sure you want to delete the SOH channel list "
                   f"{soh_list_name_item.text()} at soh_list_name_item row "
                   f"#{row_idx + 1}?")
            result = QtWidgets.QMessageBox.question(
                self, "Confirmation", msg,
                QtWidgets.QMessageBox.StandardButton.Yes |
                QtWidgets.QMessageBox.StandardButton.No)
            if result == QtWidgets.QMessageBox.StandardButton.No:
                return
            self.changed = True
        if soh_list_name_item.text() != '':
            self.changed = True
        soh_list_name_item.setText('')
        if data_type_combobox.currentText != '':
            self.changed = True
        data_type_combobox.setCurrentIndex(-1)
        if soh_list_item.text() != '':
            self.changed = True
        soh_list_item.setText('')

    def validate_row(self, check_data_type=False):
        """
        Make sure there is a row selected by checking the radio button at the
            beginning of a row.
        Check if data_type is selected if required.

        :param check_data_type: bool - flag to require checking data type
        """
        if self.curr_row == -1:
            msg = "Please select a row."
            QtWidgets.QMessageBox.information(self, "Select row", msg)
            return False

        if check_data_type:
            self.data_type = self.data_type_combobox.currentText()
            if self.data_type not in self.avail_data_types:
                msg = ("Please select a data type that isn't 'Unknown' for "
                       "the selected row.")
                QtWidgets.QMessageBox.information(
                    self, "Select data type", msg)
                return False

        return True

    @QtCore.Slot()
    def add_db_channels(self):
        """
        Add channels from DB to preferred channel list.
        A DB data_type is required in to retrieve its channels from DB
        """
        if not self.validate_row(check_data_type=True):
            return

        db_channels = self.get_db_channels(self.data_type)
        self.soh_list_item.setText(','.join(db_channels))

    @QtCore.Slot()
    def scan_channels(self):
        """
        Scan for all available channels in self.list_of_dir to add to
            preferred channel list.
        For RT130, all SOH channels are kept in a log file. It will be more
            reasonable to get channels from DB because the task of getting
            channels from log files is like reading data from it.
        """
        selected_row_idx = self.soh_list_table_widget.indexFromItem(
            self.soh_list_item
        ).row()
        row_edit_button = self.soh_list_table_widget.cellWidget(
            selected_row_idx, COL['edit']
        )
        is_default_row = not row_edit_button.isEnabled()

        if is_default_row:
            err_msg = ('The selected row is a default row and cannot be '
                       'overwritten. Please select a non-default row and try '
                       'again.')
            QMessageBox.warning(self, "Can't overwrite default row", err_msg)
            return

        if not self.validate_row():
            return
        data_type, is_multiplex = (self.parent.data_type,
                                   self.parent.is_multiplex)
        if data_type in self.avail_data_types:
            self.data_type_combobox.setCurrentText(data_type)
        else:
            self.data_type_combobox.setCurrentText('Unknown')

        if data_type == 'RT130':
            self.scan_chan_btn.setEnabled(False)
            msg = ("Data type of the current data set is RT130 for which "
                   "Scan Channel from MSeed Data isn't available.\n\n"
                   "Do you want to add channels from DB?")
            result = QtWidgets.QMessageBox.question(
                self, "Add Channels from DB for RT130", msg,
                QtWidgets.QMessageBox.StandardButton.Ok |
                QtWidgets.QMessageBox.StandardButton.Cancel
            )
            if result == QtWidgets.QMessageBox.StandardButton.Ok:
                self.add_db_channels()
        else:
            self.scan_chan_btn.setEnabled(True)
            # clear all data_set_id radio buttons
            for i in reversed(range(self.data_set_id_layout.count())):
                widget = self.data_set_id_layout.takeAt(i).widget()
                if widget is not None:
                    widget.setParent(None)
            self.channel_info = read_mseed_channels(
                self.tracking_info_text_browser,
                self.list_of_dir, is_multiplex)
            if len(self.channel_info) == 0:
                msg = "No data can be read from " + ', '.join(self.list_of_dir)
                return QtWidgets.QMessageBox.warning(self, "No data", msg)
            self.scan_chan_btn.setEnabled(True)
            self.data_set_id_layout.addWidget(QtWidgets.QLabel("Data Set ID:"))
            sta_ids = sorted(list(self.channel_info.keys()))
            self.data_set_id_radio_button_dict = {}
            for sta_id in sta_ids:
                # set new data_set_id radio buttons
                self.data_set_id_radio_button_dict[sta_id] = \
                    QtWidgets.QRadioButton(sta_id)
                self.data_set_id_radio_button_group.addButton(
                    self.data_set_id_radio_button_dict[sta_id])
                self.data_set_id_radio_button_dict[sta_id].toggled.connect(
                    lambda: self.change_station(is_multiplex))
                self.data_set_id_layout.addWidget(
                    self.data_set_id_radio_button_dict[sta_id])
            self.data_set_id_layout.addStretch(1)
            self.data_set_id_radio_button_dict[sta_ids[0]].setChecked(
                True)

    @QtCore.Slot()
    def change_station(self, is_multiplex):
        sel_sta_id = [
            sta_id for sta_id in self.channel_info
            if self.data_set_id_radio_button_dict[sta_id].isChecked()][0]

        self.soh_list_item.setText(
            ','.join(self.channel_info[sel_sta_id]['soh']))
        msg = ""
        if self.channel_info[sel_sta_id]['mass_pos']:
            msg += f"Mass position channels (MP): " \
                   f"{','.join(self.channel_info[sel_sta_id]['mass_pos'])}\n"
        if self.channel_info[sel_sta_id]['waveform']:
            msg += f"Waveform channels (WF): " \
                   f"{','.join(self.channel_info[sel_sta_id]['waveform'])}\n"
        if self.channel_info[sel_sta_id]['soh_spr_gr_1']:
            msg += (
                f"Non-WF with spr>1: "
                f"{','.join(self.channel_info[sel_sta_id]['soh_spr_gr_1'])}\n"
                f"Non-WF with spr>1 can be added to SOH channel list "
                f"manually. They are not added automatically because they are "
                f"generally not SOH channels.")
        if msg != "":
            msg = f"<pre>{msg}</pre>"
            display_tracking_info(self.tracking_info_text_browser, msg)

        start_time = format_time(self.channel_info[sel_sta_id]['start_time'],
                                 'YYYY-MM-DD',
                                 'HH:MM:SS')
        if not is_multiplex:
            time_msg = f"Start Time:  {start_time}"
        else:
            end_time = format_time(self.channel_info[sel_sta_id]['end_time'],
                                   'YYYY-MM-DD',
                                   'HH:MM:SS')
            time_msg = f"Data Time:  {start_time}  -  {end_time}"
        self.data_time_Label.setText(time_msg)

    @QtCore.Slot()
    def save(self):
        """
        Save changes to DB by doing the following steps:
            + If there are any changes to inform user before save.
            + Create insert sql for each row
            + If there is nothing in preference list reset parent's setting for
                preferred channels and finish.
            + Do truncate and execute insert sqls above
            + Set parent's setting for preferred channels with info from the
                current selected row.
        """
        if not self.validate_row():
            return
        # call setFocus() to finish any QEditLine's editing or the editing not
        # take affect
        self.save_btn.setFocus()
        sql_list = self.get_sql_list()

        if sql_list is None:
            return False
        if len(sql_list) == 0:
            self.parent.pref_soh_list = []
            self.parent.pref_soh_list_name = ''
            self.parent.pref_soh_list_data_type = 'Unknown'

        if self.changed:
            msg = ("All Preferred SOH IDs in the database will be "
                   "overwritten with current Preferred SOH IDs in the dialog."
                   "\nClick Cancel to stop updating "
                   "database.")
            result = QtWidgets.QMessageBox.question(
                self, "Confirmation", msg,
                QtWidgets.QMessageBox.StandardButton.Ok |
                QtWidgets.QMessageBox.StandardButton.Cancel)
            if result == QtWidgets.QMessageBox.StandardButton.Cancel:
                return False
        ret = trunc_add_db('ChannelPrefer', sql_list)
        if ret is not True:
            display_tracking_info(self.tracking_info_text_browser,
                                  ret, LogType.ERROR)
        self.parent.pref_soh_list = [
            t.strip() for t in self.soh_list_item.text().split(',')]
        self.parent.pref_soh_list_name = self.soh_list_name_item.text().strip()
        self.parent.pref_soh_list_data_type = \
            self.data_type_combobox.currentText()
        self.changed = False
        return True

    def get_sql_list(self) -> Optional[List[str]]:
        """
        Create list of sql to insert data to ChannelPrefer table in which:
          + If there is error it will mark it to return but continue to report
           all errors of all rows.
          + Beside checking each row, primary key will be checked to make sure
           the list_of_sql returned can be executed.

        :return:
          None if there are any error
          Otherwise, list of sql
        """
        sql_list = []
        display_id_by_primary_key_dict = {}
        has_error = False
        for row_idx in range(TOTAL_ROW):
            result = self.create_save_row_sql(row_idx)
            if result is None:
                has_error = True
                continue
            else:
                primary_key, display_id, sql = result
                if sql == '':
                    continue
                sql_list.append(sql)
                if primary_key not in display_id_by_primary_key_dict:
                    display_id_by_primary_key_dict[primary_key] = []
                display_id_by_primary_key_dict[primary_key].append(display_id)
        if self.check_primary_key_duplicated(display_id_by_primary_key_dict):
            return
        if has_error:
            return
        return sql_list

    def check_primary_key_duplicated(self, display_id_by_primary_key_dict) \
            -> bool:
        """
        Check if there are any primary keys duplicated.
        :param display_id_by_primary_key_dict: dictionary with primary key as
            key and list of display row id for that primary key as value.
        :return: True if duplicated, False otherwise
        """
        duplicated = False
        duplicated_primary_keys = [
            pk for pk in display_id_by_primary_key_dict
            if len(display_id_by_primary_key_dict[pk]) > 1]
        if duplicated_primary_keys:
            duplicated = True
            for pk in duplicated_primary_keys:
                msg = (f"Name '{pk}' is duplicated in rows "
                       f"{', '.join(display_id_by_primary_key_dict[pk])}.\n"
                       f"Please change Name '{pk}' to make Name unique.")
                QtWidgets.QMessageBox.information(self, "Missing info", msg)
        return duplicated

    def create_save_row_sql(self, row_idx: int) \
            -> Optional[Tuple[str, str, str]]:
        """
        Read info from the row with index row_idx to create insert sql.

        :param row_idx: int - index of a row
        :return:
            None: has error
            Tuple of:
              primary key of the row
              display_id of the row
              sql to insert row data to ChannelPrefer table
        """
        current = 1 if self.soh_list_table_widget.cellWidget(
            row_idx, COL['sel']).isChecked() else 0
        name = self.soh_list_table_widget.item(
            row_idx, COL['name']).text()
        data_type = self.soh_list_table_widget.cellWidget(
            row_idx, COL['dataType']).currentText()
        preferred_sohs = self.soh_list_table_widget.item(
            row_idx, COL['preferredSOHs']).text()
        row_edit_button = self.soh_list_table_widget.cellWidget(
            row_idx, COL['edit']
        )
        # The edit button is only disabled for default rows, so we can use its
        # state to determine if a row is a default row.
        is_default_row = not row_edit_button.isEnabled()
        if preferred_sohs.strip() == '' and name.strip() == '':
            return '', '', ''
        display_id = row_idx + 1

        if preferred_sohs.strip() == '' and name.strip() != '':
            msg = f"Please add Preferred SOH IDs for row {display_id}."
            QtWidgets.QMessageBox.information(self, "Missing info", msg)
            return
        if name.strip() == '' and preferred_sohs.strip() != '':
            msg = f"Please add Name for row {display_id}."
            QtWidgets.QMessageBox.information(self, "Missing info", msg)
            return
        if preferred_sohs.strip() != '' and data_type == '':
            msg = f"Please add a data type for row {display_id}."
            QtWidgets.QMessageBox.information(self, "Missing info", msg)
            return
        sql = (f"INSERT INTO ChannelPrefer (name, preferredSOHs, dataType, "
               f"current, isDefault) VALUES "
               f"('{name}', '{preferred_sohs}', '{data_type}', {current}, "
               f"{is_default_row})")
        print(sql)
        primary_key = name
        return primary_key, str(display_id), sql

    @QtCore.Slot()
    def save_add_to_main_and_close(self):
        """
        Save changes to DB and reset main window's preferred channels with info
            of the selected rows.
        """
        if not self.save():
            return
        if self.parent.pref_soh_list_name == '':
            msg = "Please select a row to add to Main Window."
            QtWidgets.QMessageBox.information(self, "Missing info", msg)
            return
        self.parent.curr_pref_soh_list_name_txtbox.setText(
            self.parent.pref_soh_list_name)
        self.parent.all_soh_chans_check_box.setChecked(False)
        self.close()

    @staticmethod
    def get_data_types(include_default: bool = True):
        """
        Get list of data types from DB.

        :param include_default: flag indicate if Default data type should be
            included or not
        :return: [str, ] - list of data types
        """
        data_type_rows = execute_db(
            'SELECT * FROM DataTypes ORDER BY dataType ASC')
        if include_default:
            return [d[0] for d in data_type_rows]
        else:
            return [d[0] for d in data_type_rows
                    if d[0] != 'Default']

    @staticmethod
    def get_db_channels(data_type) -> List[str]:
        """
        Get all channels defined in DB table Channels for the given data type.

        :param data_type: str - the given data type
        """
        channel_rows = execute_db(
            f"SELECT channel FROM CHANNELS"
            f" WHERE dataType='{data_type}'"
            f"  AND param NOT IN ('Seismic data', 'Mass position')"
            f" ORDER BY dataType ASC")
        # VST is divided into 4 channel in DB for plotting,
        # But to read from file, we need original name VST
        if ('VST0',) in channel_rows:
            for i in range(4):
                channel_rows.remove((f'VST{i}',))
            channel_rows.append(('VST',))
        return sorted([c[0] for c in channel_rows])

    @staticmethod
    def get_soh_list_rows() -> List[Dict]:
        """
        Get all data from DB table ChannelPrefer to fill up
            self.soh_list_table_widget

        :return id_rows: [dict,] - list of data for each row
        """
        id_rows = execute_db_dict(
            "SELECT * FROM ChannelPrefer "
            " ORDER BY name ASC")
        return id_rows
