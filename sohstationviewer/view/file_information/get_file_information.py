from typing import Union, Dict, List, Set, Tuple

from sohstationviewer.controller.plotting_data import format_time
from sohstationviewer.model.general_data.general_data import GeneralData
from sohstationviewer.model.mseed_data.mseed import MSeed
from sohstationviewer.model.reftek_data.reftek import RT130
from sohstationviewer.view.util.functions import extract_netcodes


def extract_data_set_info(data_obj: Union[GeneralData, RT130, MSeed],
                          date_format: str
                          ) -> Dict[str, Union[str, List[str]]]:
    """
    Extract information about a data set. The extracted information includes:
        - Sources read
        - Start / end times
        - Station ID
        - DAS Serial Number
        - Experiment number
        - Network Code
        - Tag number
        - GPS information
        - Software version

    :param data_obj: the data object to extract file information from
    :param date_format: the format by which to format file's start/end times
    :return: a dictionary containing the information of a file
    """

    data_set_info = {}
    folders = (data_obj.list_of_rt130_paths
               if data_obj.list_of_dir == [''] else data_obj.list_of_dir)

    sources_read = ', '.join([dir.name for dir in folders])
    data_set_info['Sources Read'] = sources_read

    data_type = data_obj.data_type
    data_set_info['Data Type'] = data_type

    time_format = 'HH:MM:SS'
    time_range_info_list = []
    for data_set_id in data_obj.data_time:
        time_range = data_obj.data_time[data_set_id]
        start_time_str = format_time(time_range[0], date_format, time_format)
        end_time_str = format_time(time_range[1], date_format, time_format)
        time_range_info_list.append(
            f"{data_set_id}: \n\t\tFrom: {start_time_str}"
            f"\n\t\tTo: {end_time_str}")
    data_set_info['Time ranges'] = '\n\t'.join(time_range_info_list)

    data_set_ids = data_obj.data_set_ids
    if data_type == 'RT130':
        das_serials = list({set_id[0] for set_id in data_set_ids})
        experiment_numbers = list({set_id[1] for set_id in data_set_ids})
        # The insertion order into data_set_info in this piece of code is
        # important, so we had to write it in a way that is a bit repetitive.
        data_set_info['DAS Serial Numbers'] = ',  '.join(das_serials)
        if len(das_serials) > 1:
            data_set_info['Selected DAS'] = data_obj.selected_data_set_id[0]
        data_set_info['Experiment Numbers'] = ',  '.join(experiment_numbers)
        if len(experiment_numbers) > 1:
            data_set_info['Selected Experiment'] = \
                data_obj.selected_data_set_id[1]
    else:
        stations = list(data_set_ids)
        data_set_info['Station IDs'] = ',  '.join(stations)
        if len(stations) > 1:
            data_set_info['Selected Station'] = data_obj.selected_data_set_id
        data_set_info['Network Codes'] = extract_netcodes(data_obj)

    if data_type == 'Q330':
        tag_numbers, software_versions = extract_log_info_q330(data_obj)
        data_set_info['Tag Numbers'] = sorted(list(tag_numbers))
        data_set_info['Software Versions'] = sorted(list(software_versions))
    elif data_type == 'RT130':
        cpu_versions, gps_versions = extract_log_info_rt130(data_obj)
        data_set_info['CPU Software Versions'] = sorted(list(cpu_versions))
        data_set_info['GPS Firmware Versions'] = sorted(list(gps_versions))
    elif data_type == 'Pegasus':
        firmware_versions = extract_log_info_pegasus(data_obj)
        data_set_info['Firmware Versions'] = list(firmware_versions)

    return data_set_info


def extract_log_info_q330(data_obj: MSeed) -> Tuple[Set[str], Set[str]]:
    """
    Extract tag numbers and software versions from LOG channel of Q330 data.

    :param data_obj: the object that contains the Q330 data
    :return: the set of found tag numbers and the set of found software
        versions
    """
    tag_numbers = set()
    software_versions = set()
    log_str = '\n'.join(data_obj.log_data[
                            data_obj.selected_data_set_id]['LOG'])
    log_lines = [line for line in log_str.splitlines() if line != '']
    for line in log_lines:
        if 'Tag Number' in line:
            line = line.split(': ')
            tag_numbers.add(line[1])
        if 'System Software Version' in line:
            line = line.split(': ')
            software_versions.add(line[1])
    return tag_numbers, software_versions


def extract_log_info_rt130(data_obj: RT130) -> Tuple[Set[str], Set[str]]:
    """
    Extract the CPU and GPS versions from the SOH data of RT130 data.

    :param data_obj: the object that contains the RT130 data
    :return: the set of found CPU versions and the set of found GPS versions
    """
    cpu_versions = set()
    gps_versions = set()
    log_str = '\n'.join(data_obj.log_data[
                            data_obj.selected_data_set_id]['SOH'])
    log_lines = [line for line in log_str.splitlines() if line != '']
    for line in log_lines:
        if 'CPU SOFTWARE' in line:
            cpu_versions.add(' '.join(line.split()[3:]))
        if 'GPS FIRMWARE VERSION' in line:
            gps_version = ' '.join(line.split()[4:])
            # A data set (2017149.92EB) stores its GPS firmware version
            # followed by a bunch of commas. We remove those commas so that the
            # GPS firmware version of this data set looks similar to that of
            # other data sets.
            gps_version = gps_version.strip(',')
            gps_versions.add(gps_version)
    return cpu_versions, gps_versions


def extract_log_info_pegasus(data_obj: MSeed) -> Set[str]:
    """
    Extract the firmware versions from the log data of Pegasus data.

    :param data_obj: the object that contains the Pegasus data
    :return: the set of found firmware versions
    """
    firmware_versions = set()
    log_str = '\n'.join(data_obj.log_data['TEXT'])
    log_lines = [line for line in log_str.splitlines() if line != '']
    for line in log_lines:
        if 'Firmware version' in line:
            firmware_versions.add(line.split()[6])
    return firmware_versions
