from PySide6 import QtCore
from PySide6.QtGui import (
    QContextMenuEvent, QGuiApplication, QKeySequence,
    QKeyEvent, QAction,
)
from PySide6.QtWidgets import QListWidget, QMenu


class FileInfoWidget(QListWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.setContextMenuPolicy(
            QtCore.Qt.ContextMenuPolicy.DefaultContextMenu
        )

    def contextMenuEvent(self, event: QContextMenuEvent):
        if not self.count():
            return
        menu = QMenu()

        copy_action = QAction('Copy', menu)
        copy_action.setCheckable(False)
        copy_action.triggered.connect(self.copy)

        select_all_action = QAction('Select all', menu)
        select_all_action.setCheckable(False)
        select_all_action.triggered.connect(self.selectAll)

        menu.addAction(copy_action)
        menu.addAction(select_all_action)
        menu.exec(event.globalPos())

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.matches(QKeySequence.Copy):
            self.copy()
            event.accept()
        else:
            super().keyPressEvent(event)

    @QtCore.Slot()
    def copy(self):
        copied_text = '\n'.join(item.text() for item in self.selectedItems())
        clipboard = QGuiApplication.clipboard()
        clipboard.setText(copied_text)
