# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './calendar.ui',
# licensing of './calendar.ui' applies.
#
# Created: Wed Jun  2 15:31:48 2021
#      by: pyside2-uic  running on PySide2 5.13.2
#
# WARNING! All changes made in this file will be lost!

from PySide6 import QtCore, QtWidgets

from sohstationviewer.view.calendar.calendar_widget import CalendarWidget


class Ui_CalendarDialog(object):
    def setupUi(self, CalendarDialog):
        CalendarDialog.setObjectName("CalendarDialog")
        CalendarDialog.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(CalendarDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.calendarWidget = CalendarWidget(CalendarDialog)
        self.calendarWidget.setObjectName("calendarWidget")
        self.verticalLayout.addWidget(self.calendarWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.buttonBox = QtWidgets.QDialogButtonBox(CalendarDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Orientation.Horizontal)
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.StandardButton.Ok
        )
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout.addWidget(self.buttonBox)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(CalendarDialog)
        QtCore.QObject.connect(
            self.buttonBox, QtCore.SIGNAL("accepted()"), CalendarDialog.accept)
        QtCore.QObject.connect(
            self.buttonBox, QtCore.SIGNAL("rejected()"), CalendarDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(CalendarDialog)

    def retranslateUi(self, CalendarDialog):
        CalendarDialog.setWindowTitle(QtWidgets.QApplication.translate(
            "CalendarDialog", "Calendar", None, -1))
