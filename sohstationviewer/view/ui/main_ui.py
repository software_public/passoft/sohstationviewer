# UI and connectSignals for main_window
import configparser
from typing import Union, List, Optional, Dict

from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import (
    QMainWindow, QWidget, QTextBrowser, QPushButton, QLineEdit, QDateEdit,
    QListWidget, QCheckBox, QRadioButton, QMenu, QLabel, QFrame,
    QVBoxLayout, QHBoxLayout, QGridLayout, QAbstractItemView, QButtonGroup,
    QSplitter,
)
from PySide6.QtGui import (
    QAction, QActionGroup, QShortcut
)
from sohstationviewer.view.calendar.calendar_widget import CalendarWidget
from sohstationviewer.view.date_edit_with_doy import \
    DayOfYearSupportedDateTextBox
from sohstationviewer.view.file_information.file_info_widget import \
    FileInfoWidget

from sohstationviewer.view.plotting.state_of_health_widget import SOHWidget

from sohstationviewer.conf import constants


def add_separation_line(layout):
    """
    Add a line for separation to the given layout.
    :param layout: QLayout - the layout that contains the line
    """
    label = QLabel()
    label.setFrameStyle(QFrame.Shape.HLine | QFrame.Shadow.Sunken)
    label.setLineWidth(1)
    layout.addWidget(label)


class UIMainWindow(object):
    def __init__(self):
        """
        Class that create widgets, menus and connect signals for main_window.
        """
        super().__init__()
        """
        main_window: Main window that contains main widget and menus
        """
        self.main_window: Union[QMainWindow, None] = None
        """
        central_widget: Main widget that contains all widgets
        """
        self.central_widget: Union[QWidget, None] = None
        """
        tracking_info_text_browser: textbox that can set color and background
            color for displaying tracking info of processing data
        """
        self.tracking_info_text_browser: Union[QTextBrowser, None] = None
        # =================== top row ========================
        """
        curr_dir_button: Button that helps browse to the directory for
            selecting data set
        """
        self.curr_dir_button: Union[QPushButton, None] = None
        """
        curr_dir_line_edit: textbox that display the current directory
        """
        self.curr_dir_line_edit: Union[QLineEdit, None] = None
        """
        time_from_date_edit: to help user select start day to read
            from the data set
        """
        self.time_from_date_edit: Union[QDateEdit, None] = None
        """
        time_to_date_edit: to help user select end day to read
            from the data set
        """
        self.time_to_date_edit: Union[QDateEdit, None] = None
        # ======================= second row ======================
        """
        plotting_widget: Widget for plotting data
        """
        self.plotting_widget: Union[SOHWidget, None] = None
        """
        search_line_edit: textbox for user to search in
            self.open_file_list
        """
        self.search_line_edit: Union[QLineEdit, None] = None
        """
        log_checkbox: checkbox for user to indicate that they are reading a log
        file
        """
        self.log_checkbox: Union[QCheckBox, None] = None
        """
        clear_button: clear search_line_edit
        """
        self.clear_search_action: Optional[QAction] = None
        """
        open_files_list: Widget that display the list of directories in
            current directory for user to select
        """
        self.open_files_list: Union[QListWidget, None] = None
        """
        from_data_card_check_box: to tell SOHView if current directory
            is inside the data set or the parent of the data set
        """
        self.from_data_card_check_box: Union[QCheckBox, None] = None
        """
        replot_button: reset plotting without re-loading data
        """
        self.replot_button: Union[QPushButton, None] = None
        """
        background_black_radio_button: If selected, plotting_widget will turn
            to black mode
        """
        self.background_black_radio_button: Union[QRadioButton, None] = None
        """
        background_white_radio_button: If selected, plotting_widget will turn
            to white mode
        """
        self.background_white_radio_button: Union[QRadioButton, None] = None
        """
        gap_len_line_edit: For user to enter len of gap to detect
            with unit minute (m)
        """
        self.gap_len_line_edit: Union[QLineEdit, None] = None
        """
        mass_pos_123zne_check_box: QCheckBox: If checked, mass position 1,2,3
            will be read
        """
        self.mass_pos_123zne_check_box: Union[QCheckBox, None] = None
        """
        mass_pos_456uvw_check_box: If checked, mass position 1,2,3
            will be read.
        """
        self.mass_pos_456uvw_check_box: Union[QCheckBox, None] = None

        """
        all_wf_chans_check_box: For user to select to read all
            waveform channels for mseed data
        """
        self.all_wf_chans_check_box: Union[QCheckBox, None] = None
        """
        ds_check_boxes: For user to select which data stream
            to be read in RT130
        """
        self.ds_check_boxes: Union[List[QCheckBox], None] = None
        """
        mseed_wildcard_edit: For user to enter wildcard of mseed
            channels to be read
        """
        self.mseed_wildcard_edit: Union[QLineEdit, None] = None
        """
        tps_check_box: If checked, time-power-squared will be
            displayed in time_power_squared dialog
        """
        self.tps_check_box: Union[QCheckBox, None] = None
        """
        raw_check_box: If checked, waveform will be displayed in
            waveform dialog
        """
        self.raw_check_box: Union[QCheckBox, None] = None

        """
        all_soh_chans_check_box: If checked, all soh channels will
            be read and displayed
        """
        self.all_soh_chans_check_box: Union[QCheckBox, None] = None
        """
        select_pref_soh_list_button: Button to open self.channel_prefer_dialog
            to view/add/edit/select a preferred SOH IDs
        """
        self.select_pref_soh_list_button: Union[QPushButton, None] = None
        """
        curr_pref_soh_list_name_txtbox: textbox to display name of current
            preferred SOH IDs.
            When all_soh_chans_check_box is checked, no name show up, when
            all_soh_chans_check_box is unchecked, current selected name show up
        """
        self.curr_pref_soh_list_name_txtbox: Union[QLineEdit, None] = None
        """
        read_button: Button to read data from selected directory
            from open_files_list and plot it
        """
        self.read_button: Union[QPushButton, None] = None
        """
        stop_button: Button to stop processing the data when it
            takes too long, for example.
        """
        self.stop_button: Union[QPushButton, None] = None
        """
        save_plot_button: QPushButton - Button to save the plots in
            self.plotting_widget to file
        """
        self.save_plot_button: Union[QPushButton, None] = None
        """
        info_list_widget: to list info of the read data after
            processing
        """
        self.info_list_widget: Union[QListWidget, None] = None
        # ========================= Menus =============================
        """
        forms_menu: to show all of the current available dialogs so
            that user can select a dialog to put it on top of others to even
            if the dialog has been closed by user.
        """
        self.forms_menu: Union[QMenu, None] = None
        """
       about_action: to open about box
        """
        self.about_action: Optional[QAction] = None
        """
        exit_action: to exit SOHView
        """
        self.exit_action: Union[QAction, None] = None
        # ========================== Command Menu ======================
        """
        gps_plotter_action: to open gps_dialog to plot all the
            latitude and longitude values  of GPS position information
        """
        self.gps_plotter_action: Union[QAction, None] = None

        """
        restore_default_database: menu item to reset the database back to its
            original condition
        """
        self.restore_default_database_action: Union[QAction, None] = None

        # ========================== Option Menu =======================
        """
        warn_big_file_sizes: option to check file sizes and give warning if
        total file sizes are greater than constant.BIG_FILE_SIZE
        """
        self.warn_big_file_sizes: Optional[QAction] = None
        """
        mp_regular_color_action: set self.mass_pos_volt_range_opt to 'regular'
        mp_trillium_color_action: set self.mass_pos_volt_range_opt to
            'trillium'
        """
        self.mp_regular_color_action: Union[QAction, None] = None
        self.mp_trillium_color_action: Union[QAction, None] = None
        """
        add_mass_position_to_log: option to add mass-position data to SOH
        messages of an RT130 data set
        """
        self.add_masspos_to_rt130_soh: Union[QAction, None] = None
        """
        add_positions_to_et_action: allow to add position to ET lines
        """
        self.add_positions_to_et_action: Union[QAction, None] = None
        """
        Set self.date_format
        yyyy_doy_action: set format to be "YYYY:DOY"
        yyyy_mm_dd_action: set format to be "YYYY-MM-DD"
        yyyymmmdd_action: set format to be "YYYYMMDD"
        """
        self.yyyy_doy_action: Union[QAction, None] = None
        self.yyyy_mm_dd_action: Union[QAction, None] = None
        self.yyyymmmdd_action: Union[QAction, None] = None
        """
        Dict of base_plot_font_size_action by size
        """
        self.base_plot_font_size_action_dict: Dict[int, QAction] = {}

        # ======================== Database Menu ==========================
        """
        add_edit_data_type_action: open DataTypes table to add/edit
        add_edit_param_action: open Parameters table to add/edit
        add_edit_channel_action: open Channels table to add/edit
        view_plot_type_action: view all plot types available and
            their description
        """
        self.add_edit_data_type_action: Union[QAction, None] = None
        self.add_edit_param_action: Union[QAction, None] = None
        self.add_edit_channel_action: Union[QAction, None] = None
        self.add_edit_channel_action: Union[QAction, None] = None
        self.view_plot_type_action: Union[QAction, None] = None

        # ========================= Forms Menu ============================
        """
        main_window_action: Show the main window over other windows. Primarily
        intended for MacOS, which has the menu bar always show up as long as
        the program has focus.
        """
        self.main_window_action: Union[QAction, None] = None

        # ========================= Help Menu =============================
        """
        calendar_action: Open Calendar Dialog as a helpful tool
        """
        self.calendar_action: Union[QAction, None] = None
        """
        about_action: Open About Dialog to give information about
            SOHView
        """
        self.about_action: Union[QAction, None] = None
        """
        doc_action: Open a display allowing user to browse to the
            help documents in the folder Documentation/
        """
        self.doc_action: Union[QAction, None] = None

        self.config: Union[configparser.ConfigParser, None] = None

    def setup_ui(self, main_window):
        """
        Setting up layout, widgets, menus for main_window
        :param main_window: QMainWindow - main GUI for user to interact with
        """
        self.main_window = main_window
        main_window.setWindowTitle(f"SOHViewer - Version "
                                   f"{constants.SOFTWARE_VERSION}")
        self.central_widget = QWidget(main_window)
        main_window.setCentralWidget(self.central_widget)

        self.tracking_info_text_browser = QTextBrowser(
            self.central_widget)

        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(5, 5, 5, 5)
        main_layout.setSpacing(0)
        self.central_widget.setLayout(main_layout)
        self.set_first_row(main_layout)
        self.set_second_row(main_layout)

        self.create_menu_bar(main_window)
        self.connect_signals(main_window)
        self.create_shortcuts(main_window)

    def set_first_row(self, main_layout):
        """
        Setting up first row with browsing directory controls and from/to time
            controls
        :param main_layout: QVBoxLayout - main layout to arrange other layouts
            from top to bottom
        """
        h_layout = QHBoxLayout()
        h_layout.setContentsMargins(2, 2, 2, 2)
        h_layout.setSpacing(8)
        main_layout.addLayout(h_layout)

        self.curr_dir_button = QPushButton(
            "Main Data Directory", self.central_widget)
        h_layout.addWidget(self.curr_dir_button)

        self.curr_dir_line_edit = QLineEdit(
            self.central_widget)
        h_layout.addWidget(self.curr_dir_line_edit, 1)

        h_layout.addSpacing(40)

        h_layout.addWidget(QLabel('From'))
        self.time_from_date_edit = DayOfYearSupportedDateTextBox(
            parent=self.central_widget
        )
        self.time_from_date_edit.setCalendarPopup(True)
        self.time_from_date_edit.setDisplayFormat("yyyy-MM-dd")
        h_layout.addWidget(self.time_from_date_edit)

        h_layout.addWidget(QLabel('To'))
        self.time_to_date_edit = DayOfYearSupportedDateTextBox(
            parent=self.central_widget
        )
        self.time_to_date_edit.setCalendarPopup(True)
        self.time_to_date_edit.setDisplayFormat("yyyy-MM-dd")
        h_layout.addWidget(self.time_to_date_edit)

    def set_second_row(self, main_layout):
        """
        Setting up second rows that contains control columns and
            plotting widget for state of health
        :param main_layout: QVBoxLayout - main layout to arrange other layouts
            from top to bottom
        """
        h_layout = QHBoxLayout()
        main_layout.addLayout(h_layout)
        h_layout.setContentsMargins(0, 0, 0, 0)

        left_widget = QWidget(self.central_widget)
        h_layout.addWidget(left_widget)
        left_widget.setFixedWidth(240)
        # left_widget.setMinimumHeight(650)
        left_layout = QVBoxLayout()
        left_layout.setContentsMargins(0, 0, 0, 0)
        left_layout.setSpacing(0)
        left_widget.setLayout(left_layout)

        self.set_control_column(left_layout)

        plot_splitter = QSplitter(QtCore.Qt.Orientation.Vertical)
        h_layout.addWidget(plot_splitter, 2)

        self.plotting_widget = SOHWidget(self.main_window,
                                         self.tracking_info_text_browser,
                                         'SOH',
                                         self.main_window)

        plot_splitter.addWidget(self.plotting_widget)

        self.tracking_info_text_browser.setMaximumHeight(100)
        self.tracking_info_text_browser.setMinimumHeight(30)

        plot_splitter.addWidget(self.tracking_info_text_browser)
        tracking_browser_idx = plot_splitter.indexOf(
            self.tracking_info_text_browser
        )
        plot_splitter.setCollapsible(tracking_browser_idx, False)
        plot_splitter.setSizes([1000, 1])

    def set_control_column(self, left_layout):
        """
        Setting up necessary widgets for user to setup options for plotting
        :param left_layout: QVBoxLayout - layout of the left column of controls
        """
        self.open_files_list = QListWidget(
            self.central_widget)

        file_layout = QHBoxLayout()
        file_layout.setSpacing(20)
        left_layout.addLayout(file_layout)
        self.search_line_edit = QLineEdit()
        self.search_line_edit.setPlaceholderText('Search...')
        self.search_line_edit.setToolTip('Filter the list of files based on '
                                         'the content.')
        self.search_line_edit.textChanged.connect(
            self.main_window.filter_folder_list)
        self.search_line_edit.setClearButtonEnabled(True)
        try:
            # This value was obtained from the C++ source of QT. We use it here
            # so that the QAction found is guaranteed to be the clear button.
            clear_action_name = '_q_qlineeditclearaction'
            found_actions = self.search_line_edit.findChildren(
                QAction, clear_action_name
            )
            self.clear_search_action: QAction = found_actions[0]
            self.clear_search_action.triggered.connect(
                self.main_window.clear_file_search
            )
        except IndexError:
            # If the name of the clear button is changed in the C++ source of
            # QT, nothing will be found. We raise an error to indicate this
            # problem.
            raise ValueError('No clear button could be found. Check its '
                             'objectName attribute using QObject.findChildren '
                             'without a name.')
        file_layout.addWidget(self.search_line_edit)

        self.log_checkbox = QCheckBox("log")
        file_layout.addWidget(self.log_checkbox)

        left_layout.addWidget(self.open_files_list, 1)
        pal = self.open_files_list.palette()
        pal.setColor(QtGui.QPalette.ColorRole.Highlight,
                     QtGui.QColor(128, 255, 128))
        pal.setColor(QtGui.QPalette.ColorRole.HighlightedText,
                     QtGui.QColor(0, 0, 0))
        self.open_files_list.setPalette(pal)
        # allow multiple-line selection
        self.open_files_list.setSelectionMode(
            QAbstractItemView.SelectionMode.ExtendedSelection)

        read_option_grid = QGridLayout()
        left_layout.addLayout(read_option_grid)
        self.from_data_card_check_box = QCheckBox(
            'From Memory Card', self.central_widget)
        read_option_grid.addWidget(self.from_data_card_check_box, 0, 0, 1, 2)

        self.replot_button = QPushButton('RePlot', self.central_widget)
        self.replot_button.setFixedWidth(95)
        self.replot_button.setEnabled(False)
        read_option_grid.addWidget(self.replot_button, 0, 3, 1, 1)

        data_group = QButtonGroup()
        self.data_radio_button = QRadioButton('data', self.central_widget)
        data_group.addButton(self.data_radio_button)
        self.data_radio_button.setEnabled(False)
        read_option_grid.addWidget(self.data_radio_button, 1, 0, 1, 1)
        self.sdata_radio_button = QRadioButton('sdata', self.central_widget)
        self.sdata_radio_button.setEnabled(False)
        data_group.addButton(self.sdata_radio_button)
        read_option_grid.addWidget(self.sdata_radio_button, 1, 1, 1, 1)

        color_tip_fmt = ('Set the background color of the plot '
                         ' to {0}')
        background_layout = QHBoxLayout()
        # background_layout.setContentsMargins(0, 0, 0, 0)
        left_layout.addLayout(background_layout)
        background_layout.addWidget(QLabel('Background     '))
        background_group = QButtonGroup()
        self.background_black_radio_button = QRadioButton(
            'B', self.central_widget)
        self.background_black_radio_button.setToolTip(
            color_tip_fmt.format('black'))
        background_layout.addWidget(self.background_black_radio_button)
        self.background_white_radio_button = QRadioButton(
            'W', self.central_widget)
        self.background_white_radio_button.setToolTip(
            color_tip_fmt.format('white'))
        background_layout.addWidget(self.background_white_radio_button)

        background_group.addButton(self.background_black_radio_button)
        background_group.addButton(self.background_white_radio_button)

        # For some reason, these button groups are deleted once they are out of
        # scope, which causes the radio buttons they manage to be in the same
        # group. This means that the sdata and data buttons are then also
        # exclusive with the black and white background buttons. To fix the
        # problem, we save the groups in the class so they don't get deleted.
        self.data_group = data_group
        self.background_group = background_group

        add_separation_line(left_layout)

        gap_layout = QHBoxLayout()
        left_layout.addLayout(gap_layout)

        gap_layout.addWidget(QLabel("Minimum Gap Length "))
        self.gap_len_line_edit = QLineEdit(self.central_widget)
        gap_validator = QtGui.QDoubleValidator()
        gap_validator.setDecimals(2)
        gap_validator.setNotation(
            QtGui.QDoubleValidator.Notation.StandardNotation
        )
        self.gap_len_line_edit.setValidator(gap_validator)
        gap_layout.addWidget(self.gap_len_line_edit)
        gap_layout.addWidget(QLabel(' m'))

        add_separation_line(left_layout)

        mass_pos_layout = QHBoxLayout()
        left_layout.addLayout(mass_pos_layout)
        mass_pos_layout.addWidget(QLabel('Mass Pos '))
        self.mass_pos_123zne_check_box = QCheckBox(
            '123/ZNE', self.central_widget)
        mass_pos_layout.addWidget(self.mass_pos_123zne_check_box)
        self.mass_pos_456uvw_check_box = QCheckBox(
            '456/UVW', self.central_widget)
        mass_pos_layout.addWidget(self.mass_pos_456uvw_check_box)

        add_separation_line(left_layout)

        self.all_wf_chans_check_box = QCheckBox(
            'All Waveform Channels', self.central_widget)
        left_layout.addWidget(self.all_wf_chans_check_box)

        left_layout.addSpacing(5)
        ds_grid = QGridLayout()
        left_layout.addLayout(ds_grid)
        ds_grid.addWidget(QLabel('DSs '), 0, 0, 2, 1,
                          QtGui.Qt.AlignmentFlag.AlignVCenter)
        self.ds_check_boxes = []
        count = 0
        for r in range(2):
            for c in range(4):
                count += 1
                self.ds_check_boxes.append(
                    QCheckBox('%s' % count, self.central_widget))
                ds_grid.addWidget(self.ds_check_boxes[count - 1], r, c + 1)

        left_layout.addSpacing(5)
        mseed_wildcard_layout = QHBoxLayout()
        left_layout.addLayout(mseed_wildcard_layout)
        mseed_wildcard_layout.addWidget(QLabel('MSeed Wildcard'))
        self.mseed_wildcard_edit = QLineEdit(self.central_widget)
        self.mseed_wildcard_edit.setToolTip('wildcards separated with commas')
        mseed_wildcard_layout.addWidget(self.mseed_wildcard_edit)

        left_layout.addSpacing(5)
        tps_raw_layout = QHBoxLayout()
        left_layout.addLayout(tps_raw_layout)
        self.tps_check_box = QCheckBox('TPS', self.central_widget)
        tps_raw_layout.addWidget(self.tps_check_box)
        self.raw_check_box = QCheckBox('RAW', self.central_widget)
        tps_raw_layout.addWidget(self.raw_check_box)

        add_separation_line(left_layout)

        chan_layout = QHBoxLayout()
        chan_layout.setContentsMargins(0, 0, 0, 0)
        chan_layout.setSpacing(0)
        left_layout.addLayout(chan_layout)

        self.all_soh_chans_check_box = QCheckBox(
            'All SOH  ', self.central_widget)
        self.all_soh_chans_check_box.setChecked(True)
        chan_layout.addWidget(self.all_soh_chans_check_box)

        self.select_pref_soh_list_button = QPushButton(
            'Pref', self.central_widget)
        self.select_pref_soh_list_button.setFixedWidth(47)
        chan_layout.addWidget(self.select_pref_soh_list_button)

        chan_layout.addWidget(QLabel('Cur'))
        self.curr_pref_soh_list_name_txtbox = QLineEdit(
            self.central_widget)
        self.curr_pref_soh_list_name_txtbox.setReadOnly(True)
        chan_layout.addWidget(self.curr_pref_soh_list_name_txtbox)

        submit_layout = QGridLayout()
        submit_layout.setSpacing(5)
        left_layout.addLayout(submit_layout)

        self.read_button = QPushButton('Read Files', self.central_widget)
        self.read_button.setToolTip('Read selected files')
        submit_layout.addWidget(self.read_button, 0, 0)

        self.plot_diff_data_set_id_button = QPushButton(
            'Plot Different Data Set ID', self.central_widget)
        self.plot_diff_data_set_id_button.setToolTip(
            'Plot different id of data set')
        self.plot_diff_data_set_id_button.setEnabled(False)
        submit_layout.addWidget(self.plot_diff_data_set_id_button, 0, 1)

        self.stop_button = QPushButton('Stop', self.central_widget)
        self.stop_button.setToolTip('Halt ongoing read')
        submit_layout.addWidget(self.stop_button, 1, 0)
        self.save_plot_button = QPushButton(
            'Save plot', self.central_widget)
        self.save_plot_button.setToolTip('Save plots to disk')
        submit_layout.addWidget(self.save_plot_button, 1, 1)

        self.info_list_widget = FileInfoWidget(self.central_widget)
        self.info_list_widget.setSelectionMode(
            QtWidgets.QAbstractItemView.SelectionMode.ExtendedSelection
        )
        left_layout.addWidget(self.info_list_widget, 1)

    def create_shortcuts(self, main_window):
        seq = QtGui.QKeySequence('Ctrl+F')
        chdir_shortcut = QShortcut(seq, main_window)
        chdir_shortcut.activated.connect(
            main_window.change_current_directory)

    def create_menu_bar(self, main_window):
        """
        Setting up menu bar
        :param main_window: QMainWindow - main GUI for user to interact with
        """
        main_menu = main_window.menuBar()
        file_menu = main_menu.addMenu("File")
        command_menu = main_menu.addMenu("Commands")
        option_menu = main_menu.addMenu("Options")
        database_menu = main_menu.addMenu("Database")
        self.forms_menu = main_menu.addMenu("Forms")

        help_menu = main_menu.addMenu("Help")

        # exitAction = QAction(QtGui.QIcon('exit.png'), "Exit", self)
        # exitAction.setShortcut("Ctrl+X")
        self.create_file_menu(main_window, file_menu)
        self.create_command_menu(main_window, command_menu)
        self.create_option_menu(main_window, option_menu)
        self.create_database_menu(main_window, database_menu)
        self.create_forms_menu(main_window, self.forms_menu)
        self.create_help_menu(main_window, help_menu)

    def create_file_menu(self, main_window, menu):
        """
        Create File Menu's Actions which involve in config or close SOHView
        :param main_window: QMainWindow - main GUI for user to interact with
        :param menu: QMenu - File Menu
        """
        self.about_action = QAction('About SOHViewer', main_window)
        self.about_action.setMenuRole(QAction.MenuRole.NoRole)
        menu.addAction(self.about_action)
        menu.addSeparator()
        self.exit_action = QAction('Close', main_window)
        menu.addAction(self.exit_action)

    def create_command_menu(self, main_window, menu):
        """
        Create Commands Menu's Actions to open some additional windows for user
            to interact with
        :param main_window: QMainWindow - main GUI for user to interact with
        :param menu: QMenu - Commands Menu
        """
        self.gps_plotter_action = QAction(
            'GPS Plotter', main_window)
        menu.addAction(self.gps_plotter_action)

        self.restore_default_database_action = QAction(
            'Reset Database to Default', main_window
        )
        menu.addAction(self.restore_default_database_action)

    def create_option_menu(self, main_window, menu):
        """
        Create Options Menu's Actions which help setting options for plotting.
        :param main_window: QMainWindow - main GUI for user to interact with
        :param menu: QMenu - Options Menu
        """
        self.warn_big_file_sizes = QAction(
            'Warn big file sizes', main_window
        )
        self.warn_big_file_sizes.setCheckable(True)
        menu.addAction(self.warn_big_file_sizes)
        menu.addSeparator()

        mp_coloring_menu = QMenu('Mass Position Coloring:', main_window)
        menu.addMenu(mp_coloring_menu)
        mp_coloring_group = QActionGroup(main_window)
        self.mp_regular_color_action = QAction(
            '0.5, 2.0, 4.0, 7.0 (Regular)', main_window)
        self.mp_regular_color_action.setCheckable(True)
        mp_coloring_menu.addAction(self.mp_regular_color_action)
        mp_coloring_group.addAction(self.mp_regular_color_action)
        self.mp_trillium_color_action = QAction(
            '0.5, 1.8, 2.4, 3.5 (Trillium)', main_window)
        self.mp_trillium_color_action.setCheckable(True)
        mp_coloring_menu.addAction(self.mp_trillium_color_action)
        mp_coloring_group.addAction(self.mp_trillium_color_action)
        menu.addSeparator()

        self.add_masspos_to_rt130_soh = QAction(
            'Add Mass Positions to RT130 SOH Messages', main_window
        )
        self.add_masspos_to_rt130_soh.setCheckable(True)
        menu.addAction(self.add_masspos_to_rt130_soh)

        self.add_positions_to_et_action = QAction(
            'Add Positions to ET Lines', main_window)
        self.add_positions_to_et_action.setCheckable(True)
        self.add_positions_to_et_action.setEnabled(False)
        menu.addAction(self.add_positions_to_et_action)

        date_format_menu = QMenu('Date Format:', main_window)
        date_format_action_group = QActionGroup(main_window)
        menu.addMenu(date_format_menu)
        self.yyyy_mm_dd_action = QAction('YYYY-MM-DD', main_window)
        self.yyyy_mm_dd_action.setCheckable(True)
        date_format_menu.addAction(self.yyyy_mm_dd_action)
        date_format_action_group.addAction(self.yyyy_mm_dd_action)
        self.yyyy_doy_action = QAction('YYYY:DOY', main_window)
        self.yyyy_doy_action.setCheckable(True)
        date_format_menu.addAction(self.yyyy_doy_action)
        date_format_action_group.addAction(self.yyyy_doy_action)
        self.yyyymmmdd_action = QAction('YYYYMMMDD', main_window)
        self.yyyymmmdd_action.setCheckable(True)
        date_format_menu.addAction(self.yyyymmmdd_action)
        date_format_action_group.addAction(self.yyyymmmdd_action)

        base_plot_font_size_menu = QMenu('Base Font Size:', main_window)
        base_plot_font_size_action_group = QActionGroup(main_window)
        menu.addMenu(base_plot_font_size_menu)
        for i in range(constants.MIN_FONTSIZE, constants.MAX_FONTSIZE + 1):
            self.base_plot_font_size_action_dict[i] = QAction(
                f'{i}px', main_window)
            self.base_plot_font_size_action_dict[i].setCheckable(True)
            base_plot_font_size_menu.addAction(
                self.base_plot_font_size_action_dict[i])
            base_plot_font_size_action_group.addAction(
                self.base_plot_font_size_action_dict[i])

    def create_database_menu(self, main_window, menu):
        """
        Create Database Menu's Actions which allow user to add/edit
            some tables in database.
        :param main_window: QMainWindow - main GUI for user to interact with
        :param menu: QMenu - Database Menu
        """
        self.add_edit_data_type_action = QAction(
            'Add/Edit Data Types', main_window)
        menu.addAction(self.add_edit_data_type_action)

        self.add_edit_param_action = QAction(
            'Add/Edit Parameters', main_window)
        menu.addAction(self.add_edit_param_action)

        self.add_edit_channel_action = QAction(
            'Add/Edit Channels', main_window)
        menu.addAction(self.add_edit_channel_action)

        self.view_plot_type_action = QAction(
            'View Plot Types', main_window)
        menu.addAction(self.view_plot_type_action)

    def create_help_menu(self, main_window, menu):
        """
        Create Help Menu's Actions which involve in viewing documentations or
            helpful tools.
        :param main_window: QMainWindow - main GUI for user to interact with
        :param menu: QMenu - Help Menu
        """
        self.calendar_action = QAction(
            'Calendar', main_window)
        menu.addAction(self.calendar_action)

        self.doc_action = QAction('Documentation', main_window)
        menu.addAction(self.doc_action)

    def create_forms_menu(self, main_window, menu):
        self.main_window_action = QAction("Main Window", main_window)
        menu.addAction(self.main_window_action)

    def connect_signals(self, main_window):
        """
        Connect widgets what they do
        :param main_window: QMainWindow - main GUI for user to interact with
        """
        self.connect_menu_signals(main_window)
        self.connect_widget_signals(main_window)

    def connect_menu_signals(self, main_window):
        """
        Connect menus' actions to what they do
        :param main_window: QMainWindow - main GUI for user to interact with
        """
        # File
        self.about_action.triggered.connect(main_window.open_about)
        self.exit_action.triggered.connect(main_window.close)

        # Commands
        self.gps_plotter_action.triggered.connect(main_window.open_gps_plotter)
        self.restore_default_database_action.triggered.connect(
            main_window.restore_default_database
        )

        # Options
        self.mp_regular_color_action.triggered.connect(
            lambda: setattr(
                main_window, 'mass_pos_volt_range_opt', 'regular'))
        self.mp_trillium_color_action.triggered.connect(
            lambda: setattr(
                main_window, 'mass_pos_volt_range_opt', 'trillium'))

        self.yyyy_mm_dd_action.triggered.connect(
            lambda: main_window.set_date_format('YYYY-MM-DD'))
        self.yyyymmmdd_action.triggered.connect(
            lambda: main_window.set_date_format('YYYYMMMDD'))
        self.yyyy_doy_action.triggered.connect(
            lambda: main_window.set_date_format('YYYY:DOY'))

        for i in range(constants.MIN_FONTSIZE, constants.MAX_FONTSIZE + 1):
            self.connect_base_plot_font_size_action(main_window, i)

        # Database
        self.add_edit_data_type_action.triggered.connect(
            main_window.open_data_type)
        self.add_edit_param_action.triggered.connect(
            main_window.open_param)
        self.add_edit_channel_action.triggered.connect(
            main_window.open_channel)
        self.view_plot_type_action.triggered.connect(
            main_window.open_plot_type)

        # Form
        self.main_window_action.triggered.connect(
            lambda: main_window.raise_form(main_window)
        )

        # Help
        self.calendar_action.triggered.connect(main_window.open_calendar)
        self.doc_action.triggered.connect(main_window.open_help_browser)

    def connect_base_plot_font_size_action(
            self, main_window: QMainWindow, size: int):
        """
        Connect an action of base_plot_font_size_dict with task to assign
        main_window.base_plot_font_size to the corresponding size.

        This have to be written separately to avoid problem of lambda
        in loop, which only pass the value in the last loop to the function of
        lambda.

        :param main_window: main control window
        :param size: font size
        """
        self.base_plot_font_size_action_dict[size].triggered.connect(
            lambda: setattr(
                main_window, 'base_plot_font_size', size))

    def connect_widget_signals(self, main_window):
        main_window.current_directory_changed.connect(
            self.curr_dir_line_edit.setText)

        # first Row
        self.time_from_date_edit.setCalendarWidget(CalendarWidget(main_window))
        self.time_from_date_edit.setDate(QtCore.QDate.fromString(
            constants.DEFAULT_START_TIME, QtCore.Qt.DateFormat.ISODate
        ))

        self.curr_dir_button.clicked.connect(
            main_window.change_current_directory)
        self.time_to_date_edit.setCalendarWidget(CalendarWidget(main_window))
        self.time_to_date_edit.setDate(QtCore.QDate.currentDate())

        # second Row
        self.open_files_list.itemDoubleClicked.connect(
            main_window.open_files_list_item_double_clicked)

        self.from_data_card_check_box.toggled.connect(
            main_window.from_data_card_check_box_clicked)

        # Filter files on pressing Enter
        self.search_line_edit.returnPressed.connect(
            main_window.filter_folder_list
        )

        self.log_checkbox.toggled.connect(
            main_window.on_log_file_checkbox_toggled
        )

        self.replot_button.clicked.connect(main_window.replot_loaded_data)

        self.background_black_radio_button.toggled.connect(
            lambda checked: self.main_window.set_plots_color(checked, 'B')
        )
        self.background_white_radio_button.toggled.connect(
            lambda checked: self.main_window.set_plots_color(checked, 'W')
        )

        self.all_wf_chans_check_box.clicked.connect(
            main_window.all_wf_chans_clicked)

        for cb in self.ds_check_boxes:
            cb.clicked.connect(main_window.data_stream_clicked)

        self.mseed_wildcard_edit.textChanged.connect(
            main_window.mseed_wildcard_changed)

        self.all_soh_chans_check_box.toggled.connect(
            main_window.all_soh_chans_clicked)

        self.select_pref_soh_list_button.clicked.connect(
            main_window.open_channel_preferences)

        self.read_button.clicked.connect(main_window.read_selected_files)
        self.plot_diff_data_set_id_button.clicked.connect(
            main_window.plot_diff_data_set_id)

        self.stop_button.clicked.connect(main_window.stop)

        self.save_plot_button.clicked.connect(main_window.save_plot)
