import sys
import platform
import os
from pathlib import Path
from typing import Union, Optional

from PySide6 import QtWidgets, QtCore, QtGui
from PySide6.QtWidgets import QApplication, QWidget, QDialog

from sohstationviewer.conf import constants


class SavePlotDialog(QDialog):
    def __init__(self, parent: Union[QWidget, QApplication],
                 main_window: QApplication,
                 default_name: str):
        """
        Dialog allow choosing file format and open file dialog to
            save file as

        :param parent: the parent widget
        :param main_window: to keep path to save file
        :param default_name: default name for graph file to be saved as
        """
        super(SavePlotDialog, self).__init__(parent)
        self.main_window = main_window
        """
        save_file_path: path to save file
        """
        self.save_file_path: Optional[Path] = None
        """
        save_dir_path: path to save dir
        """
        self.save_dir_path: Path = main_window.save_plot_dir
        """
        dpi: resolution for png format
        """
        self.dpi: int = 100

        self.save_dir_btn = QtWidgets.QPushButton("Save Directory", self)
        self.save_dir_textbox = QtWidgets.QLineEdit(self.save_dir_path)
        self.save_filename_textbox = QtWidgets.QLineEdit(default_name)

        self.dpi_line_edit = QtWidgets.QSpinBox(self)
        self.format_radio_btns = {}
        for fmt in constants.IMG_FORMAT:
            self.format_radio_btns[fmt] = QtWidgets.QRadioButton(fmt, self)
            if fmt == self.main_window.save_plot_format:
                self.format_radio_btns[fmt].setChecked(True)
        self.cancel_btn = QtWidgets.QPushButton('CANCEL', self)
        self.save_plot_btn = QtWidgets.QPushButton('SAVE PLOT', self)

        self.setup_ui()
        self.connect_signals()

    def setup_ui(self) -> None:
        self.setWindowTitle("Save Plot")

        main_layout = QtWidgets.QGridLayout()
        self.setLayout(main_layout)

        main_layout.addWidget(self.save_dir_btn, 0, 0, 1, 1)
        self.save_dir_textbox.setFixedWidth(500)
        main_layout.addWidget(self.save_dir_textbox, 0, 1, 1, 5)
        main_layout.addWidget(QtWidgets.QLabel('Save Filename'),
                              1, 0, 1, 1)
        main_layout.addWidget(self.save_filename_textbox, 1, 1, 1, 5)

        main_layout.addWidget(QtWidgets.QLabel('DPI'),
                              2, 2, 1, 1, QtGui.Qt.AlignmentFlag.AlignRight)
        self.dpi_line_edit.setRange(50, 300)
        self.dpi_line_edit.setValue(100)
        main_layout.addWidget(self.dpi_line_edit, 2, 3, 1, 1)
        rowidx = 2
        for fmt in self.format_radio_btns:
            main_layout.addWidget(self.format_radio_btns[fmt], rowidx, 1, 1, 1)
            rowidx += 1

        main_layout.addWidget(self.cancel_btn, rowidx, 1, 1, 1)
        main_layout.addWidget(self.save_plot_btn, rowidx, 3, 1, 1)

    def connect_signals(self) -> None:
        self.save_dir_btn.clicked.connect(self.change_save_directory)
        self.cancel_btn.clicked.connect(self.close)
        self.save_plot_btn.clicked.connect(self.save_plot)

    @QtCore.Slot()
    def change_save_directory(self) -> None:
        """
        Show a file selection window and change the GPS data save directory
        based on the folder selected by the user.
        """
        fd = QtWidgets.QFileDialog(self)
        fd.setFileMode(QtWidgets.QFileDialog.FileMode.Directory)
        fd.setDirectory(self.save_dir_textbox.text())
        fd.exec()
        new_path = fd.selectedFiles()[0]
        self.save_dir_textbox.setText(new_path)
        self.main_window.save_plot_dir = new_path

    @QtCore.Slot()
    def save_plot(self):
        self.save_dir_path = self.save_dir_textbox.text().strip()
        if self.save_dir_path == '':
            QtWidgets.QMessageBox.warning(
                self, "Add Directory",
                "A directory need to be given before continue.")
            return

        if self.save_filename_textbox.text().strip() == '':
            QtWidgets.QMessageBox.warning(
                self, "Add Filename",
                "A file name need to be given before continue.")
            return

        for img_format in self.format_radio_btns:
            if self.format_radio_btns[img_format].isChecked():
                save_format = img_format
                self.main_window.save_plot_format = img_format
                break

        self.save_file_path = Path(self.save_dir_path).joinpath(
            f"{self.save_filename_textbox.text()}.{save_format}")

        if not os.access(self.save_dir_path, os.W_OK):
            self.save_file_path = None
            QtWidgets.QMessageBox.information(
                self, "No Write Permission",
                "The directory to save file to doesn't have Write Permission."
                "\n\nPlease change its permission or "
                "select another directory."
            )
            return

        self.dpi = self.dpi_line_edit.value()
        self.close()


if __name__ == '__main__':
    os_name, version, *_ = platform.platform().split('-')
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    save_path = '/Users/ldam/Documents/GIT/sohstationviewer/tests/test_data/Q330-sample'  # noqa: E501
    test = SavePlotDialog(None, 'test_plot')
    test.set_save_directory(save_path)
    test.exec()
    print("dpi:", test.dpi)
    print("save file path:", test.save_file_path)
    sys.exit(app.exec())
