from __future__ import annotations

from typing import Dict, Optional, List, Tuple, Union, TypeVar

from PySide6 import QtWidgets, QtGui, QtCore
from PySide6.QtCore import Signal
from PySide6.QtGui import QCloseEvent
from PySide6.QtWidgets import QMessageBox, QWidget

from sohstationviewer.database.process_db import execute_db
from sohstationviewer.view.db_config.value_color_helper.value_color_edit \
    import ValueColorEdit
from sohstationviewer.view.util.one_instance_at_a_time import \
    OneWindowAtATimeDialog
from sohstationviewer.view.util.color import COLOR


class DeleteRowButton(QtWidgets.QPushButton):
    """
    A button used to delete a row in the data table of UiDBInfoDialog.
    """

    def __init__(self):
        super().__init__()
        self.setText('Delete')
        style_sheet = self.styleSheet() + (
            '.DeleteRowButton:enabled {'
            '   color: #FF474C'
            '}\n'
        )
        self.setStyleSheet(style_sheet)


class ResetRowButton(QtWidgets.QPushButton):
    """
    A button used to reset a row in the data table of UiDBInfoDialog to
    that row in backup_db.
    """

    def __init__(self):
        super().__init__()
        self.setText('Reset to org. DB')
        style_sheet = self.styleSheet() + (
            '.ResetRowButton:enabled {'
            '   color: #FF474C'
            '}\n'
        )
        self.setStyleSheet(style_sheet)


def set_widget_color(widget, changed=False, read_only=False):
    """
    Set color of the given widget depend on flag read_only or changed
    :param widget: the widget to set color
    :param changed: True: red Text, white background
    :param read_only: True: grey Text, blue background (priority)
    if both flags are False: black Text, white blackground
    """
    if isinstance(widget, ValueColorEdit):
        # ValueColorEdit handles its own styling, so we don't need to style
        # it here.
        return

    palette = widget.palette()

    if read_only:
        # grey text
        palette.setColor(QtGui.QPalette.ColorRole.Text,
                         QtGui.QColor(100, 100, 100))
        # light blue background
        palette.setColor(QtGui.QPalette.ColorRole.Base,
                         QtGui.QColor(210, 240, 255))
        widget.setReadOnly(True)
        widget.setPalette(palette)
        return
    else:
        # white background
        palette.setColor(QtGui.QPalette.ColorRole.Base,
                         QtGui.QColor(255, 255, 255))
    if changed:
        # blue text
        palette.setColor(QtGui.QPalette.ColorRole.Text,
                         QtGui.QColor(COLOR['changedStatus']))
        # The selected text in a QComboBox on Linux has ButtonText as its color
        # role (probably because the QComboBox on Linux looks like one button,
        # while the one on Mac looks like a TextEdit combined with a button).
        palette.setColor(QtGui.QPalette.ColorRole.ButtonText,
                         QtGui.QColor(COLOR['changedStatus']))
        palette.setColor(QtGui.QPalette.ColorRole.Base,
                         QtGui.QColor(COLOR['changedStatusBackground']))
    else:
        try:
            if widget.isReadOnly():
                # grey text
                palette.setColor(
                    QtGui.QPalette.ColorRole.Text,
                    QtGui.QColor(100, 100, 100))
            else:
                # black text
                palette.setColor(QtGui.QPalette.ColorRole.Text,
                                 QtGui.QColor(0, 0, 0))
                palette.setColor(QtGui.QPalette.ColorRole.Base,
                                 QtGui.QColor(255, 255, 255))
        except AttributeError:
            palette.setColor(QtGui.QPalette.ColorRole.Text,
                             QtGui.QColor(0, 0, 0))
    widget.setPalette(palette)


T = TypeVar('T')


def get_duplicates(items: List[T]) -> Dict[T, List[int]]:
    """
    Get duplicate items and their indices in a list.
    :param items: the list of items
    :return: a dict of duplicates, with the duplicated items being the keys and
        the indices they appear in as the values. The returned dict is empty
        if no duplicate was found.
    """
    # Create a dictionary to store each item and the indices it appears in.
    # At the end, we can simply filter this dictionary by value to obtain the
    # duplicated items and their indices.
    idx_dict: Dict[T, List[int]] = {}
    for idx, item in enumerate(items):
        idx_dict.setdefault(item, []).append(idx)
    return {item[0]: item[1]
            for item in idx_dict.items()
            if len(item[1]) > 1}


class UiDBInfoDialog(OneWindowAtATimeDialog):
    """
    Superclass for info database dialogs under database menu.
    """

    # This line is only to type hint the class attribute inherited from
    # OneWindowAtATimeDialog.
    current_instance: UiDBInfoDialog

    def __init__(self, parent, column_headers, primary_column, table_name,
                 resize_content_columns=[], required_columns={},
                 need_data_type_choice=False, check_fk=True):
        """
        :param parent: QMainWindow/QWidget - the parent widget
        :param column_headers: [str,] - headers of the columns
        :param primary_column: str - primary key column of the table in the
            database (not count dataType in Channels table)
        :param table_name: str - the database table
        :param resize_content_columns: [int,] - list of indexes of columns of
            which width needs to be resize to content
        :param required_columns: {int: str,} - dict of column indexes vs column
            names of the columns that are required to have a value.
        :param need_data_type_choice: bool - to indicate if a QChoicewill be
            added
            to the top of the dialog for user to select data type.
        :param check_fk: check foreign key to turn the row to readonly if not
            satified.
        """
        super(UiDBInfoDialog, self).__init__()

        self.parent = parent
        # We need an additional column to accommodate the delete and reset
        # buttons
        self.total_col = len(column_headers) + 2
        self.column_headers = column_headers
        self.resize_content_columns = resize_content_columns
        self.delete_button_col_idx = self.total_col - 1
        self.reset_button_col_idx = self.total_col - 2
        # We want to resize the columns that contain the delete buttons (the
        # last on) to be snug with those button
        self.resize_content_columns.append(self.delete_button_col_idx)
        self.resize_content_columns.append(self.reset_button_col_idx)
        self.required_columns = required_columns
        self.need_data_type_choice = need_data_type_choice
        self.primary_column = primary_column
        self.table_name = table_name
        self.check_fk = check_fk

        # ========================== internal data ===========================
        """
        database_rows: list of entries in the database.
        """
        self.database_rows = []
        """
        backup_database_rows: list of entries in the backup_database.
        """
        self.backup_database_rows = []
        """
        changes_array: a bit array that store whether a row is changed.
        """
        self.changes_array: List[List[bool]] = []
        """
        queued_row_delete_sqls: a dictionary of delete SQL statements that are
        executed when changes are saved to the database. Map a row id in the
        data table to each SQL statement to make syncing deletes in the data
        table and deletes in the database easier.
        """
        self.queued_row_delete_sqls: Dict[int, str] = {}
        """
        insert_sql_template, update_sql_template: the parameterized SQL
        statements used to insert/update rows in the database. Should be
        defined in any child class that supports editing the database.
        """
        self.insert_sql_template = ''
        self.update_sql_template = ''
        """
        delete_sql_supplement: the extension added to the sql statements used
        to delete rows from the database. Should be defined in a child
        class when needed.
        """
        self.delete_sql_supplement = ''
        """
        data_type: str - type of data being process
        """
        self.data_type: Optional[str] = None

        # =========================== pre-define ============================
        self.bottom_layout = QtWidgets.QGridLayout()
        self.setup_ui()

    def setup_ui(self):
        self.setGeometry(100, 100, 1100, 700)
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        button_layout = self.create_buttons_section()
        main_layout.addLayout(button_layout)

        self.create_data_table_widget()
        main_layout.addWidget(self.data_table_widget, 1)
        """
        Color code is given at the end of the dialog.
        Other instructions should be given when hover over the widgets.
        """

        if self.table_name != '':
            # Not really used in this (base) class, but made available so that
            # any children can use the empty space on the bottom right of the
            # widget.
            instruction = (
                "LIGHT BLUE BACKGROUND WITH GREY TEXT - Non Editable due to "
                "database constrain.\n"
                "LIGHT BLUE BACKGROUND and/or BORDER and/or BLUE TEXT - "
                "Changing/changed.")
            # TODO: add question mark button to give instruction
            self.bottom_layout.addWidget(QtWidgets.QLabel(instruction),
                                         0, 0, 1, 1)
            main_layout.addLayout(self.bottom_layout)

    def add_row_number_button(self, row_idx: int) -> None:
        """
        Add a button that shows the row number to a row of the data table. The
        button will be on the left of the row.
        :param row_idx: the index of the row to insert the button
        """
        row_button = QtWidgets.QPushButton(str(row_idx))
        set_widget_color(row_button)
        row_button.setFixedWidth(40)
        row_button.clicked.connect(
            lambda: self.row_number_clicked(row_button))
        self.data_table_widget.setCellWidget(row_idx, 0, row_button)

    def create_widget(self, widget_content: str,
                      choices: Optional[List] = None,
                      range_values: Optional[List] = None,
                      field_name: str = '', **kwargs
                      ) -> Tuple[QWidget, Signal]:
        """
        Create a widget with the given content. The actual type of widget
        created depends on the arguments passed in. By default, the created
        widget will be a QLineEdit.

        :param widget_content: the content of the created widget
        :param choices: list of choices to add to a drop-down list. If this is
            available, the created widget will be a QComboBox.
        :param range_values: list consists of min and max values. If this is
            available, the created widget will be a QSpinbox.
        :param field_name: name of the column in database.
            If field_name is 'convertFactor', a validator will be added to the
            created widget, limiting the input to numbers in the range 0.0-5.0
            with at most 6 decimal digits.
            If field_name is 'description', the created widget will be a
            QTextEdit.
        :return:
            - a widget whose type depends on the arguments passed in.
            - the QT signal that is emitted when the content of the returned
                widget is modified
        """
        widget, change_signal = None, None
        if choices is None and range_values is None:
            if field_name == 'description':
                widget = QtWidgets.QTextEdit()
                height = 12
                for line in widget_content.split('\n'):
                    widget.append(line)
                    height += 16
                widget.setFixedHeight(height)
            else:
                widget = QtWidgets.QLineEdit(widget_content)
            if field_name == 'convertFactor':
                # precision=6
                validator = QtGui.QDoubleValidator(0.0, 5.0, 6)
                validator.setNotation(
                    QtGui.QDoubleValidator.Notation.StandardNotation
                )
                widget.setValidator(validator)
                if widget_content == '':
                    widget.setText('1')
            change_signal = widget.textChanged
        elif choices:
            widget = QtWidgets.QComboBox()
            widget.addItems(choices)
            widget.setCurrentText(widget_content)
            change_signal = widget.currentTextChanged
        elif range_values:
            widget = QtWidgets.QSpinBox()
            widget.setMinimum(range_values[0])
            widget.setMaximum(range_values[1])
            if widget_content in ["", None, 'None']:
                widget.setValue(range_values[0])
            else:
                widget.setValue(int(widget_content))
            change_signal = widget.textChanged
        if widget is None:
            raise
        return widget, change_signal

    def add_widget(self, row_idx, col_idx, foreign_key=False, choices=None,
                   range_values=None, plot_type=None, field_name=''
                   ) -> QWidget:
        """
        Add a cell widget at a given row and column in the data_table_widget.

        :param row_idx: index of row
        :param col_idx: index of column
        :param foreign_key: True if there is a foreign constrain that prevents
            the row to be deleted => set readonly of cell=True
        :param choices: List of choices to add to a drop down list. If
            it is available, QComboBox will be assigned for the widget.
        :param range_values: list consists of min and max values. If
            it is available, QSpinbox will be assigned for the widget
        :param plot_type: the plot type associated with the value colors
            contain in the created widget. If it is available, ValueColorEdit
            will be assigned for the widget
        If no choices, range, or plot type available, QLineEdit will be
        assigned for the widget.
        :param field_name: name of the column in database. If field_name=
            'convertFactor', add validator to limit precision to 6 decimal
            points
        :return: value stored in created widget as a string
        """
        widget_content = (str(self.database_rows[row_idx][col_idx - 1])
                          if row_idx < len(self.database_rows) else '')

        widget, change_signal = self.create_widget(
            widget_content, choices=choices, range_values=range_values,
            field_name=field_name, plot_type=plot_type, row_idx=row_idx
        )

        if foreign_key:
            set_widget_color(widget, read_only=True)
        else:
            new = False if row_idx < len(self.database_rows) else True
            set_widget_color(widget, read_only=False, changed=new)
            change_signal.connect(
                lambda changed_text:
                self.on_cell_input_change(changed_text, widget)
            )
        self.data_table_widget.setCellWidget(row_idx, col_idx, widget)
        if field_name == 'description':
            self.data_table_widget.resizeRowToContents(row_idx)
        return widget

    @QtCore.Slot()
    def row_number_clicked(self, widget):
        """
        When a row number cell is clicked, the whole line is selected.

        :param widget: row number widget
        """
        self.data_table_widget.selectRow(int(widget.text()))
        self.data_table_widget.repaint()

    def create_buttons_section(self):
        """
        Create a horizontal layout of buttons including:
        * data_type_combo_box if self.need_data_type_choice is True
        * 'ADD ROW' button
        * 'SAVE CHANGES' button
        * 'CLOSE' button
        """
        h_layout = QtWidgets.QHBoxLayout()

        if self.need_data_type_choice:
            self.data_type_combo_box = QtWidgets.QComboBox(self)
            data_type_rows = execute_db('SELECT * FROM DataTypes')
            self.data_type_combo_box.addItems([d[0] for d in data_type_rows])
            self.data_type_combo_box.currentTextChanged.connect(
                self.data_type_changed)
            h_layout.addWidget(self.data_type_combo_box)
        if self.table_name != '':
            self.add_row_btn = QtWidgets.QPushButton(self, text='ADD ROW')
            self.add_row_btn.setFixedWidth(300)
            self.add_row_btn.clicked.connect(self.add_row)
            h_layout.addWidget(self.add_row_btn)

            self.save_changes_btn = QtWidgets.QPushButton(
                self, text='SAVE CHANGES')
            self.save_changes_btn.setFixedWidth(300)
            # The clicked signal of buttons emit a single argument with default
            # value of False. This cause self.save_changes to be called with
            # an argument, which is not what we want in this situation.
            self.save_changes_btn.clicked.connect(lambda: self.save_changes())
            h_layout.addWidget(self.save_changes_btn)

            self.reset_btn = QtWidgets.QPushButton(self, text='RESET')
            reset_btn_style_sheet = self.reset_btn.styleSheet() + (
                'QPushButton:enabled {'
                '   color: #FF474C'
                '}\n'
            )
            self.reset_btn.setStyleSheet(reset_btn_style_sheet)
            self.reset_btn.setFixedWidth(300)
            self.reset_btn.clicked.connect(self.reset_changes)
            h_layout.addWidget(self.reset_btn)
        return h_layout

    def create_data_table_widget(self):
        """
        Create self.data_table_widget based on self.total_col, self.column
        and call update_data_table_widget_items to create cells for the widget.
        """
        self.data_table_widget = QtWidgets.QTableWidget(self)
        # If we don't do this, the label that informs the user that they
        # deleted a row in the data table will be selectable, which doesn't
        # look too good.
        self.data_table_widget.setSelectionMode(
            QtWidgets.QAbstractItemView.SelectionMode.NoSelection
        )
        self.data_table_widget.verticalHeader().hide()
        self.data_table_widget.setColumnCount(self.total_col)
        # Make the two last columns have an empty header.
        self.column_headers.extend(['', ''])
        self.data_table_widget.setHorizontalHeaderLabels(self.column_headers)
        header = self.data_table_widget.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeMode.Stretch)
        for i in self.resize_content_columns:
            header.setSectionResizeMode(
                i, QtWidgets.QHeaderView.ResizeMode.ResizeToContents)

        if self.need_data_type_choice:
            self.data_type = self.data_type_combo_box.currentText()
        self.update_data_table_widget_items()

    def get_database_rows(self) -> list:
        """
        Get list of data to fill self.data_table_widgets' content. Should be
        implemented by all children.

        :return a list of data to fill in the data table
        """
        pass

    def clear_first_row(self):
        """
        Clear the content of the first row of the editor. By default, all
        editor widgets have their content set to empty, so this method does not
        need to be implemented unless a default value is needed for some editor
        widgets.
        """
        pass

    def update_data_table_widget_items(self):
        """
        Create widget cell for self.data_table_widget based on
        self.database_rows for data to be displayed.
        Each row will be check for foreign_key constrain to decide readonly
        status.
        If self.database_rows is empty, call clear_first_row to create a row
        with all necessary widget cells but empty.
        """
        # When it comes to deleted rows, we can either reset them manually or
        # remove them altogether. We chose to remove them using this line
        # because it is the cleanest way to do it, and the performance does not
        # matter because we don't really expect to show more than 100 channels
        # at a time.
        self.data_table_widget.setRowCount(0)
        self.database_rows = self.get_database_rows()
        self.changes_array = [
            [False] * (self.total_col - 1)
            for _ in range(len(self.database_rows))
        ]

        row_count = len(self.database_rows)
        if row_count == 0:
            # If there is no data in the database, we want to leave an empty
            # row in the editor.
            # self.add_row() does not work here without some light refactoring
            # which might introduce bugs, so we manually set up the first row.
            self.data_table_widget.setRowCount(0)
            self.add_row()
            self.clear_first_row()
        else:
            self.data_table_widget.setRowCount(row_count)
            for i in range(row_count):
                fk = self.check_data_foreign_key(self.database_rows[i][0])
                self.set_row_widgets(i, fk)

        self.update()

    @QtCore.Slot()
    def add_row(self):
        """
        Add a new empty row to the end of the data_table_widget
        and scroll to the bottom.
        """
        row_position = self.data_table_widget.rowCount()
        self.data_table_widget.insertRow(row_position)
        self.set_row_widgets(row_position)
        self.data_table_widget.scrollToBottom()
        self.data_table_widget.repaint()  # to show row's header
        self.data_table_widget.cellWidget(row_position, 1).setFocus()
        self.changes_array.append([True] * (self.total_col - 1))

    def remove_row(self, remove_row_idx):
        """
        Remove row at remove_row_idx and move all data below it one row up.

        :param remove_row_idx: Index of row to be removed.
        """
        self.data_table_widget.removeRow(remove_row_idx)
        for i in range(remove_row_idx, self.data_table_widget.rowCount()):
            cell_widget = self.data_table_widget.cellWidget(i, 0)
            cell_widget.setText(str(i))

    @QtCore.Slot()
    def on_cell_input_change(self, changed_text: str,
                             changed_cell_widget: QWidget):
        """
        If cell's value is changed, text's color will be red,
        otherwise, text's color will be black

        :param changed_text: new value of the cell widget
        :param changed_cell_widget: the widget whose content was changed
        """
        widget_x, widget_y = changed_cell_widget.pos().toTuple()
        col_idx = self.data_table_widget.columnAt(widget_x)
        row_idx = self.data_table_widget.rowAt(widget_y)
        if row_idx == -1:
            return
        changed = False
        if row_idx < len(self.database_rows):
            if changed_text != str(self.database_rows[row_idx][col_idx - 1]):
                changed = True
            cell_widget = self.data_table_widget.cellWidget(row_idx, col_idx)
            set_widget_color(cell_widget, changed=changed)
        else:
            changed = True
        self.changes_array[row_idx][col_idx - 1] = changed

    def check_data_foreign_key(self, val):
        """
        If self.check_fk is True, check foreign key constrain for self.col_name
        column in Channels data table to allow or disallow deleting record in
        Parameters or DataTypes table.

        :param val: the value that need to be checked again self.col_name
        """
        if not self.check_fk:
            return False
        sql = (f"SELECT {self.primary_column} FROM channels "
               f"WHERE {self.primary_column}='{val}'")
        param_rows = execute_db(sql)
        if len(param_rows) > 0:
            return True
        else:
            return False

    def set_row_widgets(self, row_idx, fk=False):
        """
        Set the widgets in a row in self.data_table_widgets. Because each table
        has their own definition of a row, all child dialogs have to implement
        this method.
        """
        pass

    def set_row_content(self, row_idx: int, row: List[Union[str, int]]):
        """
        Set content to each cell in row.
        :param row_idx: position of row to set the content
        :param row: list of contents of all cells in a row
        """
        pass

    def get_row_inputs(self, row_idx: int) -> list:
        """
        Get content of a row in the data table. Need to be implemented in all
        children that have editable rows.

        :param row_idx: index of row
        """
        pass

    def add_delete_button_to_row(self, row_idx, fk: bool = False):
        """
        Add a delete button to a row of the data table. The button will be on
        the right of the row.
        :param row_idx: the index of the row to insert a delete button
        :param fk: whether there is a foreign key constraint that prevents the
            row from being deleted. If True, the added button will be disabled.
        """
        delete_button = DeleteRowButton()
        if fk:
            delete_button.setEnabled(False)
        self.data_table_widget.setCellWidget(
            row_idx, self.delete_button_col_idx, delete_button)
        delete_button.clicked.connect(
            lambda: self.delete_table_row(delete_button)
        )

    def add_reset_button_to_row(self, row_idx):
        """
        Add a reset button to a row of the data table. The button will be on
        the right of the row.

        This button will be disabled if the current row is the same with
        the backup DB's row at the same row_idx and will be enabled otherwise.

        :param row_idx: the index of the row to insert a reset button
        """
        reset_button = ResetRowButton()
        if row_idx < len(self.database_rows):
            curr_row = self.database_rows[row_idx]
            # Primary key is always the first item in a row
            backup_rows = [r for r in self.backup_database_rows
                           if r[0] == curr_row[0]]
            if self.need_data_type_choice:
                # data type is one of primary keys
                backup_rows = [r for r in backup_rows
                               if r[-1] == curr_row[-1]]
            if backup_rows:
                # reset function available when there's backup row
                backup_row = backup_rows[0]
                backup_row_idx = self.backup_database_rows.index(backup_row)
                reset_button.clicked.connect(
                    lambda: self.reset_table_row(row_idx, backup_row_idx)
                )
        else:
            # current row out of database_rows'range means there's no backup
            backup_rows = []

        if not backup_rows or backup_row == curr_row:
            # When there's no backup row for the current row or
            # current row is similar to backup row, disable revert button
            reset_button.setEnabled(False)
        self.data_table_widget.setCellWidget(
            row_idx, self.reset_button_col_idx, reset_button)

    def display_row_deleted_notification(self, row_idx: int, primary_key: str):
        """
        Display a notification that a row has been deleted in place of the
        deleted row.

        :param row_idx: the index of the row being deleted
        :param primary_key: the identifier of the row
        """
        # Remove all the data edit widgets in the chosen row.
        data_edit_widgets_count = self.total_col - 2
        for i in range(1, data_edit_widgets_count + 1):
            self.data_table_widget.removeCellWidget(row_idx, i)

        self.data_table_widget.setSpan(row_idx, 1,
                                       1, data_edit_widgets_count)
        # Show the notification that the row has been deleted.
        row_deleted_notifications = (
            f'{self.column_headers[1]} '
            f'<font color=#0087ff>{primary_key}</font> '
            f'has been deleted.'
        )
        row_deleted_label = QtWidgets.QLabel(row_deleted_notifications)
        row_deleted_label.setTextFormat(QtCore.Qt.TextFormat.RichText)
        row_deleted_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.data_table_widget.setCellWidget(row_idx, 1,
                                             row_deleted_label)

    @QtCore.Slot()
    def delete_table_row(self, row_delete_button: QtWidgets.QPushButton):
        """
        Delete a row from the table.

        If the row contains data that is not in the database (i.e. rows added
        by the user), remove it completely.

        Otherwise, remove all data edit widgets from a row and notify the user
        that they deleted the row. Also convert the delete button into an undo
        button. On the backend, format a SQL statement to delete the row from
        the database and queue it to be run when saving changes.

        :param row_delete_button: the delete button assigned to the row to be
            deleted
        """
        # If we don't have this line, there is an issue with the undo button
        # flickering in behind the 0 row label before moving to the correct
        # location.
        self.data_table_widget.setUpdatesEnabled(False)

        delete_button_y = row_delete_button.y()
        row_idx = self.data_table_widget.rowAt(delete_button_y)

        # Because self.changes_array only track changes to row content and not
        # deletions, we want to untrack the row that is being deleted.
        if row_idx < len(self.database_rows):
            self.changes_array[row_idx] = [False] * (self.total_col - 1)
        else:
            # Because rows not in the database are removed from the table when
            # deleted, we delete the value that tracks whether they changed
            # when they are deleted.
            self.changes_array.pop(row_idx)

        if row_idx >= len(self.database_rows):
            # Deleting a row that is not in the database. Because no rows that
            # are in the database are removed until changes are saved, and the
            # user can only add rows at the end of the data table, it suffices
            # to check that the index of the removed row is greater than the
            # number of rows retrieved from the database.
            self.remove_row(row_idx)
        else:
            primary_key = self.database_rows[row_idx][0]

            self.display_row_deleted_notification(row_idx, primary_key)

            undo_button = QtWidgets.QPushButton('Undo')
            undo_button.clicked.connect(
                lambda: self.undo_delete_table_row(row_idx)
            )
            self.data_table_widget.setCellWidget(
                row_idx, self.delete_button_col_idx, undo_button
            )

            delete_sql = (f"DELETE FROM {self.table_name} "
                          f"WHERE {self.primary_column}='{primary_key}'")
            delete_sql += self.delete_sql_supplement
            self.queued_row_delete_sqls[row_idx] = delete_sql

        self.data_table_widget.setUpdatesEnabled(True)

    @QtCore.Slot()
    def reset_table_row(self, row_idx: int, backup_row_idx: int):
        """
        Reset a row to the content in backup db.

        Validate backup row before reset. If backup row is invalid, inform
        user that row cannot be reset.

        Otherwise, reset row content to backup row's content and notify user
        that the row has been reset. Also convert the reset button into an undo
        button.

        Note that set_row_content will trigger on_cell_input_change which
        will update changes_array and the cell's color.

        :param row_idx: the row number of the row being reset
        :param backup_row_idx: the row number of the backup of the row being
            reset
        """
        # If we don't have this line, there is an issue with the undo button
        # flickering in behind the 0 row label before moving to the correct
        # location.
        self.data_table_widget.setUpdatesEnabled(False)

        backup_row_content = self.backup_database_rows[backup_row_idx]
        is_reset_row_valid, msg = self.validate_row(
            row_idx, backup_row_content)
        if not is_reset_row_valid:
            header = "Backup row's validation failed.\n\n"
            footer = "\n\nReset can't be performed."
            QMessageBox.warning(self, "Resetting Failed",
                                header + msg + footer)
            self.data_table_widget.setUpdatesEnabled(True)
            return
        # update row with content from backup database
        self.set_row_content(row_idx, backup_row_content)

        undo_button = QtWidgets.QPushButton('Undo Reset')
        undo_button.clicked.connect(
            lambda: self.undo_reset_table_row(row_idx, backup_row_idx)
        )
        self.data_table_widget.setCellWidget(
            row_idx, self.reset_button_col_idx, undo_button
        )
        self.data_table_widget.setUpdatesEnabled(True)
        QMessageBox.information(
            self, "Successful Reset",
            "The row has been reset back to original database.")

    def undo_delete_table_row(self, row_idx: int):
        """
        Undo the deletion of a table row and dequeue the corresponding SQL
        statement from the list of SQL statements to be run when saving.

        :param row_idx: the row number of the row being deleted
        """
        # If we don't have this line, there is an issue with the undo button
        # flickering in behind the 0 row label before moving to the correct
        # location.
        self.data_table_widget.setUpdatesEnabled(False)

        self.data_table_widget.removeCellWidget(row_idx, 1)
        self.data_table_widget.setSpan(row_idx, 1, 1, 1)
        self.set_row_widgets(row_idx)
        del self.queued_row_delete_sqls[row_idx]

        self.data_table_widget.setUpdatesEnabled(True)

    def undo_reset_table_row(self, row_idx: int, backup_row_idx: int):
        """
        Set the content of row_idx's row to the corresponding row in the
        current database. Also convert the undo button into a reset button.

        Note that set_row_content will trigger on_cell_input_change which
        will update changes_array and the cell's color.

        :param row_idx: the row number of the row being undo reset
        :param backup_row_idx: the row number of the backup of the row being
            undo reset
        """
        # If we don't have this line, there is an issue with the undo button
        # flickering in behind the 0 row label before moving to the correct
        # location.
        self.data_table_widget.setUpdatesEnabled(False)

        self.set_row_content(row_idx, self.database_rows[row_idx])

        reset_button = ResetRowButton()
        reset_button.clicked.connect(
            lambda: self.reset_table_row(row_idx, backup_row_idx)
        )
        self.data_table_widget.setCellWidget(
            row_idx, self.reset_button_col_idx, reset_button)
        self.data_table_widget.setUpdatesEnabled(True)
        QMessageBox.information(
            self, "Successfully Undo Reset",
            "The row has been set up to current values in database.")

    def data_type_changed(self):
        """
        Load new self.database_rows when self.data_type_combo_box's value is
            changed
        """
        pass

    def reset_changes(self):
        if self.has_changes():
            reset_prompt = ('Do you want to reset all changes you made? All '
                            'unsaved changes will be discarded.')
            choice = QMessageBox.question(self, 'Resetting changes',
                                          reset_prompt,
                                          QMessageBox.StandardButton.Reset |
                                          QMessageBox.StandardButton.Cancel
                                          )
            if choice == QMessageBox.StandardButton.Cancel:
                self.data_table_widget.setUpdatesEnabled(True)
                return False
            self.untrack_changes()
            self.update_data_table_widget_items()

    def has_changes(self):
        """
        Check whether the user has made a change in the data table.
        :return: True if a change has been made (i.e. there are SQL statements
                     queued for execution)
                 False otherwise
        """
        return (len(self.queued_row_delete_sqls) != 0 or
                any([any(row_changes) for row_changes in self.changes_array]))

    def untrack_changes(self):
        """
        Untrack all changes that have been made.
        """
        self.changes_array = []
        self.queued_row_delete_sqls = {}

    def validate_row(self, row_id: int, row_content: List) -> Tuple[bool, str]:
        """
        Check if the given row is valid. Invalid rows are those that:
            - contains an empty primary key.
            - has an empty cell in a required column.

        :return:
            - Whether the given row is valid
            - A message of what the issue is; is the empty string if there is
                no issue
        """
        is_changes_invalid = True
        msg = ''

        is_empty_primary_key = (row_content[0].strip() == '')
        if is_empty_primary_key:
            is_changes_invalid = False
            msg = f'Row {row_id}: This row has an empty primary key.'

        is_required_column_empty = any(
            row_content[i] == '' for i in self.required_columns
        )
        if is_required_column_empty:
            is_changes_invalid = False
            msg = f'Row {row_id}: There is a required cell that is empty.'

        return is_changes_invalid, msg

    def validate_changes(self) -> Tuple[bool, str]:
        """
        Look through the changes the user made and attempt to check whether
        they are all valid. Only duplicate primary keys is checked for in this
        method. Look at self.validate_row() and its overrides for a list of
        other invalid changes.

        :return:
            - Whether all the changes made in the table are valid
            - A message of what the issue is; is the empty string if there is
                no issue
        """
        # We assume that the first column in the editor is the primary key
        # column. This would be a lot less fragile if we use a delegate instead
        # of cell widgets, but that would require a complete rewrite of this
        # class.
        primary_keys = [
            self.data_table_widget.cellWidget(row_idx, 1).text().strip()
            for row_idx in range(self.data_table_widget.rowCount())
        ]
        duplicated_primary_keys = get_duplicates(primary_keys)
        msg = ''
        for primary_key, indices in duplicated_primary_keys.items():
            # .join() only accepts iterables of strings, so we have to do
            # a quick conversion here.
            str_indices = [str(i) for i in indices]
            msg += (f'Rows {", ".join(str_indices)}: '
                    f'Primary key "{primary_key}" is duplicated.\n')
        # Remove the final new line for better display.
        msg = msg.strip('\n')
        if msg:
            return False, msg

        changed_row_ids = [idx
                           for (idx, is_cell_changed_in_row_array)
                           in enumerate(self.changes_array)
                           if any(is_cell_changed_in_row_array)]
        for row_id in changed_row_ids:
            row_content = self.get_row_inputs(row_id)
            is_row_valid, msg = self.validate_row(row_id, row_content)
            if not is_row_valid:
                return is_row_valid, msg
        return True, ''

    @QtCore.Slot()
    def save_changes(self, need_confirmation=True) -> bool:
        """
        Save the changes to the database. Ask for user confirmation before
        saving. If there is any invalid change, exit with an error message.

        :param need_confirmation: whether confirmation is needed before saving
            the changes made to the database
        :return: True if save is successful, False otherwise
        """
        # Disable all graphical updates to the data table so that we can update
        # it without the user seeing all the intermediate changes we made.
        self.data_table_widget.setUpdatesEnabled(False)
        if self.has_changes() and need_confirmation:
            save_prompt = ('Do you want to save the changes you made? This '
                           'action cannot be undone.')
            choice = QMessageBox.question(self, 'Saving changes', save_prompt,
                                          QMessageBox.StandardButton.Save |
                                          QMessageBox.StandardButton.Cancel
                                          )
            if choice != QMessageBox.StandardButton.Save:
                self.data_table_widget.setUpdatesEnabled(True)
                return False

        is_all_changes_valid, msg = self.validate_changes()
        if not is_all_changes_valid:
            invalid_changes_text = (f'Your changes could not be saved due to '
                                    f'some of them being invalid.\n'
                                    f'{msg}')
            QMessageBox.critical(self, 'Invalid Changes', invalid_changes_text)
            self.data_table_widget.setUpdatesEnabled(True)
            return False

        changed_row_ids = [idx
                           for (idx, is_cell_changed_in_row_array)
                           in enumerate(self.changes_array)
                           if any(is_cell_changed_in_row_array)]
        for row_id in changed_row_ids:
            if row_id < len(self.database_rows):
                current_primary_key = self.database_rows[row_id][0]
                sql_template = self.update_sql_template % current_primary_key
            else:
                sql_template = self.insert_sql_template
            execute_db(sql_template, self.get_row_inputs(row_id))

        # We need to delete rows from bottom to top for deletions to all be
        # done correctly.
        for row_id, sql in sorted(self.queued_row_delete_sqls.items(),
                                  reverse=True):
            execute_db(sql)
            self.remove_row(row_id)
            del self.database_rows[row_id]

        self.untrack_changes()

        self.update_data_table_widget_items()

        self.data_table_widget.setUpdatesEnabled(True)
        return True

    def closeEvent(self, event: QCloseEvent):
        """
        When the dialog is closed with unsaved changes, show a prompt asking
        user what to do.

        :param event: the event emitted when the currently opened window is
            closed
        """
        if not self.has_changes():
            super().closeEvent(event)
            return

        unsaved_changes_prompt = (
            '<h3>Unsaved Changes</h3>'
            'Are you sure you want to close this editor? '
            'Any changes you made will not be saved?'
        )
        do_exit = False
        options = (QMessageBox.StandardButton.Save |
                   QMessageBox.StandardButton.Discard |
                   QMessageBox.StandardButton.Cancel)
        user_choice = QMessageBox.warning(self, 'Unsaved Changes',
                                          unsaved_changes_prompt, options)
        if user_choice != QMessageBox.StandardButton.Cancel:
            if user_choice == QMessageBox.StandardButton.Save:
                do_exit = self.save_changes(need_confirmation=False)
            elif user_choice == QMessageBox.StandardButton.Discard:
                do_exit = True

            if do_exit:
                super().closeEvent(event)
            else:
                event.ignore()
        else:
            # The exit button defaults to the cancel button if there is one, so
            # we don't need to explicitly check for the exit button.
            event.ignore()
