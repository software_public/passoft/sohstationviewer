"""
GUI to view the types of plotting and their descriptions
NOTE: plot's types are defined in __init__.plot_functions
"""

from sohstationviewer.view.util.plot_type_info import plot_types
from sohstationviewer.view.db_config.db_config_dialog import UiDBInfoDialog


class PlotTypeDialog(UiDBInfoDialog):
    def __init__(self, parent):
        super().__init__(
            parent, ['No.', '       Plot Type        ', 'Description'],
            '', '', resize_content_columns=[0, 1], check_fk=False)
        self.setWindowTitle("Plotting Types")

    def set_row_widgets(self, row_idx, fk=False):
        """
        Set the widgets in a row in self.data_table_widgets.

        :param row_idx: index of row to add
        :param fk: bool: There isn't a foreign constrain in this table but
            plot's types are based on plotting functions which are hard code
            so it's needed to set to True to prevent the row to be deleted
        """
        self.add_row_number_button(row_idx)       # No.
        self.add_widget(row_idx, 1, foreign_key=True)
        self.add_widget(row_idx, 2, foreign_key=True, field_name='description')

    def get_database_rows(self):
        """
        Get list of data to fill self.data_table_widgets' content
        """
        return [[key, val['description']]
                for key, val in plot_types.items()]
