"""
param_dialog.py
GUI to add/dit/remove params
NOTE: Cannot remove or change params that are already used for channels.
"""
from typing import Optional, List, Tuple, Union

from PySide6 import QtWidgets, QtCore
from PySide6.QtCore import Qt, Signal
from PySide6.QtWidgets import QComboBox, QWidget

from sohstationviewer.conf.constants import ColorMode, ALL_COLOR_MODES
from sohstationviewer.view.db_config.value_color_helper.value_color_edit \
    import ValueColorEdit
from sohstationviewer.view.db_config.param_helper import \
    validate_value_color_str
from sohstationviewer.view.util.plot_type_info import plot_types
from sohstationviewer.view.db_config.db_config_dialog import (
    UiDBInfoDialog,
)

from sohstationviewer.database.process_db import execute_db


class ParamDialog(UiDBInfoDialog):
    def __init__(self, parent: QWidget, color_mode: ColorMode) -> None:
        """
        :param parent: QMainWindow/QWidget - the parent widget
        :param color_mode: the initial color mode of the dialog
        """
        self.color_mode = color_mode

        self.plot_types_with_value_colors = [
            p for p in plot_types.keys()
            if 'pattern' in plot_types[p]]
        super().__init__(
            parent,
            ['No.', 'Param', 'Plot Type', 'ValueColors', 'Height    '],
            'param', 'parameters',
            resize_content_columns=[0, 2, 3, 4])

        value_colors_column = 'valueColors' + self.color_mode
        self.insert_sql_template = (f"INSERT INTO Parameters "
                                    f"(param, plotType, {value_colors_column},"
                                    f" height) VALUES (?, ?, ?, ?)")
        self.update_sql_template = (f"UPDATE Parameters SET param=?, "
                                    f"plotType=?, {value_colors_column}=?, "
                                    f"height=? "
                                    f"WHERE param='%s'")

        self.setWindowTitle("Edit/Add/Delete Parameters")
        self.add_color_selector(color_mode)

    def create_widget(self, widget_content: str,
                      choices: Optional[List] = None,
                      range_values: Optional[List] = None,
                      plot_type: Optional[str] = None, field_name: str = '',
                      row_idx: int = -1, **kwargs) -> Tuple[QWidget, Signal]:
        """
        Create a widget with the given content. The actual type of widget
        created depends on the arguments passed in. This method delegates to
        the parent's implementation unless plot_type is not None.

        :param widget_content: the content of the created widget
        :param choices: list of choices to add to a drop-down list. If this is
            available, the created widget will be a QComboBox.
        :param range_values: list consists of min and max values. If this is
            available, the created widget will be a QSpinbox.
        :param plot_type: the plot type associated with the value colors
            contain in the created widget. If this is available, the created
            widget will be a ValueColorEdit.
        :param field_name: name of the column in database.
            If field_name is 'convertFactor', a validator will be added to the
            created widget, limiting the input to numbers in the range 0.0-5.0
            with at most 6 decimal digits.
            If field_name is 'description', the created widget will be a
            QTextEdit.
        :param row_idx: the index of the row the created widget will be placed
            in. Used to determine the original plot type contained in the row.
        :return:
            - a widget whose type depends on the arguments passed in.
            - the QT signal that is emitted when the content of the returned
                widget is modified
        """
        if plot_type is not None:
            # Rows that are not in the database do not have a plot type, so we
            # consider the passed in plot type the original plot type. This
            # choice was made to allow for the easiest implementation of the
            # intended behavior.
            original_plot_type = (
                self.database_rows[row_idx][1]
                if row_idx < len(self.database_rows)
                else plot_type
            )
            # A string that is known to not be a value colors string. Only
            # used to make sure that the ValueColorEdit created later is forced
            # to use the default value colors for the passed in plot type, so
            # the actual string does not matter.
            value_colors = (
                widget_content
                if original_plot_type == plot_type
                else plot_types[plot_type].get('default_value_color', '')
            )
            errmsg_header = f"Row {row_idx}"
            if row_idx < len(self.database_rows):
                param = self.database_rows[row_idx][0]
                errmsg_header += f" - Parameter '{param}': "

            # We create a new ValueColorEdit and let the created widget decides
            # what its content should be. This is far easier than managing the
            # value we want the created widget to have.
            widget = ValueColorEdit(self.data_table_widget, self.color_mode,
                                    plot_type, value_colors, errmsg_header)
            change_signal = widget.value_color_edited
            # When widget isn't valid, value color string in the widget will
            # be set to default value, widget's value need to be marked as
            # changed.
            if not widget.is_valid:
                self.changes_array[row_idx][2] = True
        else:
            widget, change_signal = super().create_widget(
                widget_content, choices, range_values, field_name
            )
        return widget, change_signal

    def add_color_selector(self, initial_color_mode: ColorMode) -> None:
        """
        Add a color mode selector widget to the dialog.

        :param initial_color_mode: the color mode to use as the initial
        selection when the dialog is shown.
        """
        color_mode_label = QtWidgets.QLabel('Color mode:')
        color_selector = QComboBox()
        color_selector.insertItem(0, initial_color_mode)
        other_color_modes = set(ALL_COLOR_MODES.keys()) - {initial_color_mode}
        color_selector.insertItems(1, other_color_modes)
        color_selector.setFixedWidth(100)
        color_selector.currentTextChanged.connect(self.on_color_mode_changed)
        self.bottom_layout.addWidget(color_mode_label, 0, 1, 1, 1,
                                     Qt.AlignRight)
        self.bottom_layout.addWidget(color_selector, 0, 2, 1, 1)

    def set_row_widgets(self, row_idx: int, fk: bool = False) -> None:
        """
        Set the widgets in a row in self.data_table_widgets.

        :param row_idx: index of row to add
        :param fk: bool: True if there is a foreign constrain that prevents the
            row to be deleted
        """
        self.add_row_number_button(row_idx)  # No.
        self.add_widget(row_idx, 1, foreign_key=fk)
        plot_type_selector = self.add_widget(
            row_idx, 2, choices=[''] + sorted(plot_types.keys())
        )
        # ValueColorEdit was designed so that each instance only handles one
        # plot type, so we need to create a new one whenever the user changes
        # the plot type.
        plot_type_selector.currentTextChanged.connect(
            lambda new_plot_type: self.add_widget(row_idx, 3,
                                                  plot_type=new_plot_type)
        )
        self.add_widget(row_idx, 3, plot_type=plot_type_selector.currentText())

        self.add_widget(row_idx, 4, range_values=[0, 10])
        self.add_delete_button_to_row(row_idx, fk)
        self.add_reset_button_to_row(row_idx)

    def get_database_rows(self):
        """
        Get list of data to fill self.data_table_widgets' content
        """
        # The valueColors for each color mode is stored in a separate column.
        # Seeing as we only need one of these columns for a color mode, we only
        # pull the needed valueColors column from the database.
        value_colors_column = 'valueColors' + self.color_mode
        sql = (
            f"SELECT param, "
            f"IFNULL(plotType, '') AS plotType, "
            f"IFNULL({value_colors_column}, '') AS valueColors, "
            f"IFNULL(height, 0) AS height FROM Parameters")
        backup_rows = execute_db(sql, db_path='backup_db_path')
        self.backup_database_rows = [[d[0], d[1], d[2], int(d[3])]
                                     for d in backup_rows]
        param_rows = execute_db(sql)

        return [[d[0], d[1], d[2], int(d[3])]
                for d in param_rows]

    def clear_first_row(self):
        """Clear the content of the first row of the editor."""
        self.data_table_widget.cellWidget(0, 4).setValue(0)

    def set_row_content(self, row_idx: int,
                        row_content: List[Union[str, int]]):
        """
        Set content to each cell in row.
        :param row_idx: position of row to set the content
        :param row_content: list of contents of all cells in a row
        """
        self.data_table_widget.cellWidget(row_idx, 1).setText(row_content[0])
        self.data_table_widget.cellWidget(row_idx, 2).setCurrentText(
            row_content[1])
        self.data_table_widget.cellWidget(row_idx, 3).set_value_color(
            row_content[2])
        self.data_table_widget.cellWidget(row_idx, 4).setValue(row_content[3])

    def get_row_inputs(self, row_idx):
        """
        Get content from a row of widgets.
        The valueColors string's format is checked here before entered into DB

        :param row_idx: index of row
        """
        return [
            self.data_table_widget.cellWidget(row_idx, 1).text().strip(),
            self.data_table_widget.cellWidget(row_idx, 2).currentText(),
            self.data_table_widget.cellWidget(row_idx, 3).value_color_str,
            int(self.data_table_widget.cellWidget(row_idx, 4).value())
        ]

    def validate_row(self, row_id, row_content):
        """
        Check if the given row is valid. Invalid rows are those whose
        value colors does not have the correct format for the plot type.

        :return:
            - Whether the given row is valid
            - A message of what the issue is; is the empty string if there is
                no issue
        """
        is_valid_general_row, msg = super().validate_row(row_id, row_content)
        if not is_valid_general_row:
            return is_valid_general_row, msg

        param = row_content[0]
        plot_type = row_content[1]
        value_colors_string = row_content[2]
        header = f"Row {row_id} - Parameter '{param}': "
        if value_colors_string == "" and plot_type == 'multiColorDots':
            err_msg = (f"{header}multiColorDots type requires some "
                       f"ValueColors value.")
            return False, err_msg

        if value_colors_string != "":
            if plot_type in self.plot_types_with_value_colors:
                is_valid, err_msg = validate_value_color_str(
                    value_colors_string, plot_type)
                if not is_valid:
                    return False, f"{header}{err_msg}"
            else:
                if plot_type == "":
                    msg = (f"{header}No ValueColors should be entered "
                           f"when no Plot Type is selected.")
                else:
                    msg = (f"{header}No ValueColors is required for "
                           f"Plot Type {plot_type}.")
                return False, msg
        return True, ''

    @QtCore.Slot()
    def on_color_mode_changed(self, new_color_mode: ColorMode):
        """
        Slot called when the color mode is changed in the color mode selector.

        :param new_color_mode: the new color mode
        """
        old_value_colors_column = 'valueColors' + self.color_mode
        self.color_mode = new_color_mode
        new_value_colors_column = 'valueColors' + self.color_mode
        self.insert_sql_template = self.insert_sql_template.replace(
            old_value_colors_column,
            new_value_colors_column)
        self.update_sql_template = self.update_sql_template.replace(
            old_value_colors_column,
            new_value_colors_column)

        # Remove all rows in the table while keeping the widths of the columns
        # intact
        self.data_table_widget.setRowCount(0)
        # Repopulates the table with data from the database. A custom routine
        # could have been written to replace only the ValueColors column in the
        # table, but that would take too much time for only a marginal increase
        # in performance, especially considering the Parameters table is
        # expected to be pretty small.
        self.update_data_table_widget_items()
