"""
datatypedialog.py
GUI to add/edit/remove dataTypes
NOTE: Cannot remove or change dataTypes that already have channels.
"""

from sohstationviewer.view.db_config.db_config_dialog import UiDBInfoDialog
from sohstationviewer.database.process_db import execute_db


class DataTypeDialog(UiDBInfoDialog):
    def __init__(self, parent):
        super().__init__(parent, ['No.', 'DataType'], 'dataType', 'dataTypes',
                         resize_content_columns=[0])
        self.setWindowTitle("Edit/Add/Delete DataTypes")
        self.insert_sql_template = "INSERT INTO DataTypes VALUES(?)"
        self.update_sql_template = ("UPDATE DataTypes SET dataType=? "
                                    "WHERE dataType='%s'")

    def set_row_widgets(self, row_idx, fk=False):
        """
        Set the widgets in a row in self.data_table_widgets.

        :param row_idx: index of row to add
        :param fk: bool: True if there is a foreign constrain that prevents the
            row to be deleted
        """
        self.add_row_number_button(row_idx)  # No.
        self.add_widget(row_idx, 1, foreign_key=fk)
        self.add_delete_button_to_row(row_idx, fk)

    def get_database_rows(self):
        """
        Get list of data to fill self.data_table_widgets' content
        """

        data_type_rows = execute_db('SELECT * FROM DataTypes')
        return [[d[0]] for d in data_type_rows]

    def get_row_inputs(self, row_idx):
        """
        Get content from a row of widgets

        :param row_idx: index of row
        """
        return [self.data_table_widget.cellWidget(row_idx, 1).text().strip()]
