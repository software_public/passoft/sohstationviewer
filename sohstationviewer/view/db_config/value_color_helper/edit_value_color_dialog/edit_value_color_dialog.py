from PySide6 import QtWidgets, QtGui
from PySide6.QtWidgets import QWidget, QDialog


def display_color(color_label: QtWidgets.QLabel, color: str):
    """
    Display color on color_label.
            Display the given color on the color_label
        :param color_label: the label to display color
        :param color: the color that is given to update the color_label
    """
    palette = color_label.palette()
    palette.setColor(QtGui.QPalette.ColorRole.Window, QtGui.QColor(color))
    color_label.setPalette(palette)


class EditValueColorDialog(QDialog):
    """Base class for value color editing dialogs of different plot types"""
    def __init__(self, parent: QWidget, value_color_str: str):
        """
        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        """
        super(EditValueColorDialog, self).__init__(parent)
        self.value_color_str = value_color_str
        self.main_layout = QtWidgets.QGridLayout()
        self.setLayout(self.main_layout)

        self.cancel_btn = QtWidgets.QPushButton('CANCEL', self)
        self.save_colors_btn = QtWidgets.QPushButton('SAVE COLORS', self)

        self.setup_ui()
        self.set_value()
        self.connect_signals()

    def setup_ui(self):
        pass

    def set_value(self):
        pass

    def save_color(self):
        pass

    def setup_complete_buttons(self, row_total) -> None:
        """
        :param row_total: total of rows to edit
        """
        self.main_layout.addWidget(self.cancel_btn, row_total, 0, 1, 1)
        self.main_layout.addWidget(self.save_colors_btn, row_total, 3, 1, 1)

    def connect_signals(self) -> None:
        self.cancel_btn.clicked.connect(self.close)
        self.save_colors_btn.clicked.connect(self.save_color)

    def on_select_color(self, color_label: QtWidgets.QLabel):
        """
        When clicking on Select Color button, Color Picker will pop up with
            the default color is color_label's color.
        User will select a color then save to update the selected color to
            the color_label.
        :param color_label: the label that display the color of the obj_type
        """
        color = color_label.palette().window().color()
        new_color = QtWidgets.QColorDialog.getColor(color)
        if new_color.isValid():
            display_color(color_label, new_color.name())
            self.raise_()
