import sys
import platform
import os
from typing import List, Dict, Optional

from PySide6 import QtWidgets, QtCore, QtGui
from PySide6.QtWidgets import QWidget

from sohstationviewer.view.db_config.value_color_helper.\
    edit_value_color_dialog.edit_value_color_dialog import \
    EditValueColorDialog, display_color

from sohstationviewer.conf.constants import SIZE_PIXEL_MAP


ROW_TOTAL = 7


class BoundValidator(QtGui.QValidator):
    """
    Validator that allow float value and empty string.
    Range of value is from -10, 10 because -20/20 are the values assigned to
        higher bounds of the previous/next when the current row is 0 and last
        so the editable higher bounds can't ever be reached to.
    Value '-'  to allow typing negative float. If user type '-' only, it will
        be checked when editing is finished.
    """
    def validate(self, input_val, pos):
        if input_val in ['', '-']:
            return QtGui.QValidator.State.Acceptable
        try:
            input_val = float(input_val)
        except ValueError:
            return QtGui.QValidator.State.Invalid
        if -10 <= input_val <= 10:
            return QtGui.QValidator.State.Acceptable
        else:
            return QtGui.QValidator.State.Invalid


class MultiColorDotDialog(EditValueColorDialog):
    def __init__(
            self, parent: Optional[QWidget],
            value_color_str: str, upper_equal: bool):
        """
        Dialog to edit color for Multi-color Dot Plot

        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        :param upper_equal: flag to know if equal is on upper bound
            or lower_bound
        """
        self.upper_equal = upper_equal
        # list of combo boxes to display sizes of points
        self.size_comboboxes: List[QtWidgets.QComboBox] = []
        # list of widgets to display lower bound which is read only of the
        # value range of rows
        self.lower_bound_lnedits: List[QtWidgets.QLineEdit] = []
        # list of widgets to display lower bound of the value range of rows
        self.higher_bound_lnedits: List[QtWidgets.QLineEdit] = []
        # List of buttons that allow user to add/edit color
        self.select_color_btns: List[QtWidgets.QPushButton] = []
        # list of labels to display color of the data points defined by
        # the value range of rows
        self.color_labels: List[QtWidgets.QLabel] = []

        # After a higher bound widget is edited and enter is hitted, button's
        # clicked signal will be called before higher_bound_editting_finished
        # is reached. The following flags are to change that order.
        # These flags are also used to prevent the case hitting Select Color or
        # Save right after user done with editing. In case there is error when
        # checking the value, the button click will be canceled too.
        self.changed_row_id = None
        self.select_color_btn_clicked = False
        self.save_colors_btn_clicked = False

        # Check box for plotting data point less than all the rest
        self.include_less_than_chkbox = QtWidgets.QCheckBox('Include')

        # Check box for including data point greater than all the rest
        self.include_greater_than_chkbox = QtWidgets.QCheckBox('Include')

        # Keep track of value by row_id
        self.row_id_value_dict: Dict[int, float] = {}

        super(MultiColorDotDialog, self).__init__(parent, value_color_str)
        self.setWindowTitle("Edit Multi Color Dot Plotting")

        # set focus policy to not be clicked by hitting enter in higher bound
        self.cancel_btn.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.save_colors_btn.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)

        self.set_value_row_id_dict()

    def setup_ui(self) -> None:

        for i in range(ROW_TOTAL):
            if i == 0:
                self.main_layout.addWidget(
                    self.include_less_than_chkbox, 0, 0, 1, 1)
                self.include_less_than_chkbox.setChecked(True)
            if i == ROW_TOTAL - 1:
                self.main_layout.addWidget(
                    self.include_greater_than_chkbox, ROW_TOTAL - 1, 0, 1, 1)
            (size_combobox, lower_bound_lnedit, higher_bound_lnedit,
             select_color_btn, color_label) = self.add_row(i)
            if i == 0:
                # We provide a default value for the first higher bound editor
                # because it is the only one that is included in the saved
                # value colors string by default.
                higher_bound_lnedit.setText('0')
            self.size_comboboxes.append(size_combobox)
            self.lower_bound_lnedits.append(lower_bound_lnedit)
            self.higher_bound_lnedits.append(higher_bound_lnedit)
            self.select_color_btns.append(select_color_btn)
            self.color_labels.append(color_label)
            self.set_color_enabled(i, False)

        self.setup_complete_buttons(ROW_TOTAL)

    def connect_signals(self) -> None:
        self.include_greater_than_chkbox.clicked.connect(
            lambda: self.on_include(ROW_TOTAL - 1,
                                    self.include_greater_than_chkbox))
        self.include_less_than_chkbox.clicked.connect(
            lambda: self.on_include(0, self.include_less_than_chkbox))

        for i in range(ROW_TOTAL):
            self.higher_bound_lnedits[i].textChanged.connect(
                lambda arg, idx=i: setattr(self, 'changed_row_id', idx))
            self.higher_bound_lnedits[i].editingFinished.connect(
                lambda idx=i: self.on_higher_bound_editing_finished(idx))
            # some how pass False to on_select_color, have to handle this
            # in add_row() instead
            # self.select_color_btns[i].clicked.connect(
            #     lambda idx=i: self.on_select_color(idx))

        super().connect_signals()

    def get_comparing_text(self, row_id):
        pass

    def add_row(self, row_id):
        """
        Adding a row including Lower bound line dit, comparing operator label,
            higher bound line edit, select color button, display color label
        :param row_id: id of current row
        """
        size_combobox = QtWidgets.QComboBox()
        size_combobox.addItems(SIZE_PIXEL_MAP.keys())
        size_combobox.setCurrentText('normal')

        lower_bound_lnedit = QtWidgets.QLineEdit()
        lower_bound_lnedit.setEnabled(False)

        comparing_text = self.get_comparing_text(row_id)
        comparing_label = QtWidgets.QLabel(comparing_text)

        higher_bound_lnedit = QtWidgets.QLineEdit()
        validator = BoundValidator()
        higher_bound_lnedit.setValidator(validator)

        if row_id == 0:
            lower_bound_lnedit.setHidden(True)
        elif row_id == ROW_TOTAL - 1:
            higher_bound_lnedit.setHidden(True)

        select_color_btn = QtWidgets.QPushButton("Select Color")
        select_color_btn.clicked.connect(
            lambda: self.on_select_color(row_id))
        # set focus policy to not be clicked by hitting enter in higher bound
        select_color_btn.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)

        color_label = QtWidgets.QLabel()
        color_label.setFixedWidth(30)
        color_label.setAutoFillBackground(True)

        # layout
        self.main_layout.addWidget(QtWidgets.QLabel('Size'), row_id, 1, 1, 1)
        self.main_layout.addWidget(size_combobox, row_id, 2, 1, 1)
        self.main_layout.addWidget(lower_bound_lnedit, row_id, 3, 1, 1)
        self.main_layout.addWidget(comparing_label, row_id, 4, 1, 1)
        self.main_layout.addWidget(higher_bound_lnedit, row_id, 5, 1, 1)
        self.main_layout.addWidget(select_color_btn, row_id, 6, 1, 1)
        self.main_layout.addWidget(color_label, row_id, 7, 1, 1)

        return (size_combobox, lower_bound_lnedit, higher_bound_lnedit,
                select_color_btn, color_label)

    def handle_clear_higher_bound(self, row_id):
        """
        If user clear higher_bound
        + in the middle, not allow.
        + of the only row, not allow.
        + in the last edited row, the lower_bound for the next row need to be
        cleared, and the very end row's value will be set to the higher_bound
        of the previous row.
        """
        if row_id < len(self.row_id_value_dict) - 1:
            # If the cleared one isn't the last one, it shouldn't be
            # allowed. An Error message should be given.
            msg = "Higher bound must be a number"
            QtWidgets.QMessageBox.information(self, "Error", msg)
            self.higher_bound_lnedits[row_id].setText(
                str(self.row_id_value_dict[row_id]))
        else:
            # If the cleared one is the last one, the lower_bound_lnedit
            # of the next row will be cleared too.
            if row_id == 0:
                # If the cleared one is the only one, this shouldn't be
                # allowed. A warning should be given.
                msg = ("There should be at least one value"
                       " for this plot type.")
                QtWidgets.QMessageBox.information(self, "Error", msg)
                return
            self.set_color_enabled(row_id, False)
            self.lower_bound_lnedits[row_id + 1].setText('')
            self.set_value_row_id_dict()

    def handle_skipping_editing_row(self, row_id):
        """
        If user add value in a row that skipping from the last edited row,
        it shouldn't be allowed, an Error message should be given before the
        higher_bound is cleared, the actions will ignite the call to this
        function one more time which should be ignored.
        """
        if self.higher_bound_lnedits[row_id].text() == '':
            # called by clearing text in this section
            return
        self.changed_row_id = None
        # When user edit the row that skips some row from the last row
        # the current higher_bound_lnedit will be cleared
        msg = ("You have to edit rows in order.\n"
               "Skipping rows isn't allowed.")
        QtWidgets.QMessageBox.information(self, "Error", msg)
        self.higher_bound_lnedits[row_id].setText('')

    def handle_edited_value_not_ascending(
            self, row_id, prev_higher_bound, next_higher_bound):
        """
        If value enter to the current higher_bound_lnedit make it out of
        increasing sorting, a warning will be given, and the widget will be
        set to the original value.
        :param row_id: id of current row
        :param prev_higher_bound: higher_bound value of the previous row
        :param next_higher_bound: higher_bound value of the next row
        """

        cond = (f"{prev_higher_bound} < "
                if prev_higher_bound != -20 else '')
        cond += (f"d < {next_higher_bound}"
                 if next_higher_bound != 20 else 'd')
        msg = f"Value d entered must be: {cond}"
        QtWidgets.QMessageBox.information(self, "Error", msg)
        try:
            self.higher_bound_lnedits[row_id].setText(
                str(self.row_id_value_dict[row_id]))
        except KeyError:
            # If the current row is the last one, there's no original value
            # So the widget will be cleared.
            self.higher_bound_lnedits[row_id].setText('')
        self.select_color_btn_clicked = False
        self.save_colors_btn_clicked = False

    def on_higher_bound_editing_finished(self, row_id: int):
        """
        Check value entered for higher bound:
            + If the last row is empty string, that value will be eliminated
                from condition.
            + If value not greater than previous row and less than latter row,
                give error message.
        Call set_value_row_id_dict to keep track of value by row_id.

        :param row_id: id of the row being checked.
            row_id == self.changed_row_id when the method is ignited by
            editing the row's value (not by clicking on some buttons)
        """
        if self.changed_row_id is None:
            # When the function isn't called from user's changing text on
            # a higher_bound_lnedit, this function will be ignored.
            return

        if len(self.row_id_value_dict) < self.changed_row_id < ROW_TOTAL - 1:
            self.handle_skipping_editing_row(row_id)
            return

        self.changed_row_id = None

        # -20 is assigned to higher bound of the row before the first row
        # to always satisfy validating this row's higher bound value for value
        # range is [-10,10]
        prev_higher_bound = (
            -20 if row_id == 0
            else float(self.higher_bound_lnedits[row_id - 1].text()))

        if self.higher_bound_lnedits[row_id].text().strip() == '':
            self.handle_clear_higher_bound(row_id)
            self.select_color_btn_clicked = False
            self.save_colors_btn_clicked = False
            return

        # 20 is assigned to higher bound of the row after the last edited row
        # to always satisfy validating this row's higher bound value for value
        # range is [-10,10]
        next_higher_bound = (
            20 if row_id >= len(self.row_id_value_dict) - 1
            else float(self.higher_bound_lnedits[row_id + 1].text()))

        curr_higher_bound = float(self.higher_bound_lnedits[row_id].text())
        if (curr_higher_bound <= prev_higher_bound
                or curr_higher_bound >= next_higher_bound):
            self.handle_edited_value_not_ascending(row_id,
                                                   prev_higher_bound,
                                                   next_higher_bound)
            return
        if ((row_id == 0 and self.include_less_than_chkbox.isChecked())
                or row_id != 0):
            # Enable button Select Color unless the row is the first row but
            # Include checkbox isn't checked
            self.set_color_enabled(row_id, True)
        self.lower_bound_lnedits[row_id + 1].setText(str(curr_higher_bound))
        self.set_value_row_id_dict()
        if self.save_colors_btn_clicked:
            self.save_color()
        if self.select_color_btn_clicked:
            self.on_select_color(row_id)

    def set_value_row_id_dict(self):
        """
        Update row_id_value_dict to the current higher bound
        Update lower bound of the last row which is for greater than all the
            rest to be the max of all higher bound
        """
        self.row_id_value_dict = {i: float(self.higher_bound_lnedits[i].text())
                                  for i in range(ROW_TOTAL - 1)
                                  if self.higher_bound_lnedits[i].text() != ''}
        last_row_lnedit = self.lower_bound_lnedits[ROW_TOTAL - 1]

        if len(self.row_id_value_dict) == 0:
            last_row_lnedit.clear()
        else:
            last_row_lnedit.setText(str(max(self.row_id_value_dict.values())))

    def set_color_enabled(self, row_id: int, enabled: bool):
        """
        Enable color: allow to edit and display color
        Disable color: disallow to edit and hide color
        """
        self.select_color_btns[row_id].setEnabled(enabled)
        self.color_labels[row_id].setHidden(not enabled)

    def on_select_color(self, row_id: int):
        """
        When clicking on Select Color button, Color Picker will pop up with
            the default color is color_label's color.
        User will select a color then save to update the selected color to
            the color_label.
        """
        if self.changed_row_id is not None:
            self.select_color_btn_clicked = True
            self.on_higher_bound_editing_finished(self.changed_row_id)
            return
        self.select_color_btn_clicked = False
        super().on_select_color(self.color_labels[row_id])

    def on_include(self, row_id: int, chkbox: QtWidgets.QCheckBox):
        """
        Enable/Disable Select Color button when include_less_than_chkbox/
        include_greater_than_chkbox is checked/unchecked.
        """
        self.set_color_enabled(row_id, chkbox.isChecked())

    def set_row(self, vc_idx: int, value: float, color: str, size_desc: str):
        """
        Add values to widgets in a row
            + row 0: consider uncheck include checkbox if color='not plot' and
                check otherwise.
            + row TOTAL-1: check include checkbox and set value for lower bound
            + all row other than TOTAL-1, set value for higher bound and
                next row's lower bound, enable and display color
        """
        if vc_idx == 0:
            if color == 'not plot':
                self.include_less_than_chkbox.setChecked(False)
            else:
                self.include_less_than_chkbox.setChecked(True)

        self.size_comboboxes[vc_idx].setCurrentText(size_desc)

        if vc_idx < ROW_TOTAL - 1:
            self.higher_bound_lnedits[vc_idx].setText(str(value))
            self.lower_bound_lnedits[vc_idx + 1].setText(str(value))
        else:
            self.include_greater_than_chkbox.setChecked(True)
            self.lower_bound_lnedits[ROW_TOTAL - 1].setText(str(value))

        if color == 'not plot':
            self.set_color_enabled(vc_idx, False)
        else:
            display_color(self.color_labels[vc_idx], color)
            self.set_color_enabled(vc_idx, True)

    def save_color(self):
        """
        Create value_color_str from GUI's info and close the GUI.
            + Skip row that has no color
            + color = color_label's color name
            + if include_less_than_chkbox is not checked, 'not_plot' will be
                set for the first color
            + Format for value_color of all row other than TOTAL - 1th:
                <=value:color
            + If include_greater_than_chkbox is checked, format will be:
                value<:color
        """
        if self.changed_row_id and self.changed_row_id < ROW_TOTAL - 1:
            self.save_colors_btn_clicked = True
            self.on_higher_bound_editing_finished(self.changed_row_id)
            return
        self.save_colors_btn_clicked = False
        value_color_list = []
        for i in range(ROW_TOTAL - 1):
            if self.color_labels[i].isHidden() and i != 0:
                continue
            value = self.higher_bound_lnedits[i].text()
            color = self.color_labels[i].palette(
            ).window().color().name().upper()
            if i == 0 and not self.include_less_than_chkbox.isChecked():
                color = 'not plot'
            operator = '<=' if self.upper_equal else '<'
            size_desc = self.size_comboboxes[i].currentText()
            size = f':{size_desc}' if size_desc != 'normal' else ''

            value_color_list.append(f"{operator}{value}:{color}{size}")

        if self.include_greater_than_chkbox.isChecked():
            color = self.color_labels[ROW_TOTAL - 1].palette().window(
                ).color().name().upper()
            val = f"{self.lower_bound_lnedits[ROW_TOTAL - 1].text()}"
            size_desc = self.size_comboboxes[ROW_TOTAL - 1].currentText()
            size = f':{size_desc}' if size_desc != 'normal' else ''

            if self.upper_equal:
                value_color_list.append(f"{val}<:{color}{size}")
            else:
                value_color_list.append(f"={val}:{color}{size}")

        self.value_color_str = '|'.join(value_color_list)
        self.accept()


class MultiColorDotLowerEqualDialog(MultiColorDotDialog):
    def __init__(self, parent, value_color_str):
        super(MultiColorDotLowerEqualDialog, self).__init__(
            parent, value_color_str, upper_equal=False)

    def get_comparing_text(self, row_id: int):
        """
        Create text that show relationship between lower_bound and higher_bound
        to data for the selected color.
        :param row_id: id of current row
        """
        if row_id == 0:
            comparing_text = "      d <"
        elif row_id < ROW_TOTAL - 1:
            comparing_text = "<= d <"
        else:
            comparing_text = "=   d"
        return comparing_text

    def set_value(self):
        """
        Change the corresponding color_labels's color, higher/lower bound,
            include check boxes according to value_color_str.
        """
        self.include_greater_than_chkbox.setChecked(False)
        self.include_less_than_chkbox.setChecked(False)
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        count = 0
        for vc_str in vc_parts:
            vcs = vc_str.split(':')
            value = vcs[0]
            color = vcs[1]
            size_desc = vcs[2] if len(vcs) > 2 else 'normal'
            if value.startswith('<'):
                # Ex: <1:#00FFFF
                # Ex: <0:not plot  (can be on first value only)
                value = value.replace('<', '')
                self.set_row(count, float(value), color, size_desc)
                count += 1
            else:
                # Ex: =1:#FF00FF
                value = value.replace('=', '')
                self.set_row(ROW_TOTAL - 1, float(value), color, size_desc)


class MultiColorDotUpperEqualDialog(MultiColorDotDialog):
    def __init__(self, parent, value_color_str):
        super(MultiColorDotUpperEqualDialog, self).__init__(
            parent, value_color_str, upper_equal=True)

    def get_comparing_text(self, row_id):
        """
        Create text that show relationship between lower_bound and higher_bound
        to data for the selected color.
        :param row_id: id of current row
        """
        if row_id == 0:
            comparing_text = "   d <="
        elif row_id < ROW_TOTAL - 1:
            comparing_text = "< d <="
        else:
            comparing_text = "< d   "
        return comparing_text

    def set_value(self):
        """
        Change the corresponding color_labels's color, higher/lower bound,
            include check boxes according to value_color_str.
        """
        self.include_greater_than_chkbox.setChecked(False)
        self.include_less_than_chkbox.setChecked(False)
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        count = 0
        for vc_str in vc_parts:
            vcs = vc_str.split(':')
            value = vcs[0]
            color = vcs[1]
            size_desc = vcs[2] if len(vcs) > 2 else 'normal'
            if value.startswith('<='):
                # Ex: <=1:#00FFFF
                # Ex: <=0:not plot  (can be on first value only)
                value = value.replace('<=', '')
                self.set_row(count, float(value), color, size_desc)
                count += 1
            else:
                # Ex: 1<:#FF00FF
                value = value.replace('<', '')
                self.set_row(ROW_TOTAL - 1, float(value), color, size_desc)


if __name__ == '__main__':
    os_name, version, *_ = platform.platform().split('-')
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    # test = MultiColorDotLowerEqualDialog(
    #     None, '<3:#FF0000|<3.3:#FFFF00|=3.3:#00FF00')
    test = MultiColorDotUpperEqualDialog(
        None, '<=0:not plot|<=1:#FFFF00|<=2:#00FF00|2<:#FF00FF')
    test.exec_()
    print("result:", test.value_color_str)
    sys.exit(app.exec_())
