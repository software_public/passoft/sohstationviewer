import sys
import platform
import os

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget

from sohstationviewer.view.db_config.value_color_helper.\
    edit_value_color_dialog.edit_value_color_dialog import \
    EditValueColorDialog, display_color


class UpDownDialog(EditValueColorDialog):
    """Dialog to edit color for Up/Down Plot"""
    def __init__(self, parent: QWidget, value_color_str: str):
        """
        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        """
        # Widget that allow user to add/edit up's color
        self.select_up_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display up's color
        self.up_color_label = QtWidgets.QLabel()
        self.up_color_label.setFixedWidth(30)
        self.up_color_label.setAutoFillBackground(True)
        # Widget that allow user to add/edit down's color
        self.select_down_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display down's color
        self.down_color_label = QtWidgets.QLabel()
        self.down_color_label.setFixedWidth(30)
        self.down_color_label.setAutoFillBackground(True)

        super(UpDownDialog, self).__init__(parent, value_color_str)
        self.setWindowTitle("Edit Up/Down Plotting's Colors")

    def setup_ui(self) -> None:
        self.main_layout.addWidget(QtWidgets.QLabel('Up Color'), 0, 0, 1, 1)
        self.main_layout.addWidget(self.up_color_label, 0, 1, 1, 1)
        self.main_layout.addWidget(self.select_up_color_btn, 0, 2, 1, 1)

        self.main_layout.addWidget(QtWidgets.QLabel('Down Color'), 1, 0, 1, 1)
        self.main_layout.addWidget(self.down_color_label, 1, 1, 1, 1)
        self.main_layout.addWidget(self.select_down_color_btn, 1, 2, 1, 1)

        self.setup_complete_buttons(2)

    def connect_signals(self) -> None:
        self.select_up_color_btn.clicked.connect(
            lambda: self.on_select_color(self.up_color_label))
        self.select_down_color_btn.clicked.connect(
            lambda: self.on_select_color(self.down_color_label))
        super().connect_signals()

    def set_value(self):
        """
        Change the corresponding color_labels's color according to the color
            from value_color_str.
        """
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        for vc_str in vc_parts:
            obj_type, color = vc_str.split(':')
            if obj_type == 'Up':
                display_color(self.up_color_label, color)
            if obj_type == 'Down':
                display_color(self.down_color_label, color)

    def save_color(self):
        """
        Create value_color_str from GUI's info and close the GUI with color
            is the hex color got from color_labels' color
        """
        up_color = self.up_color_label.palette().window().color().name()
        down_color = self.down_color_label.palette().window().color().name()
        self.value_color_str = (f"Up:{up_color.upper()}"
                                f"|Down:{down_color.upper()}")
        self.accept()


if __name__ == '__main__':
    os_name, version, *_ = platform.platform().split('-')
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    test = UpDownDialog(None, 'Down:#FF0000|Up:#00FF00')
    test.exec_()
    print("result:", test.value_color_str)
    sys.exit(app.exec_())
