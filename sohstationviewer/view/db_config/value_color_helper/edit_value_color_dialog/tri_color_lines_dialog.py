import sys
import platform
import os

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget

from sohstationviewer.view.db_config.value_color_helper.\
    edit_value_color_dialog.edit_value_color_dialog import \
    EditValueColorDialog, display_color


class TriColorLinesDialog(EditValueColorDialog):
    """Dialog to edit color for triColorLines plot"""
    def __init__(self, parent: QWidget, value_color_str: str):
        """
        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        """
        # Widget that allow user to add/edit value positive one's color
        self.select_pos_one_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display positive one's color
        self.pos_one_color_label = QtWidgets.QLabel()
        self.pos_one_color_label.setFixedWidth(30)
        self.pos_one_color_label.setAutoFillBackground(True)

        # Widget that allow user to add/edit value zero's color
        self.select_zero_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display down's color
        self.zero_color_label = QtWidgets.QLabel()
        self.zero_color_label.setFixedWidth(30)
        self.zero_color_label.setAutoFillBackground(True)

        # Widget that allow user to add/edit value positive one's color
        self.select_neg_one_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display positive one's color
        self.neg_one_color_label = QtWidgets.QLabel()
        self.neg_one_color_label.setFixedWidth(30)
        self.neg_one_color_label.setAutoFillBackground(True)

        super(TriColorLinesDialog, self).__init__(parent, value_color_str)
        self.setWindowTitle("Edit TriColor Plotting's Colors")

    def setup_ui(self) -> None:
        self.main_layout.addWidget(QtWidgets.QLabel('"  1" Color'), 0, 0, 1, 1)
        self.main_layout.addWidget(self.pos_one_color_label, 0, 1, 1, 1)
        self.main_layout.addWidget(self.select_pos_one_color_btn, 0, 2, 1, 1)

        self.main_layout.addWidget(QtWidgets.QLabel('"  0" Color'), 1, 0, 1, 1)
        self.main_layout.addWidget(self.zero_color_label, 1, 1, 1, 1)
        self.main_layout.addWidget(self.select_zero_color_btn, 1, 2, 1, 1)

        self.main_layout.addWidget(QtWidgets.QLabel('"-1" Color'), 2, 0, 1, 1)
        self.main_layout.addWidget(self.neg_one_color_label, 2, 1, 1, 1)
        self.main_layout.addWidget(self.select_neg_one_color_btn, 2, 2, 1, 1)

        self.setup_complete_buttons(3)

    def connect_signals(self) -> None:
        self.select_pos_one_color_btn.clicked.connect(
            lambda: self.on_select_color(self.pos_one_color_label))
        self.select_zero_color_btn.clicked.connect(
            lambda: self.on_select_color(self.zero_color_label))
        self.select_neg_one_color_btn.clicked.connect(
            lambda: self.on_select_color(self.neg_one_color_label))
        super().connect_signals()

    def set_value(self):
        """
        Change the corresponding color_labels's color according to the color
            from value_color_str.
        """
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        for vc_str in vc_parts:
            val, color = vc_str.split(':')
            if val == '1':
                display_color(self.pos_one_color_label, color)
            if val == '0':
                display_color(self.zero_color_label, color)
            if val == '-1':
                display_color(self.neg_one_color_label, color)

    def save_color(self):
        """
        Create value_color_str from GUI's info and close the GUI with color
            is the hex color got from color_labels' color
        """
        pos_one_color = self.pos_one_color_label.palette()\
            .window().color().name()
        zero_color = self.zero_color_label.palette().window().color().name()
        neg_one_color = self.neg_one_color_label.palette() \
            .window().color().name()

        self.value_color_str = (f"-1:{neg_one_color.upper()}"
                                f"|0:{zero_color.upper()}"
                                f"|1:{pos_one_color.upper()}")
        self.accept()


if __name__ == '__main__':
    os_name, version, *_ = platform.platform().split('-')
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    test = TriColorLinesDialog(None, '-1:#FF00FF|0:#FF0000|1:#00FF00')
    test.exec_()
    print("result:", test.value_color_str)
    sys.exit(app.exec_())
