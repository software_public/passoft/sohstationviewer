from typing import Optional

from PySide6.QtWidgets import QWidget, QPushButton, QLabel

from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog.edit_value_color_dialog import (
        EditValueColorDialog, display_color,
    )


class DotForTimeDialog(EditValueColorDialog):
    def __init__(self, parent: Optional[QWidget], value_color_str: str):
        """
        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        """
        # Widget that allow user to add/edit down's color
        self.select_color_button = QPushButton("Select Color")
        # Widget to display down's color
        self.color_label = QLabel()
        self.color_label.setFixedWidth(30)
        self.color_label.setAutoFillBackground(True)

        super().__init__(parent, value_color_str)
        self.setWindowTitle("Edit Dot For Time Plot's Colors")

    def setup_ui(self):
        self.main_layout.addWidget(QLabel('Dot Color'), 0, 0, 1, 1)
        self.main_layout.addWidget(self.color_label, 0, 1, 1, 1)
        self.main_layout.addWidget(self.select_color_button, 0, 2, 1, 1)

        self.setup_complete_buttons(1)

    def connect_signals(self) -> None:
        self.select_color_button.clicked.connect(
            lambda: self.on_select_color(self.color_label))
        super().connect_signals()

    def set_value(self):
        """
        Change the corresponding color_labels's color according to the color
            from value_color_str.
        """
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        for vc_str in vc_parts:
            obj_type, color = vc_str.split(':')
            display_color(self.color_label, color)

    def save_color(self):
        """
        Create value_color_str from GUI's info and close the GUI with color
            is the hex color got from color_labels' color
        """
        color = self.color_label.palette().window().color().name()
        self.value_color_str = f"Color:{color.upper()}"
        self.accept()
