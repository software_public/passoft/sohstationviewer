import sys
import platform
import os

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget

from sohstationviewer.view.db_config.value_color_helper.\
    edit_value_color_dialog.edit_value_color_dialog import \
    EditValueColorDialog, display_color


class LineDotDialog(EditValueColorDialog):
    """Dialog to edit color for Line/Dot Plot"""
    def __init__(self, parent: QWidget, value_color_str: str):
        """
        :param parent: the parent widget
        :param value_color_str: string for value color to be saved in DB
        """
        # Widget that allow user to add/edit line's color
        self.select_line_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display line's color
        self.line_color_label = QtWidgets.QLabel()
        self.line_color_label.setFixedWidth(30)
        self.line_color_label.setAutoFillBackground(True)

        # check box to include dot in value_color_str or not
        self.dot_include_chkbox = QtWidgets.QCheckBox('Included')
        # Widget that allow user to add/edit dot's color
        self.select_dot_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display dot's color
        self.dot_color_label = QtWidgets.QLabel()
        self.dot_color_label.setFixedWidth(30)
        self.dot_color_label.setAutoFillBackground(True)

        # check box to include zero in value_color_str or not
        self.zero_include_chkbox = QtWidgets.QCheckBox('Included')
        # Widget that allow user to add/edit zero's color
        self.select_zero_color_btn = QtWidgets.QPushButton("Select Color")
        # Widget to display dot's color
        self.zero_color_label = QtWidgets.QLabel()
        self.zero_color_label.setFixedWidth(30)
        self.zero_color_label.setAutoFillBackground(True)

        super(LineDotDialog, self).__init__(parent, value_color_str)
        self.setWindowTitle("Edit Line/Dot Plotting's Colors")

        self.on_click_include_dot()

    def setup_ui(self) -> None:
        self.main_layout.addWidget(QtWidgets.QLabel('Line Color'), 0, 1, 1, 1)
        self.main_layout.addWidget(self.line_color_label, 0, 2, 1, 1)
        self.main_layout.addWidget(self.select_line_color_btn, 0, 3, 1, 1)

        self.main_layout.addWidget(self.dot_include_chkbox, 1, 0, 1, 1)
        self.main_layout.addWidget(QtWidgets.QLabel('Dot Color'), 1, 1, 1, 1)
        self.main_layout.addWidget(self.dot_color_label, 1, 2, 1, 1)
        self.main_layout.addWidget(self.select_dot_color_btn, 1, 3, 1, 1)

        self.main_layout.addWidget(self.zero_include_chkbox, 2, 0, 1, 1)
        self.main_layout.addWidget(QtWidgets.QLabel('Zero Color'), 2, 1, 1, 1)
        self.main_layout.addWidget(self.zero_color_label, 2, 2, 1, 1)
        self.main_layout.addWidget(self.select_zero_color_btn, 2, 3, 1, 1)

        self.setup_complete_buttons(3)

    def connect_signals(self) -> None:
        self.select_line_color_btn.clicked.connect(
            lambda: self.on_select_color(self.line_color_label))
        self.select_dot_color_btn.clicked.connect(
            lambda: self.on_select_color(self.dot_color_label))
        self.dot_include_chkbox.clicked.connect(self.on_click_include_dot)
        self.select_zero_color_btn.clicked.connect(
            lambda: self.on_select_color(self.zero_color_label))
        self.zero_include_chkbox.clicked.connect(self.on_click_include_zero)
        super().connect_signals()

    def on_click_include_dot(self):
        """
        Enable/disable select color and show/hide color label according to
            dot_include_chkbox is checked or unchecked.
        """
        enabled = self.dot_include_chkbox.isChecked()
        self.select_dot_color_btn.setEnabled(enabled)
        self.dot_color_label.setHidden(not enabled)

    def on_click_include_zero(self):
        """
        Enable/disable select color and show/hide color label according to
            dot_include_chkbox is checked or unchecked.
        """
        enabled = self.zero_include_chkbox.isChecked()
        self.select_zero_color_btn.setEnabled(enabled)
        self.zero_color_label.setHidden(not enabled)

    def set_value(self):
        """
        Change the corresponding color_labels's color according to the color
            from value_color_str.
        """
        self.dot_include_chkbox.setChecked(False)
        self.zero_include_chkbox.setChecked(False)
        if self.value_color_str == "":
            return
        vc_parts = self.value_color_str.split('|')
        for vc_str in vc_parts:
            obj_type, color = vc_str.split(':')
            if obj_type == 'Line':
                display_color(self.line_color_label, color)
            if obj_type == 'Dot':
                display_color(self.dot_color_label, color)
                self.dot_include_chkbox.setChecked(True)
            if obj_type == 'Zero':
                display_color(self.zero_color_label, color)
                self.zero_include_chkbox.setChecked(True)
        self.on_click_include_dot()
        self.on_click_include_zero()

    def save_color(self):
        """
        Create value_color_str from GUI's info and close the GUI with color
            is the hex color got from color_labels' color
        """
        line_color = self.line_color_label.palette().window().color().name()
        self.value_color_str = f"Line:{line_color.upper()}"
        if self.dot_include_chkbox.isChecked():
            dot_color = self.dot_color_label.palette().window().color().name()
            self.value_color_str += f"|Dot:{dot_color.upper()}"
        if self.zero_include_chkbox.isChecked():
            zero_color = \
                self.zero_color_label.palette().window().color().name()
            self.value_color_str += f"|Zero:{zero_color.upper()}"
        self.accept()


if __name__ == '__main__':
    os_name, version, *_ = platform.platform().split('-')
    if os_name == 'macOS':
        os.environ['QT_MAC_WANTS_LAYER'] = '1'
    app = QtWidgets.QApplication(sys.argv)
    test = LineDotDialog(None, "Line:#00FF00|Dot:#00FF00")
    test.exec_()
    print("result:", test.value_color_str)
    sys.exit(app.exec_())
