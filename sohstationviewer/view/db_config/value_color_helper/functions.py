import re

from sohstationviewer.view.util.plot_type_info import plot_types
from sohstationviewer.conf.constants import SIZE_UNICODE_MAP


def convert_value(plot_type: str, old_value: str):
    """
    Convert value part in value color str to new format
    :param plot_type: type of channel's plot
    :param old_value: value in old format
    :return: value in new format
    """
    value_pattern = plot_types[plot_type].get('value_pattern',
                                              re.compile('.?'))
    if not value_pattern.match(old_value):
        raise ValueError(f'The old value does not match the pattern for plot '
                         f'type {plot_type}.')

    if old_value in ['L', 'Line'] and plot_type == 'linesDots':
        new_value = 'Line'
    elif old_value in ['D', 'Dot'] and plot_type == 'linesDots':
        new_value = 'Dot'
    elif old_value in ['Z', 'Zero'] and plot_type == 'linesDots':
        new_value = 'Zero'
    elif old_value in ['1', 'Up'] and plot_type == 'upDownDots':
        new_value = 'Up'
    elif old_value in ['0', 'Down'] and plot_type == 'upDownDots':
        new_value = 'Down'
    elif old_value in ['C', 'Color'] and plot_type == 'dotForTime':
        new_value = 'Color'
    elif plot_type == "multiColorDotsEqualOnUpperBound":
        if old_value.startswith('+'):
            new_value = old_value[1:] + '<'
        elif old_value.startswith('<=') or old_value.endswith('<'):
            new_value = old_value
        else:
            new_value = '<=' + old_value
    elif plot_type == "multiColorDotsEqualOnLowerBound":
        if old_value.startswith('=') or old_value.startswith('<'):
            new_value = old_value
        else:
            new_value = '<' + old_value
    elif plot_type == 'triColorLines':
        new_value = old_value
    else:
        raise ValueError(f'Something went wrong while converting the value '
                         f'part of a valueColors. Please make sure '
                         f'{plot_type} is a valid plot type and {old_value} '
                         f'is a valid value part of a valueColors for the '
                         f'plot type.')
    return new_value


def prepare_value_color_html(value_colors: str) -> str:
    """
    Change value_color with hex color to html to square with actual color from
        hex color.
    :param value_colors: string for value color to be saved in DB.
        Possible formats
            Line:color|Dot:color
            Up:color|Down:color
            <=value:color|value<:color
        (color can be hex color '#00FFF' or 'not plot')
    :return: value color in html
        Ex: <p>Line:<span style='#00FFFF; font-size:25px;'>&#8718;</span>|
             Dot:<span style='#FF0000; font-size:25px;'>&#8718;</span></p>
    """
    if value_colors == '':
        return ''
    html_color_parts = []
    color_parts = value_colors.split('|')
    for c_str in color_parts:
        vcs = c_str.split(':')
        value = vcs[0].replace('<=', '&le;').replace('<', '&lt;')
        size = f':{SIZE_UNICODE_MAP[vcs[2]]}' if len(vcs) > 2 else ''
        if vcs[1] == 'not plot':
            c_html = f"{value}:not plot"
        else:
            c_html = (
                f"{value}:"
                f"<span style='color: {vcs[1]}; font-size:18px;'>&#9632;"
                f"</span>{size}")

        html_color_parts.append(c_html)
    value_color_html = f"<p>{'|'.join(html_color_parts)}</p>"
    return value_color_html
