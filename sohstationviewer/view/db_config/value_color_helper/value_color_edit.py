from typing import Optional, Type

from PySide6 import QtWidgets, QtGui, QtCore
from PySide6.QtCore import Qt
from PySide6.QtGui import QPalette, QColor
from PySide6.QtWidgets import (
    QWidget, QMessageBox, QLabel, QFrame,
)

from sohstationviewer.conf.constants import ROOT_PATH
from sohstationviewer.view.db_config.param_helper import \
    validate_value_color_str
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import DotForTimeDialog
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import EditValueColorDialog
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import LineDotDialog
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import (MultiColorDotLowerEqualDialog,
                                    MultiColorDotUpperEqualDialog)
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import TriColorLinesDialog
from sohstationviewer.view.db_config.value_color_helper. \
    edit_value_color_dialog import UpDownDialog
from sohstationviewer.view.db_config.value_color_helper.functions import \
    prepare_value_color_html
from sohstationviewer.view.util.color import COLOR
from sohstationviewer.view.util.plot_type_info import plot_types

plot_types_with_value_colors = [
            p for p in plot_types
            if 'pattern' in plot_types[p]]


class ValueColorEdit(QFrame):
    """
    Widget to display valueColors and call a dialog to edit tha value
    """
    value_color_edited = QtCore.Signal(str)

    def __init__(self, parent: Optional[QWidget], background: str,
                 plot_type: str, value_color_str: str = '',
                 errmsg_header: str = ''):
        """
        value_color_str will be validated, if not valid
        'default_value_color' of the plot_type  will be used instead of
        the given value_color_str.
        :param parent: the parent widget
        :param background: 'B'/'W': flag indicating background color
        :param plot_type: the plot type the editor used to format and validate
            the value colors
        :param value_color_str: the initial value colors shown in the editor.
            If this does not fit the format required by the given plot type,
            use the default value colors for the plot type instead.
        :param errmsg_header: header for error message to help user identify
            the box that has value color string error.
        """
        super().__init__(parent)

        # Background color for setting border color match with background
        self.background: QColor = (
            QColor(Qt.GlobalColor.black)
            if background == 'B'
            else QColor(Qt.GlobalColor.white)
        )
        # Type of channel's plot
        self.plot_type: str = plot_type
        # Value colors string given as the input. Only used to determine if the
        # currently displayed value colors string is different from the one
        # given as the input.
        self.original_value_color_str: str = value_color_str
        # Current value color string displayed on widget
        self.value_color_str: str = ''
        # Flag showing if original value color string passes validation
        self.is_valid: bool = True
        # dialog that pop up when clicking on edit_button to help edit color
        # and value
        self.edit_value_color_dialog: Optional[QWidget] = None
        # Component widgets of this widget
        self.value_color_display = QLabel(self)
        self.revert_button = QtWidgets.QToolButton(self)
        self.edit_button = QtWidgets.QToolButton(self)

        self.setup_ui(background, plot_type)

        # Value colors string to revert to. Might be different from the input
        # because we always want to revert to a valid value colors string.
        self.base_value_color_str = ''
        if plot_type in plot_types_with_value_colors:
            if value_color_str == '':
                # Use default value_color_str for the empty one
                value_color_str = plot_types[plot_type]['default_value_color']
            try:
                self.is_valid, err_msg = validate_value_color_str(
                    value_color_str, plot_type)
                if not self.is_valid:
                    raise ValueError(
                        f"{errmsg_header}{err_msg}\n\n The ValueColors string "
                        f"will be replaced with {plot_type}'s default one.")
                self.base_value_color_str = value_color_str
            except ValueError as e:
                QMessageBox.critical(
                    self, f'Invalid ValueColors:{plot_type}', str(e))
                print(str(e))      # Show on terminal to track back later.
                # We set the value colors to the default value (specified in
                # plot_type_info.py) when there is a problem with the given
                # value colors string.
                self.base_value_color_str = (
                    plot_types[plot_type]['default_value_color']
                )
        self.set_value_color(self.base_value_color_str)

    def setup_ui(self, background: str, plot_type: str):
        # Set up the border of the widget.
        self.setFrameShape(QFrame.Shape.Box)
        self.setFrameShadow(QFrame.Shadow.Plain)
        self.set_border_color(self.background)
        # If we make the border disappear altogether when it is not needed, the
        # buttons will be moved a bit when the border actually shows up. This
        # does not look the best when we have multiple of this widget in a row
        # (e.g. in the Params table editor). Instead, we make it so that the
        # border always shows up but blend in with the widget's background
        # unless needed.
        self.setLineWidth(4)

        # Without this line, the background of the widget will not be filled
        # with the color we want.
        self.setAutoFillBackground(True)
        self.set_background_color(background)

        # QLabels expands only enough to fit their content, so we need to
        # expand their width manually to make space for longer value colors
        # strings.
        # The actual width was chosen pretty arbitrarily. It is simply the
        # first value we tested that makes the UI looks good.
        self.value_color_display.setFixedWidth(250)

        self.revert_button.setCursor(Qt.PointingHandCursor)
        if background == 'B':
            img_file = f"{ROOT_PATH}/images/revert_icon_black_background.png"
        else:
            img_file = f"{ROOT_PATH}/images/revert_icon_white_background.png"
        self.revert_button.setIcon(QtGui.QIcon(img_file))
        self.revert_button.setStyleSheet(
            "background: transparent; border: none;")
        self.revert_button.clicked.connect(self.revert)
        # Make it so that the widget does not change size when the revert
        # button shows up after being hidden.
        size_policy = self.revert_button.sizePolicy()
        size_policy.setRetainSizeWhenHidden(True)
        self.revert_button.setSizePolicy(size_policy)
        self.revert_button.hide()

        self.edit_button.setCursor(Qt.CursorShape.PointingHandCursor)
        if background == 'B':
            img_file = f"{ROOT_PATH}/images/edit_icon_black_background.png"
        else:
            img_file = f"{ROOT_PATH}/images/edit_icon_white_background.png"
        self.edit_button.setIcon(QtGui.QIcon(img_file))
        self.edit_button.setStyleSheet(
            "background: transparent; border: none;")
        self.edit_button.clicked.connect(self.edit)
        if plot_type not in plot_types_with_value_colors:
            self.edit_button.hide()

        layout = QtWidgets.QHBoxLayout(self)
        layout.setSpacing(0)
        layout.setContentsMargins(5, 0, 5, 0)

        layout.addWidget(self.value_color_display)
        layout.addWidget(self.revert_button)
        layout.addWidget(self.edit_button)

    def set_border_color(self, color: QColor):
        """
        Set border color for the widget.
        :param color: color to set to border
        """
        palette = self.palette()
        # The border color of a QFrame is handled by its foreground color
        # roles. This is not explicitly documented in the official QT doc.
        # Instead, this fact is only hinted. The stackoverflow question below
        # is where this information is found.
        # https://stackoverflow.com/questions/51056997/how-to-set-color-of-frame-of-qframe  # noqa
        # The color role used for the border color of a QFrame depends on
        # whether it is embedded in a QTableWidget or is used as a standalone
        # widget. This is understandable, but not entirely expected.
        palette.setColor(QPalette.ColorRole.Text, color)
        palette.setColor(QPalette.ColorRole.WindowText, color)
        self.setPalette(palette)

    def set_background_color(self, background: str):
        """
        Set black background for user to have better feeling how the colors
            displayed on black background. Text and PlaceholderText's colors
            have to be changed to be readable on the black background too.
        :param background: 'B'/'W': sign for background color
        """
        palette = self.palette()
        display_palette = self.value_color_display.palette()
        # The color role actually used in a palette depends on whether the
        # widget is embedded in a QTableWidget (and maybe other views) or is
        # used as a standalone widget. This is understandable, but not entirely
        # expected.
        if background == 'B':
            display_palette.setColor(QPalette.ColorRole.Text, Qt.white)
            display_palette.setColor(QPalette.ColorRole.WindowText, Qt.white)
            palette.setColor(QPalette.ColorRole.Base, Qt.black)
            palette.setColor(QPalette.ColorRole.Window, Qt.black)
        else:
            display_palette.setColor(QPalette.ColorRole.Text, Qt.black)
            display_palette.setColor(QPalette.ColorRole.WindowText, Qt.black)
            palette.setColor(QPalette.ColorRole.Base, Qt.white)
            palette.setColor(QPalette.ColorRole.Window, Qt.white)
        self.setPalette(palette)
        self.value_color_display.setPalette(display_palette)

    def set_value_color(self, value_color_str: str) -> None:
        """
        Set value_color_str, value_color_edit_dialog and display value color
        string in html to show color for user to have the feeling.

        :param value_color_str: string for value color to be saved in DB
        """
        self.value_color_str = value_color_str
        value_color_html = prepare_value_color_html(self.value_color_str)
        self.value_color_display.setText(value_color_html)
        self.value_color_edited.emit(self.value_color_str)
        self.set_border_color_according_to_value_color_status()
        if value_color_str != self.base_value_color_str:
            self.revert_button.show()
        else:
            self.revert_button.hide()

    def set_border_color_according_to_value_color_status(self):
        """
        Set border color according to current value color string.
          + Blue for changed
          + Same color as background (no border) for other case.
        """
        if self.value_color_str != self.original_value_color_str:
            # Show that value_color_str has been changed
            self.set_border_color(QColor(COLOR['changedStatus']))
        else:
            # Reset border the same color as background
            self.set_border_color(self.background)

    def revert(self):
        self.set_value_color(self.base_value_color_str)

    def edit(self):
        """
        Show user an editor to edit the value colors of this widget.
        """
        if self.plot_type not in plot_types_with_value_colors:
            return
        plot_type_dialog_map: dict[str, Type[EditValueColorDialog]] = {
            'linesDots': LineDotDialog,
            'triColorLines': TriColorLinesDialog,
            'dotForTime': DotForTimeDialog,
            'multiColorDotsEqualOnUpperBound': MultiColorDotUpperEqualDialog,
            'multiColorDotsEqualOnLowerBound': MultiColorDotLowerEqualDialog,
            'upDownDots': UpDownDialog,
        }
        # set border color blue showing that the widget is being edited
        self.set_border_color(COLOR['changedStatus'])
        edit_dialog = plot_type_dialog_map[self.plot_type](
            self, self.value_color_str
        )

        edit_dialog.accepted.connect(
            lambda: self.set_value_color(edit_dialog.value_color_str)
        )
        # To set border in case cancel is clicked. It's necessary because the
        # above set_value_color won't be called for clicking cancel.
        edit_dialog.finished.connect(
            self.set_border_color_according_to_value_color_status)

        edit_dialog.open()
