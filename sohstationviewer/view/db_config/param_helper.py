from typing import Tuple
import re
from sohstationviewer.view.util.plot_type_info import plot_types


def validate_value_color_str(value_colors_string: str,
                             plot_type: str) -> Tuple[bool, str]:
    """
    Validate the given value_colors_string to see if:
      + Total ValueColors match with requirement if any.
      + It match the pattern of the plot_type.
      + The value sections aren't duplicated.
      + Numbers are in increasing order.

    :param value_colors_string: string of value colors split by '|'
    :param plot_type: type of plot of which info need to be used to validate
        the value_colors_string.

    :return: validation flag, error message.
    """
    plot_type_info = plot_types.get(plot_type)

    value_colors = value_colors_string.split("|")
    if 'total_value_color_required' in plot_type_info:
        if len(value_colors) != plot_type_info['total_value_color_required']:
            msg = (f"Total ValueColors in "
                   f"'{value_colors_string}' "
                   f"is {len(value_colors)} while it requires "
                   f"{plot_type_info['total_value_color_required']} for "
                   f"plot type {plot_type}.")
            return False, msg

    values = []
    numbers = []
    for vc in value_colors:
        vc = vc.strip()
        if plot_type_info is None:
            continue
        if not plot_type_info['pattern'].match(vc):
            msg = (f"A ValueColor in '{value_colors_string}', '{vc}', "
                   f"doesn't match the required format of\n\n"
                   f"{plot_type_info['description']}")
            return False, msg
        val = vc.split(":")[0]
        if val in values:
            msg = (f"Duplicated value '{val}' in ValueColors string "
                   f"'{value_colors_string}' isn't allowed.")
            return False, msg
        number = re.sub('<|=', '', val)
        try:
            number = float(number)
            if numbers and number < max(numbers):
                msg = (f"Number '{number}' is out of increasing order in "
                       f"ValueColors string '{value_colors_string}'.")
                return False, msg
            numbers.append(number)
        except ValueError:
            pass
        values.append(val)

    return True, ''
