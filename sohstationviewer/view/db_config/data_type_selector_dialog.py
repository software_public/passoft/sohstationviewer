import sys
from typing import Union, List

from PySide6 import QtWidgets, QtCore
from PySide6.QtWidgets import (
    QApplication, QWidget, QDialog, QLabel, QFrame, QRadioButton,
    QLineEdit
)

from sohstationviewer.database.extract_data import get_data_types
from sohstationviewer.database.process_db import execute_db

from sohstationviewer.controller.util import display_tracking_info


def add_separation_line(layout):
    """
    Add a line for separation to the given layout.
    :param layout: QLayout - the layout that contains the line
    """
    label = QLabel()
    label.setFrameStyle(QFrame.Shape.HLine | QFrame.Shadow.Sunken)
    label.setLineWidth(1)
    layout.addWidget(label)


class DataTypeSelectorDialog(QDialog):
    """
    When Data Type for current Data Set is Unknown, user need to select Data
    Type for it by using one of the existing data set or create a new data type
    """
    def __init__(self, parent: Union[QWidget, QApplication]):
        """
        Dialog allow choosing file format and open file dialog to
            save file as

        :param parent: the parent widget
        """
        super(DataTypeSelectorDialog, self).__init__(parent)
        self.parent = parent
        self.use_existing_data_type_radio_btn = QRadioButton(
            "Use existing data type", self)
        self.use_existing_data_type_radio_btn.setChecked(True)

        # not include RT130 because RT130 is fixed, any channel added have to
        # be hard coded.
        self.existing_data_types: List[str] = get_data_types(
            include_default=False, include_rt130=False)
        self.existing_data_types_cbo = QtWidgets.QComboBox()
        self.existing_data_types_cbo.addItems(self.existing_data_types)

        self.create_new_data_type_radio_btn = QRadioButton(
            "Create new data type", self)

        self.new_data_type_lnedit = QLineEdit(self)
        self.new_data_type_lnedit.setEnabled(False)

        self.cancel_btn = QtWidgets.QPushButton('CANCEL', self)
        self.submit_btn = QtWidgets.QPushButton('SUBMIT', self)

        self.setup_ui()
        self.connect_signals()

    def setup_ui(self) -> None:
        self.setWindowTitle("Setup Data Type")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        main_layout.addWidget(self.use_existing_data_type_radio_btn)
        main_layout.addWidget(self.existing_data_types_cbo)

        add_separation_line(main_layout)

        main_layout.addWidget(self.create_new_data_type_radio_btn)
        main_layout.addWidget(self.new_data_type_lnedit)

        button_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(button_layout)
        button_layout.addWidget(self.cancel_btn)
        button_layout.addWidget(self.submit_btn)

    def connect_signals(self) -> None:
        self.use_existing_data_type_radio_btn.clicked.connect(
            self.select_data_type)
        self.create_new_data_type_radio_btn.clicked.connect(
            self.select_data_type)

        self.cancel_btn.clicked.connect(self.close)
        self.submit_btn.clicked.connect(self.submit)

    @QtCore.Slot()
    def select_data_type(self) -> None:
        """
        Select to use one of existing data type or create new data type for
        the current data set
        """
        if self.use_existing_data_type_radio_btn.isChecked():
            self.existing_data_types_cbo.setEnabled(True)
            self.new_data_type_lnedit.setEnabled(False)
        else:
            self.existing_data_types_cbo.setEnabled(False)
            self.new_data_type_lnedit.setEnabled(True)

    @QtCore.Slot()
    def submit(self):
        """
        Create new data type as needed, then set the selected data type
        for the data set that is being processed. Request confirmation before
        the steps are done.
        """
        existing_new_data_type = False
        if self.use_existing_data_type_radio_btn.isChecked():
            data_type = self.existing_data_types_cbo.currentText()
            msg = (f"We are going to set {data_type} as the data type for the "
                   f"current data set.\n\nDo you want to continue?")
        else:
            data_type = self.new_data_type_lnedit.text()
            if data_type in self.existing_data_types:
                msg = (f"The entered data type {data_type} is already exist."
                       f"\n\nDo you want to use it as the data type for the "
                       f"current data set?")
                existing_new_data_type = True
            else:
                msg = (f"We are going to create new data type {data_type} "
                       f"and set it as the data type for the current data set."
                       f"\n\nDo you want to continue?")

        result = QtWidgets.QMessageBox.question(
            self, "Confirmation", msg,
            QtWidgets.QMessageBox.StandardButton.Yes |
            QtWidgets.QMessageBox.StandardButton.No)
        if result == QtWidgets.QMessageBox.StandardButton.No:
            return
        if existing_new_data_type:
            self.use_existing_data_type_radio_btn.setChecked(True)
            self.existing_data_types_cbo.setCurrentText(data_type)
        if self.create_new_data_type_radio_btn.isChecked():
            self.create_new_data_type(data_type)

        self.close()
        try:
            self.setup_data_type(data_type)
            display_tracking_info(self.parent.tracking_info_text_browser,
                                  f"Successfully setting up data type "
                                  f"{data_type} for the data set.")
        except Exception as e:
            display_tracking_info(self.parent.tracking_info_text_browser,
                                  str(e))

    @staticmethod
    def create_new_data_type(data_type: str) -> None:
        """
        Insert new data type into database
        """
        sql = f"INSERT INTO DataTypes VALUES('{data_type}')"
        execute_db(sql)

    def setup_data_type(self, data_type: str) -> None:
        """
        Assign given data type to main_window, data_object and call data_loaded
        to update the content of file info box (info_list_widget)

        :param data_type: the data_type given for the current data set.
        """
        self.parent.data_type = data_type
        self.parent.data_object.data_type = data_type
        self.parent.data_loaded(self.parent.data_object,
                                need_update_data_set_info=True)


if __name__ == '__main__':
    from sohstationviewer.conf.dbSettings import modify_db_path
    modify_db_path()
    app = QtWidgets.QApplication(sys.argv)
    test = DataTypeSelectorDialog(None)
    test.exec()
    sys.exit(app.exec())
