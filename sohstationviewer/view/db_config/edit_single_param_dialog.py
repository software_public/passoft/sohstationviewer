import sys
from typing import Optional, Dict

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget, QDialog

from sohstationviewer.view.util.plot_type_info import plot_types

from sohstationviewer.database.process_db import execute_db
from sohstationviewer.database.extract_data import (
    get_param_info, create_assign_string_for_db_query
)

from sohstationviewer.conf.dbSettings import modify_db_path

from sohstationviewer.view.db_config.value_color_helper.value_color_edit \
    import ValueColorEdit


class EditSingleParamDialog(QDialog):
    """
    Dialog to add info for channel not in database or edit the existing channel
    """
    def __init__(self, parent: Optional[QWidget], param: str):
        """
        :param parent: the parent widget
        :param param: parameter that categorizes the channel
        """
        self.param = param
        # database info of the channel's parameter
        self.param_info: Dict = get_param_info(self.param)

        # layout for all values' labels and editing widgets
        self.all_values_layout = QtWidgets.QGridLayout()

        super(EditSingleParamDialog, self).__init__(parent)

        # parameter's plot type which decides the shape of the plot
        self.plot_type_cbo_box = QtWidgets.QComboBox(self)
        self.plot_type_cbo_box.addItems([""] + list(plot_types.keys()))

        # value color in black mode
        self.value_colorb_widget = ValueColorEdit(self, 'B', '')
        # value, color in white mode
        self.value_colorw_widget = ValueColorEdit(self, 'W', '')

        # height of the plot
        self.height_spnbox = QtWidgets.QSpinBox()
        self.height_spnbox.setMinimum(0)
        self.height_spnbox.setMaximum(8)
        self.height_spnbox.setToolTip("Relative height of the plot")
        self.height_warning_label = QtWidgets.QLabel(
            "(Height setting will only be applied after RePlot is clicked.)")
        self.height_warning_label.setStyleSheet(
            "QLabel {color: red; font-size: 10; font-style: italic;}"
        )

        # button to save change to DB
        self.save_param_btn = QtWidgets.QPushButton(
            "SAVE PARAMETER", self)
        # button to close dialog without doing anything
        self.cancel_btn = QtWidgets.QPushButton('CANCEL', self)

        self.setup_ui()
        self.set_param_info()
        self.connect_signals()

    def setup_ui(self) -> None:
        self.setWindowTitle(f"Edit Parameter {self.param}")

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        main_layout.addLayout(self.all_values_layout)

        self.all_values_layout.addWidget(
            QtWidgets.QLabel('Plot Type'), 0, 0, 1, 1)
        self.all_values_layout.addWidget(self.plot_type_cbo_box, 0, 1, 1, 1)

        self.all_values_layout.addWidget(QtWidgets.QLabel(
            'Value Color (black)'), 1, 0, 1, 1)
        self.all_values_layout.addWidget(self.value_colorb_widget, 1, 1, 1, 1)

        self.all_values_layout.addWidget(QtWidgets.QLabel(
            'Value Color (white)'), 2, 0, 1, 1)
        self.all_values_layout.addWidget(self.value_colorw_widget, 2, 1, 1, 1)

        self.all_values_layout.addWidget(
            QtWidgets.QLabel('Height'), 3, 0, 1, 1)
        self.all_values_layout.addWidget(self.height_spnbox, 3, 1, 1, 1)

        self.all_values_layout.addWidget(self.height_warning_label, 4, 0, 1, 2)
        self.all_values_layout.addWidget(self.cancel_btn, 5, 0, 1, 1)
        self.all_values_layout.addWidget(self.save_param_btn, 5, 1, 1, 1)

    def connect_signals(self) -> None:
        self.plot_type_cbo_box.currentTextChanged.connect(self.set_plot_type)
        self.cancel_btn.clicked.connect(self.close)
        self.save_param_btn.clicked.connect(self.on_save_param)

    def set_param_info(self) -> None:
        """
        Fill up all info boxes
        """
        self.param_info = get_param_info(self.param)
        self.set_plot_type(self.param_info['plotType'])
        self.height_spnbox.setValue(self.param_info['height'])

    def set_plot_type(self, plot_type: str) -> None:
        """
        Add Plot Type, Value Color strings.
        If there is no Plot Type, no Value Color or Height because no plot.
        :param plot_type: name of Plot Type
        """
        if plot_type in ["", None]:
            self.plot_type_cbo_box.setCurrentText('')
            self.value_colorb_widget.setEnabled(False)
            self.value_colorb_widget.clear()
            self.value_colorw_widget.setEnabled(False)
            self.value_colorw_widget.clear()
            self.height_spnbox.setValue(0)
        else:
            self.plot_type_cbo_box.setCurrentText(plot_type)
            # value color in black mode
            self.value_colorb_widget = self.set_value_color_widget(
                'B', plot_type, self.param_info['valueColorsB'])
            # value, color in white mode
            self.value_colorw_widget = self.set_value_color_widget(
                'W', plot_type, self.param_info['valueColorsW'])
            self.all_values_layout.update()

    def set_value_color_widget(self, background_color: str,
                               plot_type: str,
                               value_color: str) -> ValueColorEdit:
        """
        Replace value_color_widget for the given background color with
        the new one with new plot_type and value color.

        :param background_color: 'B'/'W': flag indicating background color
        :param plot_type: define type to plot the channel
        :param value_color: define colors and how it is applied in the channel
            plot
        """
        if background_color == 'B':
            widget = self.value_colorb_widget
            row_idx = 1
            errmsg_header = "Value Color(black) validation failed: "
        else:
            widget = self.value_colorw_widget
            row_idx = 2
            errmsg_header = "Value Color(white) validation failed: "
        self.all_values_layout.removeWidget(widget)
        widget.close()
        widget = ValueColorEdit(
            self, background_color, plot_type, value_color, errmsg_header)
        self.all_values_layout.addWidget(widget, row_idx, 1, 1, 1)
        return widget

    def on_save_param(self):
        """
        Save parameter info to Parameters table
        """
        plot_type = create_assign_string_for_db_query(
            'plotType', self.plot_type_cbo_box.currentText())
        value_colorb = create_assign_string_for_db_query(
            'valueColorsB', self.value_colorb_widget.value_color_str)
        value_colorw = create_assign_string_for_db_query(
            'valueColorsW', self.value_colorw_widget.value_color_str)
        height = f"height={self.height_spnbox.value()}"
        sql = (f"UPDATE Parameters SET {plot_type}, {value_colorb}, "
               f"{value_colorw}, {height} WHERE param='{self.param}'")
        execute_db(sql)
        self.close()


if __name__ == '__main__':
    modify_db_path()
    app = QtWidgets.QApplication(sys.argv)

    # test linesDots. Ex: param: Input power supply current
    test = EditSingleParamDialog(None, 'Input power supply current')

    # test MultiColorDotsLowerBound. Ex: param:Backup volt
    # test = EditSingleParamDialog(None, 'Backup Volt', 'RT130')

    # test MultiColorDotsUpperBound. Ex: param:GNSS status
    # test = EditSingleParamDialog(None, 'GNSS status')

    # test UpDownDots. Ex: param. Ex: param:Net Up/down
    # test = EditSingleParamDialog(None, 'Net Up/Down', 'RT130')

    # test TriColorLInes. Ex: param. Ex: param:Error/warning
    # test = EditSingleParamDialog(None, 'Error/Warning', 'RT130')
    test.exec()
    sys.exit(app.exec())
