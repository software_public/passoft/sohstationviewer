"""
channel_dialog.py
GUI to add/edit/remove channels
"""
from typing import List, Union
from PySide6.QtWidgets import QMessageBox

from sohstationviewer.view.db_config.db_config_dialog import UiDBInfoDialog
from sohstationviewer.database.process_db import execute_db


class ChannelDialog(UiDBInfoDialog):
    def __init__(self, parent):
        """
        :param parent: QMainWindow/QWidget - the parent widget
        """

        """
        param_choice: [str,] - list of parameters from db table parameters
        """
        self.param_choices = []
        super().__init__(
            parent, ['No.', 'Channel', 'Label', 'Param',
                     'ConvertFactor', 'Unit', 'FixPoint'],
            'channel', 'channels', resize_content_columns=[0, 4, 5, 6],
            need_data_type_choice=True, required_columns={2: 'Param'},
            check_fk=False)
        self.delete_sql_supplement = f" AND dataType='{self.data_type}'"
        self.setWindowTitle("Edit/Add/Delete Channels")
        self.insert_sql_template = (f"INSERT INTO Channels VALUES"
                                    f"(?, ?, ?, '', ?, ?, ?, "
                                    f"'{self.data_type}')")
        self.update_sql_template = (f"UPDATE Channels SET channel=?, "
                                    f"label=?, param=?, convertFactor=?, "
                                    f"unit=?, fixPoint=? "
                                    f"WHERE channel='%s' "
                                    f"AND dataType='{self.data_type}'")

    def update_data_table_widget_items(self):
        """
        Create list of parameters to used in widget for selecting parameters
            before update item in self.data_table_widgets
        """
        param_rows = execute_db("SELECT param from parameters")
        self.param_choices = [''] + sorted([d[0] for d in param_rows])
        super(ChannelDialog, self).update_data_table_widget_items()

    def clear_first_row(self):
        """Clear the content of the first row of the editor."""
        self.data_table_widget.cellWidget(0, 4).setText('1')
        self.data_table_widget.cellWidget(0, 6).setValue(0)

    def set_row_content(self, row_idx: int,
                        row_content: List[Union[str, int]]):
        """
        Set content to each cell in row.
        :param row_idx: position of row to set the content
        :param row_content: list of contents of all cells in a row
        """
        self.data_table_widget.cellWidget(row_idx, 1).setText(row_content[0])

        self.data_table_widget.cellWidget(row_idx, 2).setText(row_content[1])
        if row_content[2] == -1:
            self.data_table_widget.cellWidget(row_idx, 3).setCurrentIndex(-1)
        else:
            self.data_table_widget.cellWidget(row_idx, 3).setCurrentText(
                row_content[2])
        self.data_table_widget.cellWidget(row_idx, 4).setText(
            str(row_content[3]))
        self.data_table_widget.cellWidget(row_idx, 5).setText(row_content[4])
        self.data_table_widget.cellWidget(row_idx, 6).setValue(row_content[5])

    def set_row_widgets(self, row_idx, fk=False):
        """
        Set the widgets in a row in self.data_table_widgets.

        :param row_idx: index of row to add
        :param fk: bool: True if there is a foreign constrain that prevents the
            row to be deleted
        """
        self.add_row_number_button(row_idx)  # No.
        self.add_widget(row_idx, 1, foreign_key=fk)  # chanID
        self.add_widget(row_idx, 2)  # label
        self.add_widget(row_idx, 3, choices=self.param_choices)
        self.add_widget(row_idx, 4, field_name='convertFactor')
        self.add_widget(row_idx, 5)  # unit
        self.add_widget(row_idx, 6, range_values=[0, 5])  # fixPoint
        self.add_delete_button_to_row(row_idx, fk)
        self.add_reset_button_to_row(row_idx)

    def get_data_type_from_selector(self):
        """
        Update the dialog with the new data type. Also update some other
        attributes of the dialog affected by the data type.
        :return:
        """
        old_data_type = self.data_type
        self.data_type = self.data_type_combo_box.currentText()
        self.delete_sql_supplement = f" AND dataType='{self.data_type}'"
        self.insert_sql_template = self.insert_sql_template.replace(
            old_data_type, self.data_type
        )
        self.update_sql_template = self.update_sql_template.replace(
            old_data_type, self.data_type
        )

    def data_type_changed(self):
        """
        Method called when the data type of the data table is changed.

        If there are unsaved changes, ask the user what to do. If the user
        proceeds with the change, load channels of the new data type from the
        database and populate the data table with these new channels. The user
        can choose to save or not save the changes to the database with this
        option. Otherwise, change the data type back and do nothing.
        """
        # If the user cancel changing the data type when there are unsaved
        # changes, the data type is changed back, which causes another
        # execution of this method and thus, the prompt dialog shows up
        # twice. This if statement fixes that problem.
        if self.data_type == self.data_type_combo_box.currentText():
            return

        if not self.has_changes():
            self.get_data_type_from_selector()
            self.update_data_table_widget_items()
            return

        unsaved_changes_prompt = (
            '<h3>Unsaved Changes</h3>'
            'Are you sure you want to change the data type? '
            'Any changes you made will not be saved?'
        )
        options = (QMessageBox.StandardButton.Save |
                   QMessageBox.StandardButton.Discard |
                   QMessageBox.StandardButton.Cancel)
        user_choice = QMessageBox.warning(self, 'Unsaved Changes',
                                          unsaved_changes_prompt, options)
        if user_choice != QMessageBox.StandardButton.Cancel:
            if user_choice == QMessageBox.StandardButton.Save:
                self.save_changes(need_confirmation=False)
            elif user_choice == QMessageBox.StandardButton.Discard:
                self.untrack_changes()
                pass
            self.get_data_type_from_selector()
            self.update_data_table_widget_items()
        else:
            # Cover both the case where the cancel button is pressed and the
            # case where the exit button is pressed.
            self.data_type_combo_box.setCurrentText(self.data_type)

    def get_database_rows(self):
        """
        Get list of data to fill self.data_table_widgets' content
        """
        sql = (
            f"SELECT channel, "
            f"IFNULL(label, '') AS label,"
            f" IFNULL(param, '') AS param,"
            f" convertFactor,"
            f" IFNULL(unit, '') AS unit,"
            f" IFNULL(fixPoint, 0) AS fixPoint "
            f"FROM Channels "
            f"WHERE dataType='{self.data_type}'"
        )
        backup_rows = execute_db(sql, db_path='backup_db_path')
        self.backup_database_rows = [
            [d[0], d[1], d[2], float(d[3]), d[4], int(d[5])]
            for d in backup_rows]
        channel_rows = execute_db(sql)
        return [[d[0], d[1], d[2], float(d[3]), d[4], int(d[5])]
                for d in channel_rows]

    def get_row_inputs(self, row_idx):
        """
        Get content from a row of widgets

        :param row_idx: index of row
        """
        return [
            self.data_table_widget.cellWidget(row_idx, 1).text().strip(),
            self.data_table_widget.cellWidget(row_idx, 2).text().strip(),
            self.data_table_widget.cellWidget(row_idx, 3).currentText(),
            float(self.data_table_widget.cellWidget(row_idx, 4).text()),
            self.data_table_widget.cellWidget(row_idx, 5).text(),
            self.data_table_widget.cellWidget(row_idx, 6).value()
        ]

    def remove_row(self, remove_row_idx):
        """
        Remove a row in self.data_table_widget.
        Reset row number of the following rows.

        :param remove_row_idx: index of row to be removed
        """
        self.data_table_widget.removeRow(remove_row_idx)
        for i in range(remove_row_idx, self.data_table_widget.rowCount()):
            cell_widget = self.data_table_widget.cellWidget(i, 0)
            cell_widget.setText(str(i))
