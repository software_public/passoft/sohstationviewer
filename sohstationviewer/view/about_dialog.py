import sys
from typing import Union, Dict
import os
from pathlib import Path
import re

from PySide6 import QtWidgets, QtCore
from PySide6.QtWidgets import QApplication, QWidget, QDialog, QLabel, QFrame, \
    QListWidget, QPlainTextEdit

from sohstationviewer.conf import constants


def add_separation_line(layout):
    """
    Add a line for separation to the given layout.
    :param layout: QLayout - the layout that contains the line
    """
    label = QLabel()
    label.setFrameStyle(QFrame.Shape.HLine | QFrame.Shadow.Sunken)
    label.setLineWidth(1)
    layout.addWidget(label)


def remove_empty_lines(text: str) -> str:
    """
    Remove lines with no text from text
    :param text: the text with empty lines that need to be removed
    :return: text with no empty lines
    """
    lines = text.split('\n')
    no_empty_lines = [line for line in lines if line.strip()]
    return '\n'.join(no_empty_lines)


class AboutDialog(QDialog):
    """
     Dialog to show information of the software.

     About Dialog is always opened and will be raised up when the about menu
     action is triggered.
    """
    def __init__(self, parent: Union[QWidget, QApplication, None]):
        """
        :param parent: the parent widget
        """
        super(AboutDialog, self).__init__(parent)
        # set block interaction with other windows
        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)
        self.software_name_label = QLabel("SOHViewer")
        self.software_name_label.setStyleSheet(
            "QLabel {font-size: 18pt; font-style: bold; color: darkblue}"
        )
        description = ("Visualize State-of-Health packets from data in "
                       "mseed or reftek formats recorded\n"
                       "by different types of data loggers.")
        self.description_label = QLabel(description)

        self.history_label = QLabel('History')
        # Dictionary of history by version
        self.history_dict: Dict[str, str] = self.get_history_dict()

        self.version_list_widget = QListWidget(self)
        self.version_list_widget.addItems(reversed(self.history_dict.keys()))

        self.changelog_edit = QPlainTextEdit()
        self.changelog_edit.setReadOnly(True)

        version = f"Version {constants.SOFTWARE_VERSION}"
        self.version_label = QLabel(version)

        built_time = f"Built on {constants.BUILD_TIME}"
        self.built_time_label = QLabel(built_time)

        version_year = constants.SOFTWARE_VERSION.split('.')[0]
        copy_right = (u"Copyright \u00A9 "
                      f"{version_year} EarthScope Consortium")
        self.copyright_label = QLabel(copy_right)

        self.ok_button = QtWidgets.QPushButton('OK', self)
        self.setup_ui()
        self.connect_signals()
        self.version_list_widget.setCurrentRow(0)

    def setup_ui(self):
        self.setWindowTitle("About SOHViewer")
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        main_layout.addWidget(self.software_name_label)
        main_layout.addWidget(self.description_label)
        add_separation_line(main_layout)

        main_layout.addWidget(self.history_label)
        history_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(history_layout)
        self.version_list_widget.setFixedWidth(100)
        history_layout.addWidget(self.version_list_widget)
        history_layout.addWidget(self.changelog_edit)

        main_layout.addWidget(self.version_label)
        main_layout.addWidget(self.built_time_label)
        main_layout.addWidget(self.copyright_label)

        button_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(button_layout)
        button_layout.addStretch()
        button_layout.addWidget(self.ok_button)

    def connect_signals(self) -> None:
        self.version_list_widget.currentTextChanged.connect(
            self.on_version_changed)
        self.ok_button.clicked.connect(self.close)

    @staticmethod
    def get_history_dict():
        """
        Get dictionary of history by version from file HISTORY.rst
        :return: dictionary of history by version
        """
        current_file_path = os.path.abspath(__file__)
        root = Path(current_file_path).parent.parent
        history_path = root.joinpath('CHANGELOG.rst')
        version_re = re.compile('^[1-3][0-9]{3}.[1-9].[0-9].[0-9]$')
        history_dict = {}
        lines = []
        version = None
        with open(history_path) as file:
            lines = file.readlines()
        for line in lines:
            line = line.strip()
            if version_re.match(line):
                version = line
                history_dict[version] = ""
            elif version is not None and '------' not in line:
                history_dict[version] += line + '\n'
        return history_dict

    @QtCore.Slot()
    def on_version_changed(self, version):
        """
        When version is changed, place the corresponded changelog in changelog
        edit.
        :param version: the selected version
        """
        changelog = remove_empty_lines(self.history_dict[version])

        self.changelog_edit.setPlainText(changelog)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    test = AboutDialog(None)
    test.exec()
    sys.exit(app.exec())
