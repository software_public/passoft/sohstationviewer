import pathlib
import shutil
import sqlite3
import traceback
from datetime import date
from typing import List, Tuple, Union, Dict
from pathlib import Path
from obspy import UTCDateTime

from PySide6 import QtCore, QtWidgets, QtGui
from PySide6.QtCore import QSize, QCoreApplication
from PySide6.QtGui import QFont, QPalette, QColor
from PySide6.QtWidgets import (
    QFrame, QListWidgetItem, QMessageBox,
    QInputDialog,
)

from sohstationviewer.conf import constants
from sohstationviewer.conf.dbSettings import dbConf
from sohstationviewer.model.data_loader import DataLoader
from sohstationviewer.model.general_data.general_data import \
    GeneralData

from sohstationviewer.view.about_dialog import AboutDialog
from sohstationviewer.view.calendar.calendar_dialog import CalendarDialog
from sohstationviewer.view.db_config.channel_dialog import ChannelDialog
from sohstationviewer.view.db_config.data_type_dialog import DataTypeDialog
from sohstationviewer.view.db_config.param_dialog import ParamDialog
from sohstationviewer.view.db_config.plot_type_dialog import PlotTypeDialog
from sohstationviewer.view.file_information.get_file_information import \
    extract_data_set_info
from sohstationviewer.view.file_list.file_dialog_for_linux_external_drive \
    import FileDialogForLinuxExternalDrive
from sohstationviewer.view.file_list.file_list_widget import FileListItem
from sohstationviewer.view.plotting.gps_plot.extract_gps_data import \
    extract_gps_data
from sohstationviewer.view.plotting.gps_plot.gps_dialog import GPSDialog
from sohstationviewer.view.plotting.time_power_square. \
    time_power_squared_dialog import TimePowerSquaredDialog
from sohstationviewer.view.plotting.waveform_dialog import WaveformDialog
from sohstationviewer.view.search_message.search_message_dialog import (
    SearchMessageDialog
)
from sohstationviewer.view.help_view import HelpBrowser
from sohstationviewer.view.ui.main_ui import UIMainWindow
from sohstationviewer.view.util.enums import LogType
from sohstationviewer.view.util.functions import (
    check_chan_wildcards_format, check_masspos)
from sohstationviewer.view.util.check_file_size import check_folders_size

from sohstationviewer.view.channel_prefer_dialog import ChannelPreferDialog
from sohstationviewer.view.create_muti_buttons_dialog import (
    create_multi_buttons_dialog
)

from sohstationviewer.controller.processing import detect_data_type
from sohstationviewer.controller.util import (
    display_tracking_info, rt130_find_cf_dass, check_data_sdata,
)
from sohstationviewer.database.process_db import execute_db_dict, execute_db

from sohstationviewer.conf.constants import TM_FORMAT, ColorMode, CONFIG_PATH
from sohstationviewer.view.util.one_instance_at_a_time import \
    DialogAlreadyOpenedError


class MainWindow(QtWidgets.QMainWindow, UIMainWindow):
    current_directory_changed = QtCore.Signal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setup_ui(self)
        """
        vertical_ratio: ratio based selected font and min_font to spread out
            layout vertically according to the selected font
        """
        self.vertical_ratio: float = 1.
        """
        fig_height_in_ratio: ratio based selected font and min_font to resize
            plot's main_widget, figure and canvas vertically
        """
        self.fig_height_in_ratio: float = 1
        """
        dir_names: list of absolute paths of data sets
        """
        self.list_of_dir: List[Path] = []
        """
        current_dir: the current main data directory
        """
        self.current_dir: str = ''
        """
        save_plot_dir: directory to save plot
        """
        self.save_plot_dir: str = ''
        """
        save_plot_format: format to save plot
        """
        self.save_plot_format: str = 'SVG'
        """
        rt130_das_dict: dict by rt130 for data paths, so user can choose
            dasses to assign list of data paths to selected_rt130_paths
        """
        self.rt130_das_dict: Dict[str, List[str]] = {}
        """
        selected_rt130_paths: list of data paths for selected rt130 to pass
            to data_loader.init_loader for processing
        """
        self.selected_rt130_paths: List[Path] = []
        """
        rt130_log_files: list of log files to be read
        """
        self.rt130_log_files: List[Path] = []
        """
        data_type: str - type of data set
        """
        self.data_type: str = 'Unknown'
        """
        is_multiplex: flag showing if data_set is multiplex (more than one
            channels in a file)
        """
        self.is_multiplex = None
        """
        color_mode: str - the current color mode of the plot; can be either 'B'
            or 'W'
        """
        self.color_mode: ColorMode = 'B'

        self.data_loader: DataLoader = DataLoader()
        self.data_loader.finished.connect(self.replot_loaded_data)

        """
        processing_log: [(message, type)] - record the progress of processing
        """
        self.processing_log: List[Tuple[str, LogType]] = []
        """
        forms_in_forms_menu: List of forms in forms_menu
        """
        self.forms_in_forms_menu: List[QtWidgets.QWidget] = []
        """
        req_soh_chans: list of State-Of-Health channels to read data
            from. For Reftek, the list of channels is fixed => may not need
        """
        self.req_soh_chans: List[str] = []
        """
        req_wf_chans: list of waveform channels to read data from.
             For Reftek, it is [int,] which is list of index of data stream
             to read data from.
        """
        self.req_wf_chans: List[Union[str, int]] = []
        """
        start_tm: start time of data to read
        end_tm: end time of data to read
        """
        self.start_tm: float = 0
        self.end_tm: float = 0
        """
        data_object: Object that keep data read from data set for plotting
        """
        self.data_object: Union[GeneralData, None] = None
        """
        gap_minimum: minimum minutes of gap length to be display on gap bar
        """
        self.gap_minimum: Union[float, None] = None
        """
        pref_soh_list_name: name of selected preferred channels list
        """
        self.pref_soh_list_name: str = ''
        """
        pref_soh_list: selected preferred channels
        """
        self.pref_soh_list: List[str] = []
        """
        pref_soh_list_data_type: data type of the preferred channels list
        """
        self.pref_soh_list_data_type: str = 'Unknown'
        # Options
        """
        date_format: format for date
        """
        self.date_format: str = 'YYYY-MM-DD'
        """
        mass_pos_volt_range_opt: option for map value/color of mass position
        """
        self.mass_pos_volt_range_opt: str = 'regular'
        """
        bit_weight_opt: option for bitweight
        """
        self.bit_weight_opt: str = ''  # currently only need one option
        self.get_channel_prefer()

        """
        waveform_dlg: widget to display waveform channels' plotting
        """
        self.waveform_dlg: WaveformDialog = WaveformDialog(self)
        """
        tps_dlg: dialog to display time-power-squared of waveform channels
        """
        self.tps_dlg: TimePowerSquaredDialog = TimePowerSquaredDialog(self)
        """
        help_browser: Display help documents with searching feature.
        """
        self.help_browser: HelpBrowser = HelpBrowser()
        """
        about_dialog: Dialog showing info of the app.
        """
        self.about_dialog: AboutDialog = AboutDialog(self)
        """
        search_message_dialog: Display log, soh message with searching feature.
        """
        self.search_message_dialog: SearchMessageDialog = SearchMessageDialog()
        """
        gps_dialog: GPSDialog - widget to display plot of GPS points.
        """
        self.gps_dialog = GPSDialog(self)

        self.pull_current_directory_from_db()
        self.delete_old_temp_data_folder()

        self.has_problem: bool = False
        self.is_loading_data: bool = False
        self.is_plotting_soh: bool = False
        self.is_plotting_waveform: bool = False
        self.is_plotting_tps: bool = False
        self.tps_tab_total: int = 0
        self.stopped_tps_tab_total: int = 0
        self.is_stopping: bool = False

        self.yyyy_mm_dd_action.trigger()

    @QtCore.Slot()
    def open_about(self):
        """
        About dialog is always open. This function will raise it up when called
        """
        self.about_dialog.show()
        self.about_dialog.raise_()

    @QtCore.Slot()
    def save_plot(self):
        self.plotting_widget.save_plot('SOH-Plot')

    @QtCore.Slot()
    def open_data_type(self) -> None:
        """
        Open a dialog to add/edit data types in DB If a dialog of this type is
        currently open, raise it to the front.
        """
        try:
            win = DataTypeDialog(self)
            win.show()
        except DialogAlreadyOpenedError:
            DataTypeDialog.current_instance.activateWindow()
            DataTypeDialog.current_instance.raise_()

    @QtCore.Slot()
    def open_param(self) -> None:
        """
        Open a dialog to add/edit parameters in DB If a dialog of this type is
        currently open, raise it to the front.
        """
        try:
            win = ParamDialog(self, self.color_mode)
            win.show()
        except DialogAlreadyOpenedError:
            ParamDialog.current_instance.activateWindow()
            ParamDialog.current_instance.raise_()

    @QtCore.Slot()
    def open_channel(self) -> None:
        """
        Open a dialog to add/edit channels in DB If a dialog of this type is
        currently open, raise it to the front.
        """
        try:
            win = ChannelDialog(self)
            win.show()
        except DialogAlreadyOpenedError:
            ChannelDialog.current_instance.activateWindow()
            ChannelDialog.current_instance.raise_()

    @QtCore.Slot()
    def open_plot_type(self) -> None:
        """
        Open a dialog to view plot types and their description If a dialog of
        this type is currently open, raise it to the front.
        """
        try:
            win = PlotTypeDialog(self)
            win.show()
        except DialogAlreadyOpenedError:
            PlotTypeDialog.current_instance.activateWindow()
            PlotTypeDialog.current_instance.raise_()

    @QtCore.Slot()
    def open_calendar(self):
        """
        Open calendar tool
        """
        calendar = CalendarDialog(self)
        calendar.show()

    @QtCore.Slot()
    def open_help_browser(self):
        """
        Open Help Dialog to view and search help documents
        """
        self.help_browser.show()
        self.help_browser.raise_()

    @QtCore.Slot()
    def open_channel_preferences(self) -> None:
        """
        Open a dialog to view, select, add, edit, scan for preferred channels
            list. If a dialog of this type is currently open, raise it to the
            front.
        """
        try:
            self.get_file_list()
        except Exception as e:
            msg = f"{str(e)}!!!\n\nDo you want to continue?"
            result = QtWidgets.QMessageBox.question(
                self, "Confirmation", msg,
                QtWidgets.QMessageBox.StandardButton.Yes |
                QtWidgets.QMessageBox.StandardButton.No)
            if result == QtWidgets.QMessageBox.StandardButton.No:
                return
        try:
            win = ChannelPreferDialog(self, self.list_of_dir)
            win.show()
        except DialogAlreadyOpenedError:
            ChannelPreferDialog.current_instance.activateWindow()
            ChannelPreferDialog.current_instance.raise_()

    @QtCore.Slot()
    def from_data_card_check_box_clicked(self, is_from_data_card_checked):
        self.open_files_list.clear()
        self.set_open_files_list_texts()
        # self.search_button.setEnabled(not is_from_data_card_checked)

        self.log_checkbox.setEnabled(not is_from_data_card_checked)
        self.search_line_edit.setEnabled(not is_from_data_card_checked)
        # QLineEdit does not change its color when it is disabled unless
        # there is text inside, so we have to do it manually.
        palette = self.search_line_edit.palette()
        if is_from_data_card_checked:
            # We are copying the color of a disabled button
            search_line_edit_color = QColor(246, 246, 246)
        else:
            search_line_edit_color = QColor(255, 255, 255)
        palette.setColor(QPalette.Base, search_line_edit_color)
        self.search_line_edit.setPalette(palette)

        if not is_from_data_card_checked and self.search_line_edit.text():
            self.filter_folder_list(self.search_line_edit.text())

    @QtCore.Slot()
    def on_log_file_checkbox_toggled(self, is_checked):
        self.open_files_list.clear()
        self.set_open_files_list_texts()

        # We only disable the from data card checkbox because it has behavior
        # that conflicts with the log file checkbox. Other widgets are kept
        # enabled because they don't break what the log file checkbox does.
        self.from_data_card_check_box.setEnabled(not is_checked)
        self.filter_folder_list(self.search_line_edit.text())

    @QtCore.Slot()
    def all_wf_chans_clicked(self):
        if self.all_wf_chans_check_box.isChecked():
            self.mseed_wildcard_edit.clear()
            for cb in self.ds_check_boxes:
                cb.setChecked(False)

    @QtCore.Slot()
    def data_stream_clicked(self):
        ds_click = False
        for cb in self.ds_check_boxes:
            if cb.isChecked():
                ds_click = True
                break
        if ds_click:
            self.all_wf_chans_check_box.setChecked(False)

    @QtCore.Slot()
    def mseed_wildcard_changed(self):
        if self.mseed_wildcard_edit.text().strip() != "":
            self.all_wf_chans_check_box.setChecked(False)

    @QtCore.Slot()
    def all_soh_chans_clicked(self, checked: bool) -> None:
        """
        When "All SOH" is checked,
            + If checked, clear current IDs textbox
            + If unchecked, set current IDs textbox if there is a preferred
                channels list selected. If no list selected, re-check "All SOH"
        """
        if not checked:
            if self.pref_soh_list == []:
                self.all_soh_chans_check_box.setChecked(True)
            else:
                self.curr_pref_soh_list_name_txtbox.setText(
                    self.pref_soh_list_name)
        else:
            self.curr_pref_soh_list_name_txtbox.setText('')

    @QtCore.Slot()
    def set_date_format(self, display_format: str):
        """
        Sets the calendar format used by the QDateEdit text boxes.
        :param display_format: str - A valid display format to be used for date
            conversion.
        """
        display_format_to_qt_date_format_map = {
            'YYYY-MM-DD': 'yyyy-MM-dd',
            'YYYYMMMDD': 'yyyyMMMdd',
            'YYYY:DOY': 'yyyy:DOY'
        }
        qt_format = display_format_to_qt_date_format_map[display_format]
        self.time_to_date_edit.setDisplayFormat(qt_format)
        self.time_from_date_edit.setDisplayFormat(qt_format)
        self.date_format = display_format
        self.tps_dlg.date_format = self.date_format
        self.waveform_dlg.date_format = self.date_format

    @QtCore.Slot()
    def open_files_list_item_double_clicked(self, item: FileListItem):
        """
        Handles the double-click event emitted when a user double-clicks on an
        item contained within openFilesList

        :param item: FileListItem - The item contained within openFilesList
            which was clicked.
        """
        print(f'Opening {item.text()}')
        self.read_selected_files()

    @QtCore.Slot()
    def change_current_directory(self):
        """
        Constructs a QFileDialog to select a new working directory from which
            the user can load data. The starting directory is taken from
            curr_dir_line_edit.
        """
        fd = FileDialogForLinuxExternalDrive(
            self, self.curr_dir_line_edit.text())
        fd.exec()
        if fd.result() == QtWidgets.QDialog.DialogCode.Accepted:
            new_path = fd.selectedFiles()[0]
            self.set_current_directory(new_path)

    @QtCore.Slot()
    def plot_diff_data_set_id(self):
        if not self.data_object.select_diff_data_set_id():
            return
        self.clear_plots()
        self.replot_loaded_data()

    def get_requested_wf_chans(self) -> List[Union[str, int]]:
        """
        Getting requested data stream for RT130 data or mseed wildcards for
            non-RT130 data
        :return req_wf_chans: list of data streams or list of mseed wildcards
        :rtype: List[str, int]
        """
        req_wf_chans = []
        if (self.data_type != 'RT130' and
                (self.all_wf_chans_check_box.isChecked()
                 or self.mseed_wildcard_edit.text().strip() != "")
                and not self.tps_check_box.isChecked()
                and not self.raw_check_box.isChecked()):
            raise Exception(
                "Waveform channels have been selected but there are none of "
                "TPS or RAW checkboxes checked.\nPlease clear the "
                "selection of waveform if you don't want to display the data.")
        if self.all_wf_chans_check_box.isChecked():
            req_mseed_wildcards = ['*']
            req_dss = ['*']      # all data stream
        else:
            req_dss = []
            req_mseed_wildcards = []
            for idx, ds_checkbox in enumerate(self.ds_check_boxes):
                if ds_checkbox.isChecked():
                    req_dss.append(idx + 1)
            if self.mseed_wildcard_edit.text().strip() != "":
                req_mseed_wildcards = self.mseed_wildcard_edit.text(
                    ).split(",")
                req_mseed_wildcards = [
                    req.strip() for req in req_mseed_wildcards]

        if self.data_type == 'RT130':
            req_wf_chans = req_dss
            if req_dss != ['*'] and req_mseed_wildcards != []:
                msg = 'MSeed Wildcards will be ignored for RT130.'
                self.processing_log.append((msg, LogType.WARNING))
        else:
            req_wf_chans = req_mseed_wildcards
            if req_mseed_wildcards != ['*'] and req_dss != []:
                msg = ('Checked data streams will be ignored for '
                       'none-RT130 data type.')
                self.processing_log.append((msg, LogType.WARNING))
        if ((self.tps_check_box.isChecked() or self.raw_check_box.isChecked())
                and req_wf_chans == []):
            # avoid case of RAW or TPS are checked but no waveform are selected
            raise Exception(
                "TPS or RAW checkboxes shouldn't be checked if no waveform "
                "channels are selected."
                "\n\nPlease clear the TPS and RAW checkboxes if you don't "
                "want to display the data.")
        return req_wf_chans

    def get_requested_soh_chan(self):
        """
        Getting requested soh channels from preferred channel dialog
        :return list of requested soh channels
        :rtype: List[str]
        """
        if (not self.all_soh_chans_check_box.isChecked() and
                self.data_type != self.pref_soh_list_data_type):
            msg = (f"Data Type detected for the selected data set is "
                   f"{self.data_type} which is different from Channel "
                   f"Preferences {self.pref_soh_list_name}'s Data Type: "
                   f"{self.pref_soh_list_data_type}.\n\n"
                   f"SOHStationViewer will read all data available.\n\n"
                   f"Do you want to continue?")
            result = QtWidgets.QMessageBox.question(
                self, "Confirmation", msg,
                QtWidgets.QMessageBox.StandardButton.Yes |
                QtWidgets.QMessageBox.StandardButton.No)
            if result == QtWidgets.QMessageBox.StandardButton.No:
                return
            self.all_soh_chans_check_box.setChecked(True)
            self.curr_pref_soh_list_name_txtbox.setText('')
            return []
        return (self.pref_soh_list
                if not self.all_soh_chans_check_box.isChecked() else [])

    def get_data_type_from_selected_dirs(self) -> Tuple[str, bool]:
        dir_data_types = {}
        dir_is_multiplex = {}
        for dir in self.list_of_dir:
            possible_data_types, is_multiplex = detect_data_type(dir)
            dir_data_types[dir] = possible_data_types
            dir_is_multiplex[dir] = is_multiplex

        if len(set(dir_data_types.values())) > 1:
            dir_data_types_str = ''
            for dir, data_types in dir_data_types.values():
                dir_data_types_str += f'{dir}: {"".join(sorted(data_types))}'
            msg = (f"The selected directories contain different data types:\n"
                   f"{dir_data_types_str}\n\n"
                   f"Please have only data that is related to each other.")
            raise Exception(msg)
        if len(set(dir_is_multiplex.values())) > 1:
            msg = ("There are both multiplexed and non-multiplexed data "
                   "detected.\n\nPlease have only data that is related to "
                   "each other.")
            raise Exception(msg)

        possible_data_types = list(set(dir_data_types.values()))[0]
        if len(possible_data_types) == 0:
            data_type = 'Unknown'
        elif len(possible_data_types) == 1:
            data_type = list(possible_data_types)[0]
        else:
            info = ('Could not conclusively determine the data type of the '
                    'selected data set. This can happen when there are '
                    'multiple data types in the database with overlapping set '
                    'of SOH channels. To proceed, please select the data type '
                    'that best fits the data set.')
            data_type_choices = list(possible_data_types)
            data_type_choices.append('Unknown')
            data_type, ok = QInputDialog.getItem(self, 'Similar data types',
                                                 info, data_type_choices,
                                                 editable=False)
            if not ok:
                data_type = 'Unknown'
        is_multiplex = list(set(dir_is_multiplex.values()))[0]
        return data_type, is_multiplex

    def get_file_list(self):
        """
        Read from self.open_files_list to identify
            self.dir_names/self.selected_rt130_paths.
        self.data_type is identified here too.
        """
        self.list_of_dir = ['']
        self.selected_rt130_paths = []
        self.rt130_log_files = []
        root_dir = Path(self.curr_dir_line_edit.text())
        if self.log_checkbox.isChecked():
            for item in self.open_files_list.selectedItems():
                log_abspath = root_dir.joinpath(item.file_path)
                self.rt130_log_files.append(log_abspath)
            if not self.rt130_log_files:
                msg = "No RT130 log file were selected."
                raise Exception(msg)
            # Log files can only come from programs that work with RT130 data.
            self.data_type = 'RT130'
            self.is_multiplex = False
        elif self.rt130_das_dict != {}:
            # create selected_rt130_paths from the selected rt130 das names
            for item in self.open_files_list.selectedItems():
                # each item.file_path is a rt130 das name for this case
                self.selected_rt130_paths += self.rt130_das_dict[
                    item.file_path]

                # Checking size of files using list of dir
                self.list_of_dir = self.selected_rt130_paths

            if self.selected_rt130_paths == []:
                msg = "No RT130s have been selected."
                raise Exception(msg)
            self.data_type = 'RT130'
        elif self.from_data_card_check_box.isChecked():
            # When "From Memory Card" checkbox is checked, no sub directories
            # displayed in File List box. The current dir (root_dir) will be
            # the only one of the selected dir for user to start processing
            # from.
            self.list_of_dir = [root_dir]
            if self.data_radio_button.isEnabled():
                # In case of Baler's data, there will be 2 data folders for
                # user to select from, data/ and sdata/. The radio buttons
                # data and sdata allow user to select from Main Window.
                if self.data_radio_button.isChecked():
                    self.list_of_dir = [root_dir.joinpath('data')]
                else:
                    self.list_of_dir = [root_dir.joinpath('sdata')]
        else:
            # When "From Memory Card" checkbox is checked, sub directories of
            # the current dir (root_dir) will be displayed in File List box.
            # User can select one or more sub-directories to process from.
            self.list_of_dir = [
                root_dir.joinpath(item.text())
                for item in self.open_files_list.selectedItems()]
            has_data_sdata = False
            for folder in self.list_of_dir:
                has_data_sdata = check_data_sdata(folder)
                if has_data_sdata:
                    break
            if has_data_sdata:
                if len(self.list_of_dir) > 1:
                    msg = ("More than one folders are selected. At least one "
                           "of them has sub-folders data/ and sdata/.\n\n"
                           "It confuses SOH View.\n\n"
                           "SOH View will read only one folder with "
                           "sub-folder data/ and sdata/.")
                    raise Exception(msg)
                else:
                    msg = ("The selected folder contains 2 data folders:\n"
                           "'data/', 'sdata/'.\n\n"
                           "Please select one of them to read data from.")
                    result = create_multi_buttons_dialog(
                        msg, ['data/', 'sdata/'], has_abort=True)
                    if result == 0:
                        self.list_of_dir = [
                            self.list_of_dir[0].joinpath('data')]
                    elif result == 1:
                        self.list_of_dir = [
                            self.list_of_dir[0].joinpath('sdata')]
                    else:
                        raise Exception('Process has been cancelled by user.')

            if self.list_of_dir == []:
                msg = "No directories have been selected."
                raise Exception(msg)

        # Log files don't have a data type that can be detected, so we don't
        # detect the data type if we are reading them.
        if self.rt130_das_dict == {} and not self.log_checkbox.isChecked():
            self.data_type, self.is_multiplex = (
                self.get_data_type_from_selected_dirs()
            )
            if self.data_type == 'Unknown':
                # raise Exception for Unknown data_type here so that
                # data_type and is_multiplex is set in case user choose to
                # continue
                msg = ("There are no known data detected.\n\n"
                       "Do you want to cancel to select different folder(s)\n"
                       "Or continue to read any available mseed file?")
                raise Exception(msg)
        try:
            # get_requested_wf_chans have to be called after data_type is
            # detected
            self.req_wf_chans = self.get_requested_wf_chans()
        except Exception as e:
            QMessageBox.information(self, "Waveform Selection", str(e))
            self.cancel_loading()
            return

        if self.warn_big_file_sizes.isChecked():
            # call check_folder_size() here b/c it requires list_of_dir and it
            # is before the called for detect_data_type() which sometimes take
            # quite a long time.
            if not check_folders_size(self.list_of_dir, self.req_wf_chans):
                raise Exception("Big size")

    def clear_plots(self):
        self.plotting_widget.clear()
        self.waveform_dlg.plotting_widget.clear()
        for tps_widget in self.tps_dlg.tps_widget_dict.values():
            tps_widget.clear()

    def cancel_loading(self):
        display_tracking_info(self.tracking_info_text_browser,
                              "Loading cancelled",
                              LogType.WARNING)

    @QtCore.Slot()
    def read_selected_files(self):
        """
        Read data from selected files/directories, process and plot channels
            read from those according to current options set on the GUI
        """
        self.replot_button.setEnabled(False)
        self.plot_diff_data_set_id_button.setEnabled(False)
        display_tracking_info(self.tracking_info_text_browser,
                              "Loading started",
                              LogType.INFO)
        self.clear_plots()
        self.gps_dialog.clear_plot()
        self.gps_dialog.set_data_path('')
        self.gps_dialog.gps_points = []

        start_tm_str = self.time_from_date_edit.date().toString(
            QtCore.Qt.DateFormat.ISODate
        )
        end_tm_str = self.time_to_date_edit.date().toString(
            QtCore.Qt.DateFormat.ISODate
        )
        # start_tm is the beginning of the start date
        self.start_tm = UTCDateTime.strptime(start_tm_str, TM_FORMAT).timestamp
        beginning_of_end_date = UTCDateTime.strptime(end_tm_str, TM_FORMAT)
        # to cover time in the selected end date, end time must be at the
        # beginning of the next day, which means anytime >= this end_tm will be
        # excluded
        self.end_tm = beginning_of_end_date.timestamp + \
            constants.SECOND_IN_DAY
        if self.end_tm <= self.start_tm:
            msg = "To Date must be greater or equal than From Date."
            QtWidgets.QMessageBox.warning(self, "Wrong Date Given", msg)
            self.cancel_loading()
            return
        self.info_list_widget.clear()
        is_working = (self.is_loading_data or self.is_plotting_soh or
                      self.is_plotting_waveform or self.is_plotting_tps)
        if is_working:
            msg = 'Already working'
            display_tracking_info(self.tracking_info_text_browser,
                                  msg, LogType.INFO)
            return
        self.has_problem = False

        if self.gap_len_line_edit.text().strip() != '':
            # convert from minute to second
            minimum_gap_in_minutes = float(self.gap_len_line_edit.text())
            if minimum_gap_in_minutes < 0.1:
                msg = "Minimum Gap must be greater than 0.1 minute to be " \
                      "detected."
                QtWidgets.QMessageBox.warning(
                    self, "Invalid Minimum Gap request", msg)
                self.cancel_loading()
                return
            self.gap_minimum = minimum_gap_in_minutes * 60
        else:
            self.gap_minimum = None

        # if waveform channels are selected, Event DS will be read from EH/ET
        # header
        # rt130_waveform_data_req is to read data for wave form data
        rt130_waveform_data_req = False
        if self.raw_check_box.isChecked() or self.tps_check_box.isChecked():
            rt130_waveform_data_req = True

        if self.mseed_wildcard_edit.text().strip() != '':
            try:
                check_chan_wildcards_format(self.mseed_wildcard_edit.text())
            except Exception as e:
                QtWidgets.QMessageBox.warning(
                    self, "Incorrect Wildcard", str(e))
                self.cancel_loading()
                return

        try:
            del self.data_object
            self.data_object = None
            self.processing_log = []
            self.clear_actions_from_forms_menu()
        except AttributeError:
            pass

        try:
            self.get_file_list()
        except Exception as e:
            if 'no known data detected' in str(e):
                msgbox = QtWidgets.QMessageBox()
                msgbox.setWindowTitle('Do you want to continue?')
                msgbox.setText(str(e))
                msgbox.addButton(QtWidgets.QMessageBox.StandardButton.Cancel)
                msgbox.addButton(
                    'Continue', QtWidgets.QMessageBox.ButtonRole.YesRole
                )
                result = msgbox.exec()
                if result == QtWidgets.QMessageBox.StandardButton.Cancel:
                    self.cancel_loading()
                    return
            elif str(e) == "Big size":
                self.cancel_loading()
                return
            else:
                fmt = traceback.format_exc()
                QtWidgets.QMessageBox.warning(
                    self, "Select directory", str(fmt))
                self.cancel_loading()
                return

        self.req_soh_chans = self.get_requested_soh_chan()
        if self.req_soh_chans is None:
            self.cancel_loading()
            return

        self.data_loader.init_loader(
            self.data_type,
            self.tracking_info_text_browser,
            self.is_multiplex,
            self.list_of_dir,
            self.selected_rt130_paths,
            req_wf_chans=self.req_wf_chans,
            req_soh_chans=self.req_soh_chans,
            gap_minimum=self.gap_minimum,
            read_start=self.start_tm,
            read_end=self.end_tm,
            include_mp123=self.mass_pos_123zne_check_box.isChecked(),
            include_mp456=self.mass_pos_456uvw_check_box.isChecked(),
            rt130_waveform_data_req=rt130_waveform_data_req,
            rt130_log_files=self.rt130_log_files,
            include_masspos_in_soh_messages=(
                self.add_masspos_to_rt130_soh.isChecked())
        )

        self.data_loader.worker.finished.connect(self.data_loaded)
        self.data_loader.worker.stopped.connect(self.problem_happened)
        self.data_loader.worker.failed.connect(self.problem_happened)
        self.data_loader.thread.finished.connect(self.reset_flags)
        self.data_loader.connect_worker_signals()

        self.is_loading_data = True
        self.data_loader.load_data()

    def stop_load_data(self):
        # TODO: find a way to stop the data loader without a long wait.
        """
        Request the data loader thread to stop. The thread will stop at the
        earliest possible point, meaning that the wait is variable and can be
        very long.
        """
        if self.data_loader.running:
            self.is_loading_data = False
            self.data_loader.thread.requestInterruption()
            display_tracking_info(self.tracking_info_text_browser,
                                  'Stopping data loading...',
                                  LogType.INFO)

    @QtCore.Slot()
    def problem_happened(self):
        self.has_problem = True

    @QtCore.Slot()
    def stop(self):
        is_working = (self.is_loading_data or self.is_plotting_soh or
                      self.is_plotting_waveform or self.is_plotting_tps)
        if is_working:
            if self.is_stopping:
                msg = 'Already stopping'
                display_tracking_info(self.tracking_info_text_browser, msg,
                                      LogType.INFO)
                return
            self.is_stopping = True

        if self.is_loading_data:
            self.stop_load_data()
        if self.is_plotting_soh:
            display_tracking_info(self.tracking_info_text_browser,
                                  'Stopping SOH plot...')
        if self.is_plotting_waveform:
            display_tracking_info(self.waveform_dlg.info_text_browser,
                                  'Stopping waveform plot...')
            waveform_widget = self.waveform_dlg.plotting_widget
            waveform_widget.request_stop()

            self.waveform_dlg.plotting_widget.request_stop()
        if self.is_plotting_tps:
            display_tracking_info(self.tps_dlg.info_text_browser,
                                  'Stopping TPS plot...')
            for tps_widget in self.tps_dlg.tps_widget_dict.values():
                tps_widget.request_stop()

    def check_if_all_stopped(self):
        """
        Check if everything has been stopped. If true, reset the is_stopping
        flag.
        """
        not_all_stopped = (self.is_loading_data or self.is_plotting_soh or
                           self.is_plotting_waveform or self.is_plotting_tps)
        if not not_all_stopped:
            self.is_stopping = False

    @QtCore.Slot()
    def data_loaded(self, data_obj: GeneralData,
                    need_update_data_set_info: bool = False):
        """
        Process the loaded data.
        :param data_obj: the data object that contains the loaded data.
        :param need_update_data_set_info: to help raising exception if there is
            error when setting up data type for displaying in tracking_info box
        """
        self.is_loading_data = False
        self.data_object = data_obj

        gps_processing_msg = 'Extracting GPS data...'
        display_tracking_info(self.tracking_info_text_browser,
                              gps_processing_msg)
        if (self.data_type == 'Q330' and
                'LOG' not in data_obj.log_data[data_obj.selected_data_set_id]):
            log_message = ("Channel 'LOG' is required to get file info and "
                           "gps info for Q330.", LogType.WARNING)
            if need_update_data_set_info:
                raise Exception(log_message[0])
            self.processing_log.append(log_message)
            return
        try:
            self.gps_dialog.gps_points = extract_gps_data(data_obj)
        except ValueError as e:
            missing_gps_chans = e.args[0].split(': ')[1].strip('.').split(', ')
            for missing_chan in missing_gps_chans:
                log_message = (f'GPS channel {missing_chan} is missing.',
                               LogType.WARNING
                               )
                self.processing_log.append(log_message)
            log_message = ('Cannot get GPS data.', LogType.WARNING)
            self.processing_log.append(log_message)
        if self.rt130_log_files:
            data_path = self.rt130_log_files[0]
        elif self.selected_rt130_paths:
            # The selected RT130 paths looks something like
            # <Data directory>/<Data folder>/<Day 1>/<Station>,
            # <Data directory>/<Data folder>/<Day 2>/<Station>, ...
            # We want to grab the data folder, because it is the common
            # point of the paths.
            # Note: there can be more than one data folder. However, we
            # only need one.
            data_path = self.selected_rt130_paths[0].parent.parent
        elif self.list_of_dir:
            dir_path = self.list_of_dir[0]
            if dir_path.name == 'data' or dir_path.name == 'sdata':
                # When reading inside a Baler data set with the data card
                # option enabled, the data folder is appended with either
                # data or sdata.
                data_path = dir_path.parent
            else:
                data_path = dir_path
        self.gps_dialog.set_data_path(str(data_path))

        extract_data_info_msg = 'Extracting data set info...'
        display_tracking_info(self.tracking_info_text_browser,
                              extract_data_info_msg)
        data_set_info = extract_data_set_info(data_obj, self.date_format)
        self.info_list_widget.clear()
        for info_name, info in data_set_info.items():
            if isinstance(info, str):
                info_string = f'{info_name}:\n\t{info}'
            else:
                formatted_info = '\n\t'.join(info)
                info_string = f'{info_name}:\n\t{formatted_info}'
            # Tab characters are rendered as ~20 spaces in QT, which is too
            # many considering the size of the display box. So, we replace all
            # tab characters with a number of spaces to make everything fits
            # better.
            info_string = info_string.replace('\t', ' ' * 4)
            QListWidgetItem(info_string, self.info_list_widget)

    @QtCore.Slot()
    def replot_loaded_data(self):
        """
        Plot using data from self.data_object with the current options set
            from GUI
        """
        if self.data_object is None:
            return

        plotting_start_msg = 'Start plotting data...'
        display_tracking_info(self.tracking_info_text_browser,
                              plotting_start_msg)
        QCoreApplication.processEvents()
        self.replot_button.setEnabled(False)
        self.clear_plots()
        if self.has_problem:
            return
        font_ratio = self.base_plot_font_size / constants.MAX_FONTSIZE
        self.vertical_ratio = 0.45 + font_ratio
        self.fig_height_in_ratio = 2 * font_ratio
        self.is_plotting_soh = True
        self.plotting_widget.set_colors(self.color_mode)
        self.waveform_dlg.plotting_widget.set_colors(self.color_mode)
        for tps_widget in self.tps_dlg.tps_widget_dict.values():
            tps_widget.set_colors(self.color_mode)
        self.gps_dialog.set_colors(self.color_mode)

        d_obj = self.data_object

        time_tick_total = 5  # TODO: let user choose max ticks to be displayed

        selected_data_set_id = d_obj.selected_data_set_id
        d_obj.reset_all_selected_data()
        try:
            check_masspos(d_obj.mass_pos_data[selected_data_set_id],
                          selected_data_set_id,
                          self.mass_pos_123zne_check_box.isChecked(),
                          self.mass_pos_456uvw_check_box.isChecked())
        except Exception as e:
            self.processing_log.append((str(e), LogType.WARNING))

        try:
            self.plotting_widget.plot_channels(
                d_obj, selected_data_set_id, self.start_tm, self.end_tm,
                time_tick_total, self.req_soh_chans)
        except Exception:
            fmt = traceback.format_exc()
            msg = f"Can't plot SOH data due to error: {str(fmt)}"
            display_tracking_info(self.tracking_info_text_browser, msg,
                                  LogType.ERROR)
            self.reset_flags()
        finally:
            self.is_plotting_soh = False

        peer_plotting_widgets = [self.plotting_widget]

        if self.tps_check_box.isChecked():
            self.is_plotting_tps = True
            self.stopped_tps_tab_total = 0
            self.tps_dlg.set_data(
                self.data_type, ','.join([str(d) for d in self.list_of_dir]))
            self.tps_dlg.show()
            # The waveform and TPS plots is being stopped at the same time, so
            # we can't simply reset all flags. Instead, we use an intermediate
            # method that check whether all plots have been stopped before
            # resetting the is_stopping flag.
            self.tps_dlg.plot_channels(
                d_obj, selected_data_set_id, self.start_tm, self.end_tm)
            if len(d_obj.waveform_data[d_obj.selected_data_set_id]) == 0:
                # no channel sent => no processor is created
                # => the stopped signal won't have chance to emit
                # => reset_is_plotting_tps won't have chance to be called
                # so go ahead and reset is_plotting_tps here
                self.is_plotting_tps = False
            for tps_widget in self.tps_dlg.tps_widget_dict.values():
                tps_widget.stopped.connect(self.reset_is_plotting_tps)
                tps_widget.stopped.connect(self.check_if_all_stopped)
                peer_plotting_widgets.append(tps_widget)
            self.add_action_to_forms_menu('TPS Plot', self.tps_dlg)
        else:
            self.tps_dlg.hide()

        if self.raw_check_box.isChecked():
            self.is_plotting_waveform = True
            # waveformPlot
            peer_plotting_widgets.append(self.waveform_dlg.plotting_widget)
            self.waveform_dlg.set_data(
                self.data_type, ','.join([str(d) for d in self.list_of_dir]))
            self.waveform_dlg.show()
            waveform_widget = self.waveform_dlg.plotting_widget
            waveform_widget.stopped.connect(self.reset_is_plotting_waveform)
            waveform_widget.stopped.connect(self.check_if_all_stopped)
            self.waveform_dlg.plotting_widget.plot_channels(
                d_obj, selected_data_set_id,
                self.start_tm, self.end_tm, time_tick_total)
            self.add_action_to_forms_menu('Raw Data Plot', self.waveform_dlg)
        else:
            self.waveform_dlg.hide()

        self.plotting_widget.set_peer_plotting_widgets(peer_plotting_widgets)
        self.waveform_dlg.plotting_widget.set_peer_plotting_widgets(
            peer_plotting_widgets)
        for tps_widget in self.tps_dlg.tps_widget_dict.values():
            tps_widget.set_peer_plotting_widgets(
                peer_plotting_widgets)

        processing_log = (self.processing_log +
                          d_obj.processing_log +
                          self.plotting_widget.processing_log)
        self.search_message_dialog.setup_logview(
            selected_data_set_id, d_obj.log_data, processing_log)
        self.search_message_dialog.show()
        self.add_action_to_forms_menu('Search Messages',
                                      self.search_message_dialog)

    @QtCore.Slot()
    def reset_flags(self):
        """
        Reset the activity flags. Intended to be called when something goes
        wrong to reset the state of the program. Each step of the program
        should instead reset their own flag when they are finished.
        """
        self.is_loading_data = False
        self.is_plotting_soh = False
        self.is_plotting_waveform = False
        self.is_plotting_tps = False
        self.is_stopping = False
        try:
            if len(self.data_object.data_set_ids) > 1:
                self.plot_diff_data_set_id_button.setEnabled(True)
        except AttributeError:
            pass

    @QtCore.Slot()
    def reset_is_plotting_waveform(self):
        """
        Reset the is_plotting_waveform flag. Used because lambda does not allow
        assignment.
        """
        self.is_plotting_waveform = False

    @QtCore.Slot()
    def reset_is_plotting_tps(self):
        """
        For any tps_tab stopped, this function will add 1 to
        stopped_tps_tab_total.
        When all tps_tabs are stopped, is_plotting_tps is reset to False.
        """
        self.stopped_tps_tab_total += 1
        if self.stopped_tps_tab_total == self.tps_tab_total:
            self.is_plotting_tps = False

    def set_current_directory(self, path: str = '') -> None:
        """
        Update currentDirectory with path in DB table PersistentData.
        Set all directories under current directory to self.open_files_list.

        :param path: str - absolute path to current directory
        """
        # Remove entries when current directory changed
        self.open_files_list.clear()
        # Signal current_directory_changed, and gather list of files in new
        # current directory
        self.current_directory_changed.emit(path)
        self.current_dir = path
        self.save_plot_dir = path
        execute_db(f'UPDATE PersistentData SET FieldValue="{path}" WHERE '
                   'FieldName="currentDirectory"')
        self.set_open_files_list_texts()

    def set_open_files_list_texts(self):
        """
        Set texts in self.open_files_list
        """
        path = self.curr_dir_line_edit.text()
        self.data_radio_button.setEnabled(False)
        self.sdata_radio_button.setEnabled(False)
        try:
            self.rt130_das_dict = rt130_find_cf_dass(path)
            if self.log_checkbox.isChecked():
                for dent in pathlib.Path(path).iterdir():
                    # Currently, we only read file that has the extension .log.
                    # Dealing with general file is a lot more difficult and is
                    # not worth the time it takes to do so.
                    if dent.is_file() and dent.name.endswith('.log'):
                        self.open_files_list.addItem(FileListItem(dent))

            elif len(self.rt130_das_dict) != 0:
                for rt130_das in self.rt130_das_dict:
                    self.open_files_list.addItem(FileListItem(rt130_das))

            elif self.from_data_card_check_box.isChecked():
                self.open_files_list.addItem('Memory Card')
                self.tracking_info_text_browser.setText(
                    "Please select the card by clicking on button "
                    "'Main Data Directory'.")
                data_sdata = check_data_sdata(path)
                if data_sdata:
                    # Baler/B44 memory stick
                    self.data_radio_button.setEnabled(True)
                    self.sdata_radio_button.setEnabled(True)
            else:
                for dent in pathlib.Path(path).iterdir():
                    if not dent.is_dir() or dent.name.startswith('.'):
                        continue
                    self.open_files_list.addItem(FileListItem(dent))

        except FileNotFoundError:
            self.set_current_directory()

    def get_channel_prefer(self):
        """
        Read the current preferred channel list from database to set
            pref_soh_list_name, pref_soh_list, self.pref_soh_list_data_type
        """
        self.pref_soh_list_name = ''
        self.pref_soh_list = []
        self.data_type = 'Unknown'
        rows = execute_db_dict('SELECT name, preferredSOHs, dataType '
                               'FROM ChannelPrefer '
                               'WHERE current=1')
        if len(rows) > 0:
            self.pref_soh_list_name = rows[0]['name']
            self.pref_soh_list = [t.strip()
                                  for t in rows[0]['preferredSOHs'].split(',')
                                  if t.strip() != '']
            self.pref_soh_list_data_type = rows[0]['dataType']

    def pull_current_directory_from_db(self):
        """
        Set current directory with info saved in DB
        """
        rows = execute_db_dict(
            'SELECT FieldName, FieldValue FROM PersistentData '
            'WHERE FieldName="currentDirectory"')
        if len(rows) > 0 and rows[0]['FieldValue']:
            self.set_current_directory(rows[0]['FieldValue'])

    def write_config(self):
        """
        Write the current state of the program to the config file.
        """
        self.config.set('FileRead', 'from_data_card',
                        str(self.from_data_card_check_box.isChecked()))
        self.config.set('FileRead', 'data',
                        str(self.data_radio_button.isChecked()))
        self.config.set('FileRead', 'sdata',
                        str(self.sdata_radio_button.isChecked()))
        self.config.set('FileRead', 'log',
                        str(self.log_checkbox.isChecked()))
        self.config.set('ColorMode', 'black',
                        str(self.background_black_radio_button.isChecked()))
        self.config.set('ColorMode', 'white',
                        str(self.background_white_radio_button.isChecked()))
        self.config.set('Gap', 'min_gap_length',
                        self.gap_len_line_edit.text())
        self.config.set('Channels', 'mp123zne',
                        str(self.mass_pos_123zne_check_box.isChecked()))
        self.config.set('Channels', 'mp456uvw',
                        str(self.mass_pos_456uvw_check_box.isChecked()))
        self.config.set('Channels', 'all_waveform_chans',
                        str(self.all_wf_chans_check_box.isChecked()))
        for i, checkbox in enumerate(self.ds_check_boxes, start=1):
            self.config.set('Channels', f'ds{i}', str(checkbox.isChecked()))
        self.config.set('Channels', 'mseed_wildcard',
                        self.mseed_wildcard_edit.text())
        self.config.set('Channels', 'plot_tps',
                        str(self.tps_check_box.isChecked()))
        self.config.set('Channels', 'plot_raw',
                        str(self.raw_check_box.isChecked()))
        self.config.set('ChannelsPreference', 'all_soh',
                        str(self.all_soh_chans_check_box.isChecked()))
        self.config.set('ChannelsPreference', 'pref_code',
                        self.curr_pref_soh_list_name_txtbox.text())
        # If from date has the default value, it is very likely that the user
        # does not care about the value of the from date. So, we give the from
        # date the default value in the config file. The same holds for to
        # date.
        from_date = self.time_from_date_edit.date().toString('yyyy-MM-dd')
        if from_date == constants.DEFAULT_START_TIME:
            from_date = ''
        to_date = self.time_to_date_edit.date().toString('yyyy-MM-dd')
        if to_date == date.today().strftime('%Y-%m-%d'):
            to_date = ''
        self.config.set('DateRange', 'from_date', from_date)
        self.config.set('DateRange', 'to_date', to_date)
        self.config.set('MiscOptions', 'mp_color_mode',
                        'regular' if self.mp_regular_color_action.isChecked()
                        else 'trillium')
        self.config.set('MiscOptions', 'tps_color_mode',
                        self.tps_dlg.color_range_choice.currentText())
        if self.yyyy_mm_dd_action.isChecked():
            date_mode = 'YYYY-MM-DD'
        elif self.yyyymmmdd_action.isChecked():
            date_mode = 'YYYYMMMDD'
        elif self.yyyy_doy_action.isChecked():
            date_mode = 'YYYY:DOY'
        else:
            raise Exception('Something is very wrong. No date mode is chosen.'
                            'Please contact the software group.')
        self.config.set('MiscOptions', 'date_mode', date_mode)

        base_plot_font_size = None
        for i in range(constants.MIN_FONTSIZE, constants.MAX_FONTSIZE + 1):
            if self.base_plot_font_size_action_dict[i].isChecked():
                base_plot_font_size = i
                break
        if base_plot_font_size is None:
            raise Exception('Something is very wrong. No base font size is '
                            'chosen. Please contact the software group.')
        self.config.set('MiscOptions', 'base_plot_font_size',
                        str(base_plot_font_size))

        self.config.set('MiscOptions', 'add_mass_pos_to_soh',
                        str(self.add_masspos_to_rt130_soh.isChecked()))
        with open(CONFIG_PATH, 'w+') as file:
            self.config.write(file)

    def delete_old_temp_data_folder(self) -> None:
        """
        Delete temp_data_folder which is used for keeping memmap files in case
            SOHView wasn't closed correctly.
        """
        rows = execute_db(
            'SELECT FieldValue FROM PersistentData '
            'WHERE FieldName="tempDataDirectory"')
        temp_data_folder = rows[0][0]
        try:
            shutil.rmtree(temp_data_folder)
            execute_db(
                f'UPDATE PersistentData SET FieldValue="{None}" WHERE'
                f' FieldName="tempDataDirectory"'
            )
        except (FileNotFoundError, TypeError):
            pass

    def add_action_to_forms_menu(
            self, form_name: str, form: QtWidgets.QWidget) -> None:
        """
        Creating and adding an action to forms_menu, connect to function
            raise_form() to help opening and raising the form on top of
            others when triggering the action.
        Adding the form into forms_in_forms_menu to handle closing it when
            loading new data set.

        :param form_name: name of form that will be shown in forms_menu
        :type form_name: str
        :param form: QtWidget
        :type form: QtWidgets.QWidget
        """
        if not isinstance(form, QtWidgets.QWidget):
            print(f"DEVELOPING ERROR: Type of form must be 'QWidget' instead "
                  f"of '{form.__class__.__name__}'")
        if form not in self.forms_in_forms_menu:
            action = QtGui.QAction(form_name, self)
            self.forms_menu.addAction(action)
            action.triggered.connect(lambda: self.raise_form(form))
            self.forms_in_forms_menu.append(form)

    def clear_actions_from_forms_menu(self) -> None:
        """
        Remove all actions from forms_menu aside from the main window action.
        Close all forms related to forms_menu.
        """
        self.forms_menu.clear()  # remove all actions
        self.forms_menu.addAction(self.main_window_action)
        for i in range(len(self.forms_in_forms_menu) - 1, -1, -1):
            form = self.forms_in_forms_menu.pop(i)
            form.close()

    @QtCore.Slot(QtWidgets.QWidget)
    def raise_form(self, form: QtWidgets.QWidget) -> None:
        form.show()  # in case form has been closed
        form.raise_()  # to raise form on top of others

    @QtCore.Slot()
    def open_gps_plotter(self):
        self.gps_dialog.show()
        self.gps_dialog.raise_()

    @QtCore.Slot()
    def restore_default_database(self):
        """
        Restore the working database to its original state by replacing its
        content with the content of the backup database. If something goes
        wrong when restoring the database, keep the working database in its
        current state and notify the user.
        """
        reset_prompt = ('Do you want to reset the database to its default '
                        'state?')
        is_do_reset = QMessageBox.question(self, 'Restore Default Database',
                                           reset_prompt)
        if is_do_reset:
            # We want a backup of the working database to restore to if
            # something goes wrong when restoring from the backup database.
            current_main_db_backup = sqlite3.connect(':memory:')
            main_db = sqlite3.connect(dbConf['db_path'])
            backup_db = sqlite3.connect(dbConf['backup_db_path'])

            main_db.backup(current_main_db_backup)

            # Copy the content of the backup database into the working
            # database. Using SQLite's built-in backup to make sure that there
            # is no problem with the backup file.
            backup_db.backup(main_db)

            # Do one final check to make sure that the copy happened
            # successfully.
            # https://stackoverflow.com/questions/37195819/comparing-2-sqlite-databases  # noqa
            # The method used for comparing the content of two sqlite databases
            # comes from the link above.
            main_db_dump = list(main_db.iterdump())
            backup_db_dump = list(backup_db.iterdump())
            if main_db_dump != backup_db_dump:
                current_main_db_backup.backup(main_db)
                error_msg = ('Something went wrong while resetting the '
                             'working database. Please try again.')
                QMessageBox.warning(self, 'Resetting database failed',
                                    error_msg)
            current_main_db_backup.close()
            main_db.close()
            backup_db.close()

    @QtCore.Slot()
    def set_plots_color(self, checked: bool, color_mode: ColorMode) -> None:
        """
        Slot called when a new color mode radio button is pressed.

        :param checked: whether the button that calls this slot is checked
        :param color_mode: the color mode associated with the button that calls
            this slot
        """
        if not checked:
            return
        self.color_mode = color_mode

    @QtCore.Slot()
    def clear_file_search(self):
        """
        Clear the content of the file search widget.
        """
        self.search_line_edit.clear()
        self.set_current_directory(self.current_dir)

    def filter_folder_list(self, search_text: str) -> None:
        """
        Filter the current list of folders based on the search input.
        """
        if search_text == '':
            self.clear_file_search()
            return

        self.set_current_directory(self.current_dir)

        open_file_paths = [
            self.open_files_list.item(i).file_path
            for i in range(self.open_files_list.count())
        ]
        filtered_file_paths = [
            path
            for path in open_file_paths
            if search_text.casefold() in path.casefold()
        ]

        # We are inserting the widgets in reverse order because doing so means
        # that we only have to insert into the 0th row. Otherwise, we would
        # have to keep track of the current index to insert into.

        # Create a line that separate filtered list of directories and list of
        # directories.
        separator_list_item = QListWidgetItem()
        separator_list_item.setFlags(QtCore.Qt.ItemFlag.NoItemFlags)
        separator_list_item.setSizeHint(
            QSize(separator_list_item.sizeHint().width(), 10)  # noqa
        )
        self.open_files_list.insertItem(0, separator_list_item)
        line = QFrame()
        line.setFrameShape(QFrame.Shape.HLine)
        self.open_files_list.setItemWidget(separator_list_item, line)

        for path in reversed(filtered_file_paths):
            current_file_list_item = FileListItem(path)
            self.open_files_list.insertItem(0, current_file_list_item)

        found_files_list_item = QListWidgetItem('Found files:')
        bold_font = QFont()
        bold_font.setBold(True)
        found_files_list_item.setFont(bold_font)
        found_files_list_item.setFlags(QtCore.Qt.ItemFlag.NoItemFlags)
        found_files_list_item.setForeground(QtCore.Qt.GlobalColor.black)
        self.open_files_list.insertItem(0, found_files_list_item)

    # ======================== EVENTS ==========================

    def resizeEvent(self, event):
        """
        OVERRIDE Qt method.
        When main_window is resized, its plotting_widget need to initialize
            its size to fit the viewport.

        :param event: QResizeEvent - resize event
        """
        self.plotting_widget.init_size()
        return super().resizeEvent(event)

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        """
        Cleans up when the user exits the program.

        :param event: parameter of method being overridden
        """
        display_tracking_info(self.tracking_info_text_browser,
                              'Cleaning up...',
                              LogType.INFO)
        if self.data_loader.running:
            self.data_loader.thread.requestInterruption()
            self.data_loader.thread.quit()
            self.data_loader.thread.wait()

        # If we don't explicitly clean up the running processing threads,
        # there is a high chance that they will attempt to access temporary
        # files that have already been cleaned up. While this should not be a
        # problem, it is still a bad idea to touch the file system when you are
        # not supposed to.
        if self.is_plotting_waveform:
            self.waveform_dlg.plotting_widget.request_stop()
            self.waveform_dlg.plotting_widget.thread_pool.waitForDone()

        if self.is_plotting_tps:
            for tps_widget in self.tps_dlg.tps_widget_dict.values():
                tps_widget.request_stop()
                tps_widget.thread_pool.waitForDone()

        # close all remaining windows
        for window in QtWidgets.QApplication.topLevelWidgets():
            window.close()

        self.write_config()
