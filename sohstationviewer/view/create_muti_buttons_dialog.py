from typing import List

from PySide6 import QtWidgets


def create_multi_buttons_dialog(
        msg: str, button_labels: List[str], has_abort) -> int:
    """
    Create a modal dialog with buttons. Show the dialog and send the user's
    choice to the data object being created.

    :param msg: the instruction shown to the user
    :param button_labels: the list of labels that are shown on the buttons
    :param has_abort: flag to add Abort button or not
    :return chosen_idx: index of selected button. If abort_button is selected,
        return -1
    """
    msg_box = QtWidgets.QMessageBox()
    msg_box.setText(msg)
    buttons = []
    for label in button_labels:
        # RT130's labels have type Tuple[str, int], so we need to convert
        # them to strings.
        if not isinstance(label, str):
            # When we convert a tuple to a string, any strings in the tuple
            # will be surrounded by quotes in the result string. We remove
            # those quotes before displaying them to the user for aesthetic
            # reasons.
            label = str(label).replace("'", '').replace('"', '')
        buttons.append(
            msg_box.addButton(label,
                              QtWidgets.QMessageBox.ButtonRole.ActionRole)
        )
    if has_abort:
        abort_button = msg_box.addButton(
            QtWidgets.QMessageBox.StandardButton.Abort
        )

    msg_box.exec()
    try:
        if msg_box.clickedButton() == abort_button:
            return -1
    except UnboundLocalError:
        pass
    chosen_idx = buttons.index(msg_box.clickedButton())
    return chosen_idx
