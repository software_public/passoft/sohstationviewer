#!/usr/bin/env python3
import os
import sys
import traceback
from pathlib import Path

from PySide6 import QtWidgets
from PySide6.QtWidgets import QMessageBox

from sohstationviewer.view.main_window import MainWindow
from sohstationviewer.conf.config_processor import (
    ConfigProcessor,
    BadConfigError,
)
from sohstationviewer.conf import constants


def fix_relative_paths() -> None:
    """
    Change the working directory so that the relative paths in the code work
    correctly.
    """
    current_file_path = os.path.abspath(__file__)
    current_file_dir = Path(current_file_path).parent
    os.chdir(current_file_dir)


def resize_windows(main_window: MainWindow):
    """
    Resize the plotting windows in the program so that they fit well on all
    screen resolutions.
    """
    screen_size = QtWidgets.QApplication.primaryScreen().size().toTuple()
    screen_width, screen_height = screen_size
    main_window.resize(screen_width * 1, screen_height * 1)
    main_window.waveform_dlg.resize(screen_width * (2 / 3),
                                    screen_height * (2 / 3))
    main_window.tps_dlg.resize(screen_width * (2 / 3), screen_height * (2 / 3))

    main_right_x = (main_window.x() + main_window.frameSize().width())
    # This offset is based on the hard-coded geometry previously assigned to
    # the waveform and TPS dialogs
    wf_tps_x_offset = 50
    # We are moving the TPS dialog so that it is slightly offset from the right
    # edge of the main window
    tps_dlg_x = main_right_x - main_window.tps_dlg.width() - wf_tps_x_offset
    wf_tps_y = 50
    main_window.waveform_dlg.move(wf_tps_x_offset, wf_tps_y)
    main_window.tps_dlg.move(tps_dlg_x, wf_tps_y)

    gps_dlg_y = main_window.height() / 5
    main_window.gps_dialog.move(main_window.gps_dialog.y(), gps_dlg_y)


def check_if_user_want_to_reset_config() -> bool:
    """
    Show a dialog asking the user if they want to reset the config.
    :return: whether the user wants to reset the config.
    """
    bad_config_dialog = QMessageBox()
    bad_config_dialog.setText('Something went wrong when reading the '
                              'config.')
    bad_config_dialog.setDetailedText(traceback.format_exc())
    bad_config_dialog.setInformativeText('Do you want to reset the config '
                                         'file?')
    bad_config_dialog.setStandardButtons(QMessageBox.StandardButton.Ok |
                                         QMessageBox.StandardButton.Close)
    bad_config_dialog.setDefaultButton(QMessageBox.StandardButton.Ok)
    bad_config_dialog.setIcon(QMessageBox.Icon.Critical)
    reset_choice = bad_config_dialog.exec()
    return reset_choice == QMessageBox.StandardButton.Ok


def main():
    # Change the working directory so that relative paths work correctly.
    fix_relative_paths()
    print(f"SOHViewer - Version {constants.SOFTWARE_VERSION}")
    app = QtWidgets.QApplication(sys.argv)
    wnd = MainWindow()

    config = ConfigProcessor()
    config.load_config()
    do_reset = None
    try:
        config.validate_config()
        config.apply_config(wnd)
    except (BadConfigError, ValueError):
        do_reset = check_if_user_want_to_reset_config()
    if do_reset:
        try:
            config.reset()
        except OSError:
            QMessageBox.critical(None, 'Cannot reset config',
                                 'Config file cannot be reset.  Please ensure '
                                 'that it is not opened in another program.',
                                 QMessageBox.StandardButton.Close)
            sys.exit(1)
    elif do_reset is not None:
        sys.exit(1)
    config.apply_config(wnd)

    resize_windows(wnd)
    wnd.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
