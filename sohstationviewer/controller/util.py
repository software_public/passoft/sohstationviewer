"""
basic functions: format, validate, display tracking
"""

import os
import re
import string
from datetime import datetime
from pathlib import Path
from typing import Tuple, List, Union, Dict

from PySide6 import QtCore
from PySide6.QtCore import QObject, Slot
from PySide6.QtWidgets import QTextBrowser, QApplication

from obspy import UTCDateTime

from sohstationviewer.view.util.enums import LogType
from sohstationviewer.conf.dbSettings import dbConf


class DisplayTrackingInfoWrapper(QObject):
    """
    QObject wrapper for the function display_tracking_info. This class allows
    display_tracking_info to be used from another thread.

    Before PySide 6.8, this class was not required because functions are
    always run on the main thread if they are used as slots. PySide 6.8
    changed how signals and slots are implemented, however, which causes
    these functions to instead be run in the same thread as the signal which
    called them. This class is used to fix that by always setting its thread
    affinity to the main thread, which means all of its slots will run in the
    main thread.
    """
    def __init__(self):
        super().__init__()
        self.moveToThread(QApplication.instance().thread())

    @Slot()
    def display_tracking_info(self, tracking_box: QTextBrowser, text: str,
                              type: LogType = LogType.INFO):
        """
        Wrapper for the function display_tracking_info.
        """
        display_tracking_info(tracking_box, text, type)


def validate_file(path2file: Union[str, Path], file_name: str):
    """
    Check if fileName given is a file and not info file
    :param path2file: absolute path to file
    :param file_name: name of the file
    :return: True if pass checking, False if not.
    """
    if file_name.startswith('.'):
        # skip info file
        return False

    if not os.path.isfile(path2file):
        return False
    return True


def validate_dir(path: str):
    """
    When selecting option "From Memory Card", all files and folders out of
        the data folder may be processed. This check help to skip checking
        system/info/meta folders. If check fail, raise Exception.

    :param path: absolute path to foler
    """
    path_parts = path.split(os.sep)

    if (any('System Volume Information' in part for part in path_parts) or
            any(part.startswith('.') for part in path_parts)):
        raise Exception(f"Skip info folder: {path}")


@QtCore.Slot()
def display_tracking_info(tracking_box: QTextBrowser, text: str,
                          type: LogType = LogType.INFO):
    """
    Display text in the given widget with different background and text colors
    :param tracking_box: widget to display tracking info
    :param text: info to be displayed
    :param type: (info/warning/error) type of info to be displayed in
        different color
    """
    if tracking_box is None:
        print(f"{type.name}: {text}")
        return

    msg = {'text': text}
    if type == LogType.ERROR:
        msg['color'] = 'white'
        msg['bgcolor'] = '#c45259'
    elif type == LogType.WARNING:
        msg['color'] = 'white'
        msg['bgcolor'] = '#c4a347'
    else:
        msg['color'] = 'blue'
        msg['bgcolor'] = 'white'
    html_text = """<body>
        <div style='color:%(color)s'>
            <strong><pre>%(text)s</pre></strong>
        </div>
        </body>"""
    tracking_box.setHtml(html_text % msg)
    tracking_box.setStyleSheet(f"background-color: {msg['bgcolor']}")
    # parent.update()
    tracking_box.repaint()


def get_dir_size(dir: str) -> Tuple[int, int]:
    """
    Get size of directory and size of file.
    :param dir: absolute path to directory
    :return:
        total_size: total size of the directory
        total_file: total file of the directory
    """

    total_size = 0
    total_file = 0
    for path, subdirs, files in os.walk(dir):
        for file_name in files:
            if not validate_file(os.path.join(path, file_name), file_name):
                continue
            fp = os.path.join(path, file_name)
            total_size += os.path.getsize(fp)
            total_file += 1
    return total_size, total_file


def get_time_6(time_str: str) -> Tuple[float, int]:
    """
    Get time from 6 parts string.
    (year:day of year:hour:minute:second:millisecond)
    Ex: 01:251:09:41:35:656/ 2001:251:09:41:35:656
    in which year in the first part can be 2 digits or 6 digits
    :param time_str: 6 part time string
    :return the epoch time and the year of time_str.
    """
    year = time_str.split(':')[0]
    if len(year) == 2:
        return get_time_6_2y(time_str)
    else:
        return get_time_6_4y(time_str)


def get_time_6_2y(time_str: str) -> Tuple[float, int]:
    """
    Get time from 6 parts string in which year has 2 digits.
    Ex: 01:251:09:41:35:656
    :param time_str: 6 part time string with 2 digits for year
    :return the epoch time and the year of time_str.
    """
    # pad 0 so the last part has 6 digits to match with the format str
    time_str = time_str.ljust(22, "0")
    time = datetime.strptime(time_str, "%y:%j:%H:%M:%S:%f")
    utc_time = UTCDateTime(time)
    return utc_time.timestamp, time.year


def get_time_6_4y(time_str: str) -> Tuple[float, int]:
    """
    Get time from 6 parts string in which year has 4 digits.
    Ex: 2001:251:09:41:35:656
    :param time_str: 6 part time string with 4 digits for year
    :return the epoch time and the year of time_str.
    """
    # pad 0 so the last part has 6 digits to match with the format str
    time_str = time_str.ljust(24, "0")
    time = datetime.strptime(time_str, "%Y:%j:%H:%M:%S:%f")
    utc_time = UTCDateTime(time)
    return utc_time.timestamp, time.year


def get_time_4(time_str: str, nearest_prior_epoch: float) -> float:
    """
    Get time from 4 parts string (day of year:hour:minute:second). If the day
    of year in the time string is smaller than the
    Ex: 253:19:41:42
    :param time_str: time string
    :param nearest_prior_epoch: the nearest epoch time detected
    :return: the epoch time parsed from time_str
    """
    nearest_utcdatetime = UTCDateTime(nearest_prior_epoch)
    year = nearest_utcdatetime.year
    nearest_doy = nearest_utcdatetime.julday
    time_str_doy = int(time_str[:3])
    if nearest_doy > time_str_doy:
        year += 1
    time_str = f'{str(year)}:{time_str}'
    time = datetime.strptime(time_str, "%Y:%j:%H:%M:%S")
    return UTCDateTime(time).timestamp


def get_val(text: str) -> float:
    """
    Get the value part of a string. Examples:
        - 6.5V -> 6.5
        - <=2.2 -> 2.2
        - <2 -> 2.0
        - 6m -> 6.0
    :param text: value string.
    :return: value part including +/-, remove str that follows
        and remove '<' and '=' characters
    """
    text = text.replace('=', '').replace('<', '')
    # Retrieve a number (with sign if available) from a piece of text.
    re_val = '^\+?\-?[0-9]+\.?[0-9]?'            # noqa: W605
    return float(re.search(re_val, text).group())


def rtn_pattern(text: str, upper: bool = False) -> str:
    """
    This function is from logpeek's rtn_pattern.
    return routine pattern of the string with:
     + 0 for digit
     + a for lowercase
     + A for upper case
     + remain special character
    :param text: text to get format
    :param upper: flag of whether to convert all alphabetic characters
        to A
    :return rtn: routine pattern of the string
    """
    rtn = ""
    for c in text:
        if c.isdigit():
            rtn += "0"
        elif c.isupper():
            rtn += "A"
        elif c.islower():
            rtn += "a"
        else:
            rtn += c
    # So the A/a chars will always be A, so the caller knows what to look for
    if upper is True:
        return rtn.upper()
    return rtn


def add_thousand_separator(value: float) -> str:
    """
    This function is from logpeek's fmti
    Given Value will be convert to a string integer with thousand separators
    :param value: string of value with unit
    :return new_value: new value with no unit and with thousand separators
    """
    value = int(value)
    if value > -1000 and value < 1000:
        return str(value)
    value = str(value)
    new_value = ""
    # There'll never be a + sign.
    if value[0] == "-":
        offset = 1
    else:
        offset = 0
    count_digits = 0
    for i in range(len(value) - 1, -1 + offset, -1):
        new_value = value[i] + new_value
        count_digits += 1
        if count_digits == 3 and i != 0:
            new_value = "," + new_value
            count_digits = 0
    if offset != 0:
        if new_value.startswith(","):
            new_value = new_value[1:]
        new_value = value[0] + new_value
    return new_value


def is_hex(text: str) -> bool:
    """
    Check if text is hexadecimal
    :param text: the given text
    :return: True if text is hexadecimal, otherwise False
    """
    return all(c in string.hexdigits for c in text)


def rt130_find_cf_dass(root_dir: str) -> Dict[str, List[Path]]:
    """
    This fuction is from logpeek.rt130_find_cf_dass()

    Scans through root_dir directory, if found a directory with format
        YYYYDOY (7 digits), look for its sub-directories that have
        RT130 DAS format (4 chars, hex) to add their paths to das_dirs
        which is dictionary of path to data folder by DAS numbers
    :param root_dir: path to current data directory
    :return: Dict by RT130 name of absolute paths to folders of the data
    """
    das_dict = {}
    if root_dir.endswith(os.sep) is False:
        root_dir += os.sep
    day_dirs = os.listdir(root_dir)
    for day_dir in day_dirs:
        if day_dir.startswith('.'):
            continue
        if len(day_dir) == 7 and day_dir.isdigit():
            # check for directory with format YYYYDOY: 7 digits
            try:
                das_dirs = os.listdir(root_dir + day_dir)
            except OSError:
                continue
            for das_dir in das_dirs:
                if len(das_dir) == 4 and is_hex(das_dir):
                    # check for directory with RT130 DAS format: 4 chars of hex
                    das_path = Path(root_dir).joinpath(day_dir, das_dir)
                    if das_dir not in das_dict.keys():
                        das_dict[das_dir] = []
                    das_dict[das_dir].append(das_path)
        else:
            return {}
    return das_dict


def check_data_sdata(root_dir: str) -> bool:
    """
    Check if the current root_dir includes 'data' and 'sdata' folders.
    :param root_dir: path to current data directory
    :return: True if the current root_dir includes 'data' and 'sdata' folders
            otherwise, False
    """
    dir_list = [d for d in os.listdir(root_dir)
                if os.path.isdir(os.path.join(root_dir, d))]
    return 'data' in dir_list and 'sdata' in dir_list


def check_chan(chan_id: str, req_soh_chans: List[str], req_wf_chans: List[str],
               include_mp123zne: bool, include_mp456uvw: bool) \
        -> Union[str, bool]:
    """
    Check if chanID is a requested channel.
    :param chan_id: str - channel ID
    :param req_soh_chans: list of str - requested SOH channels
    :param req_wf_chans: list of str - requested waveform channels
    :param include_mp123zne: if mass position channels 1,2,3 are requested
    :param include_mp456uvw: if mass position channels 4,5,6 are requested

    :return: str/bool -
        'WF' if chanID is a requested waveform channel,
        'SOH' if chanID is a requested SOH channel,
        'MP' if chanID is a requested mass position channel
        False otherwise.
    """
    if chan_id.startswith('VM'):
        if (not include_mp123zne and
                chan_id[-1] in ['1', '2', '3', 'Z', 'N', 'E']):
            return False
        if (not include_mp456uvw
                and chan_id[-1] in ['4', '5', '6', 'U', 'V', 'W']):
            return False
        return 'MP'

    ret = check_wf_chan(chan_id, req_wf_chans)
    if ret[0] == 'WF':
        if ret[1]:
            return "WF"
        else:
            return False
    if check_soh_chan(chan_id, req_soh_chans):
        return "SOH"
    return False


def check_soh_chan(chan_id: str, req_soh_chans: List[str]) -> bool:
    """
    Check if chan_id is a requested SOH channel.
    Mass position is always included.
    This function is used for mseed only so mass position is 'VM'.
    If there is no reqSOHChans, it means all SOH channels are requested
    :param chan_id: str - channel ID
    :param req_soh_chans: list of str - requested SOH channels
    :return: bool - True if chan_id is a requested SOH channel. False otherwise
    """
    if req_soh_chans == []:
        return True
    if chan_id in req_soh_chans:
        return True
    if 'EX?' in req_soh_chans and chan_id.startswith('EX'):
        if chan_id[2] in ['1', '2', '3']:
            return True
    # TODO: remove mass position channels from SOH
    if chan_id.startswith('VM'):
        if chan_id[2] in ['0', '1', '2', '3', '4', '5', '6']:
            return True
    return False


def check_wf_chan(chan_id: str, req_wf_chans: List[str]) -> Tuple[str, bool]:
    """
    Check if chanID is a waveform channel and is requested by user
    :param chan_id: str - channel ID
    :param req_wf_chans: list of str - requested waveform channels
    :return wf: str - '' if chan_id is not a waveform channel.
                      'WF' if chan_id is a waveform channel.
    :return has_chan: bool - True if chan_id is a requested waveform channel.
    """
    if not dbConf['seisRE'].match(chan_id):
        return '', False

    for req in req_wf_chans:
        if len(req) == 1:
            req = req.replace('*', '...')
        elif len(req) == 2:
            req = req.replace('*', '..')
        elif len(req) == 3:
            req = req.replace('*', '.')

        if re.compile(f'^{req}$').match(chan_id):
            return 'WF', True

    return 'WF', False


def get_valid_file_count(list_of_dir: Path) -> int:
    """
    Get total number of valid files in valid directories from the list_of_dir
    :param list_of_dir: list of paths to the folders of data
    :return total: total number of valid files
    """
    total = 0
    for folder in list_of_dir:
        for path, _, files in os.walk(folder):
            try:
                validate_dir(path)
            except Exception:
                continue
            total += len([f for f in files
                         if validate_file(Path(path).joinpath(f), f)])
    return total


def _format_time_item(
        unit: str, value: Union[int, float], time_list: List[str]):
    """
    Append the format string of time item to time_list if not zero

    :param unit: a time unit (day/hour/minute/second)
    :param value: value of the time item
    :param time_list: list of different formatted time item
    """
    if value == 0:
        return
    else:
        formatted = f"{value} {unit}"
        if value > 1:
            formatted += 's'
    time_list.append(formatted)


def get_formatted_time_delta(time_delta: float) -> str:
    """
    Convert time_delta in seconds into formatted string of
    (days, hours, minutes, seconds)
    :param time_delta: length of time delta in seconds
    :return formatted string of time delta
    """
    time_list = []
    days = int(time_delta // 86400)
    _format_time_item('day', days, time_list)
    hours = int((time_delta - days * 86400) // 3600)
    _format_time_item('hour', hours, time_list)
    minutes = int((time_delta - days * 86400 - hours * 3600) // 60)
    _format_time_item('minute', minutes, time_list)
    seconds = time_delta - days * 86400 - hours * 3600 - minutes * 60
    _format_time_item('second', seconds, time_list)
    if not time_list:
        return "0.0 seconds"
    else:
        return " ".join(time_list)
