"""
Functions that process data for plotting
"""
import math
from typing import List, Union, Optional, Tuple, Dict

from obspy import UTCDateTime

from sohstationviewer.conf import constants as const


MAX_INT = 1E100
MAX_FLOAT = 1.0E100


def format_time(time: Union[UTCDateTime, float], date_mode: str,
                time_mode: Optional[str] = None) -> str:
    """
    Format time according to date_mode and time_mode
    :param time: time to be format, can be UTCDateTime or epoch time
    :param date_mode: the format of date
    :param time_mode: the format of time
    :return: the formatted time string
    """
    if isinstance(time, UTCDateTime):
        t = time
    else:
        t = UTCDateTime(time)

    # https://docs.python.org/3/library/datetime.html#
    # strftime-and-strptime-format-codes
    format = ''
    if date_mode == 'YYYY-MM-DD':
        format = '%Y-%m-%d'
    elif date_mode == 'YYYYMMMDD':
        format = '%Y%b%d'
    elif date_mode == 'YYYY:DOY':
        format = '%Y:%j'
    if time_mode == 'HH:MM:SS':
        format += " %H:%M:%S"

    ret = t.strftime(format).upper()
    return ret


def get_title(data_set_id: Union[str, Tuple[str, int]], min_time: float,
              max_time: float, date_mode: str) -> str:
    """
    Create title for the plot.
    :param data_set_id: sta_id for mseed, (unit_id, exp_no) for rt130
    :param min_time: start time of the plot
    :param max_time: end time of the plot
    :param date_mode: format of date
    :return: title for the plot
    """
    diff = max_time - min_time
    hours = diff / 3600
    return ("%s  %s  to  %s  (%s)" %
            (data_set_id,
             format_time(min_time, date_mode, "HH:MM:SS"),
             format_time(max_time, date_mode, "HH:MM:SS"),
             round(hours, 2))
            )


def get_gaps(gaps: List[List[float]], gap_min: float
             ) -> List[List[float]]:
    """
    :param gaps: list of gaps
    :param gap_min: minimum of gaps count in minutes
    :return: list of gaps of which gaps smaller than gapMin have been removed
    """
    gap_min_sec = gap_min * 60
    return [g for g in gaps if abs(g[1] - g[0]) >= gap_min_sec]


def get_time_ticks(earliest: float, latest: float, date_format: str,
                   label_total: int
                   ) -> Tuple[List[float], List[float], List[str]]:
    """
    split time range to use for tick labels
    Ex: getTimeTicks(1595542860.0, 1595607663.91, 'YYYY-MM-DD', 3)
    :param earliest: earliest epoch time
    :param latest: latest epoch time
    :param date_format: (YYYY:DOY, YYYY-MM-DD or YYYYMMMDD)
        (selected in Menu Options - Date Format)
    :param label_total: number of time label to be displayed,
        others will show as ticks oly
    :return:
        times: list of times to show ticks
        major_times: list of times for displayed labels
        major_time_labels: list of labels displayed
    """
    time_range = latest - earliest
    if time_range >= 2592000.0:
        mode = "DD"
        # Time of the nearest midnight before the earliest time.
        time = (earliest // 86400.0) * 86400
        interval = 864000.0
    elif time_range >= 864000.0:
        mode = "D"
        time = (earliest // 86400.0) * 86400
        interval = 86400.0
    elif time_range >= 3600.0:
        mode = "H"
        # Nearest hour.
        time = (earliest // 3600.0) * 3600
        interval = 3600.0
    elif time_range >= 60.0:
        mode = "M"
        # Nearest minute.
        time = (earliest // 60.0) * 60
        interval = 60.0
    else:
        mode = "S"
        time = (earliest // 1)
        interval = 1.0
    times = []
    time_labels = []
    time += interval
    while time <= latest:
        times.append(time)
        time_label = format_time(time, date_format, 'HH:MM:SS')
        if mode == "DD" or mode == "D":
            time_label = time_label[:-9]
        elif mode == "H":
            time_label = time_label[:-3]
        elif mode == "M" or mode == "S":
            time_label = time_label
        time_labels.append(time_label)
        time += interval

    ln = len(time_labels)
    d = math.ceil(len(time_labels) / label_total)
    major_times = [times[i] for i in range(ln) if i % d == 0]
    major_time_labels = [time_labels[i] for i in range(ln) if i % d == 0]
    return times, major_times, major_time_labels


def get_day_ticks() -> Tuple[List[int], List[int], List[str]]:
    """
    Get information for displaying time on plotting widget.
    :return:
        times: list of indexes of every hour in each_day_5_min_list
        major_times: list of indexes of every 4 hours in each_day_5_min_list
        major_time_labels: 2 digit numbers of every 4 hours in a day
    """

    times = list(range(const.NO_5M_1H,
                       const.NUMBER_OF_5M_IN_DAY,
                       const.NO_5M_1H))
    major_times = list(range(4 * const.NO_5M_1H,
                             const.NUMBER_OF_5M_IN_DAY,
                             4 * const.NO_5M_1H))
    major_time_labels = ["%02d" % int(t / const.NO_5M_1H) for t in major_times]
    return times, major_times, major_time_labels


def get_unit_bitweight(chan_db_info: Dict, bitweight_opt: str) -> str:
    """
    Get the format to display value including fixed point decimal and unit
    based on the information from the database.
    :param chan_db_info: channel's info got from database
    :param bitweight_opt: option for bitweight (none, low, high)
        (Menu Options - Q330 Gain)
    :return unit_bitweight: format for displayed value on the left of each
        plot with fixed point decimal and unit
    """

    plot_type = chan_db_info['plotType']
    # Not all channels use/have this field.
    unit = chan_db_info['unit']
    try:
        fix_point = chan_db_info['fixPoint']
    except Exception:
        fix_point = 0
    unit_bitweight = ''

    if plot_type in ['linesDots', 'linesSRate', 'linesMasspos']:

        if fix_point == 0:
            unit_bitweight = "{}%s" % unit
        else:
            unit_bitweight = "{:.%sf}%s" % (fix_point, unit)
    if chan_db_info['channel'] == 'SEISMIC':
        if fix_point != 0:
            unit_bitweight = "{:.%sf}%s" % (fix_point, unit)
        else:
            if bitweight_opt in ["low", "high"]:
                unit_bitweight = "{}V"
            else:
                unit_bitweight = "{}%s" % unit
    return unit_bitweight


def get_disk_size_format(value: float) -> str:
    """
    Break down unit in byte system.
    :param value: size in KiB
    :return: size with unit
    """
    size = value * 1024.
    if size >= 1024. ** 4:
        return "%.2fTiB" % (size / 1024. ** 4)
    elif size >= 1024. ** 3:
        return "%.2fGiB" % (size / 1024. ** 3)
    elif size >= 1024. ** 2:
        return "%.2fMiB" % (size / 1024. ** 2)
    elif size >= 1024.:
        return "%.2fKiB" % (size / 1024.)
    else:
        return "%dB" % size
