"""
Function that ignite from main_window, Dialogs to read data files for data,
channels, datatype
"""

import os
from pathlib import Path
from typing import List, Optional, Dict, Tuple, Union, BinaryIO, Set, FrozenSet

from PySide6.QtCore import QEventLoop, Qt
from PySide6.QtGui import QCursor
from PySide6.QtWidgets import QTextBrowser, QApplication
from obspy.io import reftek
from obspy import UTCDateTime

from sohstationviewer.conf.dbSettings import dbConf
from sohstationviewer.model.mseed_data.mseed_reader import \
    move_to_next_record

from sohstationviewer.model.mseed_data.record_reader import RecordReader \
    as MSeedRecordReader
from sohstationviewer.model.mseed_data.record_reader_helper import \
    MSeedReadError
from sohstationviewer.database.extract_data import (
    get_signature_channels,
    get_all_channels,
)

from sohstationviewer.controller.util import (
    validate_file, display_tracking_info, check_chan,
)

from sohstationviewer.view.util.enums import LogType

# The data header is contained in field 2 of the fixed section of the header of
# a SEED data record. Seeing as an MSEED file is pretty much a collection of
# SEED data records, we know that the only valid data headers in an MSEED file
# are D, R, Q, or M.
# This information can be found in the description of the "Fixed Section of
# Data Header" in chapter 8 of the SEED V2.4 manual.
VALID_MSEED_DATA_HEADERS = {b'D', b'R', b'Q', b'M'}


def _read_mseed_chanids(path2file: Path,
                        is_multiplex: bool,
                        channel_info: Dict) -> None:
    """
    from the given file get set of channel ids for soh, mass position,
        waveform, soh with sample rate greater than 1 which are
        calibration signals .
    :param path2file: path to file
    :param is_multiplex: flag that tell if data has more than one channel in
        a file
    :param channel_info: Dict of channels' name categorized into soh, masspos,
        waveform, soh with sample rate greater than one for each station.
    """
    file = open(path2file, 'rb')
    while 1:
        is_eof = (file.read(1) == b'')
        if is_eof:
            break
        file.seek(-1, 1)
        current_record_start = file.tell()
        try:
            record = MSeedRecordReader(file)
        except MSeedReadError:
            file.close()
            raise Exception(f"File{path2file}: Data type isn't MSeed.")
        chan_id = record.record_metadata.channel
        chan_type = check_chan(chan_id, [], ['*'], True, True)
        if chan_type is False:
            if not is_multiplex:
                break
            continue
        sta_id = record.record_metadata.station
        if sta_id not in channel_info:
            channel_info[sta_id] = {'start_time': UTCDateTime(),
                                    'end_time': UTCDateTime(0),
                                    'mass_pos': set(),
                                    'waveform': set(),
                                    'soh': set(),
                                    'soh_spr_gr_1': set()}
        if chan_type == 'MP':
            channel_info[sta_id]['mass_pos'].add(chan_id)
        elif chan_type == 'SOH':
            sample_rate = record.record_metadata.sample_rate
            if sample_rate <= 1:
                channel_info[sta_id]['soh'].add(chan_id)
            else:
                channel_info[sta_id]['soh_spr_gr_1'].add(chan_id)
        else:
            channel_info[sta_id]['waveform'].add(chan_id)
        start_time = channel_info[sta_id]['start_time']
        channel_info[sta_id]['start_time'] = min(
            record.record_metadata.start_time, start_time)
        if is_multiplex:
            end_time = channel_info[sta_id]['end_time']
            channel_info[sta_id]['end_time'] = max(
                record.record_metadata.end_time, end_time)
        else:
            break
        move_to_next_record(file, current_record_start, record)
    file.close()


def read_mseed_channels(tracking_box: QTextBrowser, list_of_dir: List[str],
                        is_multiplex: bool,
                        on_unittest: bool = False
                        ) -> Dict[str, Dict]:
    """
    Scan available for SOH channels (to be used in channel preferences dialog).
        Since channels for RT130 is hard code, this function won't be applied
        for it.
    Note that Mass position channels are excluded because the default
        include_mp123zne and include_mp456uvw for MSeed are False
    :param tracking_box: widget to display tracking info
    :param list_of_dir: list of directories selected by users
    :param is_multiplex: flag that tell if data is multiplex
    :param on_unittest: flag to avoid cursor code to display tracking_info
    :return channel_info: Dict of channels' name categorized into soh, masspos,
        waveform, soh with sample rate greater than one for each station.
    """
    channel_info = {}
    count = 0
    if not on_unittest:
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
    display_tracking_info(
        tracking_box, "Start reading channel's header", LogType.INFO)
    for d in list_of_dir:
        for path, sub_dirs, files in os.walk(d):
            if 'System Volume Information' in path:
                continue
            for file_name in files:
                if not on_unittest:
                    QApplication.processEvents(
                        QEventLoop.ExcludeUserInputEvents
                    )
                path2file = Path(path).joinpath(file_name)
                if not validate_file(path2file, file_name):
                    continue
                count += 1
                if count % 10 == 0:
                    display_tracking_info(
                        tracking_box, f'Read {count} file headers',
                        LogType.INFO)
                if reftek.core._is_reftek130(path2file):
                    # if data_type is RT130, this function shouldn't be called
                    display_tracking_info(
                        tracking_box, "There is something wrong with the "
                        "work-flow. read_mseed_channels shouldn't be fed an "
                        f"RT130 file: {path2file}",
                        LogType.ERROR
                    )
                    return channel_info
                try:
                    # skip when data type isn't mseed
                    _read_mseed_chanids(
                        path2file, is_multiplex, channel_info)
                except Exception:
                    display_tracking_info(
                        tracking_box, f'Skip non-mseed file: {file_name}',
                        LogType.WARNING
                    )
                    pass

    if not on_unittest:
        QApplication.restoreOverrideCursor()
    for sta_id in channel_info:
        channel_info[sta_id]['mass_pos'] = sorted(list(
            channel_info[sta_id]['mass_pos']))
        channel_info[sta_id]['waveform'] = sorted(list(
            channel_info[sta_id]['waveform']))
        channel_info[sta_id]['soh'] = sorted(list(
            channel_info[sta_id]['soh']))
        channel_info[sta_id]['soh_spr_gr_1'] = sorted(list(
            channel_info[sta_id]['soh_spr_gr_1']))
    return channel_info


def detect_data_type(d: Union[str, Path]) -> Tuple[FrozenSet[str], bool]:
    """
    Detect the possible data types for the given directories using
        get_data_type_from_file
    :param d: the directory of the data set
    :return:
        + the set of possible data types
        + whether the data set is multiplexed
    """
    data_types_by_signature_channels = get_signature_channels()
    data_types_by_channels = get_all_channels()
    possible_data_types: Optional[Set[str]] = None

    try:
        d = d.as_posix()
    except AttributeError:
        pass
    is_multiplex = None
    for path, subdirs, files in os.walk(d):
        for file_name in files:
            path2file = Path(path).joinpath(file_name)
            if not validate_file(path2file, file_name):
                continue
            ret = get_data_type_from_file(
                path2file, data_types_by_signature_channels,
                data_types_by_channels, is_multiplex
            )
            file_possible_data_types, file_is_multiplex = ret
            if file_possible_data_types is not None:
                is_multiplex = is_multiplex or file_is_multiplex
                if possible_data_types is None:
                    possible_data_types = file_possible_data_types
                else:
                    possible_data_types &= file_possible_data_types
                if (possible_data_types is not None and
                        len(possible_data_types) == 1):
                    break

        if (possible_data_types is not None and
                len(possible_data_types) == 1):
            break
    if possible_data_types is None or is_multiplex is None:
        raise Exception("No channel found for the data set")

    return frozenset(possible_data_types), is_multiplex


def get_data_type_from_file(path2file: Path,
                            data_types_by_signature_channels: Dict[str, str],
                            data_types_by_channels: Dict[str, Set[str]],
                            is_multiplex: bool = None
                            ) -> Tuple[Optional[Set[str]], Optional[bool]]:
    """
    Get the possible data types contained in a given file. The procedure is
        given below.
    - Assume the file is an MSeed file and loop through each record and get the
    channel of each record.
        + If the file turns out to not be an MSeed file, check if the file is
        an RT130 file. If so, the data type is RT130 and the file is not
        multiplexed. If not, return None to indicate that this is not a data
        file.
        + If more than one channels are found in the file, mark the file as
        multiplexed.
    - For each channel, determine the data types that have this channel. Add
    them to the list of possible data types.
    - If a signature channel is found, the data type of the file has been
    determined. In this case, we stop doing the previous step.
    - Anyhow, loop until all the records in the file is exhausted.

    :param path2file: path to the given file
    :param data_types_by_signature_channels: dict that maps each signature
        channel to its corresponding data type
    :param data_types_by_channels: dict that maps each channel in the database
        to its corresponding data types
    :param is_multiplex: whether the data set that contains the file is
        multiplexed
    :return: None if the given file is neither a MSeed nor RT130 file, the list
        of possible data types and whether the given file belongs to a
        multiplexed data set otherwise.
    """
    file = open(path2file, 'rb')
    chans_in_stream = set()
    possible_data_types = set()

    # Handle waveform files, which give no information about the possible data
    # type. The standard in the industry at the moment of writing is to only
    # multiplex SOH channels if at all, so we only need to check the first
    # channel in a file to determine if it is a waveform file.
    try:
        chan = get_next_channel_from_mseed_file(file)
        if dbConf['seisRE'].match(chan):
            file.close()
            return None, is_multiplex
        file.seek(0)
    except ValueError:
        file.close()
        if reftek.core._is_reftek130(path2file):
            return {'RT130'}, False
        return None, is_multiplex

    is_data_type_found = False

    while 1:
        is_eof = (file.read(1) == b'')
        if is_eof:
            break
        file.seek(-1, 1)

        try:
            chan = get_next_channel_from_mseed_file(file)
        except ValueError:
            file.close()
            if reftek.core._is_reftek130(path2file):
                return {'RT130'}, False
            return None, None

        # Handle mass-position channels, which give no information about the
        # possible data type. Mass-position channels are considered SOH
        # channels, so they can be multiplexed with other SOH channels in the
        # same file. As a result, unlike the waveform channels, we can't deal
        # with them at the file level.
        if chan.startswith('VM'):
            chans_in_stream.add(chan)
            continue

        if chan in chans_in_stream:
            continue

        chans_in_stream.add(chan)

        if chan in data_types_by_signature_channels.keys():
            possible_data_types = {data_types_by_signature_channels[chan]}
            is_data_type_found = True

        if chan in data_types_by_channels:
            if not is_data_type_found:
                possible_data_types |= data_types_by_channels[chan]

    file.close()

    is_multiplex = is_multiplex or (len(chans_in_stream) > 1)

    # Handle case where mass-position channels are not multiplexed with other
    # SOH channels. We have to do this because mass-position-only files give
    # an empty set for the possible data types, which means that any data sets
    # with these files will be processed as having no data type.
    if all(chan.startswith('VM') for chan in chans_in_stream):
        return None, is_multiplex

    return possible_data_types, is_multiplex


def get_next_channel_from_mseed_file(mseed_file: BinaryIO) -> str:
    """
    Get the channel of the current record in a mseed file and move to the next
    record. Also determines the byte order of the mseed file if that
    information is not known.

    This function has been written with performance in mind. As such, it only
    does a cursory check of whether the given file handle is that of an MSeed
    file.
    :param mseed_file: an MSeed file
    :return: the channel of the current record of the given mseed file and
        the byte order of the mseed file
    """
    fixed_header = mseed_file.read(48)
    blockette_1000 = mseed_file.read(8)
    data_header = fixed_header[6:7]
    # If the data header we read from a data record is not one of the valid
    # ones, we know for sure that the file that contains the data record is not
    # MSEED.
    if data_header not in VALID_MSEED_DATA_HEADERS:
        raise ValueError('Data header is not valid.')
    blockette_type = blockette_1000[:2]
    # Check that the blockette type is 1000. Because we do know the byte order
    # of the file, we check the raw bytes directly instead of converting them
    # into the integer they represent.
    if blockette_type != b'\x03\xe8' and blockette_type != b'\xe8\x03':
        raise ValueError('First blockette is not a blockette 1000.')

    channel = fixed_header[15:18].decode('ASCII')

    # The record length is stored in one byte, so byte order does not matter
    # when decoding it. We choose big-endian here because it makes the code a
    # bit shorter.
    record_length_exponent = int.from_bytes(blockette_1000[6:7], 'big')
    record_size = 2 ** record_length_exponent
    mseed_file.seek(record_size - 56, 1)
    return channel
