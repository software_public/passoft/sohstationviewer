from tempfile import TemporaryDirectory, NamedTemporaryFile
import shutil
import os
from pathlib import Path

from unittest import TestCase

from sohstationviewer.view.util.check_file_size import _check_folders_size
from sohstationviewer.conf.constants import BIG_FILE_SIZE

TEST_DATA_DIR = Path(__file__).resolve().parent.parent.parent.joinpath(
    'test_data')
NON_DATA_FILE = TEST_DATA_DIR.joinpath('Non-data-file/non_data_file')
MULTIPLEX_FILE = TEST_DATA_DIR.joinpath(
    'Q330_multiplex/XX-3203_4-20221222183011')
NON_MULTIPLEX_LOW_SPR_FILE = TEST_DATA_DIR.joinpath(
    'Q330-sample/day_vols_AX08/AX08.XA..VM1.2021.186')
NON_MULTIPLEX_HIGH_SPR_FILE = TEST_DATA_DIR.joinpath(
    'Q330-sample/day_vols_AX08/AX08.XA..HHE.2021.186')
NON_MULTIPLEX_HIGH_N_LOW_SPR_SET = TEST_DATA_DIR.joinpath('Q330-sample')
RT130_FILE = TEST_DATA_DIR.joinpath(
    'RT130-sample/2017149.92EB/2017150/92EB/1/010000015_0036EE80')


class TestGetDirSize(TestCase):
    def test_less_or_equal_200_text_files(self):
        number_of_text_files = 25
        with TemporaryDirectory() as directory:
            files = []
            for i in range(number_of_text_files):
                files.append(NamedTemporaryFile(dir=directory))
            expected_result = {'data_size': 0,
                               'text_count': 25,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            # Explicitly clean up the temporary files. If we don't do this,
            # the temporary directory will clean up itself and delete the
            # temporary files. Then, when the function returns, the references
            # to these temporary files will attempt to clean up the files. This
            # leads to exceptions being raised because the files being cleaned
            # up does not exist anymore.
            [file.close() for file in files]

    def test_more_than_200_text_files(self):
        number_of_text_files = 250
        with TemporaryDirectory() as directory:
            files = []
            for i in range(number_of_text_files):
                files.append(NamedTemporaryFile(dir=directory))
            expected_result = {'data_size': 0,
                               'text_count': 201,   # stop when more than 200
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [file.close() for file in files]

    def test_less_or_equal_200_binary_files(self):
        number_of_binary_files = 25
        with TemporaryDirectory() as directory:
            files = []
            for i in range(number_of_binary_files):
                new_file_path = Path(directory).joinpath(
                    f'{NON_DATA_FILE.name}_{i}')
                shutil.copy(NON_DATA_FILE, new_file_path)
                files.append(new_file_path)
            expected_result = {'data_size': 0,
                               'text_count': 0,
                               'binary_count': 25}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_more_than_200_binary_files(self):
        number_of_binary_files = 250
        with TemporaryDirectory() as directory:
            files = []
            for i in range(number_of_binary_files):
                new_file_path = Path(directory).joinpath(
                    f'{NON_DATA_FILE.name}_{i}')
                shutil.copy(NON_DATA_FILE, new_file_path)
                files.append(new_file_path)
            expected_result = {'data_size': 0,
                               'text_count': 0,
                               'binary_count': 201}   # stop when more than 200
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_less_or_equal_limit_mseed_multiplexed_files(self):
        sample_file_size = os.path.getsize(MULTIPLEX_FILE)
        expected_size = 0
        with TemporaryDirectory() as directory:
            files = []
            for i in range(3):
                new_file_path = Path(directory).joinpath(
                    f'{MULTIPLEX_FILE.name}_{i}')
                shutil.copy(MULTIPLEX_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_more_than_limit_mseed_multiplexed_files(self):
        sample_file_size = os.path.getsize(MULTIPLEX_FILE)
        expected_size = 0
        count = 0
        with TemporaryDirectory() as directory:
            files = []
            while 1:
                new_file_path = Path(directory).joinpath(
                    f'{MULTIPLEX_FILE.name}_{count}')
                shutil.copy(MULTIPLEX_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
                if expected_size > BIG_FILE_SIZE:
                    break
                count += 1
            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_less_or_equal_limit_mseed_non_multiplexed_low_spr_files(self):
        sample_file_size = os.path.getsize(NON_MULTIPLEX_LOW_SPR_FILE)
        expected_size = 0
        with TemporaryDirectory() as directory:
            files = []
            for i in range(3):
                new_file_path = Path(directory).joinpath(
                    f'{NON_MULTIPLEX_LOW_SPR_FILE.name}_{i}')
                shutil.copy(NON_MULTIPLEX_LOW_SPR_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_less_or_equal_limit_mseed_non_multiplexed_high_spr_files(self):
        sample_file_size = os.path.getsize(NON_MULTIPLEX_HIGH_SPR_FILE)
        expected_size = 0
        with TemporaryDirectory() as directory:
            files = []
            for i in range(3):
                new_file_path = Path(directory).joinpath(
                    f'{NON_MULTIPLEX_HIGH_SPR_FILE.name}_{i}')
                shutil.copy(NON_MULTIPLEX_HIGH_SPR_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_more_than_limit_mseed_non_multiplexed_high_spr_files(self):
        sample_file_size = os.path.getsize(NON_MULTIPLEX_HIGH_SPR_FILE)
        expected_size = 0
        count = 0
        with TemporaryDirectory() as directory:
            files = []
            while 1:
                new_file_path = Path(directory).joinpath(
                    f'{NON_MULTIPLEX_HIGH_SPR_FILE.name}_{count}')
                shutil.copy(NON_MULTIPLEX_HIGH_SPR_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
                if expected_size > BIG_FILE_SIZE:
                    break
                count += 1
            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], [])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_mseed_non_multiplexed_high_n_low_spr_files(self):
        expected_result = {'data_size': 11251712,
                           'text_count': 0,
                           'binary_count': 0}
        ret = _check_folders_size([NON_MULTIPLEX_HIGH_N_LOW_SPR_SET], [])
        self.assertEqual(ret, expected_result)

    def test_less_or_equal_limit_rt130_files(self):
        sample_file_size = os.path.getsize(RT130_FILE)
        expected_size = 0
        with TemporaryDirectory() as directory:
            files = []
            new_data_stream_path = Path(directory).joinpath('1')
            new_data_stream_path.mkdir(
                parents=True, exist_ok=True)
            for i in range(3):
                new_file_path = new_data_stream_path.joinpath(
                    f'{RT130_FILE.name}_{i}')
                shutil.copy(RT130_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size

            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}

            ret = _check_folders_size([directory], ['1'])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_more_than_limit_rt130_files(self):
        sample_file_size = os.path.getsize(RT130_FILE)
        expected_size = 0
        count = 0
        with TemporaryDirectory() as directory:
            files = []
            new_data_stream_path = Path(directory).joinpath('1')
            new_data_stream_path.mkdir(
                parents=True, exist_ok=True)
            while 1:
                new_file_path = new_data_stream_path.joinpath(
                    f'{RT130_FILE.name}_{count}')
                shutil.copy(RT130_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size
                if expected_size > BIG_FILE_SIZE:
                    break
                count += 1

            expected_result = {'data_size': expected_size,
                               'text_count': 0,
                               'binary_count': 0}
            ret = _check_folders_size([directory], ['1'])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_rt130_no_requested_datastream_files(self):
        sample_file_size = os.path.getsize(RT130_FILE)
        expected_size = 0
        with TemporaryDirectory() as directory:
            files = []
            new_data_stream_path = Path(directory).joinpath('1')
            new_data_stream_path.mkdir(
                parents=True, exist_ok=True)
            for i in range(3):
                new_file_path = new_data_stream_path.joinpath(
                    f'{RT130_FILE.name}_{i}')
                shutil.copy(RT130_FILE, new_file_path)
                files.append(new_file_path)
                expected_size += sample_file_size

            expected_result = {'data_size': 0,
                               'text_count': 0,
                               'binary_count': 0}

            ret = _check_folders_size([directory], ['2'])
            self.assertEqual(ret, expected_result)
            [os.unlink(file) for file in files]

    def test_empty_directory(self):
        with TemporaryDirectory() as temp_dir:
            expected_result = {'data_size': 0,
                               'text_count': 0,
                               'binary_count': 0}
            result = _check_folders_size([temp_dir], ['*'])
            self.assertEqual(result, expected_result)

    def test_directory_does_not_exist(self):
        empty_name_dir = ''
        try:
            _check_folders_size([empty_name_dir], [])
        except Exception as e:
            self.assertEqual(
                str(e),
                "'' isn't a valid directory"
            )

        non_existent_dir = 'directory does not exist'
        try:
            _check_folders_size([non_existent_dir], [])
        except Exception as e:
            self.assertEqual(
                str(e),
                "'directory does not exist' isn't a valid directory"
            )
