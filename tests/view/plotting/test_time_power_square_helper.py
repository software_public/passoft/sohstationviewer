import math

import random
import numpy as np
from obspy import UTCDateTime

from sohstationviewer.view.plotting.time_power_square.\
    time_power_squared_helper import (
        get_start_5min_blocks,
        find_tps_tm_idx,
        get_tps_for_discontinuous_data,
        get_tps_time_by_color_for_a_day
    )
from sohstationviewer.conf import constants as const
from tests.base_test_case import BaseTestCase


class TestGetEachDay5MinList(BaseTestCase):
    def test_start_in_midle_end_exact(self):
        """
        Start in the middle of a day and end at the exact end of a day
        """
        with self.subTest("start, end in different day"):
            start = UTCDateTime("2012-09-07T12:15:00").timestamp
            end = UTCDateTime("2012-09-09T00:00:00").timestamp
            start_5min_blocks, start_first_day = get_start_5min_blocks(
                start, end
            )
            self.assertEqual(len(start_5min_blocks),
                             const.NUMBER_OF_5M_IN_DAY * 2)
            self.assertEqual(start_first_day, 1346976000.0)

        with self.subTest("start, end in same day"):
            start = UTCDateTime("2012-09-07T12:15:00").timestamp
            end = UTCDateTime("2012-09-08T00:00:00").timestamp
            start_5min_blocks, start_first_day = get_start_5min_blocks(
                start, end
            )
            self.assertEqual(len(start_5min_blocks), const.NUMBER_OF_5M_IN_DAY)
            self.assertEqual(start_first_day, 1346976000.0)

    def test_start_exact_end_in_middle(self):
        """
        Start at the very beginning of a day and end in the middle of a day
        """
        with self.subTest("start, end in different day"):
            start = UTCDateTime("2012-09-07T00:0:00").timestamp
            end = UTCDateTime("2012-09-08T12:15:00").timestamp
            start_5min_blocks, start_first_day = get_start_5min_blocks(
                start, end)
            self.assertEqual(len(start_5min_blocks),
                             2 * const.NUMBER_OF_5M_IN_DAY)
            self.assertEqual(start_first_day, 1346976000)

        with self.subTest("start, end in same day"):
            start = UTCDateTime("2012-09-0700:13:00").timestamp
            end = UTCDateTime("2012-09-07T12:13:00").timestamp
            start_5min_blocks, start_first_day = get_start_5min_blocks(
                start, end
            )
            self.assertEqual(len(start_5min_blocks), const.NUMBER_OF_5M_IN_DAY)
            self.assertEqual(start_first_day, 1346976000)


class TestFindTPSTmIdx(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        start = UTCDateTime("2012-09-07T12:15:00").timestamp
        end = UTCDateTime("2012-09-09T00:00:00").timestamp
        # cover 2 days: 2012/09/07 and 2012/09/08
        cls.start_5min_blocks, cls.start_first_day = get_start_5min_blocks(
            start, end
        )

    def test_given_time_beginning_of_first_day(self):
        tm = UTCDateTime("2012-09-07T00:00:00").timestamp
        tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                     self.start_first_day)
        self.assertEqual(tps_tm_idx, (0, 0))

    def test_within_first_five_minute(self):
        tm = UTCDateTime("2012-09-07T00:00:30").timestamp
        tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                     self.start_first_day)
        self.assertEqual(tps_tm_idx, (0, 0))

    def test_given_time_middle_of_first_day(self):
        tm = UTCDateTime("2012-09-07T00:35:00").timestamp
        tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                     self.start_first_day)
        self.assertEqual(tps_tm_idx, (7, 0))

    def test_given_time_middle_of_second_day(self):
        tm = UTCDateTime("2012-09-08T00:35:00").timestamp
        tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                     self.start_first_day)
        self.assertEqual(tps_tm_idx, (7, 1))

    def test_given_time_beginning_of_day_in_middle(self):
        tm = UTCDateTime("2012-09-08T00:00:00").timestamp
        tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                     self.start_first_day)
        self.assertEqual(tps_tm_idx, (0, 1))

    def test_given_time_very_end_of_last_day(self):
        tm = UTCDateTime("2012-09-09T00:00:00").timestamp
        start_tps_tm_idx = find_tps_tm_idx(tm, self.start_5min_blocks,
                                           self.start_first_day)
        self.assertEqual(start_tps_tm_idx, (287, 1))


class TestGetTPSForDiscontinuousData(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.day_begin = UTCDateTime("2021-07-05T00:00:00").timestamp
        cls.start = UTCDateTime("2021-07-05T22:59:28.340").timestamp
        cls.end = UTCDateTime("2021-07-06T3:59:51.870").timestamp
        cls.start_5mins_blocks, _ = get_start_5min_blocks(cls.start, cls.end)

    def test_more_than_10_minute_apart(self):
        # check for empty block in between tps data
        times = np.arange(self.start, self.end, 60*60)    # 60m apart
        data = np.random.uniform(-1000, 1000, times.size)
        channel_data = {'tracesInfo': [{'times': times, 'data': data}]}
        tps = get_tps_for_discontinuous_data(
            channel_data, self.start_5mins_blocks)
        self.assertEqual(len(tps), 2)
        expected_first_index = \
            math.ceil((self.start - self.day_begin)/const.SEC_5M) - 1
        day0_indexes = np.where(tps[0] != 0)[0]
        day1_indexes = np.where(tps[1] != 0)[0]

        self.assertEqual(day0_indexes[0], expected_first_index)

        # different (60/5) = 12 blocks from each other
        self.assertTrue(np.all(np.diff(day0_indexes) == 60/5))
        self.assertTrue(np.all(np.diff(day1_indexes) == 60/5))

    def test_less_than_10_minute_apart(self):
        # though times of data are apart from each other, but with less
        # than 10m apart, the function will fill up the empty space
        times = np.arange(self.start, self.end, 9*60)    # 9m apart
        data = np.random.uniform(-1000, 1000, times.size)
        channel_data = {'tracesInfo': [{'times': times, 'data': data}]}
        tps = get_tps_for_discontinuous_data(
            channel_data, self.start_5mins_blocks)
        self.assertEqual(len(tps), 2)
        expected_first_index = \
            math.ceil((self.start - self.day_begin)/const.SEC_5M) - 1
        day0_indexes = np.where(tps[0] != 0)[0]
        day1_indexes = np.where(tps[1] != 0)[0]
        self.assertEqual(day0_indexes[0], expected_first_index)
        # no blocks apart from each other
        self.assertTrue(np.all(np.diff(day0_indexes) == 1))
        self.assertTrue(np.all(np.diff(day1_indexes) == 1))
        # last block of day0 has value
        self.assertIn(const.NUMBER_OF_5M_IN_DAY - 1, day0_indexes)

    def test_overflow_data(self):
        times = np.arange(self.start, self.end, 9*60)    # 9m apart
        # This data reproduce overflow data
        data = np.random.randint(-10**6, 0, times.size, dtype='i4')
        channel_data = {'tracesInfo': [{'times': times, 'data': data}]}
        tps = get_tps_for_discontinuous_data(
            channel_data, self.start_5mins_blocks)
        self.assertEqual(len(tps), 2)
        # Before adding dtype in np.square in get_tps_for_discontinuous_data,
        # some tps data would be less than 0
        lessthanzero_indexes = np.where(tps[0] < 0)[0]
        self.assertEqual(lessthanzero_indexes.size, 0)


class TestGetTPSTimeByColorForADay(BaseTestCase):
    def setUp(self):

        self.square_counts = [0, 6400, 640000, 64000000, 6400000000,
                              640000000000, 64000000000000]
        self.color_codes = ['K', 'U', 'C', 'G', 'Y', 'R', 'M', 'E']
        self.x = np.array([i for i in range(const.NUMBER_OF_5M_IN_DAY)])
        self.y = []
        for i in range(len(self.color_codes)):
            if i == 0:
                self.y += [0.] * 25
            elif i < len(self.color_codes) - 1:
                self.y += random.sample(range(self.square_counts[i-1],
                                              self.square_counts[i]),
                                        25)
            else:
                self.y += random.sample(range(
                    self.square_counts[i-1],
                    self.square_counts[i-1] + self.square_counts[i-1]),
                    len(self.x) - 25 * (len(self.color_codes) - 1)
                )
        self.y = np.array(self.y)

    def test_function(self):
        tps_x_by_color = get_tps_time_by_color_for_a_day(
            self.x, self.y, self.square_counts, self.color_codes
        )
        for i, c in enumerate(self.color_codes):
            if i < len(self.color_codes) - 1:
                self.assertTrue((np.array_equal(tps_x_by_color[c],
                                 np.array(range(25 * i, 25 * i + 25)))))
            else:
                self.assertTrue((np.array_equal(tps_x_by_color[c],
                                 np.array(range(25 * i, len(self.x))))))
