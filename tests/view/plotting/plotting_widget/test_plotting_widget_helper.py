import numpy as np

from sohstationviewer.view.plotting.plotting_widget.plotting_widget_helper \
    import get_total_miny_maxy, get_index_from_data_picked
from tests.base_test_case import BaseTestCase


class TestGetIndexFromDataPicked(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.plotting_data = {
            'CH1': {
                'times': [np.array([1, 2, 3, 4, 5, 6, 6])],
                'data': [np.array([1, 1, 0, 1, 1, 0, 0])],
                'chan_db_info': {'plotType': 'upDownDots',
                                 'convertFactor': 1}
            },
            'CH2': {
                'times': [np.array([1, 2, 3, 3, 5, 6])],
                'data': [np.array([6, 9, 7, 4, 3, 9])],
                'chan_db_info': {'plotType': 'linesDots',
                                 'convertFactor': 1}
            },
            'CH3': {
                'times': [np.array([1, 2, 3, 4, 5, 6])],
                'data': [np.array([6, 9, 7, 4, 3, 9])],
                'chan_db_info': {'plotType': 'dotForTime',
                                 'convertFactor': 1}
            }
        }

    def test_time_not_included(self):
        real_idxes = get_index_from_data_picked(
            self.plotting_data['CH1'], 7, 1)
        self.assertEqual(len(real_idxes), 0)

    def test_type_not_need_data_val(self):
        # CH3 has plotType='dotForTime'
        # which is in ["multiColorDots", "dotForTime"])
        real_idxes = get_index_from_data_picked(
            self.plotting_data['CH3'], 4, 4)
        self.assertEqual(real_idxes.tolist(), [3])

    def test_type_need_data_val(self):
        # CH2 has plotType='linesDots' not in ["multiColorDots", "dotForTime"])
        with self.subTest('data not match time'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH2'], 3, 5)
            self.assertEqual(len(real_idxes), 0)
        with self.subTest('data match 1st value'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH2'], 3, 7)
            self.assertEqual(real_idxes.tolist(), [2])
        with self.subTest('data match 2nd value'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH2'], 3, 4)
            self.assertEqual(real_idxes.tolist(), [3])

    def test_type_up_down(self):
        # CH1 has plotType='upDownDots'
        with self.subTest('data not match time'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH1'], 1, -0.5)
            self.assertEqual(len(real_idxes), 0)
        with self.subTest('data=1 match time'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH1'], 1, 0.5)
            self.assertEqual(real_idxes.tolist(), [0])
        with self.subTest('data=0 match time'):
            real_idxes = get_index_from_data_picked(
                self.plotting_data['CH1'], 3, -0.5)
            self.assertEqual(real_idxes.tolist(), [2])

    def test_2_overlapped_points(self):
        real_idxes = get_index_from_data_picked(
            self.plotting_data['CH1'], 6, -0.5)
        self.assertEqual(real_idxes.tolist(), [5])

    def test_convert_factor_not_1(self):
        # upDownDots channels always have a convert factor of 1 in practice, so
        # we don't need to test them.
        # multiColorDots and dotForTime channels only cares about the time,
        # which is not affected by the convert factor. Thus, we don't need to
        # test them.
        plotting_data = {
            'CH1': {
                'times': [np.array([1, 2, 3, 4, 5, 6])],
                'data': [np.array([6, 9, 7, 4, 3, 9])],
                'chan_db_info': {'plotType': 'linesDots',
                                 'convertFactor': 0.1}
            },
        }
        with self.subTest('Data point can be found'):
            expected = [1]
            actual = list(
                get_index_from_data_picked(plotting_data['CH1'], 2, 0.9)
            )
            self.assertEqual(expected, actual)
        with self.subTest('Time not in range'):
            expected = []
            actual = list(
                get_index_from_data_picked(plotting_data['CH1'], 20, 0.9)
            )
            self.assertEqual(expected, actual)
        with self.subTest('Data not found'):
            expected = []
            actual = list(
                get_index_from_data_picked(plotting_data['CH1'], 2, 9)
            )
            self.assertEqual(expected, actual)


class TestGetTotalMinyMaxy(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.arange(7, 20, 1)
        cls.y = np.arange(34, 71, 3)

    def test_with_data_in_min_max_x(self):
        ret = get_total_miny_maxy(self.x, self.y, 8.5, 15.5)
        self.assertEqual(ret[0], 7)
        self.assertEqual(ret[1], 40)
        self.assertEqual(ret[2], 58)

    def test_with_no_data_in_min_max_x(self):
        ret = get_total_miny_maxy(self.x, self.y, 8.1, 8.9)
        self.assertEqual(ret[0], 0)
        self.assertIsNone(ret[1])
        self.assertIsNone(ret[2])
