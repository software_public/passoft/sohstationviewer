import numpy as np
from numpy import testing as nptesting
from unittest.mock import patch

from sohstationviewer.view.plotting.plotting_widget.plotting_helper import (
    get_masspos_value_colors,
    get_categorized_data_from_value_color_equal_on_upper_bound,
    get_categorized_data_from_value_color_equal_on_lower_bound,
    get_colors_sizes_for_abs_y_from_value_colors,
    apply_convert_factor
)
from sohstationviewer.view.util.color import clr
from sohstationviewer.conf import constants
from tests.base_test_case import BaseTestCase


class TestGetMassposValue(BaseTestCase):
    """Test suite for getMasspossValue"""

    def test_string_output(self):
        """
        Test basic functionality of get_masspos_value_colors - the range option
        and color mode are correct, and the output is a string.
        """
        expected_input_output_pairs = {
            ('regular', 'B'): '0.5:C|2.0:G|4.0:Y|7.0:R|7.0:+M',
            ('regular', 'W'): '0.5:C|2.0:G|4.0:Y|7.0:R|7.0:+M',
            ('trillium', 'B'): '0.5:C|1.8:G|2.4:Y|3.5:R|3.5:+M',
            ('trillium', 'W'): '0.5:C|1.8:G|2.4:Y|3.5:R|3.5:+M',
        }
        test_names = (
            'test_regular_B',
            'test_regular_W',
            'test_trillium_B',
            'test_trillium_W',
        )
        idx = 0
        for input_val in expected_input_output_pairs:
            with self.subTest(test_names[idx]):
                self.assertEqual(
                    get_masspos_value_colors(input_val[0], '',
                                             input_val[1], []),
                    expected_input_output_pairs[input_val]
                )
            idx += 1

    def test_list_output(self):
        """
        Test basic functionality of get_masspos_value_colors - the range option
        and color mode are correct, and the output is a list.
        """
        expected_input_output_pairs = {
            ('regular', 'B'):
                [(0.5, 'C'), (2.0, 'G'), (4.0, 'Y'), (7.0, 'R'), (7.0, 'M')],
            ('regular', 'W'):
                [(0.5, 'C'), (2.0, 'G'), (4.0, 'Y'), (7.0, 'R'), (7.0, 'M')],
            ('trillium', 'B'):
                [(0.5, 'C'), (1.8, 'G'), (2.4, 'Y'), (3.5, 'R'), (3.5, 'M')],
            ('trillium', 'W'):
                [(0.5, 'C'), (1.8, 'G'), (2.4, 'Y'), (3.5, 'R'), (3.5, 'M')],
        }
        test_names = (
            'test_regular_B',
            'test_regular_W',
            'test_trillium_B',
            'test_trillium_W',
        )
        for i, input_val in enumerate(expected_input_output_pairs):
            with self.subTest(test_names[i]):
                self.assertListEqual(
                    get_masspos_value_colors(
                        input_val[0], '', input_val[1], [], ret_type=''),
                    expected_input_output_pairs[input_val]
                )

    def test_range_option_not_supported(self):
        """
        Test basic functionality of get_masspos_value_colors - the range option
        is not supported.
        """
        errors = []
        empty_color_option = ''
        self.assertIsNone(
            get_masspos_value_colors(empty_color_option, '', 'B', errors))
        self.assertGreater(len(errors), 0)

        errors = []
        bad_color_option = 'unsupported'
        self.assertIsNone(
            get_masspos_value_colors(bad_color_option, '', 'B', errors))
        self.assertGreater(len(errors), 0)

    def test_color_mode_not_supported(self):
        """
        Test basic functionality of get_masspos_value_colors - color mode is
        not supported.
        """
        errors = []
        empty_color_mode = ''
        with self.assertRaises(KeyError):
            get_masspos_value_colors('regular', '', empty_color_mode, errors)

        errors = []
        bad_color_mode = 'unsupported'
        with self.assertRaises(KeyError):
            get_masspos_value_colors('regular', '', bad_color_mode, errors)


class TestGetCategorizedDataFromValueColorEqualOnUpperBound(BaseTestCase):
    def setUp(self) -> None:
        self.c_data = {
            'times': [[0, 1, 2, 3, 4]],
            'data': [[1, 3, 3.2, 3.3, 3.4]]}

    def test_equal_on_upper_bound(self):
        chan_db_info = {
            'valueColors': '<=3:#FF0000:bigger|<=3.3:#00FFFF|3.3<:#00FF00'
        }
        points_list, colors, sizes = \
            get_categorized_data_from_value_color_equal_on_upper_bound(
                self.c_data, chan_db_info)
        self.assertEqual(points_list, [[0, 1], [2, 3], [4]])
        self.assertEqual(colors, ['#FF0000', '#00FFFF', '#00FF00'])
        self.assertEqual(sizes,
                         [constants.SIZE_PIXEL_MAP['bigger'],
                          constants.SIZE_PIXEL_MAP['normal'],
                          constants.SIZE_PIXEL_MAP['normal']])

    def test_not_plot(self):
        chan_db_info = {
            'valueColors': '<=3:not plot|<=3.3:#00FFFF|3.3<:#00FF00'
        }
        points_list, colors, sizes = \
            get_categorized_data_from_value_color_equal_on_upper_bound(
                self.c_data, chan_db_info)
        self.assertEqual(points_list, [[2, 3], [4]])
        self.assertEqual(colors, ['#00FFFF', '#00FF00'])
        self.assertEqual(sizes,
                         [constants.SIZE_PIXEL_MAP['normal'],
                          constants.SIZE_PIXEL_MAP['normal'],
                          constants.SIZE_PIXEL_MAP['normal']])


class TestGetCategorizedDataFromValueColorEqualOnLowerBound(BaseTestCase):
    def test_equal_on_lower_bound(self):
        c_data = {'times': [[0, 1, 2, 3, 4]],
                  'data': [[1, 3, 3.2, 3.3, 3.4]]}
        chan_db_info = {'valueColors': '3:R|3.3:Y|=3.3:G:smaller'}
        points_list, colors, sizes = \
            get_categorized_data_from_value_color_equal_on_lower_bound(
                c_data, chan_db_info)

        self.assertEqual(points_list, [[0], [1, 2], [3, 4]])
        self.assertEqual(colors, ['R', 'Y', 'G'])
        self.assertEqual(sizes,
                         [constants.SIZE_PIXEL_MAP['normal'],
                          constants.SIZE_PIXEL_MAP['normal'],
                          constants.SIZE_PIXEL_MAP['smaller']])


class TestGetColorsSizesForAbsYFromValueColors(BaseTestCase):
    def test_get_colors_sizes_for_abs_y_from_value_colors(self):
        y = [0, 0.5, 1., 2., 3., 4., 5., 7., 7.1,
             -0, -0.5, -1., -2., -3., -4., -5., -7., -7.1]
        value_colors = [(0.5, 'C'), (2.0, 'G'), (4.0, 'Y'),
                        (7.0, 'R'), (7.0, 'M')]
        colors, sizes = get_colors_sizes_for_abs_y_from_value_colors(
            y, value_colors)

        self.assertEqual(
            colors,
            [clr['C'], clr['C'], clr['G'], clr['G'], clr['Y'], clr['Y'],
             clr['R'], clr['R'], clr['M'],
             clr['C'], clr['C'], clr['G'], clr['G'], clr['Y'], clr['Y'],
             clr['R'], clr['R'], clr['M']
             ]
        )
        self.assertEqual(sizes, [1.5] * len(y))


class TestApplyConvertFactor(BaseTestCase):
    @patch('sohstationviewer.view.plotting.plotting_widget.plotting_helper.'
           'get_convert_factor')
    def test_convert_factor(self, mock_get_convert_factor):
        mock_get_convert_factor.return_value = 0.1
        test_data = [np.array([1, 2, 2, -1])]
        expected_data = [np.array([0.1, 0.2, 0.2, -0.1])]
        result_data = apply_convert_factor(test_data, 'CHA', 'Q330')
        nptesting.assert_array_equal(result_data, expected_data)
