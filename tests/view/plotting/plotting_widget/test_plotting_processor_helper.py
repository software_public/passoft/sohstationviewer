from unittest.mock import patch

from obspy.core import UTCDateTime
import numpy as np

from sohstationviewer.view.plotting.plotting_widget.plotting_processor_helper \
    import downsample, chunk_minmax
from tests.base_test_case import BaseTestCase

ZERO_EPOCH_TIME = UTCDateTime(1970, 1, 1, 0, 0, 0).timestamp


class TestDownsample(BaseTestCase):
    def setUp(self) -> None:
        patcher = patch('sohstationviewer.view.plotting.plotting_widget.'
                        'plotting_processor_helper.chunk_minmax')
        self.addCleanup(patcher.stop)
        self.mock_chunk_minmax = patcher.start()
        self.times = np.arange(1000)
        self.data = np.arange(1000)
        self.log_idx = np.arange(1000)

    def test_requested_points_greater_than_data_size(self):
        req_points = 10000
        times, data, _ = downsample(
            self.times, self.data, rq_points=req_points)
        self.assertFalse(self.mock_chunk_minmax.called)
        # Check that we did not do any processing on the times and data arrays.
        # This ensures that we don't do two unneeded copy operations.
        self.assertIs(times, self.times)
        self.assertIs(data, self.data)

    def test_requested_points_greater_than_data_size_with_logidx(self):
        req_points = 10000
        times, data, log_idx = downsample(
            self.times, self.data, self.log_idx, rq_points=req_points)
        self.assertFalse(self.mock_chunk_minmax.called)
        # Check that we did not do any processing on the times and data arrays.
        # This ensures that we don't do two unneeded copy operations.
        self.assertIs(times, self.times)
        self.assertIs(data, self.data)
        self.assertIs(log_idx, self.log_idx)

    def test_empty_times_and_data(self):
        req_points = 1000
        self.times = np.empty((0, 0))
        self.data = np.empty((0, 0))
        times, data, _ = downsample(
            self.times, self.data, rq_points=req_points)
        # Check that we did not do any processing on the times and data arrays.
        # This ensures that we don't do two unneeded copy operations.
        self.assertIs(times, self.times)
        self.assertIs(data, self.data)

    def test_empty_times_and_data_with_logidx(self):
        req_points = 1000
        self.times = np.empty((0, 0))
        self.data = np.empty((0, 0))
        self.log_idx = np.empty((0, 0))
        times, data, log_idx = downsample(
            self.times, self.data, self.log_idx, rq_points=req_points)
        # Check that we did not do any processing on the times and data arrays.
        # This ensures that we don't do two unneeded copy operations.
        self.assertIs(times, self.times)
        self.assertIs(data, self.data)
        self.assertIs(log_idx, self.log_idx)


class TestChunkMinmax(BaseTestCase):
    def setUp(self):
        self.times = np.arange(1000)
        self.data = np.arange(1000)
        self.log_idx = np.arange(1000)

    def test_data_size_is_multiple_of_requested_points(self):
        req_points = 100
        times, data, log_idx = chunk_minmax(
            self.times, self.data, self.log_idx, req_points)
        self.assertEqual(times.size, req_points)
        self.assertEqual(data.size, req_points)
        self.assertEqual(log_idx.size, req_points)

    @patch('sohstationviewer.view.plotting.plotting_widget.'
           'plotting_processor_helper.downsample', wraps=downsample)
    def test_data_size_is_not_multiple_of_requested_points(
            self, mock_downsample):
        req_points = 102
        chunk_minmax(self.times, self.data, self.log_idx, req_points)
        self.assertTrue(mock_downsample.called)

    def test_requested_points_too_small(self):
        small_req_points_list = [0, 1]
        for req_points in small_req_points_list:
            with self.subTest(f'test_requested_points_is_{req_points}'):
                times, data, log_idx = chunk_minmax(
                    self.times, self.data, self.log_idx, rq_points=req_points)
                self.assertEqual(times.size, 0)
                self.assertEqual(data.size, 0)
                self.assertEqual(data.size, 0)
