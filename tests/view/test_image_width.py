# Checking if the size of images in documentation/images meet the requirement
# for help documents
from os.path import dirname, abspath, join, isdir
from os import listdir

from PIL import Image

from tests.base_test_case import BaseTestCase


class TestCheckImagesSize(BaseTestCase):
    def test_size(self):
        home_path = abspath(dirname(dirname(dirname(__file__))))
        img_path = join(home_path,
                        'sohstationviewer', 'documentation', 'images')

        greater_width_images = []

        for d in listdir(img_path):
            # d is a folder that contains images for a help document
            d = join(img_path, d)
            if not isdir(d):
                continue
            for f in listdir(d):
                if f.startswith('.'):
                    continue
                # f is an image file
                f = join(d, f)
                with Image.open(f) as img:
                    # Check if the image's width is greater than 1584px or 11in
                    # which is the limit of being cut off when the help
                    # document is saved as pdf file.
                    if img.width > 1584:
                        greater_width_images.append(f)
        self.assertEqual(
            greater_width_images, [],
            "The following images have width GREATER than 1584ps, which "
            "will result in some cut off of the images when saving help "
            f"documents to files:\n{', '.join(greater_width_images)}"
        )
