from sohstationviewer.view.db_config.value_color_helper.functions import (
    prepare_value_color_html
)

from tests.base_test_case import BaseTestCase


class TestPrepareValueColorHTML(BaseTestCase):
    def test_lines_dots(self):
        with self.subTest("Line and dot values"):
            expected_value_colors = (
                "<p>Line:"
                "<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
                "|Dot:"
                "<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
                "</p>")
            result = prepare_value_color_html("Line:#00FF00|Dot:#00FF00")
            self.assertEqual(expected_value_colors, result)
        with self.subTest("Line value"):
            expected_value_colors = (
                "<p>Line:<span style='color: #00FF00; font-size:18px;'>&#9632;"
                "</span></p>")
            result = prepare_value_color_html("Line:#00FF00")
            self.assertEqual(expected_value_colors, result)

    def test_up_down_dots(self):
        expected_value_colors = (
            "<p>Down:"
            "<span style='color: #FF0000; font-size:18px;'>&#9632;</span>"
            "|Up:"
            "<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
            "</p>")
        result = prepare_value_color_html("Down:#FF0000|Up:#00FF00")
        self.assertEqual(expected_value_colors, result)

    def test_multi_color_dots_equal_on_upper_bound(self):
        expected_value_colors = (
            "<p>&le;0:not plot"
            "|&le;1:"
            "<span style='color: #FFFF00; font-size:18px;'>&#9632;</span>"
            ":&#9650;|&le;2:"
            "<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
            "|2&lt;:"
            "<span style='color: #FF00FF; font-size:18px;'>&#9632;</span>"
            "</p>")
        result = prepare_value_color_html(
            '<=0:not plot|<=1:#FFFF00:bigger|<=2:#00FF00|2<:#FF00FF')
        self.assertEqual(expected_value_colors, result)

    def test_multi_color_dots_equal_on_lower_bound(self):
        expected_value_colors = (
            "<p>&lt;3:not plot"
            "|&lt;3.3:"
            "<span style='color: #FFFF00; font-size:18px;'>&#9632;</span>"
            "|=3.3:"
            "<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
            ":&#9660;</p>")
        result = prepare_value_color_html(
            '<3:not plot|<3.3:#FFFF00|=3.3:#00FF00:smaller')
        self.assertEqual(expected_value_colors, result)

    def test_tri_color_lines(self):
        expected_value_colors = (
            "<p>-1:"
            "<span style='color: #FF00FF; font-size:18px;'>&#9632;</span>"
            "|0:<span style='color: #FF0000; font-size:18px;'>&#9632;</span>"
            "|1:<span style='color: #00FF00; font-size:18px;'>&#9632;</span>"
            "</p>"
        )
        result = prepare_value_color_html('-1:#FF00FF|0:#FF0000|1:#00FF00')
        self.assertEqual(expected_value_colors, result)

    def test_dot_for_time(self):
        expected_value_colors = (
            "<p>C:"
            "<span style='color: #FF00FF; font-size:18px;'>&#9632;</span>"
            "</p>"
        )
        result = prepare_value_color_html('C:#FF00FF')
        self.assertEqual(expected_value_colors, result)

    def test_empty_input(self):
        expected = ''
        actual = prepare_value_color_html('')
        self.assertEqual(expected, actual)
