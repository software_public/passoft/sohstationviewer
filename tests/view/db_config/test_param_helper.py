from sohstationviewer.view.db_config.param_helper import \
    validate_value_color_str

from tests.base_test_case import BaseTestCase


class TestValidateValueColorStr(BaseTestCase):
    def test_lines_dots(self):
        with self.subTest("Both line and dot value"):
            result = validate_value_color_str(
                "Line:#00FF00|Dot:#00FF00", 'linesDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Only line value"):
            result = validate_value_color_str("Line:#00FF00", 'linesDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Only dot value"):
            result = validate_value_color_str("Dot:#00FF00", 'linesDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Line, dot, zero"):
            result = validate_value_color_str(
                "Line:#00FF00|Dot:#00FF00|Zero:#FF0000", 'linesDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Empty str"):
            result = validate_value_color_str("", 'linesDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Invalid string with row_id != -1"):
            result = validate_value_color_str("Li:#00FF00", 'linesDots')
            self.assertEqual(
                result,
                (False,
                 "A ValueColor in 'Li:#00FF00', 'Li:#00FF00',"
                 " doesn't match the required format of\n\n"
                 "Lines, one color dots. Dot or line/color mapping "
                 "defined by ValueColors.\n"
                 "Ex: Line:#00FF00|Dot:#FF0000  means\n"
                 "   Lines are plotted with color #00FF00\n"
                 "   Dots are plotted with color #FF0000\n"
                 "If Dot is not defined, dots won't be displayed.\n"
                 "If L is not defined, lines will be plotted with color "
                 "#00FF00.\n\n"
                 "If Zero is defined, this plot type will plot a line through "
                 "bottom points (<0) and top points (>0). Zero points will be "
                 "plotted in the middle."
                 "Ex: Line:#00FF00|Dot:#FF0000|Zero:#0000FF")
            )

    def test_up_down_dots(self):
        with self.subTest("Valid value color string"):
            result = validate_value_color_str(
                "Down:#FF0000|Up:#00FF00", 'upDownDots')
            self.assertEqual(result, (True, ''))
        with self.subTest("Only Down value"):
            result = validate_value_color_str(
                "Down:#FF0000", 'upDownDots')
            self.assertEqual(
                result,
                (False,
                 "Total ValueColors in 'Down:#FF0000' "
                 "is 1 while it requires 2 for plot type upDownDots."))
        with self.subTest("Only Up value"):
            result = validate_value_color_str("Up:#FF0000", 'upDownDots')
            self.assertEqual(
                result,
                (False,
                 "Total ValueColors in 'Up:#FF0000' is 1 "
                 "while it requires 2 for plot type upDownDots."))
        with self.subTest("Invalid value color string"):
            result = validate_value_color_str(
                "Up:#FF0000|Do:#FFFF00", 'upDownDots')
            self.assertEqual(
                result,
                (False,
                 "A ValueColor in "
                 "'Up:#FF0000|Do:#FFFF00', 'Do:#FFFF00', "
                 "doesn't match the required format of\n\n"
                 "Two different values: one above the center line, "
                 "the other under it. Colors defined by ValueColors.\n"
                 "Ex: Down:#FF0000|Up:#00FFFF  means:\n"
                 "   value == 1 => plot above center line with #00FFFF color\n"
                 "   value == 0 => plot under center line with #FF0000 color"))

    def test_tri_color_line(self):
        with self.subTest("Valid value color string"):
            result = validate_value_color_str(
                '-1:#FF00FF|0:#FF0000|1:#00FF00', 'triColorLines')
            self.assertEqual(result, (True, ''))
        with self.subTest("Missing one value color"):
            result = validate_value_color_str(
                '-1:#FF00FF|0:#FF0000', 'triColorLines')
            self.assertEqual(
                result,
                (False,
                 "Total ValueColors in "
                 "'-1:#FF00FF|0:#FF0000' is 2 while "
                 'it requires 3 for plot type triColorLines.'))
        with self.subTest("Invalid value color string"):
            result = validate_value_color_str(
                '-1:#FF00FF|0:#FF0000|2:#00FF00', 'triColorLines')
            self.assertEqual(
                result,
                (False,
                 "A ValueColor in "
                 "'-1:#FF00FF|0:#FF0000|2:#00FF00', '2:#00FF00', "
                 "doesn't match the required format of\n\n"
                 "Three values -1,0,1 in three lines with three different "
                 "colors according to valueColors:\n"
                 "Ex: -1:#FF0000|0:#00FF00|1:#0000FF  means\n"
                 "value = -1  => plot on line y=-1 with #FF0000 color\n"
                 "value = 0   => plot on line y=0 with #00FF00 color\n"
                 "value = 1 => plot on line y=1 with #0000FF color"
                 ))

    def test_dot_for_time(self):
        with self.subTest("Valid value color string"):
            result = validate_value_color_str("Color:#00FF00", 'dotForTime')
            self.assertEqual(result, (True, ''))
        with self.subTest("Empty string"):
            result = validate_value_color_str("", 'dotForTime')
            self.assertEqual(result, (True, ''))
        with self.subTest("Missing one value color"):
            result = validate_value_color_str(
                'Color:#FF00FF|Color:#FF0000', 'dotForTime')
            self.assertEqual(
                result,
                (False,
                 "Total ValueColors in "
                 "'Color:#FF00FF|Color:#FF0000' is 2 while "
                 "it requires 1 for plot type dotForTime."))
        with self.subTest("Invalid value color string"):
            result = validate_value_color_str("Colo:#00FF00", 'dotForTime')
            self.assertEqual(
                result,
                (False,
                 "A ValueColor in "
                 "'Colo:#00FF00', 'Colo:#00FF00', "
                 "doesn't match the required format of\n\n"
                 "Dots according to timestamp.\n"
                 "Color defined by ValueColors.\n"
                 "Ex: Color:#00FF00"
                 ))

    def test_multi_color_dots_equal_on_upper_bound(self):
        with self.subTest("Valid value color string"):
            result = validate_value_color_str(
                '<=0:not plot|<=1:#FFFF00|<=2:#00FF00|2<:#FF00FF:smaller',
                'multiColorDotsEqualOnUpperBound')
            self.assertEqual(result, (True, ''))
        with self.subTest("Repeated value"):
            result = validate_value_color_str(
                '<=0:not plot|<=2:#FFFF00|<=2:#00FF00:smaller|2<:#FF00FF',
                'multiColorDotsEqualOnUpperBound')
            self.assertEqual(
                result,
                (False,
                 "Duplicated value '<=2' "
                 "in ValueColors string "
                 "'<=0:not plot|<=2:#FFFF00|<=2:#00FF00:smaller|2<:#FF00FF' "
                 "isn't allowed."))
        with self.subTest("Disordered value"):
            result = validate_value_color_str(
                '<=0:not plot|<=2:#FFFF00|<=1:#00FF00|2<:#FF00FF',
                'multiColorDotsEqualOnUpperBound')
            self.assertEqual(
                result,
                (False,
                 "Number '1.0' is out of "
                 "increasing order in ValueColors string "
                 "'<=0:not plot|<=2:#FFFF00|<=1:#00FF00|2<:#FF00FF'."))

    def test_multi_color_dots_equal_on_lower_bound(self):
        with self.subTest("Valid value color string"):
            result = validate_value_color_str(
                '<3:#FF0000|<3.3:#FFFF00|=3.3:#00FF00',
                'multiColorDotsEqualOnLowerBound')
            self.assertEqual(result, (True, ''))
        with self.subTest("Repeated value"):
            result = validate_value_color_str(
                '<3:#FF0000|<3.3:#FFFA00|<3.3:#FFFF00|=3.3:#00FF00',
                'multiColorDotsEqualOnLowerBound')
            self.assertEqual(
                result,
                (False,
                 "Duplicated value '<3.3' in "
                 "ValueColors string "
                 "'<3:#FF0000|<3.3:#FFFA00|<3.3:#FFFF00|=3.3:#00FF00' "
                 "isn't allowed."))
        with self.subTest("Disordered value"):
            result = validate_value_color_str(
                '<3:#FF0000|<3.3:#FFFA00|<3.1:#FFFF00|=3.3:#00FF00',
                'multiColorDotsEqualOnLowerBound')
            self.assertEqual(
                result,
                (False,
                 "Number '3.1' is out of "
                 "increasing order in ValueColors string "
                 "'<3:#FF0000|<3.3:#FFFA00|<3.1:#FFFF00|=3.3:#00FF00'."))
