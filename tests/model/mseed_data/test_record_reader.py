import tempfile
from unittest import TestCase

import numpy
import obspy
from numpy.testing import assert_array_equal
from obspy import Trace, Stream

from sohstationviewer.model.mseed_data.record_reader import RecordReader


class TestRecordReader(TestCase):
    temp_data_file = tempfile.NamedTemporaryFile(delete=False)
    temp_ascii_file = tempfile.NamedTemporaryFile(delete=False)

    @classmethod
    def setUpClass(cls) -> None:
        data_file = ('tests/test_data/Q330-sample/day_vols_AX08/'
                     'AX08.XA..VKI.2021.186')
        ascii_file = ('tests/test_data/Q330-sample/day_vols_AX08/'
                      'AX08.XA..LOG.2021.186')
        # Rewrite the data to make testing easier.
        data_stream: Stream = obspy.read(data_file)
        data_trace: Trace = data_stream[0]
        # We only want enough data that the record has exactly one hour of
        # data for easier testing. The number of points hardcoded below is
        # calculated based on the sample rate of the data.
        data_trace.data = numpy.arange(0, 361, dtype=data_trace.data.dtype)
        data_trace.stats['starttime'] -= data_trace.stats['starttime']
        data_trace.write(cls.temp_data_file.name, format='MSEED')

        ascii_stream: Stream = obspy.read(ascii_file)
        ascii_trace: Trace = ascii_stream[0]
        ascii_trace.data = numpy.array([b'\n'] * len(ascii_trace))
        ascii_trace.stats['starttime'] -= ascii_trace.stats['starttime']
        ascii_trace.write(cls.temp_ascii_file.name, format='MSEED')

    @classmethod
    def tearDownClass(cls) -> None:
        cls.temp_data_file.close()
        cls.temp_ascii_file.close()

    def test_get_two_data_points_data_record(self):
        with open(self.temp_data_file.name, 'rb') as data_file:
            expected_data_point = (0, 360)

            record = RecordReader(data_file)
            actual_data_points = record.get_two_data_points()

            self.assertEqual(expected_data_point, actual_data_points)
            self.assertEqual('', record.ascii_text)

    def test_get_two_data_points_ascii_record(self):
        with open(self.temp_ascii_file.name, 'rb') as data_file:

            record = RecordReader(data_file)
            actual_data_points = record.get_two_data_points()

            self.assertIsNone(actual_data_points)
            self.assertEqual('\n' * 3950, record.ascii_text)

    def test_get_data_points_data_record(self):
        with open(self.temp_data_file.name, 'rb') as data_file:
            expected_data_point = numpy.arange(0, 361, dtype=numpy.int32)

            record = RecordReader(data_file)
            actual_data_points = record.get_data_points()

            assert_array_equal(expected_data_point, actual_data_points)
            self.assertEqual('', record.ascii_text)

    def test_get_data_points_ascii_record(self):
        with open(self.temp_ascii_file.name, 'rb') as data_file:
            record = RecordReader(data_file)
            actual_data_points = record.get_data_points()

            self.assertIsNone(actual_data_points)
            self.assertEqual('\n' * 3950, record.ascii_text)
