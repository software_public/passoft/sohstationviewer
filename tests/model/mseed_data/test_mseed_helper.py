import numpy as np

from sohstationviewer.model.mseed_data.mseed_helper import (
    retrieve_nets_from_data_dict, split_vst_channel
)
from tests.base_test_case import BaseTestCase


class TestRetrieveNetsFromDataDict(BaseTestCase):
    def setUp(self):
        self.nets_by_sta = {}
        self.data_dict = {
            'STA1': {'CHA1': {'nets': {'NET1', 'NET2'}},
                     'CHA2': {'nets': {'NET2', 'NET3'}}
                     },
            'STA2': {'CHA1': {'nets': {'NET1'}},
                     'CHA2': {'nets': {'NET1'}}
                     }
            }

    def test_retrieve_nets(self):
        retrieve_nets_from_data_dict(self.data_dict, self.nets_by_sta)
        self.assertEqual(list(self.nets_by_sta.keys()), ['STA1', 'STA2'])
        self.assertEqual(sorted(list(self.nets_by_sta['STA1'])),
                         ['NET1', 'NET2', 'NET3'])
        self.assertEqual(sorted(list(self.nets_by_sta['STA2'])), ['NET1'])


class TestSpitVSTChannel(BaseTestCase):
    def test_has_vst(self):
        data_dict = {
            'STA': {
                'VST': {
                    'tracesInfo': [{
                        'time': np.array([1, 2, 3, 4, 5]),
                        'data': np.array([0, 1, 5, 10, 11])}]
                }}}
        split_vst_channel('STA', data_dict)
        self.assertNotIn('VST', data_dict['STA'].keys())
        self.assertIn('VST0', data_dict['STA'].keys())
        self.assertIn('VST1', data_dict['STA'].keys())
        self.assertIn('VST2', data_dict['STA'].keys())
        self.assertIn('VST3', data_dict['STA'].keys())

        self.assertListEqual(
            data_dict['STA']['VST0']['tracesInfo'][0]['time'].tolist(),
            [1, 2, 3, 4, 5])
        self.assertListEqual(
            data_dict['STA']['VST0']['tracesInfo'][0]['data'].tolist(),
            [0, 1, 1, 0, 1])
        self.assertListEqual(
            data_dict['STA']['VST1']['tracesInfo'][0]['data'].tolist(),
            [0, 0, 0, 1, 1])
        self.assertListEqual(
            data_dict['STA']['VST2']['tracesInfo'][0]['data'].tolist(),
            [0, 0, 1, 0, 0])
        self.assertListEqual(
            data_dict['STA']['VST3']['tracesInfo'][0]['data'].tolist(),
            [0, 0, 0, 1, 1])

    def test_has_vst_empty(self):
        data_dict = {
            'STA': {
                'VST': {
                    'tracesInfo': [{
                        'time': np.array([]),
                        'data': np.array([])}]
                }}}
        split_vst_channel('STA', data_dict)
        self.assertNotIn('VST', data_dict['STA'].keys())
        self.assertIn('VST0', data_dict['STA'].keys())
        self.assertIn('VST1', data_dict['STA'].keys())
        self.assertIn('VST2', data_dict['STA'].keys())
        self.assertIn('VST3', data_dict['STA'].keys())

        self.assertListEqual(
            data_dict['STA']['VST0']['tracesInfo'][0]['time'].tolist(),
            [])
        self.assertListEqual(
            data_dict['STA']['VST0']['tracesInfo'][0]['data'].tolist(),
            [])
        self.assertListEqual(
            data_dict['STA']['VST1']['tracesInfo'][0]['data'].tolist(),
            [])
        self.assertListEqual(
            data_dict['STA']['VST2']['tracesInfo'][0]['data'].tolist(),
            [])
        self.assertListEqual(
            data_dict['STA']['VST3']['tracesInfo'][0]['data'].tolist(),
            [])

    def test_has_no_vst(self):
        data_dict = {
            'STA': {
                'VDP': {
                    'tracesInfo': [{
                        'time': np.array([1, 2]),
                        'data': np.array([1, 2])}]
                }}}
        split_vst_channel('STA', data_dict)
        self.assertNotIn('VST0', data_dict['STA'].keys())
