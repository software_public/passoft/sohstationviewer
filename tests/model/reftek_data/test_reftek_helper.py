from sohstationviewer.model.reftek_data.reftek_helper import (
    get_total_data_points
)


from tests.base_test_case import BaseTestCase


class TestGetTotalDataPoints(BaseTestCase):
    def setUp(self) -> None:
        self.data_dict = {
            'STA1': {'CHA1': {'tracesInfo': [{'times': []}]},
                     'CHA2': {'tracesInfo': [{'times': []}]}},
            'STA2': {'CHA1': {'tracesInfo': [{'times': [1, 2]}]},
                     'CHA2': {'tracesInfo': [{'times': []}]}},
            'STA3': {'CHA1': {'tracesInfo': [{'times': [1, 2]}]},
                     'CHA2': {'tracesInfo': [{'times': [1, 2, 3]}]}}
        }

    def test_no_data_points(self):
        data_dict = {'STA': self.data_dict['STA1']}
        result = get_total_data_points(data_dict)
        self.assertEqual(result, 0)

    def test_some_channel_has_no_data_points(self):
        data_dict = {'STA': self.data_dict['STA2']}
        result = get_total_data_points(data_dict)
        self.assertEqual(result, 2)

    def test_all_channel_have_data_points(self):
        data_dict = {'STA': self.data_dict['STA3']}
        result = get_total_data_points(data_dict)
        self.assertEqual(result, 5)
