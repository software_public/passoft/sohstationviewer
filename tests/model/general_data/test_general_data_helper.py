from pathlib import Path

from sohstationviewer.model.general_data.general_data_helper import (
    _check_related_gaps, squash_gaps, sort_data,
    retrieve_data_time_from_data_dict, retrieve_gaps_from_data_dict,
    combine_data, read_text
)
from tests.base_test_case import BaseTestCase

TEST_DATA_DIR = Path(__file__).resolve().parent.parent.parent.joinpath(
    'test_data')
text_file = TEST_DATA_DIR.joinpath(
    "Pegasus-sample/Pegasus_SVC4/logs/2020/XX/KC01/XX.KC01...D.2020.129")
binary_file = TEST_DATA_DIR.joinpath(
    "Pegasus-sample/Pegasus_SVC4/soh/2020/XX/KC01/VDT.D/"
    "XX.KC01..VDT.D.2020.129")


class TestReadText(BaseTestCase):
    def test_text_file(self):
        ret = read_text(text_file)
        expected_ret = (
            "\n\n** STATE OF HEALTH: XX.KC01...D.2020.129"
            "\n2020-05-08 22:55:45.390 UTC: I(Initializations): Firmware")
        self.assertEqual(ret[:100], expected_ret
                         )

    def test_binary_file(self):
        ret = read_text(binary_file)
        self.assertIsNone(ret)


class TestCheckRelatedGaps(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.checked_indexes = []

    def test_minmax1_inside_minmax2(self):
        self.assertTrue(
            _check_related_gaps(3, 4, 1, 5, 1, self.checked_indexes))
        self.assertIn(1, self.checked_indexes)

    def test_minmax2_inside_minmax1(self):
        self.assertTrue(
            _check_related_gaps(1, 5, 3, 4, 2, self.checked_indexes))
        self.assertIn(2, self.checked_indexes)

    def end_minmax1_overlap_start_minmax(self):
        self.assertTrue(
            _check_related_gaps(1, 4, 3, 5, 3, self.checked_indexes))
        self.assertIn(3, self.checked_indexes)

    def end_minmax2_overlap_start_minmax1(self):
        self.assertTrue(
            _check_related_gaps(3, 5, 1, 4, 4, self.checked_indexes))
        self.assertIn(4, self.checked_indexes)

    def minmax1_less_than_minmax2(self):
        self.assertFalse(
            _check_related_gaps(1, 3, 4, 6, 5, self.checked_indexes))
        self.assertNotIn(5, self.checked_indexes, )

    def minmax1_greater_than_minmax2(self):
        self.assertFalse(
            _check_related_gaps(6, 6, 1, 3, 5, self.checked_indexes))
        self.assertEqual(5, self.checked_indexes)


class TestSquashGaps(BaseTestCase):
    def setUp(self) -> None:
        self.normal_gaps = [[4, 7], [4, 6], [5, 6], [3, 7], [5, 8]]
        self.overlap_gaps = [[17, 14], [16, 14], [16, 15], [17, 13], [18, 15]]
        self.mixed_gaps = []
        for i in range(len(self.normal_gaps)):
            self.mixed_gaps.append(self.normal_gaps[i])
            self.mixed_gaps.append(self.overlap_gaps[i])

    def test_normal_gaps(self):
        gaps = squash_gaps(self.normal_gaps)
        self.assertEqual(gaps, [[3, 8]])

    def test_overlap_gaps(self):
        gaps = squash_gaps(self.overlap_gaps)
        self.assertEqual(gaps, [[18, 13]])

    def test_mixed_gaps(self):
        gaps = squash_gaps((self.mixed_gaps))
        self.assertEqual(gaps, [[3, 8], [18, 13]])


class TestSortData(BaseTestCase):
    def setUp(self) -> None:
        self.station_data_dict = {
            'CH1': {'tracesInfo': [{'startTmEpoch': 7},
                                   {'startTmEpoch': 1},
                                   {'startTmEpoch': 5},
                                   {'startTmEpoch': 3}]},
            'CH2': {'tracesInfo': [{'startTmEpoch': 2},
                                   {'startTmEpoch': 8},
                                   {'startTmEpoch': 6},
                                   {'startTmEpoch': 4}]}
        }

    def test_sort_data(self):
        sort_data(self.station_data_dict)
        self.assertEqual(
            self.station_data_dict,
            {'CH1': {'tracesInfo': [{'startTmEpoch': 1}, {'startTmEpoch': 3},
                                    {'startTmEpoch': 5}, {'startTmEpoch': 7}]},
             'CH2': {'tracesInfo': [{'startTmEpoch': 2}, {'startTmEpoch': 4},
                                    {'startTmEpoch': 6}, {'startTmEpoch': 8}]}}
        )


class TestRetrieveDataTimeFromDataDict(BaseTestCase):
    def setUp(self) -> None:
        self.data_dict = {
            'STA1': {'CH1': {'startTmEpoch': 4, 'endTmEpoch': 6},
                     'CH2': {'startTmEpoch': 5, 'endTmEpoch': 9}
                     },
            'STA2': {'CH1': {'startTmEpoch': 2, 'endTmEpoch': 4},
                     'CH2': {'startTmEpoch': 6, 'endTmEpoch': 8}
                     }
            }
        self.data_time = {}
        self.expected_data_time = {'STA1': [4, 9], 'STA2': [2, 8]}

    def test_retrieve_data_time(self):
        retrieve_data_time_from_data_dict(
            'STA1', self.data_dict, self.data_time)
        self.assertEqual(self.data_time,
                         {'STA1': self.expected_data_time['STA1']})
        retrieve_data_time_from_data_dict(
            'STA2', self.data_dict, self.data_time)
        self.assertEqual(self.data_time,
                         self.expected_data_time)


class TestRetrieveGapsFromDataDict(BaseTestCase):
    def setUp(self) -> None:
        self.data_dict = {
            'STA1': {'CH1': {'gaps': [[1, 2], [4, 3]]},
                     'CH2': {'gaps': []}
                     },
            'STA2': {'CH1': {'gaps': [[1, 2], [4, 3], [2, 3]]},
                     'CH2': {'gaps': [[1, 3], [3, 2]]}
                     },
            }
        self.gaps = {}
        self.expected_gaps = {'STA1': [[1, 2], [4, 3]],
                              'STA2': [[1, 2], [4, 3], [2, 3], [1, 3], [3, 2]]}

    def test_retrieve_gaps(self):
        self.gaps['STA1'] = []
        retrieve_gaps_from_data_dict('STA1', self.data_dict, self.gaps)
        self.assertEqual(self.gaps,
                         {'STA1': self.expected_gaps['STA1']})

        self.gaps['STA2'] = []
        retrieve_gaps_from_data_dict('STA2', self.data_dict, self.gaps)
        self.assertEqual(self.gaps,
                         self.expected_gaps)


class TestCombineData(BaseTestCase):
    def test_overlap_lt_gap_minimum(self):
        # combine; not add to gap list
        data_dict = {'STA1': {
            'CH1': {
                'gaps': [],
                'tracesInfo': [
                    {'startTmEpoch': 5,
                     'endTmEpoch': 15,
                     'data': [1, 2, 2, -1],
                     'times': [5, 8, 11, 15]},
                    {'startTmEpoch': 13,     # delta = 2 < 10
                     'endTmEpoch': 20,
                     'data': [1, -2, 1, 1],
                     'times': [13, 16, 18, 20]}
                ]}
            }}
        gap_minimum = 10
        combine_data('STA1', data_dict, gap_minimum)
        self.assertEqual(data_dict['STA1']['CH1']['gaps'], [])

        self.assertEqual(
            len(data_dict['STA1']['CH1']['tracesInfo']),
            1)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['startTmEpoch'],
            5)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['endTmEpoch'],
            20)
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['data'].tolist(),
            [1, 2, 2, -1, 1, -2, 1, 1])
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['times'].tolist(),
            [5, 8, 11, 15, 13, 16, 18, 20])

    def test_overlap_gt_or_equal_gap_minimum(self):
        # combine; add to gap list
        data_dict = {'STA1': {
            'CH1': {
                'gaps': [],
                'tracesInfo': [
                    {'startTmEpoch': 5,
                     'endTmEpoch': 15,
                     'data': [1, 2, 2, -1],
                     'times': [5, 8, 11, 15]},
                    {'startTmEpoch': 5,     # delta = 10 >= 10
                     'endTmEpoch': 20,
                     'data': [1, -2, 1, 1],
                     'times': [5, 11, 15, 20]}
                ]}
            }}
        gap_minimum = 10
        combine_data('STA1', data_dict, gap_minimum)
        self.assertEqual(data_dict['STA1']['CH1']['gaps'], [[15, 5]])

        self.assertEqual(
            len(data_dict['STA1']['CH1']['tracesInfo']),
            1)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['startTmEpoch'],
            5)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['endTmEpoch'],
            20)
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['data'].tolist(),
            [1, 2, 2, -1, 1, -2, 1, 1])
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['times'].tolist(),
            [5, 8, 11, 15, 5, 11, 15, 20])

    def test_lt_gap_minimum(self):
        # not combine; not add to gap list
        data_dict = {'STA1': {
            'CH1': {
                'gaps': [],
                'tracesInfo': [
                    {'startTmEpoch': 5,
                     'endTmEpoch': 15,
                     'data': [1, 2, 2, -1],
                     'times': [5, 8, 11, 15]},
                    {'startTmEpoch': 22,    # delta = 7 > 6, < 10
                     'endTmEpoch': 34,
                     'data': [1, -2, 1, 1],
                     'times': [22, 26, 30, 34]}
                ]}
        }}
        gap_minimum = 10
        combine_data('STA1', data_dict, gap_minimum)
        self.assertEqual(data_dict['STA1']['CH1']['gaps'], [])

        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['startTmEpoch'],
            5)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['endTmEpoch'],
            34)
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['data'].tolist(),
            [1, 2, 2, -1, 1, -2, 1, 1])
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['times'].tolist(),
            [5, 8, 11, 15, 22, 26, 30, 34])

    def test_gap_gt_or_equal_gap_minimum(self):
        # not combine; add to gap list
        data_dict = {'STA1': {
            'CH1': {
                'gaps': [],
                'tracesInfo': [
                    {'startTmEpoch': 5,
                     'endTmEpoch': 15,
                     'data': [1, 2, 2, -1],
                     'times': [5, 8, 11, 15]},
                    {'startTmEpoch': 25,        # delta = 10 >= 10
                     'endTmEpoch': 40,
                     'data': [1, -2, 1, 1],
                     'times': [25, 29, 33, 36, 40]}
                ]}
            }}
        gap_minimum = 10
        combine_data('STA1', data_dict, gap_minimum)
        self.assertEqual(data_dict['STA1']['CH1']['gaps'], [[15, 25]])

        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['startTmEpoch'],
            5)
        self.assertEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['endTmEpoch'],
            40)
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['data'].tolist(),
            [1, 2, 2, -1, 1, -2, 1, 1])
        self.assertListEqual(
            data_dict['STA1']['CH1']['tracesInfo'][0]['times'].tolist(),
            [5, 8, 11, 15, 25, 29, 33, 36, 40])
