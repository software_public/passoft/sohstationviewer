from unittest import TestCase
from sohstationviewer.conf import dbSettings


class BaseTestCase(TestCase):
    """
    Class used to base all tests upon. This is used to run some code before any
    test is run. Primarily intended to fix problem with resource location.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        dbSettings.dbConf['db_path'] = 'sohstationviewer/database/soh.db'
