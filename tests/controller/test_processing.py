import itertools
from tempfile import TemporaryDirectory, NamedTemporaryFile
from pathlib import Path

from unittest.mock import patch

from obspy import UTCDateTime

from sohstationviewer.controller.processing import (
    read_mseed_channels,
    detect_data_type,
    get_data_type_from_file,
    get_next_channel_from_mseed_file,
)
from sohstationviewer.database.extract_data import (
    get_signature_channels,
    get_all_channels,
)
from PySide6 import QtWidgets

from tests.base_test_case import BaseTestCase

TEST_DATA_DIR = Path(__file__).resolve().parent.parent.joinpath('test_data')
rt130_dir = TEST_DATA_DIR.joinpath('RT130-sample/2017149.92EB/2017150')
q8_dir = TEST_DATA_DIR.joinpath('Q8-sample')
q330_dir = TEST_DATA_DIR.joinpath('Q330-sample/day_vols_AX08')
centaur_dir = TEST_DATA_DIR.joinpath('Centaur-sample/SOH')
pegasus_dir = TEST_DATA_DIR.joinpath('Pegasus-sample/Pegasus_SVC4/soh')
multiplex_dir = TEST_DATA_DIR.joinpath('Q330_multiplex')


class TestReadMSeedChannel(BaseTestCase):
    """Test suite for read_mseed_channels."""

    def setUp(self) -> None:
        """Set up test fixtures."""
        patcher = patch.object(QtWidgets, 'QTextBrowser')
        self.addCleanup(patcher.stop)
        self.widget_stub = patcher.start()

        # The actual value can be 'Q330', 'Centaur', or 'Pegasus'.
        # The code that uses this string only cares that it is not 'RT130',
        # though, so we are setting it to a stub value.
        self.mseed_dtype = 'MSeed'

    def test_read_channels_mseed_dir(self):
        """
        Test basic functionality of load_data - the given directory contains
        MSeed data.
        """
        with self.subTest("q330 - non multiplex"):
            q330_soh_channels = sorted(['LOG', 'VKI'])
            q330_mass_pos_channels = ['VM1']
            q330_wf_channels = ['HHE', 'LHE']
            q330_spr_gt_1 = []
            ret = read_mseed_channels(self.widget_stub, [q330_dir],
                                      False, True)
            self.assertListEqual(list(ret.keys()), ['AX08'])
            self.assertListEqual(ret['AX08']['soh'], q330_soh_channels)
            self.assertListEqual(ret['AX08']['mass_pos'],
                                 q330_mass_pos_channels)
            self.assertListEqual(ret['AX08']['waveform'], q330_wf_channels)
            self.assertListEqual(ret['AX08']['soh_spr_gr_1'], q330_spr_gt_1)
            self.assertEqual(ret['AX08']['start_time'], 1625443228.34)
            self.assertEqual(ret['AX08']['end_time'], UTCDateTime(0))

        with self.subTest(("centaur - multiplex - is_multiplex=True")):
            centaur_soh_channels = sorted(
                ['VDT', 'EX3', 'GEL', 'VEC', 'EX2', 'LCE', 'EX1', 'GLA', 'LCQ',
                 'GPL', 'GNS', 'GST', 'VCO', 'GAN', 'GLO', 'VPB', 'VEI'])
            centaur_mass_pos_channels = sorted(['VM1', 'VM2', 'VM3'])
            centaur_wf_channels = []
            centaur_spr_gt_1 = []
            ret = read_mseed_channels(self.widget_stub, [centaur_dir],
                                      True, True)
            self.assertListEqual(list(ret.keys()), ['3734'])
            self.assertListEqual(ret['3734']['soh'], centaur_soh_channels)
            self.assertListEqual(ret['3734']['mass_pos'],
                                 centaur_mass_pos_channels)
            self.assertListEqual(ret['3734']['waveform'], centaur_wf_channels)
            self.assertListEqual(ret['3734']['soh_spr_gr_1'], centaur_spr_gt_1)
            self.assertEqual(ret['3734']['start_time'], 1534464000.0)
            self.assertEqual(ret['3734']['end_time'], 1534550340.0)

        with self.subTest("centaur - multiplex - is_multiplex=False"):
            # not all channels detected if is_multiplex isn't set
            centaur_soh_channels = sorted(['GEL'])
            centaur_mass_pos_channels = sorted([])
            centaur_wf_channels = []
            centaur_spr_gt_1 = []
            ret = read_mseed_channels(self.widget_stub, [centaur_dir],
                                      False, True)
            self.assertListEqual(list(ret.keys()), ['3734'])
            self.assertListEqual(ret['3734']['soh'], centaur_soh_channels)
            self.assertListEqual(ret['3734']['mass_pos'],
                                 centaur_mass_pos_channels)
            self.assertListEqual(ret['3734']['waveform'], centaur_wf_channels)
            self.assertListEqual(ret['3734']['soh_spr_gr_1'], centaur_spr_gt_1)
            self.assertEqual(ret['3734']['start_time'], 1534464000.0)
            self.assertEqual(ret['3734']['end_time'], UTCDateTime(0))

        with self.subTest("pegasus - non multiplex"):
            pegasus_soh_channels = sorted(['VDT', 'VE1'])
            pegasus_mass_pos_channels = sorted(['VM1'])
            pegasus_wf_channels = []
            pegasus_spr_gt_1 = []
            ret = read_mseed_channels(self.widget_stub, [pegasus_dir],
                                      False, True)
            self.assertListEqual(list(ret.keys()), ['KC01'])
            self.assertListEqual(ret['KC01']['soh'], pegasus_soh_channels)
            self.assertListEqual(ret['KC01']['mass_pos'],
                                 pegasus_mass_pos_channels)
            self.assertListEqual(ret['KC01']['waveform'], pegasus_wf_channels)
            self.assertListEqual(ret['KC01']['soh_spr_gr_1'], pegasus_spr_gt_1)
            self.assertEqual(ret['KC01']['start_time'], 1588978560.0)
            self.assertEqual(ret['KC01']['end_time'], UTCDateTime(0))

        with self.subTest("q330 - multiplex - is_multiplex=True"):
            multiplex_soh_channels = ['LOG']
            multiplex_mass_pos_channels = []
            multiplex_wf_channels = sorted(
                ['BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6',
                 'EL1', 'EL2', 'EL4', 'EL5', 'EL6', 'ELZ'])
            multiplex_spr_gt_1 = sorted(
                ['BS1', 'BS2', 'BS3', 'BS4', 'BS5', 'BS6',
                 'ES1', 'ES2', 'ES3', 'ES4', 'ES5', 'ES6',
                 'LS1', 'LS2', 'LS3', 'LS4', 'LS5', 'LS6',
                 'SS1', 'SS2', 'SS3', 'SS4', 'SS5', 'SS6'])
            ret = read_mseed_channels(self.widget_stub, [multiplex_dir],
                                      True, True)
            self.assertListEqual(list(ret.keys()), ['3203'])
            self.assertListEqual(ret['3203']['soh'], multiplex_soh_channels)
            self.assertListEqual(ret['3203']['mass_pos'],
                                 multiplex_mass_pos_channels)
            self.assertListEqual(ret['3203']['waveform'],
                                 multiplex_wf_channels)
            self.assertListEqual(ret['3203']['soh_spr_gr_1'],
                                 multiplex_spr_gt_1)
            self.assertEqual(ret['3203']['start_time'],
                             1671729934.9000392)
            self.assertEqual(ret['3203']['end_time'], 1671733530.57)

        with self.subTest("q330 - multiplex - is_multiplex=False"):
            # not all channels detected if is_multiplex isn't set
            multiplex_soh_channels = []
            multiplex_mass_pos_channels = []
            multiplex_wf_channels = sorted(['EL1'])
            multiplex_spr_gt_1 = sorted([])
            ret = read_mseed_channels(self.widget_stub, [multiplex_dir],
                                      False, True)
            self.assertListEqual(list(ret.keys()), ['3203'])
            self.assertListEqual(ret['3203']['soh'], multiplex_soh_channels)
            self.assertListEqual(ret['3203']['mass_pos'],
                                 multiplex_mass_pos_channels)
            self.assertListEqual(ret['3203']['waveform'],
                                 multiplex_wf_channels)
            self.assertListEqual(ret['3203']['soh_spr_gr_1'],
                                 multiplex_spr_gt_1)
            self.assertEqual(ret['3203']['start_time'],
                             1671730004.145029)
            self.assertEqual(ret['3203']['end_time'], UTCDateTime(0))

    def test_read_channels_rt130_dir(self):
        """
        Test basic functionality of load_data - the given directory contains
        RT130 data.
        """
        ret = read_mseed_channels(self.widget_stub, [rt130_dir], True, True)
        self.assertEqual(ret, {})

    def test_read_mseed_channels_no_dir(self):
        """
        Test basic functionality of read_mseed_channels - no directory was
        given.
        """
        no_dir = []
        ret = read_mseed_channels(self.widget_stub, no_dir, True, True)
        self.assertEqual(ret, {})

    def test_read_mseed_channels_dir_does_not_exist(self):
        """
        Test basic functionality of read_mseed_channels - the given directory
        does not exist.
        """
        empty_name_dir = ['']
        ret = read_mseed_channels(self.widget_stub, empty_name_dir, True, True)
        self.assertEqual(ret, {})

        non_existent_dir = ['non_existent_dir']
        ret = read_mseed_channels(self.widget_stub, non_existent_dir,
                                  True, True)
        self.assertEqual(ret, {})

    def test_read_mseed_channels_empty_dir(self):
        """
        Test basic functionality of read_mseed_channels - the given directory
        is empty.
        """
        with TemporaryDirectory() as empty_dir:
            ret = read_mseed_channels(self.widget_stub, [empty_dir],
                                      True, True)
            self.assertEqual(ret, {})

    def test_read_mseed_channels_empty_data_dir(self):
        """
        Test basic functionality of read_mseed_channels - the given directory
        contains a data folder but no data file.
        """
        with TemporaryDirectory() as outer_dir:
            with TemporaryDirectory(dir=outer_dir):
                ret = read_mseed_channels(self.widget_stub, [outer_dir],
                                          True, True)
                self.assertEqual(ret, {})


class TestDetectDataType(BaseTestCase):
    """
    Test suite for detect_data_type and get_data_type_from_file functions.
    """

    def setUp(self) -> None:
        """Set up text fixtures."""
        func_patcher = patch('sohstationviewer.controller.processing.'
                             'get_data_type_from_file')
        self.addCleanup(func_patcher.stop)
        self.mock_get_data_type_from_file = func_patcher.start()

        self.dir = TemporaryDirectory()
        self.files = []
        for i in range(6):
            self.files.append(NamedTemporaryFile(dir=self.dir.name))

    def tearDown(self) -> None:
        """Teardown text fixtures."""
        while len(self.files) != 0:
            file = self.files.pop()  # noqa
            del file

        self.dir.cleanup()

    def test_non_multiplexed_dir_only_one_data_type(self):
        """
        Test basic functionality of detect_data_type - the data type in the
        given directory can be detected.
        """
        with self.subTest('test_files_can_only_be_one_data_type'):
            file_data_types = ({'RT130'}, False)
            self.mock_get_data_type_from_file.return_value = file_data_types

            self.assertEqual(
                detect_data_type(self.dir.name),
                ({'RT130'}, False)
            )

        with self.subTest('test_files_can_be_multiple_data_types'):
            file_data_types = itertools.cycle(
                [({'Q330', 'Q8'}, False), ({'Q330'}, False)]
            )
            self.mock_get_data_type_from_file.side_effect = file_data_types

            self.assertEqual(
                detect_data_type(self.dir.name),
                ({'Q330'}, False)
            )

    def test_multiplexed_dir_only_one_data_type(self):
        """
        Test basic functionality of detect_data_type - the given directories
        contain the same data type but the data type was detected using
        different channels.
        """
        with self.subTest('test_files_can_only_be_one_data_type'):
            file_data_types = itertools.cycle(
                [({'Q330'}, True), ({'Q330'}, False)]
            )
            self.mock_get_data_type_from_file.side_effect = file_data_types

            self.assertEqual(
                detect_data_type(self.dir.name),
                ({'Q330'}, True)
            )

        with self.subTest('test_files_can_be_multiple_data_types'):
            file_data_types = itertools.cycle(
                [({'Q330', 'Q8'}, True), ({'Q330'}, False)]
            )
            self.mock_get_data_type_from_file.side_effect = file_data_types

            self.assertEqual(
                detect_data_type(self.dir.name),
                ({'Q330'}, True)
            )

    def test_non_multiplexed_dir_multiple_possible_data_types(self):
        """
        Test basic functionality of detect_data_type - the given directories
        contain different data types.
        """
        file_data_types = itertools.cycle(
            [({'Q8', 'Q330', 'Centaur'}, False),
             ({'Q8', 'Q330'}, False)]
        )
        self.mock_get_data_type_from_file.side_effect = file_data_types

        self.assertEqual(
            detect_data_type(self.dir.name),
            ({'Q8', 'Q330'}, False)
        )

    def test_multiplexed_dir_multiple_possible_data_types(self):
        """
        Test basic functionality of detect_data_type - the given directories
        contain different data types.
        """
        file_data_types = itertools.cycle(
            [({'Q8', 'Q330', 'Centaur'}, True),
             ({'Q8', 'Q330'}, False)]
        )
        self.mock_get_data_type_from_file.side_effect = file_data_types

        self.assertEqual(
            detect_data_type(self.dir.name),
            ({'Q8', 'Q330'}, True)
        )

    def test_non_multiplexed_dir_no_possible_data_type(self):
        """
        Test basic functionality of detect_data_type - can't detect any data
        type.
        """
        file_data_types = (set(), False)
        self.mock_get_data_type_from_file.return_value = file_data_types

        self.assertEqual(
            detect_data_type(self.dir.name),
            (set(), False)
        )

    def test_multiplexed_dir_no_possible_data_type(self):
        """
        Test basic functionality of detect_data_type - can't detect any data
        type.
        """
        file_data_types = itertools.cycle(
            [(set(), False), (set(), True)]
        )
        self.mock_get_data_type_from_file.side_effect = file_data_types

        self.assertEqual(
            detect_data_type(self.dir.name),
            (set(), True)
        )

    def test_some_files_do_not_contain_data(self):
        """
        Test basic functionality of detect_data_type - can't detect any data
        type.
        """
        file_data_types = itertools.cycle(
            [(None, None),
             ({'Q8', 'Q330', 'Centaur'}, True)]
        )
        self.mock_get_data_type_from_file.side_effect = file_data_types

        self.assertEqual(
            detect_data_type(self.dir.name),
            ({'Q8', 'Q330', 'Centaur'}, True)
        )

    def test_no_file_contain_data(self):
        file_data_types = (None, None)
        self.mock_get_data_type_from_file.return_value = file_data_types

        with self.assertRaises(Exception) as context:
            detect_data_type(self.dir.name)
        self.assertEqual(
            str(context.exception),
            "No channel found for the data set"
        )


class TestGetDataTypeFromFile(BaseTestCase):
    """Test suite for get_data_type_from_file"""

    def test_can_detect_one_data_type_from_mseed_file(self):
        """
        Test basic functionality of get_data_type_from_file - given file
        contains MSeed data and the data type can be detected from the file.
        """
        q8_file = q8_dir.joinpath('MM10.XX..LCL.2023.313.ms')
        centaur_file = centaur_dir.joinpath(
            'XX.3734.SOH.centaur-3_3734..20180817_000000.miniseed.miniseed')
        pegasus_file = pegasus_dir.joinpath(
            '2020/XX/KC01/VE1.D/XX.KC01..VE1.D.2020.129')
        q8_data_type = ({'Q8'}, False)
        centaur_data_type = ({'Centaur'}, True)
        pegasus_data_type = ({'Pegasus'}, False)

        all_chan = get_all_channels()
        sig_chan = get_signature_channels()

        self.assertTupleEqual(
            get_data_type_from_file(q8_file, sig_chan, all_chan),
            q8_data_type)
        self.assertTupleEqual(
            get_data_type_from_file(centaur_file, sig_chan, all_chan),
            centaur_data_type)
        self.assertTupleEqual(
            get_data_type_from_file(pegasus_file, sig_chan, all_chan),
            pegasus_data_type)

    def test_can_detect_multiple_data_types_from_mseed_file(self):
        q330_q8_file = q330_dir.joinpath('AX08.XA..VKI.2021.186')
        centaur_pegasus_file = pegasus_dir.joinpath(
            '2020/XX/KC01/VDT.D/XX.KC01..VDT.D.2020.129'
        )

        q330_q8_data_type = ({'Q330', 'Q8'}, False)
        centaur_pegasus_data_type = ({'Centaur', 'Pegasus'}, False)

        all_chan = get_all_channels()
        sig_chan = get_signature_channels()

        self.assertTupleEqual(
            get_data_type_from_file(q330_q8_file, sig_chan, all_chan),
            q330_q8_data_type)

        self.assertTupleEqual(
            get_data_type_from_file(centaur_pegasus_file, sig_chan, all_chan),
            centaur_pegasus_data_type)

    def test_cannot_detect_data_type_from_mseed_file(self):
        """
        Test basic functionality of get_data_type_from_file - cannot detect
        data type contained in given file.
        """
        # We choose a waveform file because waveform channels cannot be used to
        # determine the data type in a file.
        mseed_file = q330_dir.joinpath('AX08.XA..LHE.2021.186')
        expected = (None, None)
        actual = get_data_type_from_file(mseed_file, get_signature_channels(),
                                         get_all_channels())
        self.assertEqual(expected, actual)

    def test_mass_position_multiplexed_with_soh_channels(self):
        """
        Test against bug found when modifying get_data_type_from_file - cannot
        detect data type from files with mass-position and SOH channels
        multiplexed together.
        """
        multiplexed_file = centaur_dir.joinpath(
            'XX.3734.SOH.centaur-3_3734..20180817_000000.miniseed.miniseed'
        )
        expected = ({'Centaur'}, True)
        actual = get_data_type_from_file(
            multiplexed_file, get_signature_channels(), get_all_channels()
        )
        self.assertEqual(expected, actual)

    def test_mass_position_only_file(self):
        """
        Test against bug found when modifying get_data_type_from_file - cannot
        detect data type from files with only mass-position data.
        """
        mseed_file = q330_dir.joinpath('AX08.XA..VM1.2021.186')
        expected = (None, False)
        actual = get_data_type_from_file(mseed_file, get_signature_channels(),
                                         get_all_channels())
        self.assertEqual(expected, actual)

    def test_rt130_data(self):
        """
        Test basic functionality of get_data_type_from_file - given file
        contains RT130 data.
        """
        rt130_file = Path(rt130_dir).joinpath(
            '92EB/0/000000000_00000000')
        expected_data_type = ({'RT130'}, False)
        self.assertTupleEqual(
            get_data_type_from_file(rt130_file, get_signature_channels(),
                                    get_all_channels()),
            expected_data_type
        )

    def test_empty_file(self):
        """
        Test basic functionality of get_data_type_from_file - the given file is
        empty.
        """
        test_file = NamedTemporaryFile()
        expected = (None, None)
        actual = get_data_type_from_file(
            Path(test_file.name), get_signature_channels(), get_all_channels()
        )
        self.assertEqual(expected, actual)

    def test_file_does_not_exist(self):
        """
        Test basic functionality of get_data_type_from_file - given file does
        not exist.
        """
        empty_name_file = Path('')
        non_existent_file = Path('non_existent_dir')
        with self.assertRaises(IsADirectoryError):
            get_data_type_from_file(empty_name_file, get_signature_channels(),
                                    get_all_channels())
        with self.assertRaises(FileNotFoundError):
            get_data_type_from_file(non_existent_file,
                                    get_signature_channels(),
                                    get_all_channels())

    def test_non_data_binary_file(self):
        binary_file = TEST_DATA_DIR / 'Non-data-file' / 'non_data_file'
        expected = (None, None)
        actual = get_data_type_from_file(binary_file, get_signature_channels(),
                                         get_all_channels())
        self.assertEqual(expected, actual)


class TestGetNextChannelFromMseedFile(BaseTestCase):
    def test_get_one_channel(self):
        """
        Test basic functionality of get_next_channel_from_mseed_file - the
        given file contains MSeed data.
        """
        with self.subTest('test_big_endian_file'):
            big_endian_file = q330_dir.joinpath('AX08.XA..VKI.2021.186')
            with open(big_endian_file, 'rb') as infile:
                expected = 'VKI'
                actual = get_next_channel_from_mseed_file(infile)
                self.assertEqual(expected, actual)

        with self.subTest('test_little_endian_file'):
            little_endian_file = pegasus_dir.joinpath(
                '2020/XX/KC01/VE1.D/XX.KC01..VE1.D.2020.129'
            )
            with open(little_endian_file, 'rb') as infile:
                expected = 'VE1'
                actual = get_next_channel_from_mseed_file(infile)
                self.assertEqual(expected, actual)

    def test_get_multiple_channel(self):
        """
        Test basic functionality of get_next_channel_from_mseed_file - call the
        function multiple times on the same file with enough channels to
        accommodate those calls.
        """
        try:
            big_mseed_file = q330_dir.joinpath('AX08.XA..LHE.2021.186')
            with open(big_mseed_file, 'rb') as infile:
                get_next_channel_from_mseed_file(infile)
                get_next_channel_from_mseed_file(infile)
                get_next_channel_from_mseed_file(infile)
        except ValueError:
            self.fail('ValueError raised before file is exhausted.')

    def test_called_after_file_is_exhausted(self):
        small_mseed_file = q330_dir.joinpath('AX08.XA..VKI.2021.186')
        with self.assertRaises(ValueError):
            with open(small_mseed_file, 'rb') as infile:
                # A manual check confirms that the given file has 2 records.
                get_next_channel_from_mseed_file(infile)
                get_next_channel_from_mseed_file(infile)
                get_next_channel_from_mseed_file(infile)

    def test_rt130_file(self):
        """
        Test basic functionality of get_next_channel_from_mseed_file - the
        given file contains RT130 data.
        """
        rt130_file = rt130_dir.joinpath('92EB/0/000000000_00000000')
        with open(rt130_file, 'rb') as infile:
            with self.assertRaises(ValueError):
                get_next_channel_from_mseed_file(infile)

    def test_non_data_file(self):
        """
        Test basic functionality of get_next_channel_from_mseed_file - the
        given file is not a data file.
        """
        non_data_file = TEST_DATA_DIR / 'Non-data-file' / 'non_data_file'
        with open(non_data_file, 'rb') as infile:
            with self.assertRaises(ValueError):
                get_next_channel_from_mseed_file(infile)
